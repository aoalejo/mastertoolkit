package com.nugganet.arget.mastertoolkit;

import android.os.AsyncTask;
import android.os.SystemClock;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.nugganet.arget.mastertoolkit.Clases_objetos.Historial_DB;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Lugar;

import org.threeten.bp.Instant;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Arget on 20/3/2018.
 */

public class Historial {

    ArrayList<Historial_DB> historial = new ArrayList<>();
    private static Principal p;



    private String titulo, jugador_principal, jugador_secundario, cuerpo, lugar;

    private boolean inclusivo = false;

    private String texto = "";

    private long fecha;
    private int rango;
    private static boolean prueba = true;

    public Historial(Principal p) {
        this.p = p;
    }

    public ArrayList<Historial_DB> getHistorial() {
        return historial;
    }

    public void setHistorial(ArrayList<Historial_DB> historial) {
        this.historial = historial;
    }

    public void appendLog(String título, String cuerpo) {

        Long i = Instant.now().getEpochSecond();

        String lugar = "";

        if (((Spinner) p.findViewById(R.id.spinner_ubicación)).getSelectedItemPosition() != 0) {
            lugar = ((Spinner) p.findViewById(R.id.spinner_ubicación)).getSelectedItem().toString();
        }

        Historial_DB log = new Historial_DB(título, cuerpo, i, lugar);

        p.getDataBase().AñadirLog(log);

        historial.add(log);
    }

    public void buscar(String titulo, String jugador_principal, String jugador_secundario, String cuerpo, String lugar, long fecha, int rango, boolean inclusivo) {
        String text = "";
        this.titulo = titulo;
        this.jugador_principal = jugador_principal;
        this.jugador_secundario = jugador_secundario;
        this.cuerpo = cuerpo;
        this.lugar = lugar;
        this.fecha = fecha;
        this.rango = rango;
        this.inclusivo = inclusivo;

        System.out.println(this.toString());

        new busqueda().execute((Historial_DB[]) historial.toArray(new Historial_DB[historial.size()]));

    }


    public String getLog() {
        String text = "";

        int min = historial.size() - 1;
        text = " <html><body  bgcolor=\"#fafafa\"> </script> <script type=\"text/javascript\"> function linkHistorial(link) { Android.showHistorial(link); } </script>   <script> function appendText_list(extraStr) { document.getElementsByTagName('ul')[0].innerHTML = document.getElementsByTagName('ul')[0].innerHTML + extraStr; } </script> Mostrando los últimos 20 registros<br><br> <ul> ";

        if (min > 20) {
            min = historial.size() - 20;
        } else {
            min = 0;
        }

        for (int i = historial.size() - 1; i >= min; i--) {
            text = text + "<li>" + WebBuilder.linkto(String.valueOf(i), historial.get(i).getEncabezado()) + "</li><br>";
        }
        text = text + "</ul></body></html>";

        return text;
    }

    private boolean buscar = false;

    public boolean isBuscar() {
        return buscar;
    }

    public void setBuscar(boolean buscar) {
        this.buscar = buscar;
    }

    public ArrayList<String> busquedaInclusive() {

        ArrayList<String> base_lugares = new ArrayList<>();

        ArrayList<String> base_lugares_temp = new ArrayList<>();

        ArrayList<Lugar> lugares = (ArrayList<Lugar>) p.arraylugares.clone();

        ArrayList<Lugar> lugares_temp;

        base_lugares.add(lugar);

        boolean banderaSalida = false;

        while (!banderaSalida) {

            base_lugares_temp = (ArrayList<String>) base_lugares.clone();

            lugares_temp = (ArrayList<Lugar>) lugares.clone();

            banderaSalida = true;

            if (buscar) {

                for (Lugar l : lugares_temp) {

                    // mostrar("Ubicación actual: " + l.getNombre() + " padre: " + l.getPadre());

                    for (String s : base_lugares_temp) {

                        // mostrar("Comparando con: " + s);

                        if (l.getPadre().equals(s)) {
                            //  mostrar("Añadiendo: " + l.getNombre());
                            lugares.remove(l);
                            base_lugares.add(l.getNombre());
                            banderaSalida = false;
                        }
                    }
                }

            } else {
                break;
            }

        }
        // mostrar("Lugar base: " + lugar + " - Lugares concordantes: " + base_lugares.toString());

        return base_lugares;
    }

    @Override
    public String toString() {
        return "Historial{" +
                "titulo='" + titulo + '\'' +
                ", jugador_principal='" + jugador_principal + '\'' +
                ", jugador_secundario='" + jugador_secundario + '\'' +
                ", cuerpo='" + cuerpo + '\'' +
                ", lugar='" + lugar + '\'' +
                ", inclusivo=" + inclusivo +
                ", fecha=" + fecha +
                ", rango=" + rango +
                ", buscar=" + buscar +
                '}';
    }

    private boolean containsStr(String base, String buscar) {
        if (buscar.isEmpty() | buscar == "") {
            return true;
        } else {
            if (base.toLowerCase().contains(buscar.toLowerCase())) {
                return true;
            } else {
                return false;
            }
        }
    }

    private boolean containsStr(String base, String[] textos_a_buscar) {
        for (String buscar : textos_a_buscar) {
            if (!containsStr(base, buscar)) {
                return false;
            }
        }
        return true;
    }

    public String getHistorial_id(String enlace) {

        int pos = Integer.parseInt(enlace);

        return this.historial.get(pos).getText();
    }

    private class busqueda extends AsyncTask<Historial_DB, String, Boolean> {

        protected Boolean doInBackground(Historial_DB... historial) {

            SystemClock.sleep(100);

            ArrayList<String> base_lugares;
            if (inclusivo) {
                base_lugares = busquedaInclusive();
            } else {
                base_lugares = new ArrayList<>();
                base_lugares.add(lugar);
            }

            for (int i = historial.length; i >= -1; i--) {
                try {
                    if (buscar) {

                        if (containsStr(historial[i].getTítulo(), new String[]{jugador_secundario, jugador_principal, titulo}) & containsStr(historial[i].getCuerpo(), cuerpo)) {

                            if (fecha != -1) {
                                if (historial[i].getTimestamp() > fecha - (86400 * rango) && historial[i].getTimestamp() < fecha + (86400 * rango)) {

                                    for (String l : base_lugares) {
                                        if (historial[i].getLugar().contains(l)) {
                                            publishProgress(WebBuilder.linkto(String.valueOf(i), historial[i].getEncabezado()));
                                            SystemClock.sleep(50);
                                            break;
                                        }
                                    }
                                }
                            } else {
                                for (String l : base_lugares) {
                                    if (historial[i].getLugar().contains(l)) {
                                        publishProgress(WebBuilder.linkto(String.valueOf(i), historial[i].getEncabezado()));
                                        SystemClock.sleep(50);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                } catch (java.lang.ArrayIndexOutOfBoundsException e) {
                    e.printStackTrace();
                }

            }
            return true;
        }

        protected void onPostExecute(Boolean coso) {
            ((TextView) p.findViewById(R.id.textView_indicador_guardando)).setText(p.getResources().getString(R.string.guardando));
            System.out.print(texto);
            if (coso) {
                buscar = false;
                p.IndicadorGuardando(false);
                p.guardando = false;
                ((Button) p.findViewById(R.id.button_buscar_log)).setText("BUSCAR");

            } else {
                ((LinearLayout) p.findViewById(R.id.IndicadorError)).setVisibility(View.VISIBLE);
            }
        }


        protected void onPreExecute() {

            ((WebView) p.findViewById(R.id.WebView_logger)).loadData(WebBuilder.WebPage,"text/html; charset=UTF-8", null);


            ((TextView) p.findViewById(R.id.textView_indicador_guardando)).setText("Buscando en el historial ");
            p.IndicadorGuardando(true);
            buscar = true;
            p.guardando = true;
            first = true;


        }

        protected void onProgressUpdate(String... text) {
            texto = texto + text[0];
            System.out.println(Arrays.toString(text));
            ((WebView) p.findViewById(R.id.WebView_logger)).loadUrl(WebBuilder.add_list(text[0]));
        }
    }

    private boolean first = false;

    public String getTexto() {
        if (texto.isEmpty()) {
            texto = getLog();
        }
        return texto;

    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    private static void mostrar(String text) {
        if (prueba) {

            System.out.println("En historial: " + text);

        }

    }


}
