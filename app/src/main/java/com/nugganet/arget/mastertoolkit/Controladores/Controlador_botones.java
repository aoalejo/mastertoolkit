package com.nugganet.arget.mastertoolkit.Controladores;

import android.view.View;

import com.nugganet.arget.mastertoolkit.Principal;
import com.nugganet.arget.mastertoolkit.R;

public class Controlador_botones {

    public static final void onclick(Principal p, View v){

        switch (v.getId()){
            case R.id.imageButton_menú_importar:
                p.cambio_de_pagina(null,R.id.Importar_Planilla,true,"Importar desde planilla");
                break;
            case R.id.imageButton_menú_sel_pjs:
                p.cambio_de_pagina(null,R.id.Selección_Personajes,true,p.getResources().getString(R.string.selecci_n_personajes));
                break;
            case R.id.imageButton_menú_sel_ubic:
                p.cambio_de_pagina(null,R.id.Selección_Ubicaciones,true,p.getResources().getString(R.string.selecci_n_ubicaciones));
                break;
            case R.id.imageButton_menú_secund:
                p.cambio_de_pagina(null,R.id.Part_Secundarias,true,p.getResources().getString(R.string.secundarias));
                break;
            case R.id.imageButton_menú_sel_psiq:
                p.cambio_de_pagina(null,R.id.Part_Psiquicos,true,"Psiquicos");
                break;
            case R.id.imageButton_menú_sel_comb:
                p.cambio_de_pagina(null,R.id.Part_Combate,true,p.getResources().getString(R.string.combate));
                break;
            case R.id.imageButton_menú_e_jugadores:
                p.cambio_de_pagina(null,R.id.Part_vistazo,true,p.getResources().getString(R.string.estado));
                break;
            case R.id.imageButton_menú_gen_tir:
                p.cambio_de_pagina(null,R.id.Tiradas,true,p.getResources().getString(R.string.tiradas));
                break;
            case R.id.imageButton_menú_historial:
                p.cambio_de_pagina(null,R.id.Extras_logger,true,p.getResources().getString(R.string.historial));
                break;
            case R.id.imageButton_menú_físico:
                p.cambio_de_pagina(null,R.id.Creac_Fis,true,p.getResources().getString(R.string.fisico));
                break;
            case R.id.imageButton_menú_creac_personajes:
                p.cambio_de_pagina(null,R.id.Creac_Pj,true,p.getResources().getString(R.string.creaci_n_personaje));
                break;
            case R.id.imageButton_menú_creac_armas:
                p.cambio_de_pagina(null,R.id.Creac_Armas,true,p.getResources().getString(R.string.creaci_n_de_armas));
                break;
            case R.id.imageButton_menú_creac_armaduras:
                p.cambio_de_pagina(null,R.id.Creac_Armaduras,true,p.getResources().getString(R.string.creaci_n_de_armaduras));
                break;
            case R.id.imageButton_menú_creac_modif:
                p.cambio_de_pagina(null,R.id.Creac_Modificadores,true,p.getResources().getString(R.string.creaci_n_de_modificadores));
                break;
            case R.id.imageButton_menú_creac_lugares:
                p.cambio_de_pagina(null,R.id.Creac_lugares_logger,true,p.getResources().getString(R.string.creaci_n_de_lugares));
                break;


        }

    }

}
