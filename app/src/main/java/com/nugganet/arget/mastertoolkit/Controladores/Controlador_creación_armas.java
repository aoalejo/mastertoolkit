package com.nugganet.arget.mastertoolkit.Controladores;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.android.material.snackbar.Snackbar;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Arma;
import com.nugganet.arget.mastertoolkit.Principal;
import com.nugganet.arget.mastertoolkit.R;

public class Controlador_creación_armas implements View.OnClickListener {

    final Principal p;
    private Arma arma_delete;

    public Controlador_creación_armas(Principal p) {
        this.p = p;
    }

    public void initListeners() {
        p.findViewById(R.id.button_Cargar_arma).setOnClickListener(this);
        p.findViewById(R.id.button_Eliminar_arma).setOnClickListener(this);
        p.findViewById(R.id.button_GuardarNuevo_arma).setOnClickListener(this);
        p.findViewById(R.id.button_Modificar_arma).setOnClickListener(this);
    }


    public void initArmas() {

        String[] nombres_armas = p.getResources().getStringArray(R.array.Datos_Creación_Nombres_Arma);
        int[] valores_armas = p.getResources().getIntArray(R.array.Datos_Creación_Bases_Arma);

        for (int i = 1; i < nombres_armas.length + 1; i++) {

            String ViewID = "cr_pj_armas_7_" + i;

            int resID = p.getResources().getIdentifier(ViewID, "id", p.getPackageName());

            ((TextView) p.findViewById(resID).findViewById(R.id.cr_pj_c_nombre)).setText(nombres_armas[i - 1]);
            ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_temp)).setVisibility(View.GONE);

            if (valores_armas[i - 1] == -6 | valores_armas[i - 1] == -4 | valores_armas[i - 1] == -5| valores_armas[i - 1] == -7) {//spinner

                ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setVisibility(View.GONE);
                ((Spinner) p.findViewById(resID).findViewById(R.id.cr_pj_c_spinner)).setVisibility(View.VISIBLE);

                switch (valores_armas[i - 1]) {
                    case -4:
                        ((Spinner) p.findViewById(resID).findViewById(R.id.cr_pj_c_spinner)).setAdapter(ArrayAdapter.createFromResource(p, R.array.Tipos_de_armas, android.R.layout.simple_spinner_dropdown_item));
                        break;
                    case -5:
                        ((Spinner) p.findViewById(resID).findViewById(R.id.cr_pj_c_spinner)).setAdapter(ArrayAdapter.createFromResource(p, R.array.Tipos_de_ataque, android.R.layout.simple_spinner_dropdown_item));
                        break;
                    case -6:
                        ((Spinner) p.findViewById(resID).findViewById(R.id.cr_pj_c_spinner)).setAdapter(ArrayAdapter.createFromResource(p, R.array.Efectos_armas, android.R.layout.simple_spinner_dropdown_item));
                        break;
                    case -7:
                        ((Spinner) p.findViewById(resID).findViewById(R.id.cr_pj_c_spinner)).setAdapter(ArrayAdapter.createFromResource(p, R.array.Habilidades_para_armas, android.R.layout.simple_spinner_dropdown_item));
                        break;

                }


            } else if (valores_armas[i - 1] == -3) {//checkbox

                ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setVisibility(View.GONE);
                ((ToggleButton) p.findViewById(resID).findViewById(R.id.cr_pj_c_toggleButton)).setVisibility(View.VISIBLE);

            } else {


                ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setText(String.valueOf(valores_armas[i - 1]));


            }
        }
    }

    @Override
    public void onClick(View v) {

        int seleccionado = ((Spinner) p.findViewById(R.id.spinner_seleccion_arma)).getSelectedItemPosition();
        Snackbar mensajeConfirmación;
        View view;
        TextView tv;

        switch (v.getId()) {
            case R.id.button_Cargar_arma:

                if (((Spinner) p.findViewById(R.id.spinner_seleccion_arma)).getSelectedItemPosition() != -1) {

                    Arma temp = p.getArrayArmas().get(seleccionado);

                    p.getControlador_creac_pj().setArma(7, temp);

                } else {
                    p.showSnack(Snackbar.make(v, "No se pudo cargar.", Snackbar.LENGTH_SHORT));
                }

                break;

            case R.id.button_GuardarNuevo_arma:

                Arma nueva_arma = p.getControlador_creac_pj().getArma(7);

                nueva_arma.setID(p.getDataBase().AñadirArma(nueva_arma));

                p.getArrayArmas().add(nueva_arma);

                p.showSnack(Snackbar.make(v, "Arma " + nueva_arma.getNombre() + " guardada.", Snackbar.LENGTH_SHORT));

                break;

            case R.id.button_Modificar_arma:

                final int index_arma = seleccionado;

                if (p.getArrayArmas().size() != 0 & seleccionado != -1) {

                    if (seleccionado < p.getArrayArmas().size()) {

                        Arma arma_modificada = p.getControlador_creac_pj().getArma(7);

                        arma_delete = new Arma().fromJson(arma_modificada.toJson());

                        mensajeConfirmación = Snackbar
                                .make(v, "Arma " + p.getArrayArmas().get(seleccionado).getNombre() + " modificada a " + arma_modificada.getNombre(), Snackbar.LENGTH_LONG)
                                .setAction("DESHACER", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        p.getArrayArmas().get(index_arma).reemplazar(arma_delete);

                                        p.getDataBase().ModificarArma(arma_delete);

                                        p.actualizarArrayArmas(R.id.spinner_seleccion_arma);

                                        ((Spinner) p.findViewById(R.id.spinner_seleccion_arma)).setSelection(index_arma);

                                        p.showSnack(Snackbar.make(view, "Restaurado", Snackbar.LENGTH_SHORT));
                                    }

                                });

                        p.getArrayArmas().get(seleccionado).reemplazar(arma_modificada);

                        p.getDataBase().ModificarArma(p.getArrayArmas().get(seleccionado));

                        p.actualizarArrayArmas(R.id.spinner_seleccion_arma);

                        ((Spinner) p.findViewById(R.id.spinner_seleccion_arma)).setSelection(seleccionado);

                        view = mensajeConfirmación.getView();

                        tv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                        tv.setTextColor(p.getResources().getColor(R.color.colorWhite));

                        mensajeConfirmación.setActionTextColor(p.getResources().getColor(R.color.colorWhite));

                        mensajeConfirmación.show();
                    }
                } else {
                    p.showSnack(Snackbar.make(v, "No hay ningún elemento que modificar", Snackbar.LENGTH_SHORT));
                }

                break;

            case R.id.button_Eliminar_arma:

                if (p.getArrayArmas().size() != 0 & seleccionado != -1) {

                    if (seleccionado < p.getArrayArmas().size()) {

                        mensajeConfirmación = Snackbar
                                .make(v, "Arma " + p.getArrayArmas().get(seleccionado).getNombre() + " eliminada", Snackbar.LENGTH_LONG)
                                .setAction("DESHACER", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        p.getArrayArmas().add(arma_delete);

                                        p.getDataBase().AñadirArma(arma_delete);

                                        p.actualizarArrayArmas(R.id.spinner_seleccion_arma);

                                        p.showSnack(Snackbar.make(view, "Restaurado", Snackbar.LENGTH_SHORT))
                                        ;
                                    }

                                });

                        arma_delete = p.getArrayArmas().get(seleccionado);

                        p.getDataBase().EliminarArma(p.getArrayArmas().get(seleccionado).getID());

                        p.getArrayArmas().remove(seleccionado);

                        p.actualizarArrayArmas(R.id.spinner_seleccion_arma);

                        view = mensajeConfirmación.getView();

                        tv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                        tv.setTextColor(p.getResources().getColor(R.color.colorWhite));

                        mensajeConfirmación.setActionTextColor(p.getResources().getColor(R.color.colorWhite));

                        mensajeConfirmación.show();
                    }
                } else {
                    p.showSnack(Snackbar.make(v, "No hay ningún elemento que eliminar", Snackbar.LENGTH_SHORT));

                }

                break;
        }

        p.actualizarArrayArmas(R.id.spinner_seleccion_arma);

        if (seleccionado < p.getArrayArmas().size()) {
            ((Spinner) p.findViewById(R.id.spinner_seleccion_arma)).setSelection(seleccionado);
        }
    }

}
