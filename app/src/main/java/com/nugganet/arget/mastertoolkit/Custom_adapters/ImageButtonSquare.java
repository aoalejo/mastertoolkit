package com.nugganet.arget.mastertoolkit.Custom_adapters;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageButton;

public class ImageButtonSquare extends AppCompatImageButton {

    public ImageButtonSquare(final Context context) {
        super(context);
    }

    public ImageButtonSquare(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageButtonSquare(final Context context, final AttributeSet attrs,
                             final int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int width, int height) {
        super.onMeasure(width, height);
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        if (measuredWidth > measuredHeight) {
            setMeasuredDimension(measuredHeight, measuredHeight);
        } else {
            setMeasuredDimension(measuredWidth, measuredWidth);

        }

    }

}
