package com.nugganet.arget.mastertoolkit.Controladores;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.nugganet.arget.mastertoolkit.Base;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Personaje;
import com.nugganet.arget.mastertoolkit.Custom_adapters.NDSpinner;
import com.nugganet.arget.mastertoolkit.Principal;
import com.nugganet.arget.mastertoolkit.R;

import java.util.ArrayList;

import Utils.DbBitmapUtility;

public class Controlador_combate implements View.OnClickListener {

    private Principal p;
    private int ultima_arma_npc = 0;
    private int ultima_vida_npc = 0;
    private int ultima_ta_npc = 0;
    private int ultimo_crit_npc = 0;


    public Controlador_combate(Principal p) {
        this.p = p;

    }

    public void setListener() {
              p.findViewById(R.id.button_añadir_npc).setOnClickListener(this);
    }

    public void controladorSpinner(int identificador, int posición) {

        int seleccion_defensor = ((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_defensor)).getSelectedItemPosition();
        int seleccion_atacante = ((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_asaltante)).getSelectedItemPosition();


        // System.out.println("Entrando en selector de datos - spinner " + posición);

        switch (identificador) {

            case R.id.spinner_Combate_Tipo_de_ataque:
                if (!(p.getDatos().Array_Personajes_En_Mision().size() > seleccion_atacante)) {
                    ultimo_crit_npc = posición;
                }
                break;

            case R.id.spinner_Combate_tipo_de_defensa:

                if (!(p.getDatos().Array_Personajes_En_Mision().size() > seleccion_defensor)) {
                    ultima_vida_npc = posición;
                }

            case R.id.spinner_Combate_Jugador_arma_atk:

                if (p.getDatos().Array_Personajes_En_Mision().size() > seleccion_atacante) {

                    p.getDatos().Array_Personajes_En_Mision().get(((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_asaltante)).getSelectedItemPosition()).setIndex_última_arma_usada(posición);

                } else {
                    ultima_arma_npc = posición;
                }

                break;
            case R.id.spinner_Combate_Jugador_arma_def:

                if (p.getDatos().Array_Personajes_En_Mision().size() > seleccion_defensor) {

                    p.getDatos().Array_Personajes_En_Mision().get(((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_defensor)).getSelectedItemPosition()).setIndex_última_arma_usada(posición);

                } else {
                    ultima_ta_npc = posición;
                }


                break;
            case R.id.spinner_Combate_Jugador_defensor:

                if (p.getDatos().Array_Personajes_En_Mision().size() > seleccion_defensor) {

                    ((TextView) p.findViewById(R.id.Personaje_defensor)).setText(((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_defensor)).getSelectedItem().toString());

                    if (p.getDatos().Array_Personajes_En_Mision().size() != 0 & p.getDatos().Array_Personajes_En_Mision() != null) {

                        Personaje temp = p.getDatos().Array_Personajes_En_Mision().get(posición);

                        ((ImageView) p.findViewById(R.id.imageView_defensor)).setImageDrawable(new BitmapDrawable(p.getResources(), DbBitmapUtility.getImage(temp.getToken())));

                        ArrayAdapter<String> adapter_Armas = new ArrayAdapter<String>(p, android.R.layout.simple_spinner_dropdown_item, temp.getArmas_nombres());

                        ((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_arma_def)).setAdapter(adapter_Armas);
                        ((Spinner) p.findViewById(R.id.spinner_Combate_tipo_de_defensa)).setAdapter(ArrayAdapter.createFromResource(p, R.array.Tipo_de_defensa, android.R.layout.simple_spinner_dropdown_item));
                        ((Spinner) p.findViewById(R.id.spinner_Combate_tipo_de_defensa)).setSelection(0);

                        ((ProgressBar) p.findViewById(R.id.ProgressBar_Combate_Jugador_defensor_vida)).setMax(temp.getCaracteristicas(Base.VIDA_MAX));
                        ((ProgressBar) p.findViewById(R.id.ProgressBar_Combate_Jugador_defensor_vida)).setProgress(temp.getCaracteristicas(Base.VIDA_ACT));

                        ((TextView) p.findViewById(R.id.textView_Combate_defensor_texto)).setText(temp.getCaracteristicas(Base.VIDA_ACT) + "/" + temp.getCaracteristicas(Base.VIDA_MAX) + " Pv. (" + temp.Penalizadores(2) + ")");

                    }


                    p.setModificadores_defensa(p.getDatos().Array_Personajes_En_Mision().get(posición).getModificadores());

                    ((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_arma_def)).setSelection(p.getDatos().Array_Personajes_En_Mision().get(posición).getIndex_última_arma_usada());

                } else {

                    int index_vida = ultima_vida_npc;
                    int index_ta = ultima_ta_npc;

                    ((Spinner) p.findViewById(R.id.spinner_Combate_tipo_de_defensa)).setAdapter(ArrayAdapter.createFromResource(p, R.array.vida_npc_plus, android.R.layout.simple_spinner_dropdown_item));
                    ((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_arma_def)).setAdapter(ArrayAdapter.createFromResource(p, R.array.ta_NPC_plus, android.R.layout.simple_spinner_dropdown_item));
                    ((ProgressBar) p.findViewById(R.id.ProgressBar_Combate_Jugador_defensor_vida)).setMax(100);
                    ((ProgressBar) p.findViewById(R.id.ProgressBar_Combate_Jugador_defensor_vida)).setProgress(100);
                    ((TextView) p.findViewById(R.id.textView_Combate_defensor_texto)).setText("Realizando tirada con datos genericos");

                    p.setModificadores_defensa(new boolean[p.getDatos().ArrayModificadores().size() + 5]);

                    ((Spinner) p.findViewById(R.id.spinner_Combate_tipo_de_defensa)).setSelection(index_vida);
                    ((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_arma_def)).setSelection(index_ta);
                }


                break;

            case R.id.spinner_Combate_Jugador_asaltante:

                int index_arma = ultima_arma_npc;
                int index_crit = ultimo_crit_npc;

                if (p.getDatos().Array_Personajes_En_Mision().size() > seleccion_atacante) {

                    index_arma = p.getDatos().Array_Personajes_En_Mision().get(posición).getIndex_última_arma_usada();

                    ((TextView) p.findViewById(R.id.Personaje_atacante)).setText(((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_asaltante)).getSelectedItem().toString());

                    if (p.getDatos().Array_Personajes_En_Mision().size() != 0 & p.getDatos().Array_Personajes_En_Mision() != null) {

                        if (posición != p.getDatos().Array_Personajes_En_Mision().size()) {

                            Personaje temp = p.getDatos().Array_Personajes_En_Mision().get(posición);

                            if (temp.getTipoDePersonaje() == Personaje.TIPO.GENERICO) {

                                p.actualizarArrayArmas(R.id.spinner_Combate_Jugador_arma_atk);

                            } else {

                                ArrayAdapter<String> adapter_Armas = new ArrayAdapter<String>(p, android.R.layout.simple_spinner_dropdown_item, temp.getArmas_nombres());

                                ((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_arma_atk)).setAdapter(adapter_Armas);

                            }

                            ((ImageView) p.findViewById(R.id.imageView_atacante)).setImageDrawable(new BitmapDrawable(p.getResources(), DbBitmapUtility.getImage(temp.getToken())));

                            ((ProgressBar) p.findViewById(R.id.ProgressBar_Combate_Jugador_atacante_vida)).setMax(temp.getCaracteristicas(Base.VIDA_MAX));
                            ((ProgressBar) p.findViewById(R.id.ProgressBar_Combate_Jugador_atacante_vida)).setProgress(temp.getCaracteristicas(Base.VIDA_ACT));

                            ((TextView) p.findViewById(R.id.textView_Combate_atacante_texto)).setText(temp.getCaracteristicas(Base.VIDA_ACT) + "/" + temp.getCaracteristicas(Base.VIDA_MAX) + " Pv. (" + temp.Penalizadores(2) + ")");

                            ((Spinner) p.findViewById(R.id.spinner_Combate_Tipo_de_ataque)).setAdapter(ArrayAdapter.createFromResource(p, R.array.Tipo_de_Ataque, android.R.layout.simple_spinner_dropdown_item));

                        }
                    }


                    p.setModificadores_ataque(p.getDatos().Array_Personajes_En_Mision().get(posición).getModificadores());

                    ((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_arma_atk)).setSelection(index_arma);

                } else {

                    ((Spinner) p.findViewById(R.id.spinner_Combate_Tipo_de_ataque)).setAdapter(ArrayAdapter.createFromResource(p, R.array.Tipos_de_ataque, android.R.layout.simple_spinner_dropdown_item));

                    if (p.getArrayArmas().isEmpty()) {
                        ((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_arma_atk)).setAdapter(ArrayAdapter.createFromResource(p, R.array.Daños_base_Armas, android.R.layout.simple_spinner_dropdown_item));

                    } else {

                        p.actualizarArrayArmas(R.id.spinner_Combate_Jugador_arma_atk);

                    }

                    ((ProgressBar) p.findViewById(R.id.ProgressBar_Combate_Jugador_atacante_vida)).setMax(100);
                    ((ProgressBar) p.findViewById(R.id.ProgressBar_Combate_Jugador_atacante_vida)).setProgress(100);
                    ((TextView) p.findViewById(R.id.textView_Combate_atacante_texto)).setText("Realizando tirada con datos genericos");

                    p.setModificadores_ataque(new boolean[p.getDatos().ArrayModificadores().size() + 5]);

                    ((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_arma_atk)).setSelection(index_arma);
                    ((Spinner) p.findViewById(R.id.spinner_Combate_Tipo_de_ataque)).setSelection(index_crit);

                }


                break;
        }
    }

    public void controlador_speech(String result, Principal p) {

        boolean sumando = false;
        boolean restando = false;
        boolean segunda_parte = false;

        Integer base = 0;

        for (String str : result.split(" ")) {

            Integer ultimaCaptura = p.SpokenTextToInt(str);

            EditText asaltante = p.findViewById(R.id.editText_Tirada_atacante);
            EditText defensor = p.findViewById(R.id.editText_Tirada_defensor);

            System.out.println("Estado: base: " + base + " /sumando: " + sumando + " /restando: " + restando + " /texto: " + str);

            if (ultimaCaptura != null) {
                if (sumando) {
                    base = base + ultimaCaptura;
                } else if (restando) {
                    base = base - ultimaCaptura;
                } else {
                    base = ultimaCaptura;
                }

                sumando = false;
                restando = false;
            } else {
                // System.out.println("Cadena: " + str);
                if (str.contains("+") | str.contains("mas") | str.contains("más")) {
                    sumando = true;
                    restando = false;
                }

                if (str.contains("-") | str.contains("menos")) {
                    restando = true;
                    sumando = false;
                }

                if (str.contains("/") | str.contains("barra") | str.contains("contra") | str.contains("versus") | str.contains("vs") | str.contains("enfrenta")) {
                    segunda_parte = true;
                    sumando = false;
                    restando = false;
                    asaltante.setText(String.valueOf(base));
                    base = 0;
                }
            }
            if (segunda_parte) {
                defensor.setText(String.valueOf(base));
            } else {
                asaltante.setText(String.valueOf(base));
            }

        }

        p.findViewById(R.id.button_atacar).callOnClick();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_añadir_npc:
                FragmentManager fm = p.getFragmentManager();
                Creacion_NPC dialogFragment = new Creacion_NPC();
                dialogFragment.setP(p);
                dialogFragment.show(fm, "Crear un nuevo NPC");
                break;
        }
    }

    public static class Creacion_NPC extends DialogFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {

        private Principal p;

        private int Edittext_seleccionado = 0;

        private int[] edittext = {R.id.editText_npc_turno, R.id.editText_npc_vida, R.id.editText_npc_ataque, R.id.editText_npc_defensa};

        public Principal getP() {
            return p;
        }

        public void setP(Principal p) {
            this.p = p;
        }

        View rootView = null;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

            rootView = inflater.inflate(R.layout.fragment_add_npc, container, false);

            rootView.findViewById(R.id.button_aceptar_npc).setOnClickListener(this);
            rootView.findViewById(R.id.button_cancelar_npc).setOnClickListener(this);

            rootView.findViewById(R.id.button_focus_ataque).setOnClickListener(this);
            rootView.findViewById(R.id.button_focus_defensa).setOnClickListener(this);
            rootView.findViewById(R.id.button_focus_turno).setOnClickListener(this);
            rootView.findViewById(R.id.button_focus_vida).setOnClickListener(this);

            rootView.findViewById(R.id.button_teclado_fragment_npc_1).setOnClickListener(this);
            rootView.findViewById(R.id.button_teclado_fragment_npc_2).setOnClickListener(this);
            rootView.findViewById(R.id.button_teclado_fragment_npc_3).setOnClickListener(this);
            rootView.findViewById(R.id.button_teclado_fragment_npc_4).setOnClickListener(this);
            rootView.findViewById(R.id.button_teclado_fragment_npc_5).setOnClickListener(this);
            rootView.findViewById(R.id.button_teclado_fragment_npc_6).setOnClickListener(this);
            rootView.findViewById(R.id.button_teclado_fragment_npc_7).setOnClickListener(this);
            rootView.findViewById(R.id.button_teclado_fragment_npc_8).setOnClickListener(this);
            rootView.findViewById(R.id.button_teclado_fragment_npc_9).setOnClickListener(this);
            rootView.findViewById(R.id.button_teclado_fragment_npc_0).setOnClickListener(this);
            rootView.findViewById(R.id.button_teclado_fragment_npc_Borrar).setOnClickListener(this);
            rootView.findViewById(R.id.button_teclado_fragment_npc_Negativo).setOnClickListener(this);

            ((NDSpinner) rootView.findViewById(R.id.Spinner_personajes_disponibles)).setOnItemSelectedListener(this);

            populate_npcs();

            return rootView;
        }

        private void populate_npcs() {
            ArrayList<String> Personajes_nombres_array = new ArrayList<>();


            for (int i = 0; i < p.getDatos().ArrayPersonajes().size(); i++) {
                Personajes_nombres_array.add(p.getDatos().ArrayPersonajes().get(i).getNombres());
            }

            String[] Personajes_nombres = new String[Personajes_nombres_array.size()];

            for (int i = 0; i < Personajes_nombres_array.size(); i++) {
                Personajes_nombres[i] = Personajes_nombres_array.get(i);
            }

            ArrayAdapter<String> adapter_Personajes = new ArrayAdapter<String>(p, android.R.layout.simple_spinner_dropdown_item, Personajes_nombres);

            ((Spinner) rootView.findViewById(R.id.Spinner_personajes_disponibles)).setAdapter(adapter_Personajes);
        }

        private int StrToInt(String text) {
            try {
                return Integer.parseInt(text);
            } catch (NumberFormatException e) {
                return 0;
            }
        }

        private void resetButtons(int selected) {

            int[] buttons = {R.id.button_focus_turno, R.id.button_focus_vida, R.id.button_focus_ataque, R.id.button_focus_defensa};
            for (int i = 0; i < 4; i++) {
                View v = rootView.findViewById(buttons[i]);
                if (i == selected) {
                    v.setBackground(p.getDrawable(R.drawable.round_rect_generic));
                } else {
                    v.setBackground(p.getDrawable(R.drawable.transparent));
                }
            }
        }

        @Override
        public void onClick(View v) {
            String text = ((EditText) rootView.findViewById(edittext[Edittext_seleccionado])).getText().toString();
            switch (v.getId()) {
                case R.id.button_aceptar_npc:
                    int cantidad = ((Spinner) rootView.findViewById(R.id.spinner_cantidad)).getSelectedItemPosition() + 1;
                    int randomize = ((Spinner) rootView.findViewById(R.id.spinner_randomize)).getSelectedItemPosition() + 1;

                    int index_pj = ((Spinner) rootView.findViewById(R.id.Spinner_personajes_disponibles)).getSelectedItemPosition();

                    int turno = StrToInt(((EditText) rootView.findViewById(R.id.editText_npc_turno)).getText().toString());
                    int vida = StrToInt(((EditText) rootView.findViewById(R.id.editText_npc_vida)).getText().toString());
                    int ataque = StrToInt(((EditText) rootView.findViewById(R.id.editText_npc_ataque)).getText().toString());
                    int defensa = StrToInt(((EditText) rootView.findViewById(R.id.editText_npc_defensa)).getText().toString());

                    for (int i = 0; i < cantidad; i++) {

                        Personaje temp = new Personaje().fromJson(p.getDatos().ArrayPersonajes().get(index_pj).toJson());

                        temp.setToken(p.getDatos().ArrayPersonajes().get(index_pj).getToken());

                        temp.reemplazar_valor_caracteristica(Base.HABILIDAD_TURNO, turno);
                        temp.reemplazar_valor_caracteristica(Base.VIDA_MAX, vida);
                        temp.reemplazar_valor_caracteristica(Base.VIDA_ACT, vida);
                        temp.reemplazar_valor_caracteristica(Base.HABILIDAD_ATAQUE, ataque);
                        temp.reemplazar_valor_caracteristica(Base.HABILIDAD_PARADA, defensa);
                        temp.reemplazar_valor_caracteristica(Base.HABILIDAD_ESQUIVA, defensa);

                        int newId = p.getCounterIds_temp();
                        int counter = 5;

                        while (p.getDatos().Busqueda_Personaje(newId) != null) {
                            p.getCounterIds_temp();
                            counter = counter - 1;
                            if (counter < 0) {
                                p.setCounterIds_temp(p.getCounterIds_temp() + 1000);
                            }
                        }

                        temp.setID(newId);

                        temp.setNombre_jugador(String.valueOf(i));

                        temp.randomize(randomize);

                        p.getDatos().Añadir_Personaje_en_misión(temp);

                    }

                    p.cambio_de_pagina(null, R.id.Part_Combate, false, p.getResources().getString(R.string.combate));

                    dismiss();
                    break;
                case R.id.button_cancelar_npc:
                    dismiss();
                    break;
                case R.id.button_teclado_fragment_npc_0:
                    ((EditText) rootView.findViewById(edittext[Edittext_seleccionado])).setText(((EditText) rootView.findViewById(edittext[Edittext_seleccionado])).getText() + "0");
                    break;
                case R.id.button_teclado_fragment_npc_1:
                    ((EditText) rootView.findViewById(edittext[Edittext_seleccionado])).setText(((EditText) rootView.findViewById(edittext[Edittext_seleccionado])).getText() + "1");
                    break;
                case R.id.button_teclado_fragment_npc_2:
                    ((EditText) rootView.findViewById(edittext[Edittext_seleccionado])).setText(((EditText) rootView.findViewById(edittext[Edittext_seleccionado])).getText() + "2");
                    break;
                case R.id.button_teclado_fragment_npc_3:
                    ((EditText) rootView.findViewById(edittext[Edittext_seleccionado])).setText(((EditText) rootView.findViewById(edittext[Edittext_seleccionado])).getText() + "3");
                    break;
                case R.id.button_teclado_fragment_npc_4:
                    ((EditText) rootView.findViewById(edittext[Edittext_seleccionado])).setText(((EditText) rootView.findViewById(edittext[Edittext_seleccionado])).getText() + "4");
                    break;
                case R.id.button_teclado_fragment_npc_5:
                    ((EditText) rootView.findViewById(edittext[Edittext_seleccionado])).setText(((EditText) rootView.findViewById(edittext[Edittext_seleccionado])).getText() + "5");
                    break;
                case R.id.button_teclado_fragment_npc_6:
                    ((EditText) rootView.findViewById(edittext[Edittext_seleccionado])).setText(((EditText) rootView.findViewById(edittext[Edittext_seleccionado])).getText() + "6");
                    break;
                case R.id.button_teclado_fragment_npc_7:
                    ((EditText) rootView.findViewById(edittext[Edittext_seleccionado])).setText(((EditText) rootView.findViewById(edittext[Edittext_seleccionado])).getText() + "7");
                    break;
                case R.id.button_teclado_fragment_npc_8:
                    ((EditText) rootView.findViewById(edittext[Edittext_seleccionado])).setText(((EditText) rootView.findViewById(edittext[Edittext_seleccionado])).getText() + "8");
                    break;
                case R.id.button_teclado_fragment_npc_9:
                    ((EditText) rootView.findViewById(edittext[Edittext_seleccionado])).setText(((EditText) rootView.findViewById(edittext[Edittext_seleccionado])).getText() + "9");
                    break;
                case R.id.button_focus_ataque:
                    Edittext_seleccionado = 2;
                    resetButtons(Edittext_seleccionado);
                    break;
                case R.id.button_focus_defensa:
                    Edittext_seleccionado = 3;
                    resetButtons(Edittext_seleccionado);
                    break;
                case R.id.button_focus_turno:
                    Edittext_seleccionado = 0;
                    resetButtons(Edittext_seleccionado);
                    break;
                case R.id.button_focus_vida:
                    Edittext_seleccionado = 1;
                    resetButtons(Edittext_seleccionado);
                    break;
                case R.id.button_teclado_fragment_npc_Borrar:

                    if (!text.isEmpty()) {
                        ((EditText) rootView.findViewById(edittext[Edittext_seleccionado])).setText(text.substring(0, text.length() - 1));
                    }
                    break;
                case R.id.button_teclado_fragment_npc_Negativo:
                    if (!text.isEmpty()) {
                        if (text.charAt(0) == '-') {
                            text = String.valueOf(text.subSequence(1, text.length()));
                        } else {
                            text = "-" + text;
                        }
                        ((EditText) rootView.findViewById(edittext[Edittext_seleccionado])).setText(text);
                    }
                    break;

            }
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Personaje temp = p.getDatos().ArrayPersonajes().get(position);

            ((EditText) rootView.findViewById(edittext[0])).setText(String.valueOf(temp.getCaracteristicas(Base.HABILIDAD_TURNO)));
            ((EditText) rootView.findViewById(edittext[1])).setText(String.valueOf(temp.getCaracteristicas(Base.VIDA_MAX)));
            ((EditText) rootView.findViewById(edittext[2])).setText(String.valueOf(temp.getCaracteristicas(Base.HABILIDAD_ATAQUE)));

            if (temp.getCaracteristicas(Base.HABILIDAD_PARADA) > temp.getCaracteristicas(Base.HABILIDAD_ESQUIVA)) {
                ((EditText) rootView.findViewById(edittext[3])).setText(String.valueOf(temp.getCaracteristicas(Base.HABILIDAD_PARADA)));
            } else {
                ((EditText) rootView.findViewById(edittext[3])).setText(String.valueOf(temp.getCaracteristicas(Base.HABILIDAD_ESQUIVA)));
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }
}
