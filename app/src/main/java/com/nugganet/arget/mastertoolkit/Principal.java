package com.nugganet.arget.mastertoolkit;


import android.animation.Animator;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.asynclayoutinflater.view.AsyncLayoutInflater;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;

import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.jakewharton.threetenabp.AndroidThreeTen;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Arma;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Armadura;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Historial_de_acciones;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Lugar;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Modificador;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Personaje;
import com.nugganet.arget.mastertoolkit.Clases_objetos.TriplaVectores;
import com.nugganet.arget.mastertoolkit.Controladores.Controlador_Psiquicos;
import com.nugganet.arget.mastertoolkit.Controladores.Controlador_Turno;
import com.nugganet.arget.mastertoolkit.Controladores.Controlador_botones;
import com.nugganet.arget.mastertoolkit.Controladores.Controlador_combate;
import com.nugganet.arget.mastertoolkit.Controladores.Controlador_creac_pj;
import com.nugganet.arget.mastertoolkit.Controladores.Controlador_creación_Lugares;
import com.nugganet.arget.mastertoolkit.Controladores.Controlador_creación_armaduras;
import com.nugganet.arget.mastertoolkit.Controladores.Controlador_creación_armas;
import com.nugganet.arget.mastertoolkit.Controladores.Controlador_creación_modificadores;
import com.nugganet.arget.mastertoolkit.Controladores.Controlador_secundarias;
import com.nugganet.arget.mastertoolkit.Controladores.Controlador_ventanas;
import com.nugganet.arget.mastertoolkit.Custom_adapters.CustomAdapter_Checkbox;
import com.nugganet.arget.mastertoolkit.Custom_adapters.CustomAdapter_TiradaSecundaria;
import com.nugganet.arget.mastertoolkit.Custom_adapters.CustomAdapter_VistazoPersonajes;
import com.nugganet.arget.mastertoolkit.Custom_adapters.Fragment_classes;
import com.nugganet.arget.mastertoolkit.Custom_adapters.JavascriptHandler;
import com.nugganet.arget.mastertoolkit.Custom_adapters.Tutorial;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import net.gotev.speech.GoogleVoiceTypingDisabledException;
import net.gotev.speech.Speech;
import net.gotev.speech.SpeechDelegate;
import net.gotev.speech.SpeechRecognitionNotAvailable;
import net.gotev.speech.ui.SpeechProgressView;

import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.zip.Inflater;

import Utils.BaseDeDatos_v2;
import Utils.DbBitmapUtility;
import Utils.Sistema_NPC;
import Utils.Teclado;
import Utils.importar_csv;
import androidx.drawerlayout.widget.DrawerLayout;
import at.wirecube.additiveanimations.additive_animator.AdditiveAnimator;
import at.wirecube.additiveanimations.additive_animator.AnimationEndListener;

public class Principal extends Activity implements NavigationView.OnNavigationItemSelectedListener, View.OnTouchListener, OnClickListener, Spinner.OnItemSelectedListener, SeekBar.OnSeekBarChangeListener, ToggleButton.OnCheckedChangeListener, View.OnLongClickListener {

    private static int LOAD_IMAGE_RESULTS = 1;
    private static int LOAD_EXCEL_RESULTS = 2;
    private static int SPEECH_REQUEST_CODE = 3;
    private final static int READ_EXTERNAL_STORAGE_RESULT = 2;

    private int counterIds_temp = 10000000;

    private boolean loadCsv = false;

    private Controlador_ventanas controlador_ventanas;
    private int focusedEdittext = -1;
    private Arma arma_delete = null;
    private Armadura armadura_delete = null;
    private Personaje personaje_delete = null;
    private Modificador modificador_delete = null;
    private int posBoludeo = 1;
    private Tutorial tutorial = null;
    private boolean stub_creacpj = true;
    private View Toolkit_creacion_pj;

    private boolean dbUpgrade = false;
    private Teclado teclado;

    private int lastTipoDeTeclado = 0;

    private String[] nombres_personajes;

    private boolean cargando = false;
    private int[] indice_cruzado_modificadores;

    private Controlador_Turno controlador_turno;

    private Combate.Resultado ultima_tirada_combate = new Combate.Resultado();

    private boolean[] modificadores_ataque = new boolean[100];
    private boolean[] modificadores_defensa = new boolean[100];

    private BaseDeDatos_v2 DataBase;

    private TouchHandler handler_touch;
    private Historial_de_acciones historial_de_acciones;

    private Datos datos;


    boolean guardando = false;
    boolean sos_un_boludo = false;
    boolean paraGuardar = false;
    boolean pantalla_tiradas = false;
    boolean historial_primer_inicio = true;

    private Controlador_creac_pj controlador_creac_pj;
    private Controlador_creación_armaduras controlador_creación_armaduras;
    private Controlador_creación_armas controlador_creación_armas;
    private Controlador_Psiquicos controlador_Psiquicos;
    private Controlador_creación_modificadores controlador_creación_modificadores;
    private Controlador_creación_Lugares controlador_creación_Lugares;
    private Controlador_secundarias controlador_secundarias;
    private Controlador_combate controlador_combate;

    private Historial historial;

    // ArrayList<Personaje> array_Personajes_En_Mision = new ArrayList<>();
    // ArrayList<Personaje> arrayPersonajes = new ArrayList<>();
    ArrayList<Arma> arrayArmas = new ArrayList<>();
    ArrayList<Armadura> arrayArmaduras = new ArrayList<>();
    //  ArrayList<Modificador> arrayModificadores = new ArrayList<>();

    ArrayList<Lugar> arraylugares = new ArrayList<>();
    ArrayList<Lugar> arraylugares_partida = new ArrayList<>();
    private ArrayList<Tirada> tiradas = new ArrayList<>();

    EditText EditText_Eff_RegenVida;
    EditText EditText_RegenVida;
    int indexPersonaje = 0;
    private ArrayAdapter<String> adapterPjs;
    private ArrayAdapter<String> adapterPjs2;

    private Toolbar toolbar;

    Principal mainPrincipal;

    public void setGuardando(boolean guardando) {
        this.guardando = guardando;
    }

    public void setParaGuardar(boolean paraGuardar) {
        this.paraGuardar = paraGuardar;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        AndroidThreeTen.init(this.getApplication());
        setContentView(R.layout.activity_principal);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        try {

            controlador_ventanas = new Controlador_ventanas(this);
            historial_de_acciones = new Historial_de_acciones(this);


            ViewGroup rootLayout = (ViewGroup) findViewById(android.R.id.content);

            datos = new Datos(this);

            //PDFBoxResourceLoader.init(getApplicationContext());

            //obtener estado del teclado

            if (obtener_estado(getString(R.string.usar_teclado_interno), true)) {
                teclado = new Teclado(this, true);
                //Nunca mostrar el teclado:
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM, WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
            } else {
                teclado = new Teclado(this, false);
            }

            setAllListener(rootLayout); //Crea listeners para acciones en todos los elementos de la interfaz


            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

            drawer.setDrawerListener(toggle);
            toggle.syncState();


            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
            mainPrincipal = this;


            DataBase = new BaseDeDatos_v2(mainPrincipal);

            handler_touch = new TouchHandler();

            ((LinearLayout) findViewById(R.id.IndicadorGuardando)).setVisibility(View.GONE);
            ((LinearLayout) findViewById(R.id.IndicadorError)).setVisibility(View.GONE);

            new CargaDB().execute();

            //escribir encabezado del Historial

            historial = new Historial(this);

            historial.setHistorial(DataBase.GetHistorial());

            historial.appendLog("Inicio de aplicación", "");

            //obtener los últimos pjs

            SharedPreferences lastPjs = this.getSharedPreferences(getString(R.string.Ultimos_pjs_usados), this.MODE_PRIVATE);

            if (lastPjs.getString(getString(R.string.Ultimos_pjs_usados), "") != "") {

                String[] Last_Pjs_IDS_text = derialize(lastPjs.getString(getString(R.string.Ultimos_pjs_usados), ""));

                int[] Last_Pjs_IDS = TextToInt(Last_Pjs_IDS_text);

                for (int id : Last_Pjs_IDS) {
                    for (int i = 0; i < datos.ArrayPersonajes().size(); i++) {
                        if (datos.ArrayPersonajes().get(i).getID() == id) {
                            datos.Array_Personajes_En_Mision_add(datos.ArrayPersonajes().get(i));
                            break;
                        }
                    }
                }
                actualizarAdapterPersonajesPartida();
            }

            //Obtener los últimos lugares

            SharedPreferences last_lugares = this.getSharedPreferences(getString(R.string.Ultimos_lugares_usados), this.MODE_PRIVATE);

            if (last_lugares.getString(getString(R.string.Ultimos_lugares_usados), "") != "") {

                String[] Last_lugares_IDS_text = derialize(last_lugares.getString(getString(R.string.Ultimos_lugares_usados), ""));

                int[] Last_Lugares_IDS = TextToInt(Last_lugares_IDS_text);

                for (int id : Last_Lugares_IDS) {
                    for (int i = 0; i < arraylugares.size(); i++) {
                        if (arraylugares.get(i).getId() == id) {
                            arraylugares_partida.add(arraylugares.get(i));
                            break;
                        }
                    }
                }

               // actualizarAdapterLugaresPartida();
            }

            //pdf = new PdfEditor(this);

            actualizarArrayLugares(R.id.spinner_ubicación, true, true);

            controlador_turno = new Controlador_Turno(mainPrincipal);

           // ((ListView) findViewById(R.id.List_view_Iniciativas)).setAdapter(controlador_turno);

            //   controlador_turno.Calcular_turnos();

            adapterPjs = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item);
            adapterPjs2 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item);

            //((ViewGroup) getWindow().getDecorView().getRootView()).getLayoutTransition().enableTransitionType(LayoutTransition.CHANGING);

            tutorial = new Tutorial(this);
            tutorial.start_ppal(false);
            controlador_Psiquicos = new Controlador_Psiquicos(this);
            controlador_creación_armaduras = new Controlador_creación_armaduras(this);
            controlador_creación_armaduras = new Controlador_creación_armaduras(this);
            controlador_creación_Lugares = new Controlador_creación_Lugares(this);
            controlador_creación_armas = new Controlador_creación_armas(this);
            controlador_creación_modificadores = new Controlador_creación_modificadores(this);
            controlador_creac_pj = new Controlador_creac_pj(this);
            controlador_secundarias = new Controlador_secundarias(this);
            controlador_combate = new Controlador_combate(this);
            Speech.init(this, getPackageName());

            int[] heights = {45, 45, 45, 45, 45};
            ((SpeechProgressView) findViewById(R.id.progress_speech)).setBarMaxHeightsInDp(heights);


        } catch (Throwable t) {//Capturar los errores para enviar el log
            mostrarThrowable(t);
        }


        try {//añadir al inicio los textos de T O D O
            ((TextView) findViewById(R.id.textView_TODO)).setText(getTODO());
        } catch (IOException | NullPointerException e) {
            System.out.println(e);
        }

    }

    public String getRawText(int resource_id) throws IOException {
        String str = "";
        StringBuffer buf = new StringBuffer();
        InputStream resource_text = this.getResources().openRawResource(resource_id);
        BufferedReader reader = new BufferedReader(new InputStreamReader(resource_text));
        if (resource_text != null) {
            while ((str = reader.readLine()) != null) {
                buf.append(str + "\n");
            }
        }
        return buf.toString();
    }

    public String getTODO() throws IOException {
        String str = "";
        StringBuffer buf = new StringBuffer();
        InputStream todo = this.getResources().openRawResource(R.raw.versiones);
        BufferedReader reader = new BufferedReader(new InputStreamReader(todo));
        if (todo != null) {
            while ((str = reader.readLine()) != null) {
                buf.append(str + "\n");
            }
        }
        buf.append("\n\n");

        todo.close();
        InputStream version = this.getResources().openRawResource(R.raw.todo_list);
        reader = new BufferedReader(new InputStreamReader(version));
        if (version != null) {
            while ((str = reader.readLine()) != null) {
                buf.append(str + "\n");
            }
        }
        version.close();

        return buf.toString();

    }

    private final Runnable indicador_de_procesado = new Runnable() {
        @Override
        public void run() {

            if (guardando) {

                View tri_punto_1 = findViewById(R.id.tri_punto_1);
                View tri_punto_2 = findViewById(R.id.tri_punto_2);
                View tri_punto_3 = findViewById(R.id.tri_punto_3);
                View tri_punto_4 = findViewById(R.id.tri_punto_4);
                View tri_punto_5 = findViewById(R.id.tri_punto_5);
                View tri_punto_6 = findViewById(R.id.tri_punto_6);

                View tri_base_layout = findViewById(R.id.tri_base_layout);

                final long value_duration = 1000;
                final long value_delay = 500;

                long valueTotal = value_duration * 3 + value_delay * 3;

                View layout_animator = findViewById(R.id.layout_animator);
                float height_layout_animator = layout_animator.getHeight();
                int height_dot = layout_animator.getHeight() / 10;

                new AdditiveAnimator().targets(tri_punto_1, tri_punto_2, tri_punto_3, tri_punto_4, tri_punto_5, tri_punto_6)
                        .size(height_dot).setDuration(0).start();

                final AdditiveAnimator anim_p1 = new AdditiveAnimator().targets(tri_punto_1, tri_punto_2, tri_punto_3, tri_punto_4)
                        .setDuration(value_duration)
                        .y(resize_porcentual(height_layout_animator, 50) - (height_dot / 2)).x(resize_porcentual(height_layout_animator, 50) - (height_dot / 2))
                        .thenWithDelay(value_delay)
                        .alpha(255)
                        .targets(tri_punto_1, tri_punto_2)
                        .y(resize_porcentual(height_layout_animator, 53) - (height_dot / 2)).x(resize_porcentual(height_layout_animator, 36) - (height_dot / 2))
                        .thenWithDelay(value_delay)
                        .target(tri_punto_1)
                        .y(resize_porcentual(height_layout_animator, 61) - (height_dot / 2)).x(resize_porcentual(height_layout_animator, 50) - (height_dot / 2));


                final AdditiveAnimator anim_p2 = new AdditiveAnimator().targets(tri_punto_1, tri_punto_2, tri_punto_3, tri_punto_4)
                        .setDuration(value_duration)
                        .thenWithDelay(value_delay)
                        .targets(tri_punto_3, tri_punto_4)
                        .y(resize_porcentual(height_layout_animator, 53) - (height_dot / 2)).x(resize_porcentual(height_layout_animator, 64) - (height_dot / 2))
                        .thenWithDelay(value_delay)
                        .target(tri_punto_4)
                        .y(resize_porcentual(height_layout_animator, 61) - (height_dot / 2)).x(resize_porcentual(height_layout_animator, 50) - (height_dot / 2));

                final AdditiveAnimator anim_p3 = new AdditiveAnimator().targets(tri_punto_5, tri_punto_6)
                        .setDuration(value_duration)
                        .y(resize_porcentual(height_layout_animator, 73) - (height_dot / 2)).x(resize_porcentual(height_layout_animator, 50) - (height_dot / 2))
                        .thenWithDelay(value_delay)
                        .alpha(255)
                        .y(resize_porcentual(height_layout_animator, 78) - (height_dot / 2)).x(resize_porcentual(height_layout_animator, 50) - (height_dot / 2))
                        .thenWithDelay(value_delay)
                        .target(tri_punto_6)
                        .y(resize_porcentual(height_layout_animator, 61) - (height_dot / 2)).x(resize_porcentual(height_layout_animator, 50) - (height_dot / 2))
                        .thenWithDelay(value_delay)
                        .thenWithDelay(value_delay)
                        .targets(tri_punto_1, tri_punto_2, tri_punto_3, tri_punto_4, tri_punto_5, tri_punto_6)
                        .y(resize_porcentual(height_layout_animator, 61) - (height_dot / 2)).x(resize_porcentual(height_layout_animator, 50) - (height_dot / 2));


                anim_p3.addEndAction(new AnimationEndListener() {
                    @Override
                    public void onAnimationEnd(boolean wasCancelled) {
                        final View indicador = findViewById(R.id.layout_animator);
                        indicador.postDelayed(indicador_de_procesado, value_delay);
                    }
                });

                anim_p1.start();
                anim_p2.start();
                anim_p3.start();

            }

        }
    };

    private void mostrarThrowable(Throwable t) {
        FragmentManager fm = getFragmentManager();
        Fragment_classes.DialogError dialogFragment = new Fragment_classes.DialogError();

        t.printStackTrace();

        dialogFragment.setText("Error: " + t.getMessage() + "\n En: " + Arrays.toString(t.getStackTrace()));

        dialogFragment.show(fm, "Error grave: ");

    }


    public void actualizarVistazo() {

        ArrayList<Personaje> personajes_temp = new ArrayList<>();

        for (Personaje pj : datos.Array_Personajes_En_Mision()) {
            personajes_temp.add(pj);
        }

        CustomAdapter_VistazoPersonajes adapter = new CustomAdapter_VistazoPersonajes(this, personajes_temp, this);

        ((ListView) findViewById(R.id.List_view_Personajes)).setAdapter(adapter);
    }

    public void reemplazarPj(Personaje pj) {

        for (int i = 0; i < datos.ArrayPersonajes().size(); i++) {

            if (pj.getID() == datos.ArrayPersonajes().get(i).getID()) {

                datos.ArrayPersonajes().get(i).reemplazar(pj);

                longSave();

                break;
            }

        }


    }

    public class TouchHandler {

        private float last_touch_event_x = 0;
        private float last_touch_event_y = 0;
        private long startClickTime;
        static final int MIN_DISTANCE = 150;
        static final int MAX_Y_DISTANCE = 100;
        static final int MAX_SWIPE_TIME = 200;

        public TouchHandler() {
        }

        public float getLast_touch_event_x() {
            return last_touch_event_x;
        }

        public void setLast_touch_event_x(float last_touch_event_x) {
            this.last_touch_event_x = last_touch_event_x;
        }

        public void setLast_touch_event_y(float last_touch_event_y) {
            this.last_touch_event_y = last_touch_event_y;
        }

        public long getStartClickTime() {
            return startClickTime;
        }

        public void setStartClickTime(long startClickTime) {
            this.startClickTime = startClickTime;
        }

        public void TouchEvent(MotionEvent ev) {

            if (ev.getAction() == MotionEvent.ACTION_DOWN) {
                last_touch_event_x = ev.getX();
                last_touch_event_y = ev.getY();
                startClickTime = System.currentTimeMillis();
            }

            if (ev.getAction() == MotionEvent.ACTION_UP) {

                float deltaX = Math.abs(ev.getX() - last_touch_event_x);
                float deltaY = Math.abs(ev.getY() - last_touch_event_y);
                long epoch_actual = System.currentTimeMillis();

                if (epoch_actual - startClickTime < 1000 & deltaX > MIN_DISTANCE & deltaY < MAX_Y_DISTANCE) {
                    System.out.print(" SWIPE! ");
                    if (ev.getX() > last_touch_event_x) {
                        System.out.println(" derecha ");


                        if (controlador_ventanas.getToolkit_combate() != null) {
                            if (controlador_ventanas.getToolkit_combate().getVisibility() == View.VISIBLE) {

                                TabLayout tabLayout_combate = (TabLayout) findViewById(R.id.tab_layout_combate);
                                try {
                                    TabLayout.Tab tab = tabLayout_combate.getTabAt(tabLayout_combate.getSelectedTabPosition() - 1);
                                    tab.select();
                                } catch (java.lang.NullPointerException e) {
                                    TabLayout.Tab tab = tabLayout_combate.getTabAt(tabLayout_combate.getTabCount() - 1);
                                    tab.select();
                                    //the tab doesnt exist
                                }
                            }
                        } else if (controlador_ventanas.getToolkit_creacion_pj() != null) {
                            if (controlador_ventanas.getToolkit_creacion_pj().getVisibility() == View.VISIBLE) {

                                TabLayout tabLayout_crear_pj = (TabLayout) findViewById(R.id.tab_layout_pj);
                                try {
                                    TabLayout.Tab tab = tabLayout_crear_pj.getTabAt(tabLayout_crear_pj.getSelectedTabPosition() - 1);
                                    tab.select();
                                } catch (java.lang.NullPointerException e) {
                                    TabLayout.Tab tab = tabLayout_crear_pj.getTabAt(tabLayout_crear_pj.getTabCount() - 1);
                                    tab.select();
                                    //the tab doesnt exist
                                }
                            }
                        }

                    } else {
                        if (controlador_ventanas.getToolkit_combate() != null) {
                            if (controlador_ventanas.getToolkit_combate().getVisibility() == View.VISIBLE) {

                                TabLayout tabLayout_combate = (TabLayout) findViewById(R.id.tab_layout_combate);
                                try {
                                    TabLayout.Tab tab = tabLayout_combate.getTabAt(tabLayout_combate.getSelectedTabPosition() + 1);
                                    tab.select();
                                } catch (java.lang.NullPointerException e) {

                                    TabLayout.Tab tab = tabLayout_combate.getTabAt(0);
                                    tab.select();
                                    //the tab doesnt exist
                                }
                            }
                        } else if (controlador_ventanas.getToolkit_creacion_pj() != null) {
                            if (controlador_ventanas.getToolkit_creacion_pj().getVisibility() == View.VISIBLE) {
                                TabLayout tabLayout_crear_pj = (TabLayout) findViewById(R.id.tab_layout_pj);
                                try {
                                    TabLayout.Tab tab = tabLayout_crear_pj.getTabAt(tabLayout_crear_pj.getSelectedTabPosition() + 1);
                                    tab.select();
                                } catch (java.lang.NullPointerException e) {

                                    TabLayout.Tab tab = tabLayout_crear_pj.getTabAt(0);
                                    tab.select();
                                    //the tab doesnt exist
                                }
                            }
                        }

                        System.out.println(" izquierda ");
                    }

                }

            }
        }

        public boolean longClick(MotionEvent ev) {
            if (ev.getAction() == MotionEvent.ACTION_UP) {

                long epoch_actual = System.currentTimeMillis();

                if (epoch_actual - startClickTime > 200) {
                    return true;
                }
            }
            return false;
        }

    }


    public void AccionarTeclado() {
        if (teclado.isActivo()) {
            if (getCurrentFocus() instanceof EditText) {
                //System.out.println("Edittext!" + ((EditText) getCurrentFocus()).getInputType());
                if (((EditText) getCurrentFocus()).getInputType() == 4098) {
                    teclado.controlTeclado(2);
                } else {
                    teclado.controlTeclado(1);
                }
                getCurrentFocus().postDelayed(corrector_teclado, 50);
            } else {
                teclado.controlTeclado(-1);
            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        //System.out.println("On Touch event");

        if (teclado.isActivo() & getCurrentFocus() instanceof EditText & v instanceof EditText & !(v instanceof Button) & !(v instanceof ImageButton)) {

            AccionarTeclado();

        }

        if (!(getCurrentFocus() instanceof EditText) & !(v instanceof EditText)) {
            teclado.controlTeclado(-1);

        }

        if (handler_touch.longClick(event)) {
            System.out.println("is long event!");
            teclado.controladorToggleLongPress(v.getId());
        }

        handler_touch.TouchEvent(event);

        if (v instanceof EditText & (event.getAction() == MotionEvent.ACTION_UP)) {
            final EditText ed = (EditText) v;
            ed.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ((EditText) ed).setSelection(((EditText) ed).getText().toString().length());
                }
            }, 5);
        }


        return false;
    }


    public void CombateTV() {

        ultima_tirada_combate = Combate.controlador(this);

        ((WebView) findViewById(R.id.Web_View_desc)).loadData("<font size=\"4\">" + ultima_tirada_combate.getDetalle(), "text/html; charset=UTF-8", null);

    }

    private long mLastSave = 0;

    public boolean longSave() {
        long currentTime = System.currentTimeMillis();

        if (currentTime - mLastSave > 5000) {
            mLastSave = currentTime;

            ((LinearLayout) findViewById(R.id.IndicadorError)).setVisibility(View.GONE);

            ArrayList<Personaje> personajes_a_guardar = new ArrayList<>();

            for (Personaje pj : datos.Array_Personajes_En_Mision()) {
                personajes_a_guardar.add(pj);
            }

            new longSaveProcess().execute(personajes_a_guardar);

            paraGuardar = false;

            return true;
        }
        setParaGuardar(true);
        return false;
    }

    private class longSaveProcess extends AsyncTask<ArrayList<Personaje>, Integer, Boolean> {

        protected Boolean doInBackground(ArrayList<Personaje>... personajes) {

            DataBase.ModificarPersonajes(personajes[0]);

            return true;
        }

        @Override
        protected void onPostExecute(Boolean coso) {
            if (coso) {
                IndicadorGuardando(false);
                guardando = false;
            } else {
                ((LinearLayout) findViewById(R.id.IndicadorError)).setVisibility(View.VISIBLE);
            }


        }

        protected void onPreExecute() {
            IndicadorGuardando(true);

            guardando = true;
        }
    }


    public void actualizarArrayArmaduras(int id_Spinner) {

        String[] Armaduras_Nombres = new String[arrayArmaduras.size()];

        for (int i = 0; i < arrayArmaduras.size(); i++) {
            Armaduras_Nombres[i] = arrayArmaduras.get(i).getNombre();
        }

        ArrayAdapter<String> adapter_Armaduras = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, Armaduras_Nombres);

        ((Spinner) findViewById(id_Spinner)).setAdapter(adapter_Armaduras);

    }

    public void actualizarArrayPersonajes(int id_Spinner, boolean iniciovacio) {

        ArrayList<String> Personajes_nombres_array = new ArrayList<>();

        if (iniciovacio) {
            Personajes_nombres_array.add("");
        }
        for (int i = 0; i < datos.ArrayPersonajes().size(); i++) {
            Personajes_nombres_array.add(datos.ArrayPersonajes().get(i).getNombre_personaje() + " - " + datos.ArrayPersonajes().get(i).getNombre_jugador());
        }

        String[] Personajes_nombres = new String[Personajes_nombres_array.size()];

        for (int i = 0; i < Personajes_nombres_array.size(); i++) {
            Personajes_nombres[i] = Personajes_nombres_array.get(i);
        }

        ArrayAdapter<String> adapter_Personajes = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, Personajes_nombres);

        ((Spinner) findViewById(id_Spinner)).setAdapter(adapter_Personajes);

    }

    public void actualizarArrayPersonajes_EnPartida(int id_Spinner, boolean extra, int tipo) {
        String[] vacio = {"NPC++"};

        if (datos.Array_Personajes_En_Mision() != null & datos.Array_Personajes_En_Mision().size() != 0) {
            String[] Personajes_nombres;

            if (extra) {
                Personajes_nombres = new String[datos.Array_Personajes_En_Mision().size() + 1];
            } else {
                Personajes_nombres = new String[datos.Array_Personajes_En_Mision().size()];
            }


            try {
                for (int i = 0; i < datos.Array_Personajes_En_Mision().size(); i++) {
                    Personajes_nombres[i] = datos.Array_Personajes_En_Mision().get(i).getNombres();
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println(e.toString());
            }

            if (extra) {
                Personajes_nombres[datos.Array_Personajes_En_Mision().size()] = "NPC ++";
            }


            ArrayAdapter<String> adapter_Personajes = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, Personajes_nombres);

            ((Spinner) findViewById(id_Spinner)).setAdapter(adapter_Personajes);
        } else {
            switch (tipo) {
                case 0://ataque

                    ((Spinner) findViewById(R.id.spinner_Combate_Tipo_de_ataque)).setAdapter(ArrayAdapter.createFromResource(this, R.array.Tipos_de_ataque, android.R.layout.simple_spinner_dropdown_item));

                    ((Spinner) findViewById(R.id.spinner_Combate_Jugador_arma_atk)).setAdapter(ArrayAdapter.createFromResource(this, R.array.Daños_base_Armas, android.R.layout.simple_spinner_dropdown_item));
                    ((ProgressBar) findViewById(R.id.ProgressBar_Combate_Jugador_atacante_vida)).setMax(100);
                    ((ProgressBar) findViewById(R.id.ProgressBar_Combate_Jugador_atacante_vida)).setProgress(100);
                    ((TextView) findViewById(R.id.textView_Combate_atacante_texto)).setText("Realizando tirada con datos genericos");

                    for (int i = 0; i < modificadores_ataque.length; i++) {
                        modificadores_ataque[i] = false;
                    }


                    ArrayAdapter<String> adapter_Personajes = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, vacio);

                    ((Spinner) findViewById(id_Spinner)).setAdapter(adapter_Personajes);

                    break;
                case 1://Defensor
                    ((Spinner) findViewById(R.id.spinner_Combate_tipo_de_defensa)).setAdapter(ArrayAdapter.createFromResource(this, R.array.vida_npc_plus, android.R.layout.simple_spinner_dropdown_item));
                    ((Spinner) findViewById(R.id.spinner_Combate_Jugador_arma_def)).setAdapter(ArrayAdapter.createFromResource(this, R.array.ta_NPC_plus, android.R.layout.simple_spinner_dropdown_item));
                    ((ProgressBar) findViewById(R.id.ProgressBar_Combate_Jugador_defensor_vida)).setMax(100);
                    ((ProgressBar) findViewById(R.id.ProgressBar_Combate_Jugador_defensor_vida)).setProgress(100);
                    ((TextView) findViewById(R.id.textView_Combate_defensor_texto)).setText("Realizando tirada con datos genericos");
                    Arrays.fill(modificadores_defensa, false);


                    ((Spinner) findViewById(id_Spinner)).setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, vacio));
                    break;
                case 2:

                    ((Spinner) findViewById(id_Spinner)).setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, new String[]{"NPC++"}));
                    break;
                default:

                    ((Spinner) findViewById(id_Spinner)).setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, new String[]{"Añada personajes a la partida"}));
                    break;
            }
        }
    }

    public void actualizarArrayLugares(int id_Spinner, boolean mostrarVacio, boolean listado_parcial) {
        String[] vacio = {"Seleccione una ubicación"};

        int index = ((Spinner) findViewById(id_Spinner)).getSelectedItemPosition();

        if (listado_parcial) {
            if (arraylugares_partida != null & arraylugares_partida.size() != 0) {
                String[] Lugares_nombres;

                if (mostrarVacio) {
                    Lugares_nombres = new String[arraylugares_partida.size() + 1];
                    try {
                        Lugares_nombres[0] = "Seleccione una ubicación";

                        for (int i = 0; i < arraylugares_partida.size() + 1; i++) {
                            Lugares_nombres[i + 1] = arraylugares_partida.get(i).getNombre();
                        }

                    } catch (IndexOutOfBoundsException e) {
                        System.out.println(e.toString());
                    }
                } else {
                    Lugares_nombres = new String[arraylugares_partida.size()];
                    try {
                        for (int i = 0; i < arraylugares_partida.size(); i++) {
                            Lugares_nombres[i] = arraylugares_partida.get(i).getNombre();
                        }
                    } catch (ArrayIndexOutOfBoundsException e) {
                        System.out.println(e.toString());
                    }
                }

                ArrayAdapter<String> adapter_Lugares = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, Lugares_nombres);

                ((Spinner) findViewById(id_Spinner)).setAdapter(adapter_Lugares);
                ((Spinner) findViewById(id_Spinner)).setSelection(index);

            } else {

                ArrayAdapter<String> adapter_Lugares = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, vacio);

                ((Spinner) findViewById(id_Spinner)).setAdapter(adapter_Lugares);
            }
        } else {

            if (arraylugares != null & arraylugares.size() != 0) {
                String[] Lugares_nombres;

                if (mostrarVacio) {
                    Lugares_nombres = new String[arraylugares.size() + 1];
                    try {
                        Lugares_nombres[0] = "Seleccione una ubicación";

                        for (int i = 0; i < arraylugares.size() + 1; i++) {
                            Lugares_nombres[i + 1] = arraylugares.get(i).getNombre();
                        }

                    } catch (IndexOutOfBoundsException e) {
                        System.out.println(e.toString());
                    }
                } else {
                    Lugares_nombres = new String[arraylugares.size()];
                    try {
                        for (int i = 0; i < arraylugares.size(); i++) {
                            Lugares_nombres[i] = arraylugares.get(i).getNombre();
                        }
                    } catch (ArrayIndexOutOfBoundsException e) {
                        System.out.println(e.toString());
                    }
                }


                ArrayAdapter<String> adapter_Lugares = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, Lugares_nombres);


                ((Spinner) findViewById(id_Spinner)).setAdapter(adapter_Lugares);
                ((Spinner) findViewById(id_Spinner)).setSelection(index);

            } else {

                ArrayAdapter<String> adapter_Lugares = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, vacio);

                ((Spinner) findViewById(id_Spinner)).setAdapter(adapter_Lugares);
            }
        }
    }

    public void actualizarArrayArmas(int id_Spinner) {

        String[] Armas_Nombres = new String[arrayArmas.size()];

        for (int i = 0; i < arrayArmas.size(); i++) {
            Armas_Nombres[i] = arrayArmas.get(i).getNombre();
        }

        ArrayAdapter<String> adapter_Armas = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, Armas_Nombres);

        ((Spinner) findViewById(id_Spinner)).setAdapter(adapter_Armas);

    }

    public void actualizarFisicos() {

        CalculoAtributosFisicos AttrView = new CalculoAtributosFisicos();

        int agi = ((Spinner) findViewById(R.id.spinner_agi)).getSelectedItemPosition();
        int con = ((Spinner) findViewById(R.id.spinner_con)).getSelectedItemPosition();
        int fue = ((Spinner) findViewById(R.id.spinner_fue)).getSelectedItemPosition();

        int edad = ((SeekBar) findViewById(R.id.seekBar_edad)).getProgress();
        double pos = ((SeekBar) findViewById(R.id.seekBar_rango)).getProgress();

        AttrView.setAtrs(fue, con, agi, edad);

        double pesoMax = AttrView.pesoMax();
        double pesoMin = AttrView.pesoMin();

        double alturaMax = AttrView.alturaMax();
        double alturaMin = AttrView.alturaMin();

        double alturaActual = ((alturaMax - alturaMin) * (pos / 100) + alturaMin);

        double pesoActual = ((pesoMax - pesoMin) * (pos / 100)) + pesoMin;

        ((TextView) findViewById(R.id.textView_edad)).setText("Edad: " + edad + " años.");
        ((TextView) findViewById(R.id.textView_rango)).setText("Rango:: " + pos + "%.");

        ((TextView) findViewById(R.id.textView_altura)).setText(String.format(Locale.ENGLISH, "Altura: %.2f m.", alturaActual));
        ((TextView) findViewById(R.id.textView_peso)).setText(String.format(Locale.ENGLISH, "Peso aparente: %.2f Kg.", pesoActual));

    }


    public void actualizarAdapterLugaresPartida() {
        ListView ListViewLugaresPartida = (ListView) findViewById(R.id.List_lugares_activos);

        String[] nombres_lugares = new String[arraylugares_partida.size()];

        for (int i = 0; i < arraylugares_partida.size(); i++) {
            nombres_lugares[i] = arraylugares_partida.get(i).getNombre();
        }

        ListViewLugaresPartida.setAdapter(new ArrayAdapter<String>(this, R.layout.simple_list_custom, nombres_lugares));

        SharedPreferences lastPjs = this.getSharedPreferences(getString(R.string.Ultimos_lugares_usados), this.MODE_PRIVATE);
        SharedPreferences.Editor editor = lastPjs.edit();

        String[] valores = new String[arraylugares_partida.size()];

        for (int i = 0; i < arraylugares_partida.size(); i++) {
            valores[i] = String.valueOf(arraylugares_partida.get(i).getId());
        }

        editor.putString(getString(R.string.Ultimos_lugares_usados), serialize(valores));
        editor.commit();

    }

    public void actualizarAdapterPersonajesPartida() {
        try {
            ListView ListViewPersonajesPartida = (ListView) findViewById(R.id.List_pjs_activos);

            String[] nombres_pjs = new String[datos.Array_Personajes_En_Mision().size()];

            for (int i = 0; i < datos.Array_Personajes_En_Mision().size(); i++) {
                nombres_pjs[i] = datos.Array_Personajes_En_Mision().get(i).getNombres();
            }

            ListViewPersonajesPartida.setAdapter(new ArrayAdapter<String>(this, R.layout.simple_list_custom, nombres_pjs));

            SharedPreferences lastPjs = this.getSharedPreferences(getString(R.string.Ultimos_pjs_usados), this.MODE_PRIVATE);
            SharedPreferences.Editor editor = lastPjs.edit();

            String[] valores = new String[datos.Array_Personajes_En_Mision().size()];

            for (int i = 0; i < datos.Array_Personajes_En_Mision().size(); i++) {
                valores[i] = String.valueOf(datos.Array_Personajes_En_Mision().get(i).getID());
            }

            editor.putString(getString(R.string.Ultimos_pjs_usados), serialize(valores));
            editor.commit();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void actualizarArrayModificadores() {
        String[] Modificadores_nombres = new String[datos.ArrayModificadores().size()];

        for (int i = 0; i < datos.ArrayModificadores().size(); i++) {
            Modificadores_nombres[i] = datos.ArrayModificadores().get(i).getNombre();
        }
        ArrayAdapter<String> adapter_Personajes = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, Modificadores_nombres);

        ((Spinner) findViewById(R.id.spinner_seleccion_modif)).setAdapter(adapter_Personajes);
    }


    public void IndicadorGuardando(boolean mostrar) {
        if (mostrar) {
            //((TextView) findViewById(R.id.textView_indicador_guardando)).setText("Guardando");
            final View indicador = findViewById(R.id.layout_animator);
            indicador.setVisibility(View.VISIBLE);
            indicador.postDelayed(indicador_de_procesado, 100);
            ((LinearLayout) findViewById(R.id.IndicadorGuardando)).setVisibility(View.VISIBLE);
        } else {
            ((LinearLayout) findViewById(R.id.IndicadorGuardando)).setVisibility(View.GONE);
            posBoludeo = 1;
        }

    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        guardarEstado(String.valueOf(buttonView.getId()), isChecked);
        teclado.controladorToggleButton(buttonView.getId(), isChecked);
        switch (buttonView.getId()) {

            case R.id.toggleButton_cr_pj_herramientas:
                if (isChecked) {
                    ((View) findViewById(R.id.layout_selección_personaje)).setVisibility(View.GONE);
                } else {
                    ((View) findViewById(R.id.layout_selección_personaje)).setVisibility(View.VISIBLE);
                }
                break;
            case R.id.checkBox_teclado_interno:

                guardarEstado(getString(R.string.usar_teclado_interno), isChecked);

                break;
            case R.id.checkBox_toogle_buttons:

                guardarEstado("Ocultar_botones_de_configuración", isChecked);

                break;

            case R.id.toggleButton_mostrar_ultimas_tiradas:
                if (isChecked) {
                    ((LinearLayout) findViewById(R.id.LinearLayout_ultimas_tiradas)).setVisibility(View.VISIBLE);
                    // ((LinearLayout) findViewById(R.id.LinearLayout_ultimas_tiradas)).postDelayed(redimensionar_dado, 5);

                } else {
                    ((LinearLayout) findViewById(R.id.LinearLayout_ultimas_tiradas)).setVisibility(View.GONE);
                    //((LinearLayout) findViewById(R.id.LinearLayout_ultimas_tiradas)).postDelayed(redimensionar_dado, 5);
                }

                break;
        }
    }

    public int lowest(int x, int y) {
        if (x > y) {
            return y;
        } else {
            return x;
        }
    }

    @Override
    public boolean onLongClick(View v) {

        teclado.controladorTeclas_LClick(v.getId());
        switch (v.getId()) {
            case R.id.button_roll_atacante:
                ((EditText) findViewById(R.id.editText_Tirada_atacante)).setText(String.valueOf(Base.tirada(0, 3, 90)));

                CombateTV();
                break;

            case R.id.button_roll_defensor:

                ((EditText) findViewById(R.id.editText_Tirada_defensor)).setText(String.valueOf(Base.tirada(0, 3, 90)));
                CombateTV();
                break;

            case R.id.editText_Tirada_atacante:
            case R.id.editText_Modificador_atacante:
            case R.id.editText_Modificador_atacante_extra:
            case R.id.editText_Tirada_defensor:
            case R.id.editText_Modificador_Defensor:
            case R.id.editText_Modificador_Defensor_Extra:

                ((EditText) v).setText("");

                Snackbar
                        .make(v, "¿Desea borrar los otros campos?", Snackbar.LENGTH_LONG)
                        .setAction("BORRAR TODOS", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                ((EditText) findViewById(R.id.editText_Tirada_atacante)).setText("");
                                ((EditText) findViewById(R.id.editText_Modificador_atacante)).setText("");
                                ((EditText) findViewById(R.id.editText_Modificador_atacante_extra)).setText("");
                                ((EditText) findViewById(R.id.editText_Tirada_defensor)).setText("");
                                ((EditText) findViewById(R.id.editText_Modificador_Defensor)).setText("");
                                ((EditText) findViewById(R.id.editText_Modificador_Defensor_Extra)).setText("");

                            }

                        }).show();

                break;

        }


        return true;
    }

    private void writeToFile(String data, Context context, String name) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(name, Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private String readFromFile(Context context, String name) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput(name);

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ret;
    }

    @Override
    public void onClick(View v) {

        try {

            if (paraGuardar) {
                longSave();
                paraGuardar = false;
            }


            if (tutorial.isOnTutorial()) {

                tutorial.handler_events(v);

            } else {
                Controlador_botones.onclick(this, v);
                teclado.controladorTeclas_Click(v.getId());

                String text = "";

                int seleccionado = 0;

                if (controlador_ventanas.existsAndVisible(controlador_ventanas.getToolkit_creacion_pj())) {
                    indexPersonaje = ((Spinner) findViewById(R.id.spinner_seleccionPj)).getSelectedItemPosition();

                } else {
                    indexPersonaje = 0;
                }


                Snackbar mensajeConfirmación;
                Snackbar snackbar1;
                Personaje pj;
                int index_armadura_pesonaje = 0;
                int index_arma_pesonaje = 0;
                Snackbar snackbar = null;
                View view = null;
                TextView tv = null;
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);

                String titulo = "";
                String cuerpo = "";
                String lugar = "";
                String jugador_pri = "";
                String jugador_sec = "";

                Intent sendIntent = new Intent();

                Fragment_classes.Dialog_tuto_speech dialog_tuto_speech = new Fragment_classes.Dialog_tuto_speech();

                if(controlador_ventanas.existsAndVisible(controlador_ventanas.getToolkit_combate())){
                    controlador_turno.Controlador_botones(v.getId());
                }

                FragmentManager fm;
                switch (v.getId()) {
                    case R.id.button_pruebas:
                        System.out.println("inicio inflater");

                        AsyncLayoutInflater inflater_async = new AsyncLayoutInflater(mainPrincipal);

                        inflater_async.inflate(R.layout.fragment_error, ((LinearLayout) findViewById(R.id.viewGroup_test)), new AsyncLayoutInflater.OnInflateFinishedListener() {
                            @Override
                            public void onInflateFinished(@NonNull View view, int resid, @Nullable ViewGroup parent) {

                                ((LinearLayout) findViewById(R.id.viewGroup_test)).addView(view);

                                System.out.println("fin inflater");

                            }
                        });
                        break;
                    case R.id.button_Mostrar_tutorial_speech:

                        fm = getFragmentManager();

                        dialog_tuto_speech.show(fm, "Información: ");

                        break;
                    case R.id.ImageButton_microphone:


                        ((ImageButton) v).setImageDrawable(getDrawable(R.drawable.ic_baseline_mic_24px));
                        ((ImageButton) findViewById(R.id.ImageButton_microphone)).setBackground(getDrawable(R.drawable.circle_shape_drawable_green_bkgr));


                        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {

                            // Permission is not granted

                            System.out.println("Sin permisos!");

                            fm = getFragmentManager();

                            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.RECORD_AUDIO}, SPEECH_REQUEST_CODE);

                            dialog_tuto_speech.show(fm, "Información: ");

                            ((ImageButton) findViewById(R.id.ImageButton_microphone)).setImageDrawable(getDrawable(R.drawable.ic_baseline_mic_off_24px));
                            ((ImageButton) findViewById(R.id.ImageButton_microphone)).setBackground(getDrawable(R.drawable.circle_shape_drawable));


                        } else {

                            try {
                                // you must have android.permission.RECORD_AUDIO granted at this point
                                //INDICADOR TEXTO INGRESADO

                                findViewById(R.id.linearLayout_indicador_grabando).setVisibility(View.VISIBLE);
                                findViewById(R.id.IndicadorGuardando).setVisibility(View.VISIBLE);

                                ((TextView) findViewById(R.id.textView_indicador_guardando)).setText("");
                                findViewById(R.id.layout_animator).setVisibility(View.GONE);


                                SpeechProgressView spv = ((SpeechProgressView) findViewById(R.id.progress_speech));
                                Speech.getInstance().startListening(spv, new SpeechDelegate() {


                                    @Override
                                    public void onStartOfSpeech() {
                                        Log.i("speech", "speech recognition is now active");
                                    }

                                    @Override
                                    public void onSpeechRmsChanged(float value) {
                                        //   Log.d("speech", "rms is now: " + value);
                                    }

                                    @Override
                                    public void onSpeechPartialResults(List<String> results) {


                                        System.out.println(" Resultados: " + results.toString());

                                        ((TextView) findViewById(R.id.textView_indicador_guardando)).setText(results.toString().replace("[", "").replace("]", ""));

                                        ((TextView) findViewById(R.id.textView_settings_todo)).setText(results.toString());

                                    }

                                    @Override
                                    public void onSpeechResult(String result) {
                                        Log.i("speech", "result: " + result);

                                        ((ImageButton) findViewById(R.id.ImageButton_microphone)).setImageDrawable(getDrawable(R.drawable.ic_baseline_mic_off_24px));
                                        ((ImageButton) findViewById(R.id.ImageButton_microphone)).setBackground(getDrawable(R.drawable.circle_shape_drawable));

                                        if (controlador_ventanas.existsAndVisible(controlador_ventanas.getPart_Psiquicos())) {
                                            controlador_Psiquicos.controlador_speech(result);
                                        }
                                        if (controlador_ventanas.existsAndVisible(controlador_ventanas.getToolkit_combate())) {
                                            controlador_combate.controlador_speech(result, mainPrincipal);
                                        }
                                        if (controlador_ventanas.existsAndVisible(controlador_ventanas.getToolkit_secundarias())) {
                                            controlador_secundarias.controlador_speech(result);
                                        }


                                        ((LinearLayout) findViewById(R.id.linearLayout_indicador_grabando)).setVisibility(View.GONE);
                                        ((LinearLayout) findViewById(R.id.IndicadorGuardando)).setVisibility(View.GONE);

                                    }
                                });

                            } catch (SpeechRecognitionNotAvailable exc) {
                                Log.e("speech", "Speech recognition is not available on this device!");

                                fm = getFragmentManager();

                                Fragment_classes.Dialog_config_speech dialog_config_speech = new Fragment_classes.Dialog_config_speech();

                                dialog_config_speech.setP(this);

                                dialog_config_speech.show(fm, "Información: ");

                                // You can prompt the user if he wants to install Google App to have
                                // speech recognition, and then you can simply call:
                                //
                                // SpeechUtil.redirectUserToGoogleAppOnPlayStore(this);
                                //
                                // to redirect the user to the Google App page on Play Store
                            } catch (GoogleVoiceTypingDisabledException exc) {
                                Log.e("speech", "Google voice typing must be enabled!");


                            }
                        }


                        /*
                        String[] personajes = getResources().getStringArray(R.array.personajes);

                        int contador = 0;

                        ArrayList<Personaje> personajes_dbtest = new ArrayList<>();
                        for (String text_pjs : personajes) {

                            Personaje_old temp = new Personaje_old(text_pjs);
                            // personajes_dbtest.add(new Personaje(temp));

                            DataBase.AñadirPersonaje(new Personaje(temp));

                            System.out.println(temp.toJson());

                        }
*/

                        break;
                    case R.id.button_intercambiar:
                        int aux1 = ((Spinner) findViewById(R.id.spinner_Combate_Jugador_asaltante)).getSelectedItemPosition();
                        int aux2 = ((Spinner) findViewById(R.id.spinner_Combate_Jugador_defensor)).getSelectedItemPosition();

                        ((Spinner) findViewById(R.id.spinner_Combate_Jugador_asaltante)).setSelection(aux2);
                        ((Spinner) findViewById(R.id.spinner_Combate_Jugador_defensor)).setSelection(aux1);

                        String[] valores = new String[6];

                        valores[0] = ((EditText) findViewById(R.id.editText_Tirada_atacante)).getText().toString();
                        valores[1] = ((EditText) findViewById(R.id.editText_Modificador_atacante)).getText().toString();
                        valores[2] = ((EditText) findViewById(R.id.editText_Modificador_atacante_extra)).getText().toString();

                        valores[3] = ((EditText) findViewById(R.id.editText_Tirada_defensor)).getText().toString();
                        valores[4] = ((EditText) findViewById(R.id.editText_Modificador_Defensor)).getText().toString();
                        valores[5] = ((EditText) findViewById(R.id.editText_Modificador_Defensor_Extra)).getText().toString();

                        ((EditText) findViewById(R.id.editText_Tirada_atacante)).setText(valores[3]);
                        ((EditText) findViewById(R.id.editText_Modificador_atacante)).setText(valores[4]);
                        ((EditText) findViewById(R.id.editText_Modificador_atacante_extra)).setText(valores[5]);

                        ((EditText) findViewById(R.id.editText_Tirada_defensor)).setText(valores[0]);
                        ((EditText) findViewById(R.id.editText_Modificador_Defensor)).setText(valores[1]);
                        ((EditText) findViewById(R.id.editText_Modificador_Defensor_Extra)).setText(valores[2]);

                        break;
                    case R.id.button_tutorial_carga_planilla:
                        fm = getFragmentManager();

                        Fragment_classes.Dialog_tuto_importar dialogFragment_tuto = new Fragment_classes.Dialog_tuto_importar();

                        dialogFragment_tuto.show(fm, "Información: ");

                        break;
                    case R.id.button_gen_pdf:

                        // ((ImageView) findViewById(R.id.pdfview)).setImageBitmap(pdf.crearPlanilla_bitmap());

                        break;
                    case R.id.button_importar_CSV:
                        // startActivityForResult(intent_select_image, LOAD_IMAGE_RESULTS);

                        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                            // Permission is not granted

                            System.out.println("Sin permisos!");

                            loadCsv = true;

                            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, READ_EXTERNAL_STORAGE_RESULT);

                        } else {

                            Intent intent_select_excel = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                            intent_select_excel.addCategory(Intent.CATEGORY_OPENABLE);
                            intent_select_excel.setType("text/comma-separated-values");
                            startActivityForResult(intent_select_excel, LOAD_EXCEL_RESULTS);

                            // startActivityForResult(intent_select_image, LOAD_IMAGE_RESULTS);

                        }

                        break;
                    case R.id.button_Mostrar_tutorial_ppal:

                        tutorial.start_ppal(true);


                        break;
                    case R.id.button_restart:

                        Intent intent_exit = getBaseContext().getPackageManager()
                                .getLaunchIntentForPackage(getBaseContext().getPackageName());
                        intent_exit.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        finish();
                        startActivity(intent_exit);
                        System.exit(0);

                        break;
                    case R.id.button_borrar_todos_los_lugares:
                        arraylugares_partida.clear();
                        actualizarArrayLugares(R.id.spinner_ubicación, true, true);
                        actualizarAdapterLugaresPartida();
                        break;
                    case R.id.button_añadir_lugares_a_mision:
                        arraylugares_partida.addAll(arraylugares);
                        actualizarArrayLugares(R.id.spinner_ubicación, true, true);
                        actualizarAdapterLugaresPartida();
                        break;
                    case R.id.imageButton_cambiar_imagen:
                        //si la app posee permisos:
                        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                            // Permission is not granted

                            System.out.println("Sin permisos!");

                            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, READ_EXTERNAL_STORAGE_RESULT);

                        } else {

                            CropImage.activity().setGuidelines(CropImageView.Guidelines.ON)
                                    .setCropShape(CropImageView.CropShape.OVAL)
                                    .setAspectRatio(1, 1)
                                    .setInitialCropWindowPaddingRatio(0)
                                    .setRequestedSize(256, 256, CropImageView.RequestSizeOptions.RESIZE_INSIDE)
                                    .start(this);

                            // Intent intent_select_image = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                            // startActivityForResult(intent_select_image, LOAD_IMAGE_RESULTS);

                        }

                        break;

                    case R.id.button_historial_tiradas:

                        cambio_de_pagina(null, R.id.Extras_logger, true, getResources().getString(R.string.tiradas));

                        historial.buscar("", "", "", "tipo: Tirada generada.", "", -1, 0, false);

                        if (!(((Button) findViewById(R.id.button_ocultar_log)).getText() == "Simple")) {
                            ((Button) findViewById(R.id.button_ocultar_log)).callOnClick();
                        }


                        break;


                    case R.id.button_ocultar_log:

                        if (((Button) v).getText() == "Simple") {
                            ((LinearLayout) findViewById(R.id.layout_logger_simple)).setVisibility(View.VISIBLE);
                            ((LinearLayout) findViewById(R.id.layout_añadir_buscar_log)).setVisibility(View.GONE);
                            ((Button) v).setText("Avanzado");
                        } else {
                            ((LinearLayout) findViewById(R.id.layout_logger_simple)).setVisibility(View.GONE);
                            ((LinearLayout) findViewById(R.id.layout_añadir_buscar_log)).setVisibility(View.VISIBLE);
                            ((Button) v).setText("Simple");
                        }


                        break;
                    case R.id.button_compartir_log:


                        text = historial.getTexto();

                        writeToFile(text, this, "historial.html");//TODO

                        //text = ((WebView) findViewById(R.id.WebView_logger)).toString();
                   /* sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, text);
                    sendIntent.setType("text/plain");
                    startActivity(sendIntent);
                    */
                        sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, text);
                        sendIntent.setType("text/plain");
                        startActivity(sendIntent);


                        break;

                    case R.id.button_datePicker_log:
                        fm = getFragmentManager();
                        Fragment_classes.Dialog_date_picker dialogFragment = new Fragment_classes.Dialog_date_picker();
                        dialogFragment.setP(this);
                        dialogFragment.show(fm, "elegir fecha para buscar: ");
                        break;

                    case R.id.button_buscar_log:
                        // System.out.println("." + ((Button) v).getWebContent() + ".");
                        if (((Button) v).getText() == "Cancelar") {
                            ((Button) v).setText("Buscar");
                            historial.setBuscar(false);
                        } else {
                            ((Button) v).setText("Cancelar");

                            boolean inclusive = false;
                            try {
                                titulo = ((EditText) findViewById(R.id.editText_Título_logger)).getText().toString();
                                cuerpo = ((EditText) findViewById(R.id.editText_Cuerpo_logger)).getText().toString();
                                jugador_pri = ((Spinner) findViewById(R.id.spinner_jugadores_log_primario)).getSelectedItem().toString();

                                try {
                                    jugador_pri = datos.ArrayPersonajes().get(((Spinner) findViewById(R.id.spinner_jugadores_log_primario)).getSelectedItemPosition() - 1).getNombre_personaje();
                                } catch (ArrayIndexOutOfBoundsException e) {
                                    e.printStackTrace();
                                }

                                jugador_sec = ((Spinner) findViewById(R.id.spinner_jugadores_log_secundario)).getSelectedItem().toString();

                                try {
                                    jugador_sec = datos.ArrayPersonajes().get(((Spinner) findViewById(R.id.spinner_jugadores_log_secundario)).getSelectedItemPosition() - 1).getNombre_personaje();

                                } catch (ArrayIndexOutOfBoundsException e) {
                                    e.printStackTrace();
                                }

                                if (((Spinner) findViewById(R.id.spinner_ubicación_log)).getSelectedItemPosition() != 0) {
                                    lugar = ((Spinner) findViewById(R.id.spinner_ubicación_log)).getSelectedItem().toString();

                                    if (((ToggleButton) findViewById(R.id.toggleButton_metodo_lugar)).isChecked()) {

                                        inclusive = true;

                                    }
                                }


                            } catch (java.lang.NullPointerException e) {
                                System.out.println(e.toString());
                            }

                            boolean inclusivo = ((ToggleButton) findViewById(R.id.toggleButton_metodo_lugar)).isChecked();

                            historial.buscar(titulo, jugador_pri, jugador_sec, cuerpo, lugar, -1, 1, inclusivo);
                            //((TextView) findViewById(R.id.textView_logger)).setWebContent();
                        }


                        break;
                    case R.id.button_añadir_al_log:
                        try {
                            titulo = ((EditText) findViewById(R.id.editText_Título_logger)).getText().toString();
                            cuerpo = ((EditText) findViewById(R.id.editText_Cuerpo_logger)).getText().toString();
                            jugador_pri = ((Spinner) findViewById(R.id.spinner_jugadores_log_primario)).getSelectedItem().toString();
                            jugador_sec = ((Spinner) findViewById(R.id.spinner_jugadores_log_secundario)).getSelectedItem().toString();
                            lugar = ((Spinner) findViewById(R.id.spinner_ubicación_log)).getSelectedItem().toString();
                        } catch (java.lang.NullPointerException e) {
                            System.out.println(e.toString());
                        }

                        historial.appendLog(jugador_pri + " " + titulo + " " + jugador_sec, cuerpo);

                        ((WebView) findViewById(R.id.WebView_logger)).loadData("<font size=\"4\">" + historial.getLog(), "text/html; charset=UTF-8", null);

                        break;


                    case R.id.button_generar_caracteristicas:
                        int tipoGen = ((Spinner) findViewById(R.id.spinner_tipo_generacion_caract)).getSelectedItemPosition();

                        int[] caract = Base.generadorAtributos(tipoGen);

                        String texto_caract = getResources().getStringArray(R.array.tipos_De_generacion_De_atributos_desc)[tipoGen];

                        texto_caract = texto_caract + "\r\n Resultado: " + Arrays.toString(caract);

                        ((TextView) findViewById(R.id.textView_generar_caractreristicas)).setText(texto_caract);

                        break;
                    case R.id.button_aplicar_tiempo:
                        setParaGuardar(true);

                        int cantidad = ((Spinner) findViewById(R.id.spinner_cantidad)).getSelectedItemPosition();
                        int tipoTiempo = ((Spinner) findViewById(R.id.spinner_tipo)).getSelectedItemPosition();

                        double[] normalizador_a_dia = {0.0000347222, 0.000694444, 0.0416666666, 1, 7, 30, 365};

                        double multiplicador = (cantidad + 1) * normalizador_a_dia[tipoTiempo];

                        for (int j = 0; j < datos.Array_Personajes_En_Mision().size() - 1; j++) {

                            int index_personaje = j;
                            pj = datos.ArrayPersonajes().get(index_personaje);

                            int valor_actual = pj.getCaracteristicas(Base.VIDA_ACT);
                            int valor_maximo = pj.getCaracteristicas(Base.VIDA_MAX);

                            int suma = ((int) Math.round(pj.getCaracteristicas(Base.VIDA_REG) * multiplicador));

                            valor_actual = valor_actual + suma;

                            if (valor_actual > valor_maximo) {
                                pj.reemplazar_valor_caracteristica(Base.VIDA_ACT, valor_maximo);
                            } else {
                                pj.reemplazar_valor_caracteristica(Base.VIDA_ACT, valor_actual);
                            }

                            valor_actual = pj.getCaracteristicas(Base.ZEON_ACT);
                            valor_maximo = pj.getCaracteristicas(Base.ZEON_MAX);

                            suma = ((int) Math.round(pj.getCaracteristicas(Base.ZEON_REGEN) * multiplicador));

                            valor_actual = valor_actual + suma;

                            if (valor_actual > valor_maximo) {
                                pj.reemplazar_valor_caracteristica(Base.ZEON_ACT, valor_maximo);
                            } else {
                                pj.reemplazar_valor_caracteristica(Base.ZEON_ACT, valor_actual);
                            }

                            valor_actual = pj.getCaracteristicas(Base.ENERGÍA_ACT);
                            valor_maximo = pj.getCaracteristicas(Base.ENERGÍA_MAX);

                            suma = ((int) Math.round(pj.getCaracteristicas(Base.ENERGÍA_REGEN) * multiplicador));

                            valor_actual = valor_actual + suma;

                            if (valor_actual > valor_maximo) {
                                pj.reemplazar_valor_caracteristica(Base.ENERGÍA_ACT, valor_maximo);
                            } else {
                                pj.reemplazar_valor_caracteristica(Base.ENERGÍA_ACT, valor_actual);
                            }

                            valor_actual = pj.getCaracteristicas(Base.ESCUDO_ACT);
                            valor_maximo = pj.getCaracteristicas(Base.ESCUDO_MAX);

                            suma = ((int) Math.round(pj.getCaracteristicas(Base.ESCUDO_REGEN) * multiplicador));

                            valor_actual = valor_actual + suma;

                            if (valor_actual > valor_maximo) {
                                pj.reemplazar_valor_caracteristica(Base.ESCUDO_ACT, valor_maximo);
                            } else {
                                pj.reemplazar_valor_caracteristica(Base.ESCUDO_ACT, valor_actual);
                            }

                            valor_actual = pj.getPenalizadores()[Base.PEN_TEMPORAL];
                            valor_maximo = 0;

                            int recup_negativos = 0;
                            for (int i = 0; i < Base.RECUPERACION_PENALIZADORES_INDICE.length; i++) {
                                if (Base.RECUPERACION_PENALIZADORES_INDICE[i] <= pj.getCaracteristicas(Base.VIDA_REG)) {
                                    recup_negativos = Base.RECUPERACION_PENALIZADORES_VALORES[i];
                                } else {
                                    break;
                                }
                            }

                            suma = ((int) Math.round(recup_negativos * multiplicador));

                            valor_actual = valor_actual + suma;

                            if (valor_actual > valor_maximo) {
                                pj.reemplazar_valor_penalizador(Base.PEN_TEMPORAL, valor_maximo);
                            } else {
                                pj.reemplazar_valor_penalizador(Base.PEN_TEMPORAL, valor_actual);
                            }

                            datos.ArrayPersonajes().get(index_personaje).reemplazar(pj);
                        }

                        ArrayList<Personaje> personajes_temp = new ArrayList<>();

                        for (Personaje Personaje : datos.Array_Personajes_En_Mision()) {
                            personajes_temp.add(Personaje);
                        }

                        CustomAdapter_VistazoPersonajes adapter = new CustomAdapter_VistazoPersonajes(this, personajes_temp, this);

                        ((ListView) findViewById(R.id.List_view_Personajes)).setAdapter(adapter);

                        longSave();

                        break;


                    case R.id.button_pegar:
                        ((EditText) findViewById(R.id.editText_ImportarRápido)).setText(clipboard.getPrimaryClip().getItemAt(0).getText());
                        break;
                    case R.id.button_borrar:
                        ((EditText) findViewById(R.id.editText_ImportarRápido)).setText("");
                        break;
                    case R.id.button_copiar:
                        text = ((EditText) findViewById(R.id.editText_ImportarRápido)).getText().toString();

                        ClipData clip = ClipData.newPlainText("Personaje de Anima", text);
                        clipboard.setPrimaryClip(clip);

                        snackbar = Snackbar.make(v, "Personaje copiado al portapapeles", Snackbar.LENGTH_SHORT);

                        view = snackbar.getView();

                        tv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                        tv.setTextColor(getResources().getColor(R.color.colorWhite));

                        snackbar.show();

                        break;

                    case R.id.button_compartir_pj:

                        text = ((EditText) findViewById(R.id.editText_ImportarRápido)).getText().toString();

                        sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, text);
                        sendIntent.setType("text/plain");
                        startActivity(sendIntent);

                        break;


                    case R.id.button_atacar:
                        CombateTV();
                        break;
                    case R.id.button_pruebas_C:

                        int selected_pj = ((Spinner) findViewById(R.id.spinner_pruebas_A)).getSelectedItemPosition();
                        int selected_tipo_atk = ((Spinner) findViewById(R.id.spinner_pruebas_B)).getSelectedItemPosition();

                        int result = 0;


                        Personaje temp_prueba = datos.Array_Personajes_En_Mision().get(selected_pj);

                        result = temp_prueba.TA(selected_tipo_atk, 1);

                        ((TextView) findViewById(R.id.textView_pruebas_D)).setText(result + " - " + temp_prueba.getCaracteristicas(selected_tipo_atk + Base.TA_FIL));

                        ((ProgressBar) findViewById(R.id.progress_bar)).setProgress(selected_pj);
                        ((ProgressBar) findViewById(R.id.progress_bar)).setMax(((Spinner) findViewById(R.id.spinner_pruebas_A)).getCount());

                        break;

                    case R.id.imageButton_TirarNPC:

                        int secundaria = ((Spinner) findViewById(R.id.spinner_habilidad_secundaria)).getSelectedItemPosition();

                        int tirada = Base.tirada();

                        Integer[] habilidad = {getEdInt(R.id.Habilidad_extra) + tirada};
                        String[] nombre = {"NPC+"};

                        Integer[] desc = {Sistema_NPC.indiceGeneral(secundaria), Sistema_NPC.indiceEspecifico(secundaria), tirada, getEdInt(R.id.Habilidad_extra), getEdInt(R.id.atributo_extra)};
                        Integer[] tiradaarray = {tirada};
                        String[] descrip = {Sistema_NPC.descrpción(desc)};

                        int tipo = getResources().getIntArray(R.array.Tipos_secundarias)[secundaria];

                        CustomAdapter_TiradaSecundaria adapter_secundaria = new CustomAdapter_TiradaSecundaria(mainPrincipal, nombre, habilidad, descrip, tipo, tiradaarray, false);
                        ((ListView) findViewById(R.id.ListView_Tiradas)).setAdapter(adapter_secundaria);
                        break;


                    case R.id.button_borrar_todos_los_pjs_en_mision:

                        datos.Array_Personajes_En_Mision().clear();
                        datos.setModificado_pjs_en_mision(true);
                        actualizarAdapterPersonajesPartida();

                        break;
                    case R.id.button_añadir_personajes_a_mision:

                        añadir_personajes_a_misión();

                        break;
                    case R.id.button_roll_atacante:
                        ((EditText) findViewById(R.id.editText_Tirada_atacante)).setText(String.valueOf(Base.tirada(0, 3, 90)));
                        break;
                    case R.id.button_roll_defensor:
                        ((EditText) findViewById(R.id.editText_Tirada_defensor)).setText(String.valueOf(Base.tirada(0, 3, 90)));
                        break;
                    case R.id.button_aplicar_efectos:

                        setParaGuardar(true);

                        if (ultima_tirada_combate != null) {

                            Personaje atacante = datos.Busqueda_Personaje(ultima_tirada_combate.ID_ATACANTE());
                            Personaje defensor = datos.Busqueda_Personaje(ultima_tirada_combate.ID_DEFENSOR());
                        /*
                            int DAÑOS_PV = 0;
                            int PENALIZADORES = 0;
                            int BONO_CONTRAATAQUE = 0;
                            int ID_ATACANTE = 0;
                            int ID_DEFENSOR = 0;
                            int ESCUDO_RESULTADO = 0;
                            int NIVEL_CRITICO = 0;
                            int Robo_de_vida = 0;

                         */

                            String nombre_atacante = "NPC";
                            String nombre_defensor = "NPC";


                            if (defensor != null) {

                                defensor.caracteristicasSum(Base.VIDA_ACT, -ultima_tirada_combate.DAÑOS_PV());

                                defensor.caracteristicasSum(Base.ESCUDO_ACT, -ultima_tirada_combate.ESCUDO_RESULTADO());

                                defensor.penalizadoresSum(Base.PEN_A_TODA_ACCION, -ultima_tirada_combate.PENALIZADORES());

                                ((EditText) findViewById(R.id.editText_Modificador_atacante_extra)).setText(String.valueOf(ultima_tirada_combate.BONO_CONTRAATAQUE()));

                                nombre_defensor = defensor.getNombre_personaje();

                                ((ProgressBar) findViewById(R.id.ProgressBar_Combate_Jugador_defensor_vida)).setProgress(defensor.getCaracteristicas(Base.VIDA_ACT));
                                ((TextView) findViewById(R.id.textView_Combate_defensor_texto)).setText(defensor.getCaracteristicas(Base.VIDA_ACT) +
                                        "/" + defensor.getCaracteristicas(Base.VIDA_MAX) + " Pv. (" + defensor.Penalizadores(Personaje.PENALIZADORES.TODA_ACCION) + ")");

                            }

                            if (atacante != null) {

                                atacante.caracteristicasSum(Base.VIDA_ACT, ultima_tirada_combate.Robo_de_vida());

                                nombre_atacante = atacante.getNombre_personaje();

                                ((ProgressBar) findViewById(R.id.ProgressBar_Combate_Jugador_atacante_vida)).setProgress(atacante.getCaracteristicas(Base.VIDA_ACT));
                                ((TextView) findViewById(R.id.textView_Combate_atacante_texto)).setText(atacante.getCaracteristicas(Base.VIDA_ACT) +
                                        "/" + atacante.getCaracteristicas(Base.VIDA_MAX) + " Pv. (" + atacante.Penalizadores(Personaje.PENALIZADORES.TODA_ACCION) + ")");

                                if (defensor != null) {
                                    ((ProgressBar) findViewById(R.id.ProgressBar_Combate_Jugador_defensor_vida)).setProgress(defensor.getCaracteristicas(Base.VIDA_ACT));
                                    ((TextView) findViewById(R.id.textView_Combate_defensor_texto)).setText(defensor.getCaracteristicas(Base.VIDA_ACT) +
                                            "/" + defensor.getCaracteristicas(Base.VIDA_MAX) + " Pv. (" + defensor.Penalizadores(Personaje.PENALIZADORES.TODA_ACCION) + ")");

                                }

                            }

                            historial.appendLog(WebBuilder.nombres(nombre_atacante) + " ataca a " + WebBuilder.nombres(nombre_defensor), ultima_tirada_combate.getDatos_combate());

                        }

                        break;
                    case R.id.imageButton_tirada_random:

                        ((TextView) findViewById(R.id.Text_view_tiradas)).setVisibility(View.GONE);
                        ((TextView) findViewById(R.id.Text_view_tiradas)).setText("-");
                        ((TextView) findViewById(R.id.Text_view_tiradas)).setVisibility(View.VISIBLE);

                        int minTmp = textToInt(((EditText) findViewById(R.id.editText_min_random)).getText().toString());
                        int maxTmp = textToInt(((EditText) findViewById(R.id.editText_max_random)).getText().toString());

                        if (maxTmp < minTmp) {
                            int aux = minTmp;
                            minTmp = maxTmp;
                            maxTmp = aux;
                            sos_un_boludo = true;
                        }

                        final int min = minTmp;
                        final int max = maxTmp;

                        final int tirada_min = textToInt(((EditText) findViewById(R.id.editText_mínimo_pase)).getText().toString());

                        final int total = max - min + 1;

                        final ImageButton button = ((ImageButton) findViewById(R.id.imageButton_tirada_random));

                        final int cantidad_dados = textToInt(((EditText) findViewById(R.id.editText_tiradas_random_cantidad)).getText().toString());

                        //animación (?

                        for (int i = 0; i < 15; i++) {
                            boolean flagTmp = false;
                            if (i == 14) {
                                flagTmp = true;
                            }
                            final boolean flag = flagTmp;


                            final long changeTime = Math.round(Math.pow((((i) * 2.5)), 2));
                            button.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    final int tirada_random = (int) ((Math.random() * total) + min);
                                    if (total > 0) {
                                        int comp = ((int) Math.floor((tirada_random * 20) / total));
                                        if (total < 20) {
                                            comp = tirada_random + min - 1;
                                        }
                                        GraficosTiradaRandom(comp, tirada_random, flag, min, max, cantidad_dados, tirada_min);
                                    }
                                }
                            }, changeTime);

                        }

                        break;
                }
            }
        } catch (Throwable t) {
            mostrarThrowable(t);
        }

    }

    private void añadir_personajes_a_misión() {
        for (int i = 0; i < datos.ArrayPersonajes().size(); i++) {
            if (datos.ArrayPersonajes().get(i).getTipoDePersonaje() == 0) {
                Personaje temp = new Personaje().fromJson(datos.ArrayPersonajes().get(i).toJson());
                temp.setToken(datos.ArrayPersonajes().get(i).getToken());
                datos.Array_Personajes_En_Mision().add(temp);
            }
        }

        datos.setModificado_pjs_en_mision(true);

        actualizarAdapterPersonajesPartida();
    }


    public void toCombate(Personaje personaje, final int Arma_index, final int[] parameters) {

        cambio_de_pagina(null, R.id.Part_Combate, true, getResources().getString(R.string.combate));

        final int index_asaltante = datos.Array_Personajes_En_Mision().indexOf(personaje);

        System.out.println("Parametros: " + Arrays.toString(parameters));

        ((TabLayout) findViewById(R.id.tab_layout_combate)).getTabAt(0).select();

        Runnable toCombate = new Runnable() {
            @Override
            public void run() {
                try {

                    ((Spinner) findViewById(R.id.spinner_Combate_Jugador_asaltante)).setSelection(index_asaltante);

                    Runnable setWeapon = new Runnable() {
                        @Override
                        public void run() {
                            ((Spinner) findViewById(R.id.spinner_Combate_Jugador_arma_atk)).setSelection(Arma_index);
                        }
                    };

                    ((Spinner) findViewById(R.id.spinner_Combate_Jugador_arma_atk)).postDelayed(setWeapon, 100);

                    for (int i = 0; i < parameters.length; i++) {
                        try {
                            switch (i) {
                                case 0:
                                    ((EditText) findViewById(R.id.editText_Tirada_atacante)).setText(String.valueOf(parameters[i]));
                                    break;
                                case 1:
                                    ((EditText) findViewById(R.id.editText_Modificador_atacante)).setText(String.valueOf(parameters[i]));
                                    break;
                                case 2:
                                    ((EditText) findViewById(R.id.editText_Modificador_atacante_extra)).setText(String.valueOf(parameters[i]));
                                    break;
                                case 3:
                                    ((EditText) findViewById(R.id.editText_Tirada_defensor)).setText(String.valueOf(parameters[i]));
                                    break;
                                case 4:
                                    ((EditText) findViewById(R.id.editText_Modificador_Defensor)).setText(String.valueOf(parameters[i]));
                                    break;
                                case 5:
                                    ((EditText) findViewById(R.id.editText_Modificador_Defensor_Extra)).setText(String.valueOf(parameters[i]));
                                    break;
                            }

                        } catch (Exception e) {
                            System.out.println("Error en i=" + i + " :" + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        ((Spinner) findViewById(R.id.spinner_Combate_Jugador_asaltante)).postDelayed(toCombate, 100);


    }

    public void cambio_de_pagina(MenuItem item, int id, boolean añadir_al_historial, String text) {
        int i = 0;

        if (datos.Array_Personajes_En_Mision().isEmpty()) {
            añadir_personajes_a_misión();
        }

        controlador_Psiquicos.parar();

        getCurrentFocus().clearFocus();

        historial.setBuscar(false);

        if (añadir_al_historial) {
            historial_de_acciones.añadirHistorial(item, id, false, text);
        }

        int max = datos.Array_Personajes_En_Mision().size();
        try {
            if (datos.isModificado_pjs()) {

                for (i = max - 1; i >= 0; i--) {

                    if (!datos.ArrayPersonajes().contains(datos.Array_Personajes_En_Mision().get(i))) {

                        datos.Eliminar_Personaje_en_misión(i);

                    }

                }

                datos.setModificado_pjs(false);
            }


        } catch (java.lang.IndexOutOfBoundsException e) {
            //System.out.println("Parado en " + i + " de " + max + "\n" + e.toString());
            datos.Array_Personajes_En_Mision().clear();
            datos.setModificado_pjs_en_mision(true);
        }

        if (datos.isModificado_modificadores()) {

            datos.reset_tamaño_modificadores();

            datos.setModificado_modificadores(false);
        }


        try {
            int index = id;

            if (item != null) {
                index = item.getItemId();
                toolbar.setTitle(item.getTitle());
            } else {
                toolbar.setTitle(text);
            }
            System.out.println("Entrando al controlador de ventanas");

            controlador_ventanas.incializar(index);
            controlador_ventanas.ocultar(index);

            View Menu_ppal = findViewById(R.id.Extras_pantalla_principal);

            Hide(Menu_ppal);


/*
            pantalla_tiradas = false;



            View toolkit_seleccionar_ubicaciones_activas = findViewById(R.id.Toolkit_seleccionar_lugares_activos);
            View Extras_importar = findViewById(R.id.Extras_importar);

            View Toolkit_fisico = findViewById(R.id.Toolkit_fisico);
            View Toolkit_secundarias = findViewById(R.id.Toolkit_secundarias);
            View Toolkit_creacion_armas = findViewById(R.id.Toolkit_creacion_armas);
            View Toolkit_creacion_armaduras = findViewById(R.id.Toolkit_creacion_armaduras);
            View Toolkit_seleccion_pjs_activos = findViewById(R.id.Toolkit_seleccionar_pjs_activos);
            View Toolkit_combate = findViewById(R.id.Toolkit_combate);
            View Toolkit_pruebas = findViewById(R.id.Toolkit_Pruebas);
            View Toolkit_vistazo = findViewById(R.id.Toolkit_vistazo);
            View Toolkit_creacion_modificadores = findViewById(R.id.Toolkit_creacion_modificadores);
            View extras_tiradas_layout = findViewById(R.id.extras_tiradas_layout);
            View Extras_logger = findViewById(R.id.Extras_logger);
            View Extras_creacion_lugares = findViewById(R.id.Extras_creacion_lugares);
            View Extras_config = findViewById(R.id.Extras_config);
            View Part_Psiquicos = findViewById(R.id.pantalla_Psiquicos);



            if (index != R.id.Part_Psiquicos) {
                Hide(Part_Psiquicos);
            }
            if (index != R.id.Selección_Personajes) {
                Hide(Extras_importar);
            }
            if (index != R.id.Selección_Personajes) {
                Hide(Toolkit_seleccion_pjs_activos);
            }
            if (index != R.id.Selección_Ubicaciones) {
                Hide(toolkit_seleccionar_ubicaciones_activas);
            }
            if (index != R.id.Creac_Pj & !stub_creacpj) {
                Hide(Toolkit_creacion_pj);
            }

            if (index != R.id.Creac_Armas) {
                Hide(Toolkit_creacion_armas);
            }
            if (index != R.id.Creac_Armaduras) {
                Hide(Toolkit_creacion_armaduras);
            }
            if (index != R.id.Creac_Fis) {
                Hide(Toolkit_fisico);
            }
            if (index != R.id.Part_Secundarias) {
                Hide(Toolkit_secundarias);
            }
            if (index != R.id.Creac_Modificadores) {
                Hide(Toolkit_creacion_modificadores);
            }
            if (index != R.id.Tiradas) {
                Hide(extras_tiradas_layout);
            }
            if (index != R.id.Extras_logger) {
                Hide(Extras_logger);
            }
            if (index != R.id.Creac_lugares_logger) {
                Hide(Extras_creacion_lugares);
            }
            if (index != R.id.Configuración) {
                Hide(Extras_config);
            }
            if (index != R.id.Part_vistazo) {
                Hide(Toolkit_vistazo);
            }
            if (index != R.id.Part_Combate) {
                Hide(Toolkit_combate);
            }

            Hide(Toolkit_pruebas);
*/


            switch (index) {
                case -1:
                    Show(Menu_ppal);
                    break;
/*
                case R.id.Part_Psiquicos:
                    controlador_Psiquicos.init_spinners();
                    Show(Part_Psiquicos);
                    controlador_Psiquicos.iniciar();
                    break;

                case R.id.Importar_Planilla:
                    Show(Extras_importar);
                    break;

                case R.id.Selección_Ubicaciones:

                    cargando = true;

                    actualizarArrayLugares(R.id.spinner_lugares_disponibles, false, false);

                    String[] nombres_lugares = new String[arraylugares.size()];

                    i = 0;
                    for (Lugar lug : arraylugares) {
                        nombres_lugares[i] = lug.getNombre();
                        i = i + 1;
                    }

                    ArrayAdapter<String> adapter_busqueda_lugares = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item, nombres_lugares);

                    ((AutoCompleteTextView) findViewById(R.id.autoCompleteTextView_seleccion_lugares)).setThreshold(1);
                    ((AutoCompleteTextView) findViewById(R.id.autoCompleteTextView_seleccion_lugares)).setAdapter(adapter_busqueda_lugares);

                    ((ListView) findViewById(R.id.List_lugares_activos)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            arraylugares_partida.remove(position);

                            actualizarArrayLugares(R.id.spinner_ubicación, true, true);
                            actualizarAdapterLugaresPartida();
                        }
                    });
                    actualizarAdapterLugaresPartida();

                    Show(toolkit_seleccionar_ubicaciones_activas);

                    break;
                case R.id.Configuración:

                    ((CheckBox) findViewById(R.id.checkBox_toogle_buttons)).setChecked(obtener_estado("Ocultar_botones_de_configuración", false));

                    ((TextView) findViewById(R.id.textView_settings_todo)).setText(getTODO());

                    Show(Extras_config);

                    break;
                case R.id.Creac_Modificadores:
                    Show(Toolkit_creacion_modificadores);

                    actualizarArrayModificadores();
                    break;
                case R.id.Creac_Fis:
                    Show(Toolkit_fisico);

                    actualizarArrayPersonajes(R.id.spinner_pjs_físico, false);

                    ((Spinner) findViewById(R.id.spinner_agi)).setSelection(6);
                    ((Spinner) findViewById(R.id.spinner_fue)).setSelection(6);
                    ((Spinner) findViewById(R.id.spinner_con)).setSelection(6);

                    ((Spinner) findViewById(R.id.spinner_tipo_generacion_caract)).setAdapter(ArrayAdapter.createFromResource(this, R.array.tipos_De_generacion_De_atributos, android.R.layout.simple_spinner_dropdown_item));

                    break;
                case R.id.Creac_Pj:

                    if (stub_creacpj) {

                        //Toolkit_creacion_pj = ((ViewStub)findViewById(R.id.Toolkit_creacion_pj_stub)).inflate();

                        Toolkit_creacion_pj = findViewById(R.id.Toolkit_creacion_pj);

                        AsyncLayoutInflater inflater = new AsyncLayoutInflater(this);

                        inflater.inflate(R.layout.creacion_personaje, (ViewGroup) Toolkit_creacion_pj, new AsyncLayoutInflater.OnInflateFinishedListener() {
                            @Override
                            public void onInflateFinished(@NonNull View view, int resid, @Nullable ViewGroup parent) {



                                controlador_creac_pj.inicializar();

                                TabLayout tl = (TabLayout) findViewById(R.id.tab_layout_pj);

                                tl.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                                    @Override
                                    public void onTabSelected(TabLayout.Tab tab) {

                                        View Include_cr_pj_principal = ((View) findViewById(R.id.Include_cr_pj_principal));
                                        View Include_cr_pj_armaduras = ((View) findViewById(R.id.Include_cr_pj_armaduras));
                                        View Include_cr_pj_armas = ((View) findViewById(R.id.Include_cr_pj_armas));
                                        View Include_cr_pj_penalizadores = ((View) findViewById(R.id.Include_cr_pj_penalizadores));
                                        View Include_cr_pj_puntos = ((View) findViewById(R.id.Include_cr_pj_puntos));
                                        View Include_cr_pj_psiquicos = ((View) findViewById(R.id.Include_cr_pj_psiquicos));


                                        if (Include_cr_pj_principal.getVisibility() == View.VISIBLE) {
                                            Hide(Include_cr_pj_principal);
                                        }

                                        if (Include_cr_pj_armaduras.getVisibility() == View.VISIBLE) {
                                            Hide(Include_cr_pj_armaduras);
                                        }
                                        if (Include_cr_pj_armas.getVisibility() == View.VISIBLE) {
                                            Hide(Include_cr_pj_armas);
                                        }
                                        if (Include_cr_pj_penalizadores.getVisibility() == View.VISIBLE) {
                                            Hide(Include_cr_pj_penalizadores);
                                        }
                                        if (Include_cr_pj_puntos.getVisibility() == View.VISIBLE) {
                                            Hide(Include_cr_pj_puntos);
                                        }

                                        if (Include_cr_pj_psiquicos.getVisibility() == View.VISIBLE) {
                                            Hide(Include_cr_pj_psiquicos);
                                        }


                                        switch (tab.getPosition()) {
                                            case 0://pj
                                                Show(Include_cr_pj_principal);
                                                break;
                                            case 1://puntos
                                                Show(Include_cr_pj_puntos);
                                                break;
                                            case 2://arma
                                                Show(Include_cr_pj_armas);
                                                break;
                                            case 3://armadura
                                                Show(Include_cr_pj_armaduras);
                                                break;
                                            case 4://bonificadores penalizadores y extras
                                                Show(Include_cr_pj_penalizadores);
                                                break;
                                            case 5://psiquicos
                                                Show(Include_cr_pj_psiquicos);
                                                break;

                                        }

                                    }


                                    @Override
                                    public void onTabUnselected(TabLayout.Tab tab) {

                                    }

                                    @Override
                                    public void onTabReselected(TabLayout.Tab tab) {

                                    }
                                });


                                stub_creacpj = false;

                                Show(Toolkit_creacion_pj);



                                actualizarArrayPersonajes(R.id.spinner_seleccionPj, false);

                                actualizarArrayArmaduras(R.id.spinner_Armaduras_Para_Añadir);
                                actualizarArrayArmas(R.id.spinner_Arma_Para_Añadir);
                            }
                        });
                    } else {

                        Show(Toolkit_creacion_pj);

                        actualizarArrayPersonajes(R.id.spinner_seleccionPj, false);

                        actualizarArrayArmaduras(R.id.spinner_Armaduras_Para_Añadir);
                        actualizarArrayArmas(R.id.spinner_Arma_Para_Añadir);
                    }


                    break;

                case R.id.Part_Secundarias:
                    Show(Toolkit_secundarias);


                    ArrayAdapter<String> adapter_busqueda_secundarias = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item, getResources().getStringArray(R.array.Secundarias));

                    ((AutoCompleteTextView) findViewById(R.id.autoCompleteTextView_habilidades_Secundarias)).setThreshold(1);
                    ((AutoCompleteTextView) findViewById(R.id.autoCompleteTextView_habilidades_Secundarias)).setAdapter(adapter_busqueda_secundarias);

                    break;
                case R.id.Part_Combate:
                    Show(Toolkit_combate);


                    ((Spinner) findViewById(R.id.spinner_Combate_distancia)).setAdapter(ArrayAdapter.createFromResource(this, R.array.Distancias, android.R.layout.simple_spinner_dropdown_item));
                    ((Spinner) findViewById(R.id.spinner_Combate_tipo_de_defensa)).setAdapter(ArrayAdapter.createFromResource(this, R.array.Tipo_de_defensa, android.R.layout.simple_spinner_dropdown_item));
                    ((Spinner) findViewById(R.id.spinner_Combate_Tipo_de_ataque)).setAdapter(ArrayAdapter.createFromResource(this, R.array.Tipo_de_Ataque, android.R.layout.simple_spinner_dropdown_item));
                    ((Spinner) findViewById(R.id.spinner_Combate_apuntado)).setAdapter(ArrayAdapter.createFromResource(this, R.array.Apuntado, android.R.layout.simple_spinner_dropdown_item));
                    ((Spinner) findViewById(R.id.spinner_Combate_Jugador_arma_atk)).setAdapter(ArrayAdapter.createFromResource(this, R.array.Sin_personajes, android.R.layout.simple_spinner_dropdown_item));
                    ((Spinner) findViewById(R.id.spinner_Combate_Jugador_arma_def)).setAdapter(ArrayAdapter.createFromResource(this, R.array.Sin_personajes, android.R.layout.simple_spinner_dropdown_item));

                    modificadores_ataque = new boolean[datos.ArrayModificadores().size()];
                    modificadores_defensa = new boolean[datos.ArrayModificadores().size()];

                    Arrays.fill(modificadores_ataque, false);
                    Arrays.fill(modificadores_defensa, false);

                    ListView List_view_modificadores = (ListView) findViewById(R.id.List_view_modificadores);

                    CustomAdapter_Checkbox adapter_ataque = new CustomAdapter_Checkbox(this, datos.ArrayModificadores());

                    List_view_modificadores.setAdapter(adapter_ataque);

                    actualizarArrayPersonajes_EnPartida(R.id.spinner_Combate_Jugador_asaltante, true, 0);
                    actualizarArrayPersonajes_EnPartida(R.id.spinner_Combate_Jugador_defensor, true, 1);

                    break;
                case R.id.Creac_Armaduras:
                    Show(Toolkit_creacion_armaduras);
                    actualizarArrayArmaduras(R.id.spinner_seleccion_armadura);

                    break;
                case R.id.Creac_Armas:
                    Show(Toolkit_creacion_armas);
                    actualizarArrayArmas(R.id.spinner_seleccion_arma);
                    break;
                case R.id.Selección_Personajes:
                    cargando = true;

                    nombres_personajes = new String[datos.ArrayPersonajes().size()];
                    i = 0;
                    for (Personaje pj : datos.ArrayPersonajes()) {
                        nombres_personajes[i] = pj.getNombres();
                        i = i + 1;
                    }

                    ArrayAdapter<String> adapter_busqueda = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item, nombres_personajes);

                    ((AutoCompleteTextView) findViewById(R.id.autoCompleteTextView_seleccion_personajes)).setThreshold(1);
                    ((AutoCompleteTextView) findViewById(R.id.autoCompleteTextView_seleccion_personajes)).setAdapter(adapter_busqueda);

                    Show(Toolkit_seleccion_pjs_activos);

                    actualizarArrayPersonajes(R.id.spinner_pjs_disponibles, false);

                    ((ListView) findViewById(R.id.List_pjs_activos)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            datos.Eliminar_Personaje_en_misión(position);
                            actualizarAdapterPersonajesPartida();
                        }
                    });
                    actualizarAdapterPersonajesPartida();
                    break;
                case R.id.Part_vistazo:

                    Show(Toolkit_vistazo);


                    ArrayList<Personaje> personajes_temp = new ArrayList<>();

                    for (Personaje pj : datos.Array_Personajes_En_Mision()) {
                        personajes_temp.add(pj);
                    }

                    CustomAdapter_VistazoPersonajes adapter = new CustomAdapter_VistazoPersonajes(this, personajes_temp, this);

                    ((Spinner) findViewById(R.id.spinner_cantidad)).setAdapter(ArrayAdapter.createFromResource(this, R.array.Cantidades_50, android.R.layout.simple_spinner_dropdown_item));

                    ((Spinner) findViewById(R.id.spinner_tipo)).setAdapter(ArrayAdapter.createFromResource(this, R.array.tipos_De_tiempo, android.R.layout.simple_spinner_dropdown_item));


                    ((ListView) findViewById(R.id.List_view_Personajes)).setAdapter(adapter);
                    break;

                case R.id.Tiradas:

                    Show(extras_tiradas_layout);


                    pantalla_tiradas = true;

                    ((TextView) findViewById(R.id.textView_tiradas)).postDelayed(actualizador_tiradas, 1000);

                    ((Spinner) findViewById(R.id.spinner_tiradas_random)).setSelection(8);

                    break;
                case R.id.Extras_logger:

                    actualizarArrayPersonajes(R.id.spinner_jugadores_log_primario, true);
                    actualizarArrayPersonajes(R.id.spinner_jugadores_log_secundario, true);

                    Show(Extras_logger);

                    ((WebView) findViewById(R.id.WebView_logger)).loadData("<font size=\"4\">" + historial.getLog(), "text/html; charset=UTF-8", null);

                    ((WebView) findViewById(R.id.WebView_logger_head)).loadData("<font size=\"4\">" + WebBuilder.DETALLE + "</font>", "text/html; charset=UTF-8", null);

                    actualizarArrayLugares(R.id.spinner_ubicación_log, true, false);

                    break;
                case R.id.Creac_lugares_logger:
                    Show(Extras_creacion_lugares);

                    actualizarArrayLugares(R.id.spinner_seleccion_lugar, false, false);

                    actualizarArrayLugares(R.id.spinner_lugar_padre, true, false);

                    break;*/
            }

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        } catch (Throwable t) {
            mostrarThrowable(t);
        }


    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

        if (event.isShiftPressed()) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_C:
                    cambio_de_pagina(null, R.id.Part_Combate, true, getResources().getString(R.string.combate));
                    return true;
                case KeyEvent.KEYCODE_S:
                    cambio_de_pagina(null, R.id.Part_Secundarias, true, getResources().getString(R.string.secundarias));
                    return true;
                case KeyEvent.KEYCODE_E:
                    cambio_de_pagina(null, R.id.Part_vistazo, true, getResources().getString(R.string.estado));
                    return true;
                case KeyEvent.KEYCODE_T:
                    cambio_de_pagina(null, R.id.Tiradas, true, getResources().getString(R.string.tiradas));
                    return true;
                case KeyEvent.KEYCODE_H:
                    cambio_de_pagina(null, R.id.Extras_logger, true, getResources().getString(R.string.historial));
                    return true;
                default:
                    return super.onKeyUp(keyCode, event);
            }
        } else {
            return super.onKeyUp(keyCode, event);
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (tutorial != null) {
            if (tutorial.isOnTutorial()) {
                tutorial.handler_events(parent);
            }
        }


        try {
            try {
                controlador_combate.controladorSpinner(parent.getId(), position);
            } catch (NullPointerException e) {
                Log.e("controlador_combate", "No se ha iniciado aún");
            }

            teclado.controladorSpinner(parent.getId());


            switch (parent.getId()) {

                case R.id.spinner_pjs_físico:
                    try {

                        int temp = datos.ArrayPersonajes().get(position).getCaracteristicas(Base.ATRIBUTO_AGILIDAD);
                        if (temp < 20) {
                            ((Spinner) findViewById(R.id.spinner_agi)).setSelection(temp - 1);
                        }

                        temp = datos.ArrayPersonajes().get(position).getCaracteristicas(Base.ATRIBUTO_FUERZA);
                        if (temp < 20) {
                            ((Spinner) findViewById(R.id.spinner_fue)).setSelection(temp - 1);
                        }

                        temp = datos.ArrayPersonajes().get(position).getCaracteristicas(Base.ATRIBUTO_CONSTITUCIÓN);
                        if (temp < 20) {
                            ((Spinner) findViewById(R.id.spinner_con)).setSelection(temp - 1);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    break;

                case R.id.spinner_Combate_distancia:
                    if (position == 0) {
                        ((ImageView) findViewById(R.id.imageView_atacante_icon)).setImageResource(R.drawable.ic_swordsman);
                    } else {
                        ((ImageView) findViewById(R.id.imageView_atacante_icon)).setImageResource(R.drawable.ic_bowman);
                    }
                    break;

                case R.id.spinner_Combate_tipo_de_defensa:
                    if (position > 4) {
                        ((ImageView) findViewById(R.id.imageView_defensor_icon)).setImageResource(R.drawable.ic_sprint);
                    } else {
                        ((ImageView) findViewById(R.id.imageView_defensor_icon)).setImageResource(R.drawable.ic_shield_cover);
                    }
                    break;

                case R.id.spinner_tipo_busqueda_logger:
                    if (historial_primer_inicio) {
                        historial_primer_inicio = false;
                    } else {
                        switch (position) {
                            case 0://tiradas
                                historial.buscar("", "", "", "tipo: Tirada generada.", "", -1, 0, false);
                                break;
                            case 1://secundarias
                                historial.buscar(" ha lanzado ", "", "", "", "", -1, 0, false);
                                break;
                            case 2://combate
                                historial.buscar(" ataca a ", "", "", "", "", -1, 0, false);
                                break;
                        }
                    }

                    break;
                case R.id.spinner_lugares_disponibles:

                    if (!cargando) {
                        this.arraylugares_partida.add(arraylugares.get(position));

                        actualizarArrayLugares(R.id.spinner_ubicación, true, true);

                        actualizarAdapterLugaresPartida();
                    } else {
                        cargando = false;
                    }

                    break;
                case R.id.spinner_tiradas_random_mínimo:

                    ((EditText) findViewById(R.id.editText_mínimo_pase)).setText(String.valueOf(position + 1));

                    break;

                case R.id.spinner_tiradas_random_cantidad:
                    ((EditText) findViewById(R.id.editText_tiradas_random_cantidad)).setText(String.valueOf(position + 1));
                    break;

                case R.id.spinner_tiradas_random:

                    EditText min = ((EditText) findViewById(R.id.editText_min_random));
                    EditText max = ((EditText) findViewById(R.id.editText_max_random));
                    min.setText("1");

                    ((LinearLayout) findViewById(R.id.Layout_tirada_personalizada)).setVisibility(View.GONE);
                    ((EditText) findViewById(R.id.editText_mínimo_pase)).setVisibility(View.GONE);
                    ((Spinner) findViewById(R.id.spinner_tiradas_random_mínimo)).setVisibility(View.VISIBLE);
                    ((EditText) findViewById(R.id.editText_tiradas_random_cantidad)).setVisibility(View.GONE);
                    ((Spinner) findViewById(R.id.spinner_tiradas_random_cantidad)).setVisibility(View.VISIBLE);

                    switch (position) {

                        case 0://custom
                            ((LinearLayout) findViewById(R.id.Layout_tirada_personalizada)).setVisibility(View.VISIBLE);
                            ((EditText) findViewById(R.id.editText_mínimo_pase)).setVisibility(View.VISIBLE);
                            ((Spinner) findViewById(R.id.spinner_tiradas_random_mínimo)).setVisibility(View.GONE);
                            ((EditText) findViewById(R.id.editText_tiradas_random_cantidad)).setVisibility(View.VISIBLE);
                            ((Spinner) findViewById(R.id.spinner_tiradas_random_cantidad)).setVisibility(View.GONE);
                            break;
                        case 1://d2
                            max.setText("2");

                            actualizar_minimo_tiradas(2);
                            break;
                        case 2://d3
                            max.setText("3");
                            actualizar_minimo_tiradas(3);
                            break;
                        case 3://d4
                            max.setText("4");
                            actualizar_minimo_tiradas(4);
                            break;
                        case 4://d6
                            max.setText("6");
                            actualizar_minimo_tiradas(6);
                            break;
                        case 5://d8
                            max.setText("8");
                            actualizar_minimo_tiradas(8);
                            break;
                        case 6://d10
                            max.setText("10");
                            actualizar_minimo_tiradas(10);
                            break;
                        case 7://d12
                            max.setText("12");
                            actualizar_minimo_tiradas(12);
                            break;
                        case 8://d20
                            max.setText("20");
                            actualizar_minimo_tiradas(20);
                            break;
                        case 9://d100
                            max.setText("100");
                            actualizar_minimo_tiradas(100);
                            break;
                    }

                    // ((LinearLayout) findViewById(R.id.LinearLayout_ultimas_tiradas)).postDelayed(redimensionar_dado, 5);

                    break;
                case R.id.spinner_agi:
                case R.id.spinner_con:
                case R.id.spinner_fue:
                    actualizarFisicos();
                    break;
                case R.id.spinner_pjs_disponibles:
                    if (!cargando) {
                        datos.Añadir_Personaje_en_misión(new Personaje(datos.ArrayPersonajes().get(position), datos.Array_Personajes_En_Mision().size()));

                        actualizarAdapterPersonajesPartida();

                    } else {
                        cargando = false;
                    }
                    break;


                case R.id.spinner_habilidad_secundaria:

                    if (datos.Array_Personajes_En_Mision() != null & datos.Array_Personajes_En_Mision().size() != 0) {

                        TriplaVectores resultados = Base.tiradas(datos.Array_Personajes_En_Mision(), datos.ArrayPersonajes(), position);
                        resultados.ordenar();

                        int tipo = getResources().getIntArray(R.array.Tipos_secundarias)[position];

                        CustomAdapter_TiradaSecundaria adapter = new CustomAdapter_TiradaSecundaria(mainPrincipal, resultados.getNombres(), resultados.getNumeros(), resultados.getResultados(), tipo, resultados.getTiradas(), false);
                        ((ListView) findViewById(R.id.ListView_Tiradas)).setAdapter(adapter);

                        ((TextView) findViewById(R.id.textView_desc_secundaria)).setText("Los personajes realizan tiradas de " + ((Spinner) parent).getSelectedItem().toString());
                    }

                    break;
                case R.id.spinner_Dificultad:

                    if (datos.Array_Personajes_En_Mision() != null & datos.Array_Personajes_En_Mision().size() != 0) {

                        int caracteristica = ((Spinner) findViewById(R.id.spinner_habilidad_secundaria)).getSelectedItemPosition();
                        TriplaVectores resultados = Base.dificultades(datos.Array_Personajes_En_Mision(), datos.ArrayPersonajes(), caracteristica, position, false);
                        resultados.ordenar();

                        int tipo = getResources().getIntArray(R.array.Tipos_secundarias)[caracteristica];

                        CustomAdapter_TiradaSecundaria adapter = new CustomAdapter_TiradaSecundaria(mainPrincipal, resultados.getNombres(), resultados.getNumeros(), resultados.getResultados(), tipo, resultados.getTiradas(), true);
                        ((ListView) findViewById(R.id.ListView_Tiradas)).setAdapter(adapter);

                        ((TextView) findViewById(R.id.textView_desc_secundaria)).setText("Los personajes intentan superar una dificultad de " + ((Spinner) parent).getSelectedItem().toString() + " en " + ((Spinner) findViewById(R.id.spinner_habilidad_secundaria)).getSelectedItem().toString());

                    }

                    break;
            }
        } catch (Throwable t) {
            mostrarThrowable(t);
        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        switch (seekBar.getId()) {
            case R.id.seekBar_edad:
            case R.id.seekBar_rango:
                actualizarFisicos();
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    public void setAllListener(ViewGroup viewGroup) {

        View v;

        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            v = viewGroup.getChildAt(i);

            v.setOnTouchListener(this);

            if (v instanceof ViewGroup) {
                setAllListener((ViewGroup) v);
            }
            if (v instanceof ImageButton) {
                ((ImageButton) v).setOnClickListener(this);
                ((ImageButton) v).setOnLongClickListener(this);
            }
            if (v instanceof Button) {
                ((Button) v).setOnClickListener(this);
                ((Button) v).setOnLongClickListener(this);
            }
            if (v instanceof Spinner) {
                ((Spinner) v).setOnItemSelectedListener(this);
            }
            if (v instanceof SeekBar) {
                ((SeekBar) v).setOnSeekBarChangeListener(this);
            }
            if (v instanceof ToggleButton) {

                if (obtener_estado("Ocultar_botones_de_configuración", false)) {
                    switch ((v).getId()) {
                        case R.id.toggleButton_orden:
                        case R.id.toggleButton_actualizar_ronda:
                        case R.id.toggleButton_mostrar_ultimas_tiradas:
                            ((ToggleButton) v).setVisibility(View.GONE);
                            break;
                    }
                }

                ((ToggleButton) v).setOnCheckedChangeListener(this);

                ((ToggleButton) v).setChecked(obtener_estado(String.valueOf(v.getId()), false));
            }
            if (v instanceof CheckBox) {

                ((CheckBox) v).setOnCheckedChangeListener(this);

                ((CheckBox) v).setChecked(obtener_estado(String.valueOf(v.getId()), false));

            }
            if (v instanceof WebView) {
                ((WebView) v).getSettings().setJavaScriptEnabled(true);
                ((WebView) v).loadData("<font size=\"4\">" + ultima_tirada_combate.getDetalle(), "text/html; charset=UTF-8", null);

                ((WebView) v).addJavascriptInterface(new JavascriptHandler(this), "Android");
            }

            if (v instanceof EditText) {
                v.setOnLongClickListener(this);
            }
        }

    }

    @Override
    protected void onDestroy() {
        Speech.getInstance().shutdown();
        super.onDestroy();
    }

    private long mLastPress = 0;

    @Override
    public void onBackPressed() {
        getCurrentFocus().clearFocus();
        if (tutorial.isOnTutorial()) {
            long currentTime = System.currentTimeMillis();

            int TOAST_DURATION = 5000;
            Toast onBackPressedToast;
            onBackPressedToast = Toast.makeText(this, "Presiona otra vez para omitir el tutorial", Toast.LENGTH_SHORT);

            if (currentTime - mLastPress > TOAST_DURATION) {
                onBackPressedToast.show();
                mLastPress = currentTime;
            } else {
                if (onBackPressedToast != null) {
                    onBackPressedToast.cancel();  //Difference with previous answer. Prevent continuing showing toast after application exit.
                    onBackPressedToast = null;
                }
                tutorial.stop();
            }
        } else {
            if (!teclado.controlTeclado(-1)) {
                historial_de_acciones.undo();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }
*/
        return super.onOptionsItemSelected(item);
    }


    public void toCreacPj(Personaje personaje) {

        cambio_de_pagina(null, R.id.Creac_Pj, true, getResources().getString(R.string.creaci_n_personaje));

        actualizarArrayPersonajes(R.id.spinner_seleccionPj, false);

        actualizarArrayArmaduras(R.id.spinner_Armaduras_Para_Añadir);
        actualizarArrayArmas(R.id.spinner_Arma_Para_Añadir);

        controlador_creac_pj.setPj(personaje);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        teclado.controlTeclado(-1);
        cambio_de_pagina(item, item.getItemId(), true, null);

        return true;
    }

    //Getter and setter


    public Controlador_creac_pj getControlador_creac_pj() {
        return controlador_creac_pj;
    }

    public void setControlador_creac_pj(Controlador_creac_pj controlador_creac_pj) {
        this.controlador_creac_pj = controlador_creac_pj;
    }

    public ArrayList<Arma> getArrayArmas() {
        return arrayArmas;
    }

    public void setArrayArmas(ArrayList<Arma> arrayArmas) {
        this.arrayArmas = arrayArmas;
    }

    public ArrayList<Armadura> getArrayArmaduras() {
        return arrayArmaduras;
    }

    public void setArrayArmaduras(ArrayList<Armadura> arrayArmaduras) {
        this.arrayArmaduras = arrayArmaduras;
    }

    public boolean[] getModificadores_ataque() {
        return modificadores_ataque;
    }

    public void setModificadores_ataque(boolean[] modificadores_ataque) {
        this.modificadores_ataque = modificadores_ataque;


        ((ListView) findViewById(R.id.List_view_modificadores)).invalidateViews();
    }

    public Boolean getModificadores_ataque(int index) {
        return this.modificadores_ataque[index];
    }

    public void setModificador_ataque(int index, boolean valorNuevo) {
        this.modificadores_ataque[index] = valorNuevo;

        int selected_pj = ((Spinner) findViewById(R.id.spinner_Combate_Jugador_asaltante)).getSelectedItemPosition();
        if (datos.Array_Personajes_En_Mision().size() > selected_pj) {
            datos.modif_comb_pj_en_mision(selected_pj, index, valorNuevo);
        }
    }

    public boolean[] getModificadores_defensa() {
        return modificadores_defensa;

    }

    public void setModificadores_defensa(boolean[] modificadores_defensa) {
        this.modificadores_defensa = modificadores_defensa;
        ((ListView) findViewById(R.id.List_view_modificadores)).invalidateViews();
    }

    public void setModificador_defensa(int index, boolean valorNuevo) {
        this.modificadores_defensa[index] = valorNuevo;

        int selected_pj = ((Spinner) findViewById(R.id.spinner_Combate_Jugador_defensor)).getSelectedItemPosition();
        if (datos.Array_Personajes_En_Mision().size() > selected_pj) {
            datos.modif_comb_pj_en_mision(selected_pj, index, valorNuevo);
        }
    }

    public Boolean getModificador_defensa(int index) {
        return this.modificadores_defensa[index];
    }

    private void actualizar_minimo_tiradas(int cantidad) {

        ArrayList<String> textos = new ArrayList<>();

        for (int i = 1; i < cantidad + 1; i++) {
            textos.add(String.valueOf(i));
        }
        String[] posibilidades = new String[textos.size()];

        posibilidades = textos.toArray(posibilidades);

        ArrayAdapter<String> adapter_mínimo = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, posibilidades);
        ((Spinner) findViewById(R.id.spinner_tiradas_random_mínimo)).setAdapter(adapter_mínimo);
        ((Spinner) findViewById(R.id.spinner_tiradas_random_mínimo)).setSelection(1);
    }

    //Utilidades


    public String getEdStr(int id) {


        try {
            String s = ((EditText) findViewById(id)).getText().toString();
            return s;
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return "";
    }

    public int textToInt(String entrada) {
        try {
            int i = Integer.parseInt(entrada);
            return i;
        } catch (NumberFormatException e) {
        }

        return 0;
    }

    public boolean intToBool(int entrada) {
        if (entrada == 1) {
            return true;
        } else {
            return false;
        }

    }

    public int boolToInt(boolean entrada) {
        if (entrada) {
            return 1;
        } else {
            return 0;
        }
    }

    public int getEdInt(int id) {
        try {
            int i = Integer.parseInt(((EditText) findViewById(id)).getText().toString());
            return i;
        } catch (NumberFormatException e) {

        }
        return 0;
    }

    public int[] TextToInt(String[] cadena) {
        int[] valores = new int[cadena.length];
        for (int i = 0; i < cadena.length; i++) {
            valores[i] = Integer.parseInt(cadena[i]);
        }
        return valores;
    }

    private String ARRAY_DIVIDER = "#";

    public String serialize(String content[]) {
        return TextUtils.join(ARRAY_DIVIDER, content);
    }

    public String[] derialize(String content) {
        return content.split(ARRAY_DIVIDER);
    }

    private void GraficosTiradaRandom(int index, int tirada, boolean flag, int min, int max, int cantidad, int mínimo) {


        final ImageButton button = ((ImageButton) findViewById(R.id.imageButton_tirada_random));
        final TextView Text_view_tiradas = ((TextView) findViewById(R.id.Text_view_tiradas));
        final WebView Web_view_tiradas = ((WebView) findViewById(R.id.Web_view_tiradas));

        switch (index) {
            case 1:
                button.setImageDrawable(getResources().getDrawable(R.drawable.dado_1));
                break;
            case 2:
                button.setImageDrawable(getResources().getDrawable(R.drawable.dado_2));
                break;
            case 3:
                button.setImageDrawable(getResources().getDrawable(R.drawable.dado_3));
                break;
            case 4:
                button.setImageDrawable(getResources().getDrawable(R.drawable.dado_4));
                break;
            case 5:
                button.setImageDrawable(getResources().getDrawable(R.drawable.dado_5));
                break;
            case 6:
                button.setImageDrawable(getResources().getDrawable(R.drawable.dado_6));
                break;
            case 7:
                button.setImageDrawable(getResources().getDrawable(R.drawable.dado_7));
                break;
            case 8:
                button.setImageDrawable(getResources().getDrawable(R.drawable.dado_8));
                break;
            case 9:
                button.setImageDrawable(getResources().getDrawable(R.drawable.dado_9));
                break;
            case 10:
                button.setImageDrawable(getResources().getDrawable(R.drawable.dado_10));
                break;
            case 11:
                button.setImageDrawable(getResources().getDrawable(R.drawable.dado_11));
                break;
            case 12:
                button.setImageDrawable(getResources().getDrawable(R.drawable.dado_12));
                break;
            case 13:
                button.setImageDrawable(getResources().getDrawable(R.drawable.dado_13));
                break;
            case 14:
                button.setImageDrawable(getResources().getDrawable(R.drawable.dado_14));
                break;
            case 15:
                button.setImageDrawable(getResources().getDrawable(R.drawable.dado_15));
                break;
            case 16:
                button.setImageDrawable(getResources().getDrawable(R.drawable.dado_16));
                break;
            case 17:
                button.setImageDrawable(getResources().getDrawable(R.drawable.dado_17));
                break;
            case 18:
                button.setImageDrawable(getResources().getDrawable(R.drawable.dado_18));
                break;
            case 19:
                button.setImageDrawable(getResources().getDrawable(R.drawable.dado_19));
                break;
            case 20:
                button.setImageDrawable(getResources().getDrawable(R.drawable.dado_20));
                break;

        }

        if (tirada == min) {
            button.setImageDrawable(getResources().getDrawable(R.drawable.dado_1));

        } else if (tirada == max) {
            button.setImageDrawable(getResources().getDrawable(R.drawable.dado_20));
        }

        String text_web = "";


        String header = "<!DOCTYPE html> <html> <body> <font size=\"14\"> <center> ";
        String bottom = " </center> </font> </body> </html>";

        if (tirada >= mínimo) {
            text_web = text_web + WebBuilder.alto(String.valueOf(tirada));
        } else {
            text_web = text_web + WebBuilder.bajo(String.valueOf(tirada));
        }

        ((TextView) findViewById(R.id.Text_view_tiradas)).setText(String.valueOf(tirada));
        if (flag) {

            int contador = tirada;
            int aciertos = 0;

            String text_nuevo = String.valueOf(tirada);


            if (cantidad > 1) {


                if (tirada >= mínimo) {
                    aciertos = 1;
                }

                int total = max - min;

                for (int i = 0; i < cantidad - 1; i++) {

                    int nuevo_val = (int) ((Math.random() * total) + min);

                    contador = contador + nuevo_val;


                    if (nuevo_val >= mínimo) {
                        text_web = text_web + " " + WebBuilder.alto(String.valueOf(nuevo_val));
                        aciertos = aciertos + 1;
                    } else {
                        text_web = text_web + " " + WebBuilder.bajo(String.valueOf(nuevo_val));
                    }

                    text_nuevo = text_nuevo + " " + String.valueOf(nuevo_val);

                }

                text_nuevo = text_nuevo + " = " + contador + " (" + aciertos + " Éxitos)";
                text_web = text_web + WebBuilder.result(" = " + contador + " (" + aciertos + " Éxitos)");

                historial.appendLog("Tirada (Resultado = " + contador + " y " + aciertos + " Éxitos)", "Tipo: " + "Tirada generada." + "\n Tirada: " + text_web + " (" + min + " a " + max + ", éxito " + mínimo + " y cantidad " + cantidad + ")");

            } else {
                //tirada_actual.setText(text_nuevo);

                historial.appendLog("Tirada (" + text_nuevo + ")", "Tipo: " + "Tirada generada." + "\n Tirada: " + text_web + " (" + min + " a " + max + " y éxito " + mínimo + ")");

            }
            Spanned sp = null;
            if (text_nuevo.length() < 200) {
                sp = Html.fromHtml(text_web);
            } else {
                sp = Html.fromHtml(WebBuilder.result(" Total: " + contador + " (" + aciertos + " Éxitos)") + "<br> vea el historial para más detalles.");
            }
            tiradas.add(new Tirada(text_nuevo, -1, -1, "(" + min + " a " + max + " y éxito " + mínimo + ")"));

            //Spanned sp = Html.fromHtml(text_web);

            Text_view_tiradas.setVisibility(View.GONE);

            Text_view_tiradas.setText(sp);

            Text_view_tiradas.setVisibility(View.VISIBLE);


        }

    }

    public boolean isDbUpgrade() {
        return dbUpgrade;
    }

    public void setDbUpgrade(boolean dbUpgrade) {
        this.dbUpgrade = dbUpgrade;
    }

    public void cargaDB() {
        new CargaDB().execute();
    }

    private class CargaDB extends AsyncTask<Void, Integer, Boolean> {

        protected Boolean doInBackground(Void... voids) {

            arrayArmas = DataBase.GetArmas();
            arrayArmaduras = DataBase.GetArmaduras();
            datos.ArrayPersonajes(DataBase.GetPersonajes());
            datos.ArrayModificadores(DataBase.GetModif());
            arraylugares = DataBase.GetLugares();

            if (arraylugares.size() == 0 & !dbUpgrade) {

                String[] Lugares = getResources().getStringArray(R.array.Lugares);

                for (String text : Lugares) {

                    Lugar temp = new Lugar(text);

                    DataBase.AñadirLugar(temp);
                }
                arraylugares = DataBase.GetLugares();

            }

            //actualizarArrayLugares(R.id.spinner_ubicación, true, true);

            if (datos.ArrayModificadores().size() == 0 & !dbUpgrade) {

                String[] modificadores = getResources().getStringArray(R.array.modificadores);

                for (String text : modificadores) {

                    Modificador temp = new Modificador(text);

                    DataBase.AñadirModificador(temp);
                }

                datos.ArrayModificadores(DataBase.GetModif());
            }


            if (arrayArmas.size() == 0 & !dbUpgrade) {
                String[] armas = getResources().getStringArray(R.array.armas);

                for (String text : armas) {

                    String[] derializado = derialize(text);

                    Arma temp = new Arma();

                    temp.setNombre(derializado[0]);

                    int[] caracteristicas = new int[30];
                    for (int i = 1; i < derializado.length; i++) {
                        caracteristicas[i - 1] = Integer.valueOf(derializado[i]);
                    }

                    temp.setCaracteristicas(caracteristicas);


                    DataBase.AñadirArma(temp);
                }

                arrayArmas = DataBase.GetArmas();

            }

            if (datos.ArrayPersonajes().size() == 0 & !dbUpgrade) {

                //TODO ARREGLAR LOS STRING EN JSON

                /*
                String[] personajes = getResources().getStringArray(R.array.personajes);

                int contador = 0;
                for (String text : personajes) {

                    Personaje temp = new Personaje().fromJson(text);

                    temp.setToken(getToken_raw(contador));

                    DataBase_old

                    DataBase.AñadirPersonaje(temp);

                    contador = contador + 1;
                }


                datos.ArrayPersonajes(DataBase.GetPersonajes());

                for (Personaje personaje : datos.ArrayPersonajes()) {

                    System.out.println("\n\n" + personaje.toString());

                }
*/

            }

            if (arrayArmaduras.size() == 0 & !dbUpgrade) {
                String[] armaduras = getResources().getStringArray(R.array.armaduras);

                for (String text : armaduras) {

                    String[] derializado = derialize(text);

                    Armadura temp = new Armadura();

                    temp.setNombre(derializado[0]);

                    int[] caracteristicas = new int[25];
                    for (int i = 1; i < derializado.length; i++) {
                        caracteristicas[i - 1] = Integer.valueOf(derializado[i]);
                    }

                    temp.setCaracteristicas(caracteristicas);


                    DataBase.AñadirArmadura(temp);
                }

                arrayArmaduras = DataBase.GetArmaduras();

            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean coso) {
            ((TextView) findViewById(R.id.textView_indicador_guardando)).setText(getResources().getString(R.string.guardando));
            if (coso) {

                actualizarArrayLugares(R.id.spinner_ubicación, true, true);

                IndicadorGuardando(false);
                guardando = false;
            } else {
                ((LinearLayout) findViewById(R.id.IndicadorError)).setVisibility(View.VISIBLE);
            }
        }

        protected void onPreExecute() {
            ((TextView) findViewById(R.id.textView_indicador_guardando)).setText("Generando DB ");
            IndicadorGuardando(true);

            guardando = true;
        }

    }

    public void showSnack(Snackbar snackbar) {

        View view = snackbar.getView();

        TextView tv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);

        tv.setTextColor(getResources().getColor(R.color.colorWhite));

        snackbar.show();
    }


    public byte[] getToken_raw(int tipo) {

        Bitmap bitmap = null;

        switch (tipo) {
            case 0:
                bitmap = BitmapFactory.decodeStream(getResources().openRawResource(R.raw.token_soldier));
                break;
            case 1:
                bitmap = BitmapFactory.decodeStream(getResources().openRawResource(R.raw.token_rogue));
                break;
            default:
                bitmap = BitmapFactory.decodeStream(getResources().openRawResource(R.raw.token_soldier));
                break;
        }

        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);

        byte[] devol = stream.toByteArray();

        return devol;
    }

    public class Tirada {
        private String tirada;
        private String descripción;
        private int index;
        private int id;
        ZonedDateTime ldt;

        public Tirada(String tirada, int index, int id, String descripción) {
            this.tirada = tirada;
            this.index = index;//Index -1 tirada generada -2 combate: ataque -3 combate:defensa
            this.id = id;// Id del pj, si no tiene poner -1
            this.descripción = descripción;
            ldt = ZonedDateTime.now(ZoneId.systemDefault());

            String text_index = "";

            switch (index) {
                case -1:
                    text_index = "Tirada generada.";
                    break;
                case -2:
                    text_index = "Combate: ataque.";
                    break;
                case -3:
                    text_index = "Combate: defensa.";
                    break;
            }

            String text_id = "";

            if (id != -1) {
                text_id = " / Nombre del personaje: " + datos.ArrayPersonajes().get(id).getNombres() + ".";
            }
        }


        public String getTirada() {
            return tirada;
        }

        public void setTirada(String tirada) {
            this.tirada = tirada;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public ZonedDateTime getLdt() {
            return ldt;
        }

        public void setLdt(ZonedDateTime ldt) {
            this.ldt = ldt;
        }


        public long getDiferenciaTiempo() {
            ZonedDateTime actualTime = ZonedDateTime.now(ZoneId.systemDefault());

            return actualTime.toEpochSecond() - ldt.toEpochSecond();
        }

    }

    private final Runnable Focus_selection = new Runnable() {
        @Override
        public void run() {
            if (getCurrentFocus() instanceof EditText) {
                EditText editando = (EditText) getCurrentFocus();
                editando.setSelection(editando.getText().toString().length());
            }
        }
    };

    private final Runnable corrector_teclado = new Runnable() {
        @Override
        public void run() {
            if (getCurrentFocus() instanceof EditText) {
                if (((EditText) getCurrentFocus()).getInputType() == 4098) {
                    teclado.controlTeclado(2);
                } else {
                    teclado.controlTeclado(1);
                }
            }
        }
    };
    public final Runnable actualizador_tiradas = new Runnable() {
        @Override
        public void run() {

            while (tiradas.size() > 20) {
                tiradas.remove(0);
            }

            if (pantalla_tiradas) {

                TextView visor = ((TextView) findViewById(R.id.textView_tiradas));

                String text = "";

                for (int i = tiradas.size() - 1; i >= 0; i--) {

                    if (tiradas.get(i).getDiferenciaTiempo() < 60) {
                        text = text + tiradas.get(i).getDiferenciaTiempo() + " s - " + tiradas.get(i).tirada + " " + tiradas.get(i).descripción;
                    } else {
                        int minutos = Math.round(tiradas.get(i).getDiferenciaTiempo() / 60);
                        text = text + minutos + " min - " + tiradas.get(i).tirada + " " + tiradas.get(i).descripción;
                    }

                    text = text + "\n\n";
                }

                visor.setText(text);

                visor.postDelayed(actualizador_tiradas, 1000);
            }
        }
    };

    private final Runnable redimensionar_view = new Runnable() {
        @Override
        public void run() {//TODO
            final WebView Web_view_tiradas = ((WebView) findViewById(R.id.Web_view_tiradas));
            Web_view_tiradas.loadUrl("javascript:Android.scrollHeight(Height())");
        }
    };


    public Datos getDatos() {
        return datos;
    }

    public ArrayList<Lugar> getArraylugares() {
        return arraylugares;
    }

    public BaseDeDatos_v2 getDataBase() {
        return DataBase;
    }

    public Historial getHistorial() {
        return historial;
    }

    public boolean obtener_estado(String clave, boolean estado_base) {

        SharedPreferences estado = this.getSharedPreferences(clave, this.MODE_PRIVATE);

        return estado.getBoolean(clave, estado_base);

    }

    public void Show(final View v) {
        if (v == null) {
            return;
        }
        if (v.getVisibility() == View.VISIBLE) {
            return;
        }

        // Animate the hidden linear layout as visible and set
        // the alpha as 0.0. Otherwise the animation won't be shown

        v.setAlpha(0.0f);
        v
                .animate()
                .setDuration(200)
                .alpha(1.0f)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        v.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        v.animate().setListener(null);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
        ;
    }

    public void Hide(final View v) {
        if (v == null) {
            return;
        }
        if (v.getVisibility() == View.GONE) {
            return;
        }

        v
                .animate()
                .setDuration(200)
                .alpha(0.0f)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        v.animate().setListener(null);
                        v.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
        ;
    }

    public void Refresh(final View v) {
        if (v.getVisibility() == View.GONE) {
            return;
        }

        v
                .animate()
                .setDuration(200)
                .alpha(0.0f)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        v.setVisibility(View.GONE);
                        v.animate().setListener(null);
                        Show(v);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
        ;
    }


    public void guardarEstado(String clave, boolean dato) {

        SharedPreferences estado = this.getSharedPreferences(clave, this.MODE_PRIVATE);

        SharedPreferences.Editor editor = estado.edit();

        editor.putBoolean(clave, dato);

        editor.commit();

    }

    public View getRoot() {
        return this.getRoot();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Here we need to check if the activity that was triggers was the Image Gallery.
        // If it is the requestCode will match the LOAD_IMAGE_RESULTS value.
        // If the resultCode is RESULT_OK and there is some data we know that an image was picked.

        System.out.println("Result code: " + resultCode + " (OK=" + RESULT_OK + ") & Request Code: " + requestCode);

        if (requestCode == SPEECH_REQUEST_CODE && resultCode == RESULT_OK) {

            FragmentManager fm = getFragmentManager();

            Fragment_classes.Dialog_tuto_speech dialog_tuto_speech = new Fragment_classes.Dialog_tuto_speech();

            dialog_tuto_speech.show(fm, "Información: ");


        }

        if (requestCode == LOAD_EXCEL_RESULTS && resultCode == RESULT_OK && data != null) {
            // Let's read picked image data - its URI
            Uri pickedExcel = data.getData();

            try {

                InputStream fileStream = getContentResolver().openInputStream(pickedExcel);

                Personaje importado = importar_csv.readExcelFile(fileStream, this);

                importado.setToken(getToken_raw(-1));

                toCreacPj(importado);

                controlador_creac_pj.onClick(findViewById(R.id.button_GuardarNuevo_PJ));

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }


        if (requestCode == LOAD_IMAGE_RESULTS && resultCode == RESULT_OK && data != null) {
            // Let's read picked image data - its URI
            Uri pickedImage = data.getData();
            // Let's read picked image path using content resolver
            String[] filePath = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(pickedImage, filePath, null, null, null);
            cursor.moveToFirst();
            String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);

            bitmap = DbBitmapUtility.getResizedBitmap(bitmap, 256, 256);

            // Do something with the bitmap

            datos.setImage_temp(DbBitmapUtility.getBytes(bitmap));

            ((ImageButton) findViewById(R.id.imageButton_cambiar_imagen)).setImageBitmap(bitmap);

            cursor.close();
            // At the end remember to close the cursor or you will end with the RuntimeException!

        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                System.out.println("Conseguido! " + resultUri.toString());
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                } catch (java.io.IOException e) {
                    System.out.println("Error! " + e.toString());
                }

                bitmap = CropImage.toOvalBitmap(bitmap);

                InputStream bm = getResources().openRawResource(R.raw.tokenize);
                BufferedInputStream bufferedInputStream = new BufferedInputStream(bm);
                Bitmap bmp = BitmapFactory.decodeStream(bufferedInputStream);
                bmp = Bitmap.createScaledBitmap(bmp, 256, 256, true);

                bitmap = createSingleImageFromMultipleImages(bitmap, bmp);

                ((ImageButton) findViewById(R.id.imageButton_cambiar_imagen)).setImageBitmap(bitmap);

                datos.setImage_temp(DbBitmapUtility.getBytes(bitmap));

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                System.out.println("Error! " + result.getError());
                Exception error = result.getError();
            }
        }


    }

    public Integer SpokenTextToInt(String entrada) {
        try {
            return Integer.parseInt(entrada);
        } catch (NumberFormatException e) {
            Log.i("Error", "la entrada '" + entrada + "' no es un número.");
            return null;
        }
    }


    private Bitmap createSingleImageFromMultipleImages(Bitmap firstImage, Bitmap secondImage) {

        Bitmap result = Bitmap.createBitmap(firstImage.getWidth(), firstImage.getHeight(), firstImage.getConfig());

        Bitmap bmp = Bitmap.createScaledBitmap(secondImage, firstImage.getWidth(), firstImage.getHeight(), true);

        Canvas canvas = new Canvas(result);
        canvas.drawBitmap(firstImage, 0, 0, null);
        canvas.drawBitmap(bmp, 0, 0, null);


        return result;


    }

    public int getCounterIds_temp() {
        counterIds_temp = counterIds_temp + 1;
        return counterIds_temp;
    }

    public void setCounterIds_temp(int counterIds_temp) {
        this.counterIds_temp = counterIds_temp;
    }

    public Controlador_Turno getControlador_turno() {
        return controlador_turno;
    }

    public void setControlador_turno(Controlador_Turno controlador_turno) {
        this.controlador_turno = controlador_turno;
    }

    public Controlador_secundarias getControlador_secundarias() {
        return controlador_secundarias;
    }

    public void setControlador_secundarias(Controlador_secundarias controlador_secundarias) {
        this.controlador_secundarias = controlador_secundarias;
    }

    public Controlador_combate getControlador_combate() {
        return controlador_combate;
    }

    public void setControlador_combate(Controlador_combate controlador_combate) {
        this.controlador_combate = controlador_combate;
    }

    public Controlador_ventanas getControlador_ventanas() {
        return controlador_ventanas;
    }

    public void setControlador_ventanas(Controlador_ventanas controlador_ventanas) {
        this.controlador_ventanas = controlador_ventanas;
    }

    public Controlador_creación_armaduras getControlador_creación_armaduras() {
        return controlador_creación_armaduras;
    }

    public void setControlador_creación_armaduras(Controlador_creación_armaduras controlador_creación_armaduras) {
        this.controlador_creación_armaduras = controlador_creación_armaduras;
    }

    public Controlador_creación_armas getControlador_creación_armas() {
        return controlador_creación_armas;
    }

    public void setControlador_creación_armas(Controlador_creación_armas controlador_creación_armas) {
        this.controlador_creación_armas = controlador_creación_armas;
    }

    public Controlador_Psiquicos getControlador_Psiquicos() {
        return controlador_Psiquicos;
    }

    public void setControlador_Psiquicos(Controlador_Psiquicos controlador_Psiquicos) {
        this.controlador_Psiquicos = controlador_Psiquicos;
    }

    public Controlador_creación_modificadores getControlador_creación_modificadores() {
        return controlador_creación_modificadores;
    }

    public static int getLoadImageResults() {
        return LOAD_IMAGE_RESULTS;
    }

    public static void setLoadImageResults(int loadImageResults) {
        LOAD_IMAGE_RESULTS = loadImageResults;
    }

    public static int getLoadExcelResults() {
        return LOAD_EXCEL_RESULTS;
    }

    public static void setLoadExcelResults(int loadExcelResults) {
        LOAD_EXCEL_RESULTS = loadExcelResults;
    }

    public static int getSpeechRequestCode() {
        return SPEECH_REQUEST_CODE;
    }

    public static void setSpeechRequestCode(int speechRequestCode) {
        SPEECH_REQUEST_CODE = speechRequestCode;
    }

    public static int getReadExternalStorageResult() {
        return READ_EXTERNAL_STORAGE_RESULT;
    }

    public boolean isLoadCsv() {
        return loadCsv;
    }

    public void setLoadCsv(boolean loadCsv) {
        this.loadCsv = loadCsv;
    }

    public int getFocusedEdittext() {
        return focusedEdittext;
    }

    public void setFocusedEdittext(int focusedEdittext) {
        this.focusedEdittext = focusedEdittext;
    }

    public Arma getArma_delete() {
        return arma_delete;
    }

    public void setArma_delete(Arma arma_delete) {
        this.arma_delete = arma_delete;
    }

    public Armadura getArmadura_delete() {
        return armadura_delete;
    }

    public void setArmadura_delete(Armadura armadura_delete) {
        this.armadura_delete = armadura_delete;
    }

    public Personaje getPersonaje_delete() {
        return personaje_delete;
    }

    public void setPersonaje_delete(Personaje personaje_delete) {
        this.personaje_delete = personaje_delete;
    }

    public Modificador getModificador_delete() {
        return modificador_delete;
    }

    public void setModificador_delete(Modificador modificador_delete) {
        this.modificador_delete = modificador_delete;
    }

    public int getPosBoludeo() {
        return posBoludeo;
    }

    public void setPosBoludeo(int posBoludeo) {
        this.posBoludeo = posBoludeo;
    }

    public Tutorial getTutorial() {
        return tutorial;
    }

    public void setTutorial(Tutorial tutorial) {
        this.tutorial = tutorial;
    }

    public boolean isStub_creacpj() {
        return stub_creacpj;
    }

    public void setStub_creacpj(boolean stub_creacpj) {
        this.stub_creacpj = stub_creacpj;
    }

    public View getToolkit_creacion_pj() {
        return Toolkit_creacion_pj;
    }

    public void setToolkit_creacion_pj(View toolkit_creacion_pj) {
        Toolkit_creacion_pj = toolkit_creacion_pj;
    }

    public Teclado getTeclado() {
        return teclado;
    }

    public void setTeclado(Teclado teclado) {
        this.teclado = teclado;
    }

    public int getLastTipoDeTeclado() {
        return lastTipoDeTeclado;
    }

    public void setLastTipoDeTeclado(int lastTipoDeTeclado) {
        this.lastTipoDeTeclado = lastTipoDeTeclado;
    }

    public String[] getNombres_personajes() {
        return nombres_personajes;
    }

    public void setNombres_personajes(String[] nombres_personajes) {
        this.nombres_personajes = nombres_personajes;
    }

    public boolean isCargando() {
        return cargando;
    }

    public void setCargando(boolean cargando) {
        this.cargando = cargando;
    }

    public int[] getIndice_cruzado_modificadores() {
        return indice_cruzado_modificadores;
    }

    public void setIndice_cruzado_modificadores(int[] indice_cruzado_modificadores) {
        this.indice_cruzado_modificadores = indice_cruzado_modificadores;
    }

    public Combate.Resultado getUltima_tirada_combate() {
        return ultima_tirada_combate;
    }

    public void setUltima_tirada_combate(Combate.Resultado ultima_tirada_combate) {
        this.ultima_tirada_combate = ultima_tirada_combate;
    }

    public void setDataBase(BaseDeDatos_v2 dataBase) {
        DataBase = dataBase;
    }

    public TouchHandler getHandler_touch() {
        return handler_touch;
    }

    public void setHandler_touch(TouchHandler handler_touch) {
        this.handler_touch = handler_touch;
    }

    public Historial_de_acciones getHistorial_de_acciones() {
        return historial_de_acciones;
    }

    public void setHistorial_de_acciones(Historial_de_acciones historial_de_acciones) {
        this.historial_de_acciones = historial_de_acciones;
    }

    public void setDatos(Datos datos) {
        this.datos = datos;
    }

    public boolean isGuardando() {
        return guardando;
    }

    public boolean isSos_un_boludo() {
        return sos_un_boludo;
    }

    public void setSos_un_boludo(boolean sos_un_boludo) {
        this.sos_un_boludo = sos_un_boludo;
    }

    public boolean isParaGuardar() {
        return paraGuardar;
    }

    public boolean isPantalla_tiradas() {
        return pantalla_tiradas;
    }

    public void setPantalla_tiradas(boolean pantalla_tiradas) {
        this.pantalla_tiradas = pantalla_tiradas;
    }

    public boolean isHistorial_primer_inicio() {
        return historial_primer_inicio;
    }

    public void setHistorial_primer_inicio(boolean historial_primer_inicio) {
        this.historial_primer_inicio = historial_primer_inicio;
    }

    public void setArraylugares(ArrayList<Lugar> arraylugares) {
        this.arraylugares = arraylugares;
    }

    public ArrayList<Lugar> getArraylugares_partida() {
        return arraylugares_partida;
    }

    public void setArraylugares_partida(ArrayList<Lugar> arraylugares_partida) {
        this.arraylugares_partida = arraylugares_partida;
    }

    public ArrayList<Tirada> getTiradas() {
        return tiradas;
    }

    public void setTiradas(ArrayList<Tirada> tiradas) {
        this.tiradas = tiradas;
    }

    public EditText getEditText_Eff_RegenVida() {
        return EditText_Eff_RegenVida;
    }

    public void setEditText_Eff_RegenVida(EditText editText_Eff_RegenVida) {
        EditText_Eff_RegenVida = editText_Eff_RegenVida;
    }

    public EditText getEditText_RegenVida() {
        return EditText_RegenVida;
    }

    public void setEditText_RegenVida(EditText editText_RegenVida) {
        EditText_RegenVida = editText_RegenVida;
    }

    public int getIndexPersonaje() {
        return indexPersonaje;
    }

    public void setIndexPersonaje(int indexPersonaje) {
        this.indexPersonaje = indexPersonaje;
    }

    public ArrayAdapter<String> getAdapterPjs() {
        return adapterPjs;
    }

    public void setAdapterPjs(ArrayAdapter<String> adapterPjs) {
        this.adapterPjs = adapterPjs;
    }

    public ArrayAdapter<String> getAdapterPjs2() {
        return adapterPjs2;
    }

    public void setAdapterPjs2(ArrayAdapter<String> adapterPjs2) {
        this.adapterPjs2 = adapterPjs2;
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public void setToolbar(Toolbar toolbar) {
        this.toolbar = toolbar;
    }

    public Principal getMainPrincipal() {
        return mainPrincipal;
    }

    public void setMainPrincipal(Principal mainPrincipal) {
        this.mainPrincipal = mainPrincipal;
    }

    public Runnable getIndicador_de_procesado() {
        return indicador_de_procesado;
    }

    public long getmLastSave() {
        return mLastSave;
    }

    public void setmLastSave(long mLastSave) {
        this.mLastSave = mLastSave;
    }

    public long getmLastPress() {
        return mLastPress;
    }

    public void setmLastPress(long mLastPress) {
        this.mLastPress = mLastPress;
    }

    public String getARRAY_DIVIDER() {
        return ARRAY_DIVIDER;
    }

    public void setARRAY_DIVIDER(String ARRAY_DIVIDER) {
        this.ARRAY_DIVIDER = ARRAY_DIVIDER;
    }

    public Runnable getFocus_selection() {
        return Focus_selection;
    }

    public Runnable getCorrector_teclado() {
        return corrector_teclado;
    }

    public Runnable getActualizador_tiradas() {
        return actualizador_tiradas;
    }

    public Runnable getRedimensionar_view() {
        return redimensionar_view;
    }

    public void setControlador_creación_modificadores(Controlador_creación_modificadores controlador_creación_modificadores) {
        this.controlador_creación_modificadores = controlador_creación_modificadores;
    }

    public Controlador_creación_Lugares getControlador_creación_Lugares() {
        return controlador_creación_Lugares;
    }

    public void setControlador_creación_Lugares(Controlador_creación_Lugares controlador_creación_Lugares) {
        this.controlador_creación_Lugares = controlador_creación_Lugares;
    }

    public void setHistorial(Historial historial) {
        this.historial = historial;
    }

    private float resize_porcentual(float total, float actual) {
        return (total / 100 * actual);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case READ_EXTERNAL_STORAGE_RESULT: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (loadCsv) {

                        Intent intent_select_excel = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                        intent_select_excel.addCategory(Intent.CATEGORY_OPENABLE);
                        intent_select_excel.setType("text/comma-separated-values");
                        startActivityForResult(intent_select_excel, LOAD_EXCEL_RESULTS);

                    } else {

                        CropImage.activity().setGuidelines(CropImageView.Guidelines.ON)
                                .setCropShape(CropImageView.CropShape.OVAL)
                                .setAspectRatio(1, 1)
                                .setInitialCropWindowPaddingRatio(0)
                                .setRequestedSize(256, 256, CropImageView.RequestSizeOptions.RESIZE_INSIDE)
                                .start(this);
                    }

                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }


    }
}
