package com.nugganet.arget.mastertoolkit.Controladores;

import android.app.FragmentManager;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.nugganet.arget.mastertoolkit.Base;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Arma;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Personaje;
import com.nugganet.arget.mastertoolkit.Custom_adapters.Fragment_classes;
import com.nugganet.arget.mastertoolkit.Custom_adapters.NDSpinner;
import com.nugganet.arget.mastertoolkit.Principal;
import com.nugganet.arget.mastertoolkit.R;
import com.nugganet.arget.mastertoolkit.WebBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import Utils.importar_csv;

public class Controlador_Psiquicos implements View.OnClickListener, View.OnLongClickListener, NDSpinner.OnItemSelectedListener, SeekBar.OnSeekBarChangeListener {

    static Principal p;
    private List<String> Array_nombres_habilidades;
    private String[] Array_nombres_habilidades_vector;
    private String[] Array_habilidades_vector;
    private String[] Array_descripciones;
    private String[] habilidad_actual;

    private int[] Array_buttons_pot = {R.id.button_desc_proy_1, R.id.button_desc_proy_2, R.id.button_desc_proy_3, R.id.button_desc_proy_4, R.id.button_desc_proy_5, R.id.button_desc_proy_6, R.id.button_desc_proy_7, R.id.button_desc_proy_8, R.id.button_desc_proy_9, R.id.button_desc_proy_10};
    ArrayList<Integer> habilidades_aprendidas_pos = new ArrayList<>();
    private boolean actualizar_por_voz_proy = false;
    private boolean actualizar_por_voz_pot = false;

    public void actualizar_por_voz() {
        actualizar_por_voz_proy = true;
        actualizar_por_voz_pot = true;
    }


    boolean ventana_visible = false;

    public void iniciar() {
        ventana_visible = true;

        ((Button) p.findViewById(R.id.button_calcular_proy_psiquica)).postDelayed(actualizar_potencial, 200);
        ((Button) p.findViewById(R.id.button_calcular_proy_psiquica)).postDelayed(actualizar_proyeccion, 100);
    }

    public void parar() {
        ventana_visible = false;
    }

    public void controlador_speech(String result) {

        boolean sumando = false;
        boolean restando = false;
        Integer base = 0;

        ((EditText) p.findViewById(R.id.autoCompleteTextView_habilides_psiquicas)).setText("");
        AutoCompleteTextView autoc = p.findViewById(R.id.autoCompleteTextView_habilides_psiquicas);

        for (String str : result.split(" ")) {

            Integer ultimaCaptura = p.SpokenTextToInt(str);

            EditText potencial = p.findViewById(R.id.editText_potencial_bono);
            EditText proyeccion = p.findViewById(R.id.editText_proyección_bono);

            if (ultimaCaptura != null) {
                if (sumando) {

                    base = Integer.parseInt(potencial.getText().toString());

                    base = base + ultimaCaptura;

                    potencial.setText(String.valueOf(base));
                    proyeccion.setText(String.valueOf(base));

                } else if (restando) {

                    base = Integer.parseInt(potencial.getText().toString());

                    base = base - ultimaCaptura;

                    potencial.setText(String.valueOf(base));
                    proyeccion.setText(String.valueOf(base));


                } else {
                    potencial.setText(String.valueOf(ultimaCaptura));
                    proyeccion.setText(String.valueOf(ultimaCaptura));

                }
                sumando = false;
                restando = false;
                actualizar_por_voz();
            } else {
                System.out.println("Cadena: " + str);
                if (str.contains("+")) {
                    sumando = true;
                } else if (str.contains("-")) {
                    restando = true;
                    sumando = false;
                } else {

                    autoc.setText(str.substring(0, str.length() - (str.length() / 3)));

                    autoc.showDropDown();

                    autoc.postDelayed(actualizar_dropdown, 50);
                }
            }
        }


    }


    public Controlador_Psiquicos(Principal p) {
        this.p = p;

        Array_nombres_habilidades_vector = p.getResources().getStringArray(R.array.habilidades_psiquicas);
        Array_nombres_habilidades = Arrays.asList(Array_nombres_habilidades_vector);
        Array_habilidades_vector = p.getResources().getStringArray(R.array.datos_habilidades_psiquicas);
        Array_descripciones = p.getResources().getStringArray(R.array.descripciones_psiquicas);

    }

    public void init_spinners() {

        int index_personaje = ((Spinner) p.findViewById(R.id.spinner_seleccion_personaje_psiquico)).getSelectedItemPosition();

        p.actualizarArrayPersonajes_EnPartida(R.id.spinner_seleccion_personaje_psiquico, true, 2);

        ((Spinner) p.findViewById(R.id.spinner_seleccion_hab_psiquica)).setAdapter(ArrayAdapter.createFromResource(p, R.array.habilidades_psiquicas, android.R.layout.simple_spinner_dropdown_item));

        ArrayAdapter<String> adapter_busqueda_secundarias = new ArrayAdapter<String>(p, android.R.layout.select_dialog_item, p.getResources().getStringArray(R.array.habilidades_psiquicas));

        ((AutoCompleteTextView) p.findViewById(R.id.autoCompleteTextView_habilides_psiquicas)).setThreshold(1);
        ((AutoCompleteTextView) p.findViewById(R.id.autoCompleteTextView_habilides_psiquicas)).setAdapter(adapter_busqueda_secundarias);
        ((AutoCompleteTextView) p.findViewById(R.id.autoCompleteTextView_habilides_psiquicas)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ((Spinner) p.findViewById(R.id.spinner_seleccion_hab_psiquica)).setSelection(Array_nombres_habilidades.indexOf(((AutoCompleteTextView) p.findViewById(R.id.autoCompleteTextView_habilides_psiquicas)).getText().toString()));
                //((AutoCompleteTextView) p.findViewById(R.id.autoCompleteTextView_habilides_psiquicas)).setText("");
            }


        });

        if (index_personaje != -1 & index_personaje < p.getDatos().Array_Personajes_En_Mision().size()) {
            ((Spinner) p.findViewById(R.id.spinner_seleccion_personaje_psiquico)).setSelection(index_personaje);
        }


    }

    public void init_listeners() {

        ((SeekBar) p.findViewById(R.id.seekBar_cansancio_psiquico)).setOnSeekBarChangeListener(this);
        ((SeekBar) p.findViewById(R.id.seekBar_cvs_libres)).setOnSeekBarChangeListener(this);

        ((NDSpinner) p.findViewById(R.id.spinner_seleccion_personaje_psiquico)).setOnItemSelectedListener(this);
        ((NDSpinner) p.findViewById(R.id.spinner_seleccion_hab_psiquica)).setOnItemSelectedListener(this);
        ((NDSpinner) p.findViewById(R.id.spinner_seleccion_habilidades_psi_de_pj)).setOnItemSelectedListener(this);

        p.findViewById(R.id.imageButton_tirar_psiquico).setOnClickListener(this);

        ((AutoCompleteTextView) p.findViewById(R.id.autoCompleteTextView_habilides_psiquicas)).setOnLongClickListener(this);

        p.findViewById(R.id.button_calcular_proy_psiquica).setOnClickListener(this);
        p.findViewById(R.id.editText_potencial).setOnLongClickListener(this);
        p.findViewById(R.id.editText_potencial_bono).setOnLongClickListener(this);
        p.findViewById(R.id.editText_proyección).setOnLongClickListener(this);
        p.findViewById(R.id.editText_proyección_bono).setOnLongClickListener(this);

        for (int id : Array_buttons_pot) {
            p.findViewById(id).setOnClickListener(this);
        }

    }


    private static int calcProy(double x) {
        double y;

        if (x > 282) {
            return 100000;
        }
        if (x < 20) {
            return -1;
        }
        x = x + 1;
        y = (-4.565277691099153 + x * (0.2957789247393440 +
                x * (-0.003690264486580407 + x * 1.585032488531660E-05))) /
                (1.0 + x * (-0.01646939971404080 + x * (9.026579338583001E-05 +
                        x * -1.572393421905861E-07)));


        return redondeo((int) y);
    }

    private static int redondeo(int x) {

        switch (String.valueOf(x).length()) {
            default:
            case 1:
                return x;
            case 2:
                return ((x / 5) * 5);
            case 3:
                return ((x / 10) * 10);
            case 4:
                return ((x / 100) * 100);
            case 5:
                return ((x / 1000) * 1000);
        }


    }


    @Override
    public void onClick(View v) {
        int button = 99;
        switch (v.getId()) {
            case R.id.button_calcular_proy_psiquica:

                ((EditText) p.findViewById(R.id.editText_proyección_bono)).setText(String.valueOf(Base.tirada()));

                int proyeccion_total = p.getEdInt(R.id.editText_proyección) + p.getEdInt(R.id.editText_proyección_bono);

                int distancia = calcProy(proyeccion_total);

                String text;
                if (distancia != -1) {
                    text = ("Puede alcanzar " + distancia + " metros (" + proyeccion_total + ").");
                } else {
                    text = ("Desastroso (" + proyeccion_total + ").");
                }

                ((Button) v).setText(text);

                p.getCurrentFocus().clearFocus();


                break;
            case R.id.imageButton_tirar_psiquico:

                p.getCurrentFocus().clearFocus();

                ((EditText) p.findViewById(R.id.editText_potencial_bono)).setText(String.valueOf(Base.tirada()));
                ((EditText) p.findViewById(R.id.editText_proyección_bono)).setText(String.valueOf(Base.tirada()));

                act_proy();
                act_pot();


                break;
            case R.id.button_desc_proy_1:
                button = min(0, 0);
            case R.id.button_desc_proy_2:
                button = min(button, 1);
            case R.id.button_desc_proy_3:
                button = min(button, 2);
            case R.id.button_desc_proy_4:
                button = min(button, 3);
            case R.id.button_desc_proy_5:
                button = min(button, 4);
            case R.id.button_desc_proy_6:
                button = min(button, 5);
            case R.id.button_desc_proy_7:
                button = min(button, 6);
            case R.id.button_desc_proy_8:
                button = min(button, 7);
            case R.id.button_desc_proy_9:
                button = min(button, 8);
            case R.id.button_desc_proy_10:
                button = min(button, 9);

                Habilidad_psiquica hab;
                int pos_hab_selected = ((Spinner) p.findViewById(R.id.spinner_seleccion_hab_psiquica)).getSelectedItemPosition();
                System.out.println("Button: " + button + " - Habilidad: " + pos_hab_selected);

                if (((Button) v).getText().toString().toLowerCase().contains("fatiga")) {

                    String fatiga = ((Button) v).getText().toString().toLowerCase().split("fatiga")[1];

                    int cantidad = importar_csv.separate_number_start(fatiga).getValor();

                    hab = new Habilidad_psiquica(0, new int[]{cantidad});

                    hab.descripción();

                } else {


                    if (pos_hab_selected == 20) {
                        hab = new Habilidad_psiquica(Habilidad_psiquica.PROYECTIL, new int[]{button, 0, 0, 0, 10, 20, 30, 40, 50, 60}, new String[]{"El psiquico atacará con un bono de %d a su proyección psiquica."});
                        hab.descripción();
                    }

                    if (pos_hab_selected == 17) {
                        hab = new Habilidad_psiquica(Habilidad_psiquica.ESCUDO, new int[]{button, 0, 300, 500, 700, 1000, 1500, 2000, 3000, 5000}, new String[]{"El psiquico ganará un escudo de %d puntos."});
                        hab.descripción();
                    }


                }


                break;
        }
    }

    private int min(int a, int b) {
        if (a > b) {
            return b;
        } else {
            return a;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        switch (v.getId()) {
            case R.id.editText_potencial:
            case R.id.editText_potencial_bono:
            case R.id.editText_proyección:
            case R.id.editText_proyección_bono:
            case R.id.autoCompleteTextView_habilides_psiquicas:
                ((EditText) v).setText("");

                Snackbar
                        .make(v, "¿Desea borrar los otros campos?", Snackbar.LENGTH_LONG)
                        .setAction("BORRAR TODOS", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                ((EditText) p.findViewById(R.id.editText_potencial)).setText("");
                                ((EditText) p.findViewById(R.id.editText_potencial_bono)).setText("");
                                ((EditText) p.findViewById(R.id.editText_proyección)).setText("");
                                ((EditText) p.findViewById(R.id.editText_proyección_bono)).setText("");
                                ((AutoCompleteTextView) p.findViewById(R.id.autoCompleteTextView_habilides_psiquicas)).setText("");

                            }

                        }).show();
                break;
        }

        return false;
    }

    private void Tirada_potencial(String[] habilidad) {

        int altura_texto = p.findViewById(R.id.button_desc_proy_1).getHeight();


        int base_potencial = p.getEdInt(R.id.editText_potencial_bono) + p.getEdInt(R.id.editText_potencial);
        int resultado = base_potencial;

        int difSup = Base.ControlHabilidad(resultado);


        ((TextView) p.findViewById(R.id.textView_resultado_tirada_psiquico)).setText("Con una tirada total de " + resultado + " supera una dificultad " + Base.DificultadStr(resultado));
        Spanned sp;

        for (int i = 4; i < 14; i++) {

            String dificultad = Base.DificultadStr(Base.dificultades[i - 4]) + ": " + habilidad[i] + "<br> (Requiere un " + (Base.dificultades[i - 4] - base_potencial) + ")";

            if (i == difSup + 3) {
                sp = Html.fromHtml(WebBuilder.result(dificultad));
                ((Button) p.findViewById(Array_buttons_pot[i - 4])).setTextColor(p.getResources().getColor(R.color.colorNames));
            } else {
                if (habilidad[i].contains("Fatiga ")) {
                    sp = Html.fromHtml(WebBuilder.bajo(dificultad));
                    ((Button) p.findViewById(Array_buttons_pot[i - 4])).setTextColor(p.getResources().getColor(R.color.colorRed));
                } else {
                    sp = Html.fromHtml(WebBuilder.alto(dificultad));
                    ((Button) p.findViewById(Array_buttons_pot[i - 4])).setTextColor(p.getResources().getColor(R.color.colorPrimary));
                }
            }

            ((Button) p.findViewById(Array_buttons_pot[i - 4])).setText(sp);

        }

        ((ScrollView) p.findViewById(R.id.ScrollView_desc_resultado_psiquicos)).scrollTo(0, altura_texto * ((difSup - 1)));


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        int pos_pj_selected = ((Spinner) p.findViewById(R.id.spinner_seleccion_personaje_psiquico)).getSelectedItemPosition();

        switch (parent.getId()) {
            case R.id.spinner_seleccion_habilidades_psi_de_pj:


                if (!p.getDatos().Array_Personajes_En_Mision().isEmpty() & pos_pj_selected < p.getDatos().Array_Personajes_En_Mision().size()) {
                    ((Spinner) p.findViewById(R.id.spinner_seleccion_hab_psiquica)).setSelection(habilidades_aprendidas_pos.get(position));
                } else {
                    ((Spinner) p.findViewById(R.id.spinner_seleccion_hab_psiquica)).setSelection(position);
                }


                break;
            case R.id.spinner_seleccion_personaje_psiquico:


                if (!p.getDatos().Array_Personajes_En_Mision().isEmpty() & position < p.getDatos().Array_Personajes_En_Mision().size()) {

                    Personaje temp = p.getDatos().Array_Personajes_En_Mision().get(position);

                    int PotencialTotal = temp.getHabilidades_psiquicas()[0] + temp.getHabilidades_psiquicas()[((Spinner) p.findViewById(R.id.spinner_seleccion_hab_psiquica)).getSelectedItemPosition() + 5];

                    ((EditText) p.findViewById(R.id.editText_potencial)).setText(String.valueOf(PotencialTotal));
                    ((EditText) p.findViewById(R.id.editText_proyección)).setText(String.valueOf(temp.getCaracteristicas(Base.HABILIDAD_PROYECCION_PSIQUICA)));

                    ((EditText) p.findViewById(R.id.editText_potencial_bono)).setText("");
                    ((EditText) p.findViewById(R.id.editText_proyección_bono)).setText("");

                    ((SeekBar) p.findViewById(R.id.seekBar_cansancio_psiquico)).setMax(temp.getCaracteristicas(Base.ENERGÍA_MAX));
                    ((SeekBar) p.findViewById(R.id.seekBar_cansancio_psiquico)).setProgress(temp.getCaracteristicas(Base.ENERGÍA_ACT));

                    ((SeekBar) p.findViewById(R.id.seekBar_cvs_libres)).setMax(temp.getHabilidades_psiquicas()[Personaje.PSIQUICOS.CVS_MAX]);
                    ((SeekBar) p.findViewById(R.id.seekBar_cvs_libres)).setProgress(temp.getHabilidades_psiquicas()[Personaje.PSIQUICOS.CVS_ACT]);

                    ((TextView) p.findViewById(R.id.textView_energía_pj_psiquicos)).setText(temp.getCaracteristicas(Base.ENERGÍA_ACT) + "/" + temp.getCaracteristicas(Base.ENERGÍA_MAX));

                    ((TextView) p.findViewById(R.id.textView_cvs_libres)).setText(temp.getHabilidades_psiquicas()[Personaje.PSIQUICOS.CVS_ACT] + "/" + temp.getHabilidades_psiquicas()[Personaje.PSIQUICOS.CVS_MAX]);

                    String[] hab_psiquicas = p.getResources().getStringArray(R.array.habilidades_psiquicas);
                    ArrayList<String> habilidades_aprendidas = new ArrayList<>();
                    habilidades_aprendidas_pos = new ArrayList<>();

                    for (int i = 0; i < hab_psiquicas.length; i++) {

                        if (temp.getHabilidades_psiquicas()[i + 4] != 0) {
                            habilidades_aprendidas.add(hab_psiquicas[i]);
                            habilidades_aprendidas_pos.add(i);
                        }
                    }

                    ((Spinner) p.findViewById(R.id.spinner_seleccion_habilidades_psi_de_pj)).setAdapter(new ArrayAdapter<>(p, android.R.layout.simple_spinner_dropdown_item, habilidades_aprendidas));


                } else {

                    ((Spinner) p.findViewById(R.id.spinner_seleccion_habilidades_psi_de_pj)).setAdapter(ArrayAdapter.createFromResource(p, R.array.habilidades_psiquicas, android.R.layout.simple_spinner_dropdown_item));


                    ((EditText) p.findViewById(R.id.editText_potencial)).setText("50");
                    ((EditText) p.findViewById(R.id.editText_proyección)).setText("50");

                    ((EditText) p.findViewById(R.id.editText_potencial_bono)).setText("");
                    ((EditText) p.findViewById(R.id.editText_proyección_bono)).setText("");

                    ((SeekBar) p.findViewById(R.id.seekBar_cansancio_psiquico)).setMax(8);
                    ((SeekBar) p.findViewById(R.id.seekBar_cansancio_psiquico)).setProgress(8);

                    ((SeekBar) p.findViewById(R.id.seekBar_cvs_libres)).setMax(8);
                    ((SeekBar) p.findViewById(R.id.seekBar_cvs_libres)).setProgress(8);
                }


                break;
            case R.id.spinner_seleccion_hab_psiquica:

                if (!p.getDatos().Array_Personajes_En_Mision().isEmpty() & pos_pj_selected < p.getDatos().Array_Personajes_En_Mision().size()) {
                    Personaje temp = p.getDatos().Array_Personajes_En_Mision().get(((Spinner) p.findViewById(R.id.spinner_seleccion_personaje_psiquico)).getSelectedItemPosition());

                    int PotencialTotal = temp.getHabilidades_psiquicas()[0] + temp.getHabilidades_psiquicas()[((Spinner) p.findViewById(R.id.spinner_seleccion_hab_psiquica)).getSelectedItemPosition() + 4];

                    ((EditText) p.findViewById(R.id.editText_potencial)).setText(String.valueOf(PotencialTotal));

                }

                String titulo = parent.getSelectedItem().toString();

                ((TextView) p.findViewById(R.id.textView_desc_poder_psiquicos)).setText(titulo + "\n\n" + Array_descripciones[position]);

                String[] habilidad = p.derialize(Array_habilidades_vector[position]);

                String desc = habilidad[0] + "(" + habilidad[1] + ") - " + habilidad[3];

                if (habilidad[2].toLowerCase().contains("s")) {
                    desc = desc + " - Mantenida";
                }

                ((TextView) p.findViewById(R.id.textView_Disciplina)).setText(desc);

                Tirada_potencial(habilidad);

                break;

        }


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        switch (seekBar.getId()) {
            case R.id.seekBar_cansancio_psiquico:
                String[] old_text_energ = ((TextView) p.findViewById(R.id.textView_energía_pj_psiquicos)).getText().toString().split("/");

                ((TextView) p.findViewById(R.id.textView_energía_pj_psiquicos)).setText(progress + "/" + old_text_energ[1]);

                if (fromUser) {
                    int position = ((Spinner) p.findViewById(R.id.spinner_seleccion_personaje_psiquico)).getSelectedItemPosition();

                    if (!p.getDatos().Array_Personajes_En_Mision().isEmpty() & position < p.getDatos().Array_Personajes_En_Mision().size()) {

                        Personaje temp = p.getDatos().Array_Personajes_En_Mision().get(position);

                        temp.reemplazar_valor_caracteristica(Base.ENERGÍA_ACT, progress);

                        p.longSave();

                    }
                }
                break;
            case R.id.seekBar_cvs_libres:

                String[] old_text_cvs = ((TextView) p.findViewById(R.id.textView_cvs_libres)).getText().toString().split("/");

                ((TextView) p.findViewById(R.id.textView_cvs_libres)).setText(progress + "/" + old_text_cvs[1]);

                if (fromUser) {
                    int position = ((Spinner) p.findViewById(R.id.spinner_seleccion_personaje_psiquico)).getSelectedItemPosition();

                    if (!p.getDatos().Array_Personajes_En_Mision().isEmpty() & position < p.getDatos().Array_Personajes_En_Mision().size()) {

                        Personaje temp = p.getDatos().Array_Personajes_En_Mision().get(position);

                        temp.getHabilidades_psiquicas()[2] = progress;

                        p.longSave();

                    }
                }
                break;
        }


    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    private final Runnable actualizar_potencial = new Runnable() {
        @Override
        public void run() {

            if (ventana_visible) {
                if ((p.getCurrentFocus().getId() == R.id.editText_potencial) | (p.getCurrentFocus().getId() == R.id.editText_potencial_bono) | actualizar_por_voz_pot) {

                    act_pot();

                    actualizar_por_voz_pot = false;

                }
                ((Button) p.findViewById(R.id.button_calcular_proy_psiquica)).postDelayed(actualizar_potencial, 200);
            }
        }
    };

    private final void act_pot() {
        String[] habilidad = p.derialize(Array_habilidades_vector[((Spinner) p.findViewById(R.id.spinner_seleccion_hab_psiquica)).getSelectedItemPosition()]);

        Tirada_potencial(habilidad);
    }

    private final Runnable actualizar_proyeccion = new Runnable() {
        @Override
        public void run() {
            if (ventana_visible) {
                try {
                    if ((p.getCurrentFocus().getId() == R.id.editText_proyección) | (p.getCurrentFocus().getId() == R.id.editText_proyección_bono) | actualizar_por_voz_proy) {

                        act_proy();

                        actualizar_por_voz_proy = false;

                    }
                } catch (java.lang.NullPointerException e) {
                    e.printStackTrace();
                }

                ((Button) p.findViewById(R.id.button_calcular_proy_psiquica)).postDelayed(actualizar_proyeccion, 100);
            }

        }
    };

    private final void act_proy() {
        int proyeccion_total = p.getEdInt(R.id.editText_proyección) + p.getEdInt(R.id.editText_proyección_bono);

        int distancia = calcProy(proyeccion_total);

        String text;
        if (distancia != -1) {
            text = ("Puede alcanzar " + distancia + " metros (" + proyeccion_total + ").");
        } else {
            text = ("Desastroso (" + proyeccion_total + ").");
        }


        ((Button) p.findViewById(R.id.button_calcular_proy_psiquica)).setText(text);
    }

    private final Runnable actualizar_dropdown = new Runnable() {
        @Override
        public void run() {
            AutoCompleteTextView autoc = p.findViewById(R.id.autoCompleteTextView_habilides_psiquicas);

            System.out.println("autoc.getAdapter().getCount(): " + autoc.getAdapter().getCount());
            if (autoc.getAdapter().getCount() == 1) {

                autoc.setText(autoc.getAdapter().getItem(0).toString());

                ((Spinner) p.findViewById(R.id.spinner_seleccion_hab_psiquica)).setSelection(Array_nombres_habilidades.indexOf(autoc.getAdapter().getItem(0).toString()));

                autoc.dismissDropDown();

            }
        }
    };


    private class Habilidad_psiquica implements View.OnClickListener {
        @Expose
        private int t = 0;
        @Expose
        private int[] e = new int[5];
        @Expose
        private String[] d = new String[5];

        public static final int FATIGA = 0;
        public static final int PROYECTIL = 1;
        public static final int ESCUDO = 2;
        public static final int AÑADIR_CARACT_TEMP = 3;
        public static final int TIRADA_CARACT = 4;
        public static final int DAÑO = 5;


        public Habilidad_psiquica(int tipo, int[] efecto) {
            this.t = tipo;

            this.e = efecto;
        }

        public Habilidad_psiquica(int t, int[] e, String[] d) {
            this.t = t;
            this.e = e;
            this.d = d;
        }

        public Habilidad_psiquica fromJson(String json) {
            return new Gson().fromJson(json, this.getClass());
        }

        public String toJson() {
            String pjJson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(this, this.getClass());
            return pjJson;
        }

        public void descripción() {

            int pos_pj_selected = ((Spinner) p.findViewById(R.id.spinner_seleccion_personaje_psiquico)).getSelectedItemPosition();

            Personaje temp = null;
            String text_desc = "";

            if (!p.getDatos().Array_Personajes_En_Mision().isEmpty() & pos_pj_selected < p.getDatos().Array_Personajes_En_Mision().size()) {
                temp = p.getDatos().Array_Personajes_En_Mision().get(pos_pj_selected);
            }

            FragmentManager fm = p.getFragmentManager();
            final Fragment_classes.Dialog_confirmación dialogFragment = new Fragment_classes.Dialog_confirmación();

            final Habilidad_psiquica habilidad = this;

            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int dev = 0;
                    try {
                        dev = dialogFragment.objSpinner();
                    } catch (Exception e) {
                    }
                    habilidad.procesar(dev);
                    dialogFragment.dismiss();
                }
            };

            dialogFragment.setOnClickListener(clickListener);

            switch (t) {
                case FATIGA:

                    dialogFragment.setFragment_info_cuerpo("El personaje perderá " + e[0] + " Cvs libres, y en caso de no tener disponibles, se fatigará la cantidad restante.");
                    dialogFragment.setFragment_info_titulo("Aplicar Fatiga");

                    if (temp != null) {
                        if (temp.getHabilidades_psiquicas()[Personaje.PSIQUICOS.CVS_ACT] >= e[0]) {

                            text_desc = "El personaje pierde " + e[0] + " Cvs libres.";

                        } else {

                            e[0] = e[0] - temp.getHabilidades_psiquicas()[Personaje.PSIQUICOS.CVS_ACT];

                            text_desc = "El personaje pierde " + temp.getHabilidades_psiquicas()[Personaje.PSIQUICOS.CVS_ACT] + " Cvs libres y además pierde " + e[0] + " puntos de cansancio.";

                            int Energía_nueva = temp.getCaracteristicas(Base.ENERGÍA_ACT) - e[0];

                            if (Energía_nueva <= 0) {
                                int rp = ((-Energía_nueva * 10) + 100);
                                text_desc = text_desc + "\n Además, debe superar una RP contra " + rp + " o quedará en coma una cantidad de días equivalente a su nivel de fracaso. Si falla por más de 80 puntos, Si falla el control por más de 80 puntos, su mente queda automáticamente destrozada, muriendo irremediablemente (Pag 211 CE)";
                            }

                        }
                    }

                    if (!text_desc.isEmpty()) {
                        dialogFragment.setFragment_info_cuerpo(text_desc);
                    }

                    dialogFragment.show(fm, "Información: ");


                    break;
                case PROYECTIL:

                    String text = "Se reemplazará la sexta arma del psiquico con una llamada " + ((Spinner) p.findViewById(R.id.spinner_seleccion_hab_psiquica)).getSelectedItem().toString() + ", esta usará su proyección como habilidad de ataque y el daño base que selecciones:";

                    text = text + "\n\n" + String.format(d[0], e[e[0]]);

                    dialogFragment.setFragment_info_cuerpo(text);

                    dialogFragment.set_spinner(new ArrayAdapter<>(p, android.R.layout.simple_list_item_1, p.getResources().getStringArray(R.array.Daños_base_Armas)));

                    dialogFragment.setFragment_info_titulo("Aplicar efecto de " + ((Spinner) p.findViewById(R.id.spinner_seleccion_hab_psiquica)).getSelectedItem().toString());

                    dialogFragment.show(fm, "Información: ");

                    break;
                case ESCUDO:

                    dialogFragment.setFragment_info_cuerpo(String.format(d[0], e[e[0]]));
                    dialogFragment.setFragment_info_titulo("Aplicar efecto de " + ((Spinner) p.findViewById(R.id.spinner_seleccion_hab_psiquica)).getSelectedItem().toString());

                    dialogFragment.show(fm, "Información: ");


                    break;
                case AÑADIR_CARACT_TEMP:

                    break;
                case TIRADA_CARACT:

                    break;
                case DAÑO:

                    break;

            }

            ((EditText) p.findViewById(R.id.editText_plaintext_herr)).setText(this.toJson());

        }

        public void procesar(int dev) {

            int pos_pj_selected = ((Spinner) p.findViewById(R.id.spinner_seleccion_personaje_psiquico)).getSelectedItemPosition();
            int pos_hab_selected = ((Spinner) p.findViewById(R.id.spinner_seleccion_hab_psiquica)).getSelectedItemPosition();

            Personaje temp = null;

            if (!p.getDatos().Array_Personajes_En_Mision().isEmpty() & pos_pj_selected < p.getDatos().Array_Personajes_En_Mision().size()) {
                temp = p.getDatos().Array_Personajes_En_Mision().get(pos_pj_selected);
            }

            switch (t) {
                case FATIGA:

                    if (temp != null) {


                        if (temp.getHabilidades_psiquicas()[Personaje.PSIQUICOS.CVS_ACT] >= e[0]) {

                            temp.getHabilidades_psiquicas()[Personaje.PSIQUICOS.CVS_ACT] = temp.getHabilidades_psiquicas()[Personaje.PSIQUICOS.CVS_ACT] - e[0];

                        } else {

                            e[0] = e[0] - temp.getHabilidades_psiquicas()[Personaje.PSIQUICOS.CVS_ACT];

                            temp.getHabilidades_psiquicas()[Personaje.PSIQUICOS.CVS_ACT] = 0;

                            int Energía_nueva = temp.getCaracteristicas(Base.ENERGÍA_ACT) - e[0];

                            temp.reemplazar_valor_caracteristica(Base.ENERGÍA_ACT, Energía_nueva);

                        }

                    }

                    ((Spinner) p.findViewById(R.id.spinner_seleccion_personaje_psiquico)).setSelection(pos_pj_selected);

                    ((Spinner) p.findViewById(R.id.spinner_seleccion_hab_psiquica)).setSelection(pos_hab_selected);

                    break;
                case PROYECTIL:

                    if (temp != null) {

                        int Base = p.getResources().getIntArray(R.array.Daños_base_Armas_int)[dev];

                        String nombre = ((Spinner) p.findViewById(R.id.spinner_seleccion_hab_psiquica)).getSelectedItem().toString();

                        Arma temp_arma = new Arma();

                        temp_arma.setNombre(nombre);
                        temp_arma.setCaracteristica(Arma.DAÑO_BASE, Base);
                        temp_arma.setCaracteristica(Arma.HABILIDAD_USADA, Arma.HABILIDAD_P_PSIQUICA);

                        temp.setArma(temp_arma, 5);

                        p.toCombate(temp, 5, new int[]{0, 0, e[e[0]]});

                    }

                    break;
                case ESCUDO:

                    if (temp != null) {

                        temp.reemplazar_valor_caracteristica(Base.ESCUDO_ACT, e[e[0]]);
                        temp.reemplazar_valor_caracteristica(Base.ESCUDO_MAX, e[e[0]]);
                        temp.reemplazar_valor_caracteristica(Base.ESCUDO_REGEN, 0);

                    }

                    break;
                case AÑADIR_CARACT_TEMP:

                    break;
                case TIRADA_CARACT:

                    break;
                case DAÑO:

                    break;

            }
        }

        @Override
        public void onClick(View v) {


        }
    }


}
