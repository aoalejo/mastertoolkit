package com.nugganet.arget.mastertoolkit.Controladores;

import android.animation.Animator;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Lugar;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Personaje;
import com.nugganet.arget.mastertoolkit.Custom_adapters.CustomAdapter_Checkbox;
import com.nugganet.arget.mastertoolkit.Custom_adapters.CustomAdapter_VistazoPersonajes;
import com.nugganet.arget.mastertoolkit.Principal;
import com.nugganet.arget.mastertoolkit.R;
import com.nugganet.arget.mastertoolkit.WebBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.asynclayoutinflater.view.AsyncLayoutInflater;

public class Controlador_ventanas {

    private View Toolkit_fisico = null;
    private View Toolkit_secundarias = null;
    private View Toolkit_creacion_armas = null;
    private View Toolkit_creacion_armaduras = null;
    private View Toolkit_seleccion_pjs_activos = null;
    private View Toolkit_combate = null;
    private View Toolkit_pruebas = null;
    private View Toolkit_vistazo = null;
    private View Toolkit_creacion_modificadores = null;
    private View extras_tiradas_layout = null;
    private View Extras_logger = null;
    private View Extras_creacion_lugares = null;
    private View Extras_config = null;
    private View Part_Psiquicos = null;
    private View Menu_ppal = null;
    private View toolkit_seleccionar_ubicaciones_activas = null;
    private View Extras_importar = null;
    private View Toolkit_creacion_pj = null;
    private FrameLayout root_layout = null;

    private boolean estado_toolkit_seleccionar_ubicaciones_activas = false;
    private boolean estado_Extras_importar = false;
    private boolean estado_Toolkit_creacion_pj = false;
    private boolean estado_Toolkit_fisico = false;
    private boolean estado_Toolkit_secundarias = false;
    private boolean estado_Toolkit_creacion_armas = false;
    private boolean estado_Toolkit_creacion_armaduras = false;
    private boolean estado_Toolkit_seleccion_pjs_activos = false;
    private boolean estado_Toolkit_combate = false;
    private boolean estado_Toolkit_vistazo = false;
    private boolean estado_Toolkit_creacion_modificadores = false;
    private boolean estado_extras_tiradas_layout = false;
    private boolean estado_Extras_logger = false;
    private boolean estado_Extras_creacion_lugares = false;
    private boolean estado_Extras_config = false;
    private boolean estado_Part_Psiquicos = false;

    private final static int HOT_LOAD = 1;
    private final static int COLD_LOAD = 0;


    private Principal context;

    public Controlador_ventanas(Principal context) {
        this.context = context;
        root_layout = context.findViewById(R.id.Layout_base_ventanas);
    }


    public void inflate(int id, final int index) {

        System.out.println("Infating...");

        AsyncLayoutInflater inflater_async = new AsyncLayoutInflater(context);

        inflater_async.inflate(id, root_layout, new AsyncLayoutInflater.OnInflateFinishedListener() {
            @Override
            public void onInflateFinished(@NonNull View view, int resid, @Nullable ViewGroup parent) {

                System.out.println("finished inflating...");

                view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

                root_layout.addView(view);

                switch (index) {
                    case R.id.Part_Psiquicos:
                        estado_Part_Psiquicos = true;
                        Part_Psiquicos = view;
                        break;
                    case R.id.Importar_Planilla:
                        estado_Extras_importar = true;
                        Extras_importar = view;
                        break;
                    case R.id.Selección_Ubicaciones:
                        estado_toolkit_seleccionar_ubicaciones_activas = true;
                        toolkit_seleccionar_ubicaciones_activas = view;
                        break;
                    case R.id.Configuración:
                        estado_Extras_config = true;
                        Extras_config = view;
                        break;
                    case R.id.Creac_Modificadores:
                        estado_Toolkit_creacion_modificadores = true;
                        Toolkit_creacion_modificadores = view;
                        break;
                    case R.id.Creac_Fis:
                        estado_Toolkit_fisico = true;
                        Toolkit_fisico = view;
                        break;
                    case R.id.Creac_Pj:
                        estado_Toolkit_creacion_pj = true;
                        Toolkit_creacion_pj = view;
                        break;
                    case R.id.Part_Secundarias:
                        estado_Toolkit_secundarias = true;
                        Toolkit_secundarias = view;
                        break;
                    case R.id.Part_Combate:
                        estado_Toolkit_combate = true;
                        Toolkit_combate = view;
                        break;
                    case R.id.Creac_Armaduras:
                        estado_Toolkit_creacion_armaduras = true;
                        Toolkit_creacion_armaduras = view;
                        break;
                    case R.id.Creac_Armas:
                        estado_Toolkit_creacion_armas = true;
                        Toolkit_creacion_armas = view;
                        break;
                    case R.id.Selección_Personajes:
                        estado_Toolkit_seleccion_pjs_activos = true;
                        Toolkit_seleccion_pjs_activos = view;
                        break;
                    case R.id.Part_vistazo:
                        estado_Toolkit_vistazo = true;
                        Toolkit_vistazo = view;
                        break;
                    case R.id.Tiradas:
                        estado_extras_tiradas_layout = true;
                        extras_tiradas_layout = view;
                        break;
                    case R.id.Extras_logger:
                        estado_Extras_logger = true;
                        Extras_logger = view;
                        break;
                    case R.id.Creac_lugares_logger:
                        estado_Extras_creacion_lugares = true;
                        Extras_creacion_lugares = view;
                        break;
                }

                cargaVentana(index);

                context.setAllListener(parent);

                context.IndicadorGuardando(false);
            }
        });

    }

    public void ocultar(int index) {

        if (index != R.id.Part_Psiquicos) {
            Hide(Part_Psiquicos);
        }
        if (index != R.id.Selección_Personajes) {
            Hide(Extras_importar);
        }
        if (index != R.id.Selección_Personajes) {
            Hide(Toolkit_seleccion_pjs_activos);
        }
        if (index != R.id.Selección_Ubicaciones) {
            Hide(toolkit_seleccionar_ubicaciones_activas);
        }
        if (index != R.id.Creac_Pj) {
            Hide(Toolkit_creacion_pj);
        }
        if (index != R.id.Creac_Armas) {
            Hide(Toolkit_creacion_armas);
        }
        if (index != R.id.Creac_Armaduras) {
            Hide(Toolkit_creacion_armaduras);
        }
        if (index != R.id.Creac_Fis) {
            Hide(Toolkit_fisico);
        }
        if (index != R.id.Part_Secundarias) {
            Hide(Toolkit_secundarias);
        }
        if (index != R.id.Creac_Modificadores) {
            Hide(Toolkit_creacion_modificadores);
        }
        if (index != R.id.Tiradas) {
            Hide(extras_tiradas_layout);
        }
        if (index != R.id.Extras_logger) {
            Hide(Extras_logger);
        }
        if (index != R.id.Creac_lugares_logger) {
            Hide(Extras_creacion_lugares);
        }
        if (index != R.id.Configuración) {
            Hide(Extras_config);
        }
        if (index != R.id.Part_vistazo) {
            Hide(Toolkit_vistazo);
        }
        if (index != R.id.Part_Combate) {
            Hide(Toolkit_combate);
        }
    }

    public void Hide(final View v) {
        if (v == null) {
            return;
        }
        if (v.getVisibility() == View.GONE) {
            return;
        }

        v
                .animate()
                .setDuration(200)
                .alpha(0.0f)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        v.animate().setListener(null);
                        //((ViewGroup) v.getParent()).removeView(v);

                        v.setVisibility(View.GONE);

                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
        ;
    }

    public void cargaVentana(int index) {
        int i = 0;

        if (context.getDatos().isModificado_pjs_en_mision()) {

            context.setControlador_turno(new Controlador_Turno(context));

            if (existsAndVisible(getToolkit_combate())) {
                ((ListView) context.findViewById(R.id.List_view_Iniciativas)).setAdapter(context.getControlador_turno());

                context.getControlador_turno().Calcular_turnos();
            }

            context.getDatos().setModificado_pjs_en_mision(false);

            if (existsAndVisible(getPart_Psiquicos())) {

                context.actualizarArrayPersonajes_EnPartida(R.id.spinner_seleccion_personaje_psiquico, true, 2);

            }

        }

        System.out.println("Index: " + index + " vs Part Psiq " + R.id.Part_Psiquicos);
        switch (index) {

            case R.id.Part_Psiquicos:
                context.getControlador_Psiquicos().init_listeners();
                context.getControlador_Psiquicos().init_spinners();

                context.cambio_de_pagina(null, R.id.Part_Psiquicos, true, "Psiquicos");
                break;
            case R.id.Importar_Planilla:
                break;
            case R.id.Selección_Ubicaciones:
                context.setCargando(true);

                context.actualizarArrayLugares(R.id.spinner_lugares_disponibles, false, false);

                String[] nombres_lugares = new String[context.getArraylugares().size()];

                i = 0;
                for (Lugar lug : context.getArraylugares()) {
                    nombres_lugares[i] = lug.getNombre();
                    i = i + 1;
                }

                ArrayAdapter<String> adapter_busqueda_lugares = new ArrayAdapter<String>(context, android.R.layout.select_dialog_item, nombres_lugares);

                ((AutoCompleteTextView) context.findViewById(R.id.autoCompleteTextView_seleccion_lugares)).setThreshold(1);
                ((AutoCompleteTextView) context.findViewById(R.id.autoCompleteTextView_seleccion_lugares)).setAdapter(adapter_busqueda_lugares);

                ((ListView) context.findViewById(R.id.List_lugares_activos)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        context.getArraylugares_partida().remove(position);

                        context.actualizarArrayLugares(R.id.spinner_ubicación, true, true);
                        context.actualizarAdapterLugaresPartida();
                    }
                });
                context.actualizarAdapterLugaresPartida();

                break;
            case R.id.Configuración:
                ((CheckBox) context.findViewById(R.id.checkBox_toogle_buttons)).setChecked(context.obtener_estado("Ocultar_botones_de_configuración", false));

                try {
                    ((TextView) context.findViewById(R.id.textView_settings_todo)).setText(context.getTODO());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                break;
            case R.id.Creac_Modificadores:
                context.getControlador_creación_modificadores().initListeners();
                context.getControlador_creación_modificadores().initModif();
                context.actualizarArrayModificadores();
                break;
            case R.id.Creac_Fis:
                ((Spinner) context.findViewById(R.id.spinner_agi)).setAdapter(ArrayAdapter.createFromResource(context, R.array.Atributos, android.R.layout.simple_spinner_dropdown_item));
                ((Spinner) context.findViewById(R.id.spinner_con)).setAdapter(ArrayAdapter.createFromResource(context, R.array.Atributos, android.R.layout.simple_spinner_dropdown_item));
                ((Spinner) context.findViewById(R.id.spinner_fue)).setAdapter(ArrayAdapter.createFromResource(context, R.array.Atributos, android.R.layout.simple_spinner_dropdown_item));

                ((Spinner) context.findViewById(R.id.spinner_agi)).setSelection(6);
                ((Spinner) context.findViewById(R.id.spinner_fue)).setSelection(6);
                ((Spinner) context.findViewById(R.id.spinner_con)).setSelection(6);

                ((Spinner) context.findViewById(R.id.spinner_tipo_generacion_caract)).setAdapter(ArrayAdapter.createFromResource(context, R.array.tipos_De_generacion_De_atributos, android.R.layout.simple_spinner_dropdown_item));

                break;
            case R.id.Creac_Pj:
                context.getControlador_Psiquicos().init_spinners();
                context.getControlador_Psiquicos().iniciar();

                context.getControlador_creac_pj().inicializar();

                TabLayout tl = (TabLayout) context.findViewById(R.id.tab_layout_pj);

                tl.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {

                        View Include_cr_pj_principal = ((View) context.findViewById(R.id.Include_cr_pj_principal));
                        View Include_cr_pj_armaduras = ((View) context.findViewById(R.id.Include_cr_pj_armaduras));
                        View Include_cr_pj_armas = ((View) context.findViewById(R.id.Include_cr_pj_armas));
                        View Include_cr_pj_penalizadores = ((View) context.findViewById(R.id.Include_cr_pj_penalizadores));
                        View Include_cr_pj_puntos = ((View) context.findViewById(R.id.Include_cr_pj_puntos));
                        View Include_cr_pj_psiquicos = ((View) context.findViewById(R.id.Include_cr_pj_psiquicos));


                        if (Include_cr_pj_principal.getVisibility() == View.VISIBLE) {
                            Hide(Include_cr_pj_principal);
                        }

                        if (Include_cr_pj_armaduras.getVisibility() == View.VISIBLE) {
                            Hide(Include_cr_pj_armaduras);
                        }
                        if (Include_cr_pj_armas.getVisibility() == View.VISIBLE) {
                            Hide(Include_cr_pj_armas);
                        }
                        if (Include_cr_pj_penalizadores.getVisibility() == View.VISIBLE) {
                            Hide(Include_cr_pj_penalizadores);
                        }
                        if (Include_cr_pj_puntos.getVisibility() == View.VISIBLE) {
                            Hide(Include_cr_pj_puntos);
                        }

                        if (Include_cr_pj_psiquicos.getVisibility() == View.VISIBLE) {
                            Hide(Include_cr_pj_psiquicos);
                        }


                        switch (tab.getPosition()) {
                            case 0://pj
                                context.Show(Include_cr_pj_principal);
                                break;
                            case 1://puntos
                                context.Show(Include_cr_pj_puntos);
                                break;
                            case 2://arma
                                context.Show(Include_cr_pj_armas);
                                break;
                            case 3://armadura
                                context.Show(Include_cr_pj_armaduras);
                                break;
                            case 4://bonificadores penalizadores y extras
                                context.Show(Include_cr_pj_penalizadores);
                                break;
                            case 5://psiquicos
                                context.Show(Include_cr_pj_psiquicos);
                                break;

                        }

                    }


                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                });

                context.actualizarArrayPersonajes(R.id.spinner_seleccionPj, false);

                context.actualizarArrayArmaduras(R.id.spinner_Armaduras_Para_Añadir);
                context.actualizarArrayArmas(R.id.spinner_Arma_Para_Añadir);

                break;
            case R.id.Part_Secundarias:

                context.getControlador_secundarias().init();

                ((AutoCompleteTextView) context.findViewById(R.id.autoCompleteTextView_habilidades_Secundarias)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        String seleccionado = ((AutoCompleteTextView) context.findViewById(R.id.autoCompleteTextView_habilidades_Secundarias)).getText().toString();

                        for (int i = 0; i < context.getResources().getStringArray(R.array.Secundarias).length; i++) {

                            if (seleccionado.toString().equalsIgnoreCase(context.getResources().getStringArray(R.array.Secundarias)[i])) {
                                ((Spinner) context.findViewById(R.id.spinner_habilidad_secundaria)).setSelection(i);
                                break;
                            }
                        }
                        ((AutoCompleteTextView) context.findViewById(R.id.autoCompleteTextView_habilidades_Secundarias)).setText("");
                    }
                });

                ((Spinner) context.findViewById(R.id.spinner_habilidad_secundaria)).setAdapter(ArrayAdapter.createFromResource(context, R.array.Secundarias, android.R.layout.simple_spinner_dropdown_item));
                ((Spinner) context.findViewById(R.id.spinner_Dificultad)).setAdapter(ArrayAdapter.createFromResource(context, R.array.Dificultades, android.R.layout.simple_spinner_dropdown_item));


                ArrayAdapter<String> adapter_busqueda_secundarias = new ArrayAdapter<String>(context, android.R.layout.select_dialog_item, context.getResources().getStringArray(R.array.Secundarias));

                ((AutoCompleteTextView) context.findViewById(R.id.autoCompleteTextView_habilidades_Secundarias)).setThreshold(1);
                ((AutoCompleteTextView) context.findViewById(R.id.autoCompleteTextView_habilidades_Secundarias)).setAdapter(adapter_busqueda_secundarias);
                break;
            case R.id.Part_Combate:

                context.getControlador_combate().setListener();

                TabLayout t2 = (TabLayout) context.findViewById(R.id.tab_layout_combate);

                t2.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {

                        LinearLayout tab_combate_1 = (LinearLayout) context.findViewById(R.id.tab_combate_1);
                        LinearLayout tab_combate_2 = (LinearLayout) context.findViewById(R.id.tab_combate_2);
                        LinearLayout tab_combate_3 = (LinearLayout) context.findViewById(R.id.tab_combate_3);

                        tab_combate_1.setVisibility(View.GONE);
                        tab_combate_2.setVisibility(View.GONE);
                        tab_combate_3.setVisibility(View.GONE);

                        context.getHistorial_de_acciones().añadirHistorial(null, tab.getPosition(), true, null);

                        switch (tab.getPosition()) {
                            case 0://Combate
                                tab_combate_1.setVisibility(View.VISIBLE);
                                break;
                            case 1://Modificadores
                                tab_combate_2.setVisibility(View.VISIBLE);
                                break;
                            case 2://Iniciativa
                                tab_combate_3.setVisibility(View.VISIBLE);

                                break;

                        }

                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                });

                ((Spinner) context.findViewById(R.id.spinner_Combate_distancia)).setAdapter(ArrayAdapter.createFromResource(context, R.array.Distancias, android.R.layout.simple_spinner_dropdown_item));
                ((Spinner) context.findViewById(R.id.spinner_Combate_tipo_de_defensa)).setAdapter(ArrayAdapter.createFromResource(context, R.array.Tipo_de_defensa, android.R.layout.simple_spinner_dropdown_item));
                ((Spinner) context.findViewById(R.id.spinner_Combate_Tipo_de_ataque)).setAdapter(ArrayAdapter.createFromResource(context, R.array.Tipo_de_Ataque, android.R.layout.simple_spinner_dropdown_item));
                ((Spinner) context.findViewById(R.id.spinner_Combate_apuntado)).setAdapter(ArrayAdapter.createFromResource(context, R.array.Apuntado, android.R.layout.simple_spinner_dropdown_item));
                ((Spinner) context.findViewById(R.id.spinner_Combate_Jugador_arma_atk)).setAdapter(ArrayAdapter.createFromResource(context, R.array.Sin_personajes, android.R.layout.simple_spinner_dropdown_item));
                ((Spinner) context.findViewById(R.id.spinner_Combate_Jugador_arma_def)).setAdapter(ArrayAdapter.createFromResource(context, R.array.Sin_personajes, android.R.layout.simple_spinner_dropdown_item));

                context.setModificadores_ataque(new boolean[context.getDatos().ArrayModificadores().size()]);
                context.setModificadores_defensa(new boolean[context.getDatos().ArrayModificadores().size()]);

                Arrays.fill(context.getModificadores_ataque(), false);
                Arrays.fill(context.getModificadores_defensa(), false);

                ListView List_view_modificadores = (ListView) context.findViewById(R.id.List_view_modificadores);

                CustomAdapter_Checkbox adapter_ataque = new CustomAdapter_Checkbox(context, context.getDatos().ArrayModificadores());

                List_view_modificadores.setAdapter(adapter_ataque);

                context.actualizarArrayPersonajes_EnPartida(R.id.spinner_Combate_Jugador_asaltante, true, 0);
                context.actualizarArrayPersonajes_EnPartida(R.id.spinner_Combate_Jugador_defensor, true, 1);
                break;
            case R.id.Creac_Armaduras:
                context.getControlador_creación_armaduras().initListeners();
                context.getControlador_creación_armaduras().initArmaduras();
                context.actualizarArrayArmaduras(R.id.spinner_seleccion_armadura);
                break;
            case R.id.Creac_Armas:

                context.getControlador_creación_armas().initListeners();
                context.getControlador_creación_armas().initArmas();


                context.actualizarArrayArmas(R.id.spinner_seleccion_arma);
                break;
            case R.id.Selección_Personajes:

                ((AutoCompleteTextView) context.findViewById(R.id.autoCompleteTextView_seleccion_personajes)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        String seleccionado = ((AutoCompleteTextView) context.findViewById(R.id.autoCompleteTextView_seleccion_personajes)).getText().toString();

                        for (int i = 0; i < context.getNombres_personajes().length; i++) {

                            if (seleccionado.toString().equalsIgnoreCase(context.getNombres_personajes()[i])) {

                                context.getDatos().Array_Personajes_En_Mision_add(context.getDatos().ArrayPersonajes().get(i));
                                context.actualizarAdapterPersonajesPartida();
                                break;
                            }
                        }
                        ((AutoCompleteTextView) context.findViewById(R.id.autoCompleteTextView_seleccion_personajes)).setText("");
                    }
                });

                context.setCargando(true);

                context.setNombres_personajes(new String[context.getDatos().ArrayPersonajes().size()]);
                i = 0;
                for (Personaje pj : context.getDatos().ArrayPersonajes()) {
                    context.getNombres_personajes()[i] = pj.getNombres();
                    i = i + 1;
                }

                ArrayAdapter<String> adapter_busqueda = new ArrayAdapter<String>(context, android.R.layout.select_dialog_item, context.getNombres_personajes());

                ((AutoCompleteTextView) context.findViewById(R.id.autoCompleteTextView_seleccion_personajes)).setThreshold(1);
                ((AutoCompleteTextView) context.findViewById(R.id.autoCompleteTextView_seleccion_personajes)).setAdapter(adapter_busqueda);

                context.actualizarArrayPersonajes(R.id.spinner_pjs_disponibles, false);

                ((ListView) context.findViewById(R.id.List_pjs_activos)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        context.getDatos().Eliminar_Personaje_en_misión(position);
                        context.actualizarAdapterPersonajesPartida();
                    }
                });
                context.actualizarAdapterPersonajesPartida();
                break;
            case R.id.Part_vistazo:
                ArrayList<Personaje> personajes_temp = new ArrayList<>();

                for (Personaje pj : context.getDatos().Array_Personajes_En_Mision()) {
                    personajes_temp.add(pj);
                }

                CustomAdapter_VistazoPersonajes adapter = new CustomAdapter_VistazoPersonajes(context, personajes_temp, context);

                ((Spinner) context.findViewById(R.id.spinner_cantidad)).setAdapter(ArrayAdapter.createFromResource(context, R.array.Cantidades_50, android.R.layout.simple_spinner_dropdown_item));

                ((Spinner) context.findViewById(R.id.spinner_tipo)).setAdapter(ArrayAdapter.createFromResource(context, R.array.tipos_De_tiempo, android.R.layout.simple_spinner_dropdown_item));


                ((ListView) context.findViewById(R.id.List_view_Personajes)).setAdapter(adapter);
                break;
            case R.id.Tiradas:
                context.setPantalla_tiradas(true);

                ((TextView) context.findViewById(R.id.textView_tiradas)).postDelayed(context.actualizador_tiradas, 1000);

                ((Spinner) context.findViewById(R.id.spinner_tiradas_random)).setSelection(8);
                break;
            case R.id.Extras_logger:
                context.actualizarArrayPersonajes(R.id.spinner_jugadores_log_primario, true);
                context.actualizarArrayPersonajes(R.id.spinner_jugadores_log_secundario, true);

                ((WebView) context.findViewById(R.id.WebView_logger)).loadData("<font size=\"4\">" + context.getHistorial().getLog(), "text/html; charset=UTF-8", null);

                ((WebView) context.findViewById(R.id.WebView_logger_head)).loadData("<font size=\"4\">" + WebBuilder.DETALLE + "</font>", "text/html; charset=UTF-8", null);

                context.actualizarArrayLugares(R.id.spinner_ubicación_log, true, false);
                break;
            case R.id.Creac_lugares_logger:

                context.getControlador_creación_Lugares().initListeners();
                context.getControlador_creación_Lugares().initLugar();


                context.actualizarArrayLugares(R.id.spinner_seleccion_lugar, false, false);

                context.actualizarArrayLugares(R.id.spinner_lugar_padre, true, false);
                break;
        }
    }

    public void incializar(int index) {

        System.out.println("Index: " + index);

        context.IndicadorGuardando(true);

        switch (index) {
            case R.id.Part_Psiquicos:

                if (!estado_Part_Psiquicos & Part_Psiquicos == null) {
                    System.out.println("Infating...");
                    inflate(R.layout.psiquicos, index);
                } else {
                    System.out.println("Showing...");
                    context.Show(Part_Psiquicos);
                }
                break;
            case R.id.Importar_Planilla:
                if (!estado_Extras_importar & Extras_importar == null) {
                    inflate(R.layout.extras_importar_planilla, index);
                } else {

                    context.Show(Extras_importar);

                }

                break;
            case R.id.Selección_Ubicaciones:
                if (!estado_toolkit_seleccionar_ubicaciones_activas & toolkit_seleccionar_ubicaciones_activas == null) {
                    inflate(R.layout.toolkit_seleccionar_ubicaciones_activas, index);
                } else {

                    context.Show(toolkit_seleccionar_ubicaciones_activas);

                }
                break;
            case R.id.Configuración:
                if (!estado_Extras_config & Extras_config == null) {
                    inflate(R.layout.extras_herramientas, index);
                } else {

                    context.Show(Extras_config);

                }
                break;
            case R.id.Creac_Modificadores:
                if (!estado_Toolkit_creacion_modificadores & Toolkit_creacion_modificadores == null) {
                    inflate(R.layout.cr_modificadores_v2, index);
                } else {

                    context.Show(Toolkit_creacion_modificadores);

                }
                break;
            case R.id.Creac_Fis:
                if (!estado_Toolkit_fisico & Toolkit_fisico == null) {
                    inflate(R.layout.toolkit_fisico, index);
                } else {

                    context.Show(Toolkit_fisico);

                }
                break;
            case R.id.Creac_Pj:
                if (!estado_Toolkit_creacion_pj & Toolkit_creacion_pj == null) {
                    inflate(R.layout.creacion_personaje, index);
                } else {

                    context.Show(Toolkit_creacion_pj);

                }
                break;
            case R.id.Part_Secundarias:
                if (!estado_Toolkit_secundarias & Toolkit_secundarias == null) {
                    inflate(R.layout.toolkit_secundarias, index);
                } else {

                    context.Show(Toolkit_secundarias);

                }
                break;
            case R.id.Part_Combate:
                if (!estado_Toolkit_combate & Toolkit_combate == null) {
                    inflate(R.layout.toolkit_combate_v2, index);
                } else {

                    context.Show(Toolkit_combate);

                }
                break;
            case R.id.Creac_Armaduras:
                if (!estado_Toolkit_creacion_armaduras & Toolkit_creacion_armaduras == null) {
                    inflate(R.layout.cr_armaduras_v2, index);
                } else {

                    context.Show(Toolkit_creacion_armaduras);

                }
                break;
            case R.id.Creac_Armas:
                if (!estado_Toolkit_creacion_armas & Toolkit_creacion_armas == null) {
                    inflate(R.layout.cr_armas_v2, index);
                } else {

                    context.Show(Toolkit_creacion_armas);

                }
                break;
            case R.id.Selección_Personajes:
                if (!estado_Toolkit_seleccion_pjs_activos & Toolkit_seleccion_pjs_activos == null) {
                    inflate(R.layout.toolkit_seleccionar_pjs_activos, index);
                } else {

                    context.Show(Toolkit_seleccion_pjs_activos);

                }
                break;
            case R.id.Part_vistazo:
                if (!estado_Toolkit_vistazo & Toolkit_vistazo == null) {
                    inflate(R.layout.toolkit_vistazo, index);
                } else {

                    context.Show(Toolkit_vistazo);

                }
                break;
            case R.id.Tiradas:
                if (!estado_extras_tiradas_layout & extras_tiradas_layout == null) {
                    inflate(R.layout.extras_tiradas, index);
                } else {

                    context.Show(extras_tiradas_layout);

                }
                break;
            case R.id.Extras_logger:
                if (!estado_Extras_logger & Extras_logger == null) {
                    inflate(R.layout.extras_logger, index);
                } else {

                    context.Show(Extras_logger);

                }
                break;
            case R.id.Creac_lugares_logger:
                if (!estado_Extras_creacion_lugares & Extras_creacion_lugares == null) {
                    inflate(R.layout.cr_lugares_v2, index);
                } else {

                    context.Show(Extras_creacion_lugares);

                }
                break;

        }


    }

    public View getToolkit_fisico() {
        return Toolkit_fisico;
    }

    public void setToolkit_fisico(View toolkit_fisico) {
        Toolkit_fisico = toolkit_fisico;
    }

    public View getToolkit_secundarias() {
        return Toolkit_secundarias;
    }

    public void setToolkit_secundarias(View toolkit_secundarias) {
        Toolkit_secundarias = toolkit_secundarias;
    }

    public View getToolkit_creacion_armas() {
        return Toolkit_creacion_armas;
    }

    public void setToolkit_creacion_armas(View toolkit_creacion_armas) {
        Toolkit_creacion_armas = toolkit_creacion_armas;
    }

    public View getToolkit_creacion_armaduras() {
        return Toolkit_creacion_armaduras;
    }

    public void setToolkit_creacion_armaduras(View toolkit_creacion_armaduras) {
        Toolkit_creacion_armaduras = toolkit_creacion_armaduras;
    }

    public View getToolkit_seleccion_pjs_activos() {
        return Toolkit_seleccion_pjs_activos;
    }

    public void setToolkit_seleccion_pjs_activos(View toolkit_seleccion_pjs_activos) {
        Toolkit_seleccion_pjs_activos = toolkit_seleccion_pjs_activos;
    }

    public View getToolkit_combate() {
        return Toolkit_combate;
    }

    public void setToolkit_combate(View toolkit_combate) {
        Toolkit_combate = toolkit_combate;
    }

    public View getToolkit_pruebas() {
        return Toolkit_pruebas;
    }

    public void setToolkit_pruebas(View toolkit_pruebas) {
        Toolkit_pruebas = toolkit_pruebas;
    }

    public View getToolkit_vistazo() {
        return Toolkit_vistazo;
    }

    public void setToolkit_vistazo(View toolkit_vistazo) {
        Toolkit_vistazo = toolkit_vistazo;
    }

    public View getToolkit_creacion_modificadores() {
        return Toolkit_creacion_modificadores;
    }

    public void setToolkit_creacion_modificadores(View toolkit_creacion_modificadores) {
        Toolkit_creacion_modificadores = toolkit_creacion_modificadores;
    }

    public View getExtras_tiradas_layout() {
        return extras_tiradas_layout;
    }

    public void setExtras_tiradas_layout(View extras_tiradas_layout) {
        this.extras_tiradas_layout = extras_tiradas_layout;
    }

    public View getExtras_logger() {
        return Extras_logger;
    }

    public void setExtras_logger(View extras_logger) {
        Extras_logger = extras_logger;
    }

    public View getExtras_creacion_lugares() {
        return Extras_creacion_lugares;
    }

    public void setExtras_creacion_lugares(View extras_creacion_lugares) {
        Extras_creacion_lugares = extras_creacion_lugares;
    }

    public View getExtras_config() {
        return Extras_config;
    }

    public void setExtras_config(View extras_config) {
        Extras_config = extras_config;
    }

    public View getPart_Psiquicos() {
        return Part_Psiquicos;
    }

    public void setPart_Psiquicos(View part_Psiquicos) {
        Part_Psiquicos = part_Psiquicos;
    }

    public View getMenu_ppal() {
        return Menu_ppal;
    }

    public void setMenu_ppal(View menu_ppal) {
        Menu_ppal = menu_ppal;
    }

    public View getToolkit_seleccionar_ubicaciones_activas() {
        return toolkit_seleccionar_ubicaciones_activas;
    }

    public void setToolkit_seleccionar_ubicaciones_activas(View toolkit_seleccionar_ubicaciones_activas) {
        this.toolkit_seleccionar_ubicaciones_activas = toolkit_seleccionar_ubicaciones_activas;
    }

    public View getExtras_importar() {
        return Extras_importar;
    }

    public void setExtras_importar(View extras_importar) {
        Extras_importar = extras_importar;
    }

    public View getToolkit_creacion_pj() {
        return Toolkit_creacion_pj;
    }

    public void setToolkit_creacion_pj(View toolkit_creacion_pj) {
        Toolkit_creacion_pj = toolkit_creacion_pj;
    }

    public FrameLayout getRoot_layout() {
        return root_layout;
    }

    public void setRoot_layout(FrameLayout root_layout) {
        this.root_layout = root_layout;
    }

    public boolean isEstado_toolkit_seleccionar_ubicaciones_activas() {
        return estado_toolkit_seleccionar_ubicaciones_activas;
    }

    public void setEstado_toolkit_seleccionar_ubicaciones_activas(boolean estado_toolkit_seleccionar_ubicaciones_activas) {
        this.estado_toolkit_seleccionar_ubicaciones_activas = estado_toolkit_seleccionar_ubicaciones_activas;
    }

    public boolean isEstado_Extras_importar() {
        return estado_Extras_importar;
    }

    public void setEstado_Extras_importar(boolean estado_Extras_importar) {
        this.estado_Extras_importar = estado_Extras_importar;
    }

    public boolean isEstado_Toolkit_creacion_pj() {
        return estado_Toolkit_creacion_pj;
    }

    public void setEstado_Toolkit_creacion_pj(boolean estado_Toolkit_creacion_pj) {
        this.estado_Toolkit_creacion_pj = estado_Toolkit_creacion_pj;
    }

    public boolean isEstado_Toolkit_fisico() {
        return estado_Toolkit_fisico;
    }

    public void setEstado_Toolkit_fisico(boolean estado_Toolkit_fisico) {
        this.estado_Toolkit_fisico = estado_Toolkit_fisico;
    }

    public boolean isEstado_Toolkit_secundarias() {
        return estado_Toolkit_secundarias;
    }

    public void setEstado_Toolkit_secundarias(boolean estado_Toolkit_secundarias) {
        this.estado_Toolkit_secundarias = estado_Toolkit_secundarias;
    }

    public boolean isEstado_Toolkit_creacion_armas() {
        return estado_Toolkit_creacion_armas;
    }

    public void setEstado_Toolkit_creacion_armas(boolean estado_Toolkit_creacion_armas) {
        this.estado_Toolkit_creacion_armas = estado_Toolkit_creacion_armas;
    }

    public boolean isEstado_Toolkit_creacion_armaduras() {
        return estado_Toolkit_creacion_armaduras;
    }

    public void setEstado_Toolkit_creacion_armaduras(boolean estado_Toolkit_creacion_armaduras) {
        this.estado_Toolkit_creacion_armaduras = estado_Toolkit_creacion_armaduras;
    }

    public boolean isEstado_Toolkit_seleccion_pjs_activos() {
        return estado_Toolkit_seleccion_pjs_activos;
    }

    public void setEstado_Toolkit_seleccion_pjs_activos(boolean estado_Toolkit_seleccion_pjs_activos) {
        this.estado_Toolkit_seleccion_pjs_activos = estado_Toolkit_seleccion_pjs_activos;
    }

    public boolean isEstado_Toolkit_combate() {
        return estado_Toolkit_combate;
    }

    public void setEstado_Toolkit_combate(boolean estado_Toolkit_combate) {
        this.estado_Toolkit_combate = estado_Toolkit_combate;
    }

    public boolean isEstado_Toolkit_vistazo() {
        return estado_Toolkit_vistazo;
    }

    public void setEstado_Toolkit_vistazo(boolean estado_Toolkit_vistazo) {
        this.estado_Toolkit_vistazo = estado_Toolkit_vistazo;
    }

    public boolean isEstado_Toolkit_creacion_modificadores() {
        return estado_Toolkit_creacion_modificadores;
    }

    public void setEstado_Toolkit_creacion_modificadores(boolean estado_Toolkit_creacion_modificadores) {
        this.estado_Toolkit_creacion_modificadores = estado_Toolkit_creacion_modificadores;
    }

    public boolean isEstado_extras_tiradas_layout() {
        return estado_extras_tiradas_layout;
    }

    public void setEstado_extras_tiradas_layout(boolean estado_extras_tiradas_layout) {
        this.estado_extras_tiradas_layout = estado_extras_tiradas_layout;
    }

    public boolean isEstado_Extras_logger() {
        return estado_Extras_logger;
    }

    public void setEstado_Extras_logger(boolean estado_Extras_logger) {
        this.estado_Extras_logger = estado_Extras_logger;
    }

    public boolean isEstado_Extras_creacion_lugares() {
        return estado_Extras_creacion_lugares;
    }

    public void setEstado_Extras_creacion_lugares(boolean estado_Extras_creacion_lugares) {
        this.estado_Extras_creacion_lugares = estado_Extras_creacion_lugares;
    }

    public boolean isEstado_Extras_config() {
        return estado_Extras_config;
    }

    public void setEstado_Extras_config(boolean estado_Extras_config) {
        this.estado_Extras_config = estado_Extras_config;
    }

    public boolean isEstado_Part_Psiquicos() {
        return estado_Part_Psiquicos;
    }

    public void setEstado_Part_Psiquicos(boolean estado_Part_Psiquicos) {
        this.estado_Part_Psiquicos = estado_Part_Psiquicos;
    }

    public static int getHotLoad() {
        return HOT_LOAD;
    }

    public static int getColdLoad() {
        return COLD_LOAD;
    }


    public boolean existsAndVisible(View view) {

        if (view != null) {
            return (view.getVisibility() == View.VISIBLE);
        }

        return false;
    }
}
