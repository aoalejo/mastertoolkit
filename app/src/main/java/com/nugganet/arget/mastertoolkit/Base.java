package com.nugganet.arget.mastertoolkit;

import com.nugganet.arget.mastertoolkit.Clases_objetos.Arma;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Modificador;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Personaje;
import com.nugganet.arget.mastertoolkit.Clases_objetos.TriplaVectores;

import java.util.ArrayList;
import java.util.Arrays;

//import com.nugganet.arget.mastertoolkit.Clases_objetos.Personaje;

/**
 * Created by Arget on 19/4/2017.
 */

public abstract class Base {

    public final static boolean MODO_DE_PRUEBAS = true;


    // Caracteristicas
    public final static int ATRIBUTO_AGILIDAD = 0;
    public final static int ATRIBUTO_CONSTITUCIÓN = 1;
    public final static int ATRIBUTO_DESTREZA = 2;
    public final static int ATRIBUTO_FUERZA = 3;
    public final static int ATRIBUTO_INTELIGENCIA = 4;
    public final static int ATRIBUTO_PERCEPCIÓN = 5;
    public final static int ATRIBUTO_PODER = 6;
    public final static int ATRIBUTO_VOLUNTAD = 7;

    public final static int RESISTENCIA_FISICA = 8;
    public final static int RESISTENCIA_ENFERMEDADES = 9;
    public final static int RESISTENCIA_VENENOS = 10;
    public final static int RESISTENCIA_MAGICA = 11;
    public final static int RESISTENCIA_PSIQUICA = 12;
    public final static int RESISTENCIA_PRESENCIA = 13;

    public final static int HABILIDAD_ATAQUE = 14;
    public final static int HABILIDAD_PARADA = 15;
    public final static int HABILIDAD_ESQUIVA = 16;
    public final static int HABILIDAD_ARMADURA = 17;
    public final static int HABILIDAD_TURNO = 18;
    public final static int HABILIDAD_PROYECCION_MAGICA = 19;
    public final static int HABILIDAD_PROYECCION_PSIQUICA = 20;

    public final static int HABILIDAD_ACROBACIAS = 21;
    public final static int HABILIDAD_ATLETISMO = 22;
    public final static int HABILIDAD_MONTAR = 23;
    public final static int HABILIDAD_NADAR = 24;
    public final static int HABILIDAD_TREPAR = 25;
    public final static int HABILIDAD_SALTAR = 26;
    public final static int HABILIDAD_ESTILO = 27;
    public final static int HABILIDAD_INTIMIDAR = 28;
    public final static int HABILIDAD_LIDERAZGO = 29;
    public final static int HABILIDAD_PERSUASIÓN = 30;
    public final static int HABILIDAD_COMERCIO = 31;
    public final static int HABILIDAD_CALLEJEO = 32;
    public final static int HABILIDAD_ETIQUETA = 33;
    public final static int HABILIDAD_ADVERTIR = 34;
    public final static int HABILIDAD_BUSCAR = 35;
    public final static int HABILIDAD_RASTREAR = 36;
    public final static int HABILIDAD_ANIMALES = 37;
    public final static int HABILIDAD_CIENCIA = 38;
    public final static int HABILIDAD_LEY = 39;
    public final static int HABILIDAD_HERBOLARIA = 40;
    public final static int HABILIDAD_HISTORIA = 41;
    public final static int HABILIDAD_TACTICA = 42;
    public final static int HABILIDAD_MEDICINA = 43;
    public final static int HABILIDAD_MEMORIZAR = 44;
    public final static int HABILIDAD_NAVEGACIÓN = 45;
    public final static int HABILIDAD_OCULTISMO = 46;
    public final static int HABILIDAD_TASACIÓN = 47;
    public final static int HABILIDAD_MÁGICA = 48;
    public final static int HABILIDAD_FRIALDAD = 49;
    public final static int HABILIDAD_PFUERZA = 50;
    public final static int HABILIDAD_RESDOLOR = 51;
    public final static int HABILIDAD_CERRAJERÍA = 52;
    public final static int HABILIDAD_DISFRAZ = 53;
    public final static int HABILIDAD_OCULTARSE = 54;
    public final static int HABILIDAD_ROBO = 55;
    public final static int HABILIDAD_SIGILO = 56;
    public final static int HABILIDAD_TRAMPERÍA = 57;
    public final static int HABILIDAD_VENENOS = 58;
    public final static int HABILIDAD_ARTE = 59;
    public final static int HABILIDAD_BAILE = 60;
    public final static int HABILIDAD_FORJA = 61;
    public final static int HABILIDAD_RUNAS = 62;
    public final static int HABILIDAD_ALQUIMIA = 63;
    public final static int HABILIDAD_ANIMISMO = 64;
    public final static int HABILIDAD_MÚSICA = 65;
    public final static int HABILIDAD_TMANOS = 66;
    public final static int HABILIDAD_ESP_1 = 67;
    public final static int HABILIDAD_ESP_2 = 68;
    public final static int HABILIDAD_ESP_3 = 69;
    public final static int HABILIDAD_ESP_4 = 70;
    public final static int HABILIDAD_ESP_5 = 71;
    public final static int HABILIDAD_ESP_6 = 72;
    public final static int HABILIDAD_ESP_7 = 73;
    public final static int HABILIDAD_ESP_8 = 74;
    public final static int HABILIDAD_ESP_9 = 75;
    public final static int HABILIDAD_ESP_10 = 76;

    public final static int VIDA_MAX = 77;
    public final static int VIDA_ACT = 78;
    public final static int VIDA_REG = 79;
    public final static int ZEON_MAX = 80;
    public final static int ZEON_ACT = 81;
    public final static int ZEON_REGEN = 82;
    public final static int ESCUDO_MAX = 83;
    public final static int ESCUDO_ACT = 84;
    public final static int ESCUDO_REGEN = 85;
    public final static int ENERGÍA_MAX = 86;
    public final static int ENERGÍA_ACT = 87;
    public final static int ENERGÍA_REGEN = 88;

    public final static int TA_FIL = 90;
    public final static int TA_CON = 91;
    public final static int TA_PEN = 92;
    public final static int TA_CAL = 93;
    public final static int TA_ELE = 94;
    public final static int TA_FRI = 95;
    public final static int TA_ENE = 96;
    public final static int BARRERA_DE_DAÑO = 97;

    public final static String[] CARACTERISTICAS = {"ATRIBUTO_AGILIDAD", "ATRIBUTO_CONSTITUCIÓN", "ATRIBUTO_DESTREZA", "ATRIBUTO_FUERZA", "ATRIBUTO_INTELIGENCIA", "ATRIBUTO_PERCEPCIÓN", "ATRIBUTO_PODER", "ATRIBUTO_VOLUNTAD", "RESISTENCIA_FISICA", "RESISTENCIA_ENFERMEDADES", "RESISTENCIA_VENENOS", "RESISTENCIA_MAGICA", "RESISTENCIA_PSIQUICA", "RESISTENCIA_PRESENCIA", "HABILIDAD_ATAQUE", "HABILIDAD_PARADA", "HABILIDAD_ESQUIVA", "HABILIDAD_ARMADURA", "HABILIDAD_TURNO", "HABILIDAD_PROYECCION_MAGICA", "HABILIDAD_PROYECCION_PSIQUICA", "HABILIDAD_ACROBACIAS", "HABILIDAD_ATLETISMO", "HABILIDAD_MONTAR", "HABILIDAD_NADAR", "HABILIDAD_TREPAR", "HABILIDAD_SALTAR", "HABILIDAD_ESTILO", "HABILIDAD_INTIMIDAR", "HABILIDAD_LIDERAZGO", "HABILIDAD_PERSUASIÓN", "HABILIDAD_COMERCIO", "HABILIDAD_CALLEJEO", "HABILIDAD_ETIQUETA", "HABILIDAD_ADVERTIR", "HABILIDAD_BUSCAR", "HABILIDAD_RASTREAR", "HABILIDAD_ANIMALES", "HABILIDAD_CIENCIA", "HABILIDAD_LEY", "HABILIDAD_HERBOLARIA", "HABILIDAD_HISTORIA", "HABILIDAD_TACTICA", "HABILIDAD_MEDICINA", "HABILIDAD_MEMORIZAR", "HABILIDAD_NAVEGACIÓN", "HABILIDAD_OCULTISMO", "HABILIDAD_TASACIÓN", "HABILIDAD_MÁGICA", "HABILIDAD_FRIALDAD", "HABILIDAD_PFUERZA", "HABILIDAD_RESDOLOR", "HABILIDAD_CERRAJERÍA", "HABILIDAD_DISFRAZ", "HABILIDAD_OCULTARSE", "HABILIDAD_ROBO", "HABILIDAD_SIGILO", "HABILIDAD_TRAMPERÍA", "HABILIDAD_VENENOS", "HABILIDAD_ARTE", "HABILIDAD_BAILE", "HABILIDAD_FORJA", "HABILIDAD_RUNAS", "HABILIDAD_ALQUIMIA", "HABILIDAD_ANIMISMO", "HABILIDAD_MÚSICA", "HABILIDAD_TMANOS", "HABILIDAD_ESP_1", "HABILIDAD_ESP_2", "HABILIDAD_ESP_3", "HABILIDAD_ESP_4", "HABILIDAD_ESP_5", "HABILIDAD_ESP_6", "HABILIDAD_ESP_7", "HABILIDAD_ESP_8", "HABILIDAD_ESP_9", "HABILIDAD_ESP_10", "", "VIDA_MAX ", "VIDA_ACT ", "VIDA_REG ", "ZEON_MAX ", "ZEON_ACT ", "ZEON_REGEN ", "ESCUDO_MAX ", "ESCUDO_ACT ", "ESCUDO_REGEN ", "ENERGÍA_MAX ", "ENERGÍA_ACT ", "ENERGÍA_REGEN  ", "TA_FIL", "TA_CON", "TA_PEN", "TA_CAL", "TA_ELE", "TA_FRI", "TA_ENE", "BARRERA_DE_DAÑO"};

    public final static Integer[] tablaRegenVida = {-999, 0, 10, 20, 30, 40, 50, 75, 100, 250, 500, 1440, 2880, 7200, 14400, 28800, 144000, 288000, 720000, 1440000, 2880000, 7200000};


    //VARIABLES


    //PENALIZADORES

    public final static int MOVIMIENTO = 0;
    public final static int VISUAL = 1;
    public final static int TODA_ACCIÓN = 2;
    public final static int PENALIZADORES_ACTUALES = 3;


    public final static int PEN_A_TODA_ACCION = 0;
    public final static int PEN_TEMPORAL = 1;
    public final static int PEN_VISUAL = 2;


    public final static int[] RECUPERACION_PENALIZADORES_INDICE = {10, 20, 30, 40, 50, 75, 100, 250, 500, 1500, 3000, 7500, 15000, 30000, 150000, 300000, 750000, 1500000, 3000000, 7500000};
    public final static int[] RECUPERACION_PENALIZADORES_VALORES = {5, 5, 5, 10, 10, 15, 20, 25, 30, 40, 50, 125, 250, 375, 500, 15000, 300000, 750000, 999999999, 999999999};

    //ESPECIALES

    public final static int ROBO_DE_VIDA_PORCENTUAL = 0;
    public final static int REDUCCIÓN_DE_DAÑO_PLENO = 1;
    public final static int REDUCCIÓN_DE_DAÑO_PORCENTUAL = 2;
    public final static int REDUCCION_DE_PENALIZADORES_PORCENTUAL = 3;
    public final static int REDUCCION_DE_PENALIZADORES_PLENO = 4;
    public final static int REEMPLAZAR_TIPO_DE_ATAQUE_CON_ARMA = 5;
    public final static int AÑADIR_CRÍTICO_AL_ARMA = 6;


    public final static String[] ESPECIALES = {"ROBO_DE_VIDA_PORCENTUAL", "REDUCCIÓN_DE_DAÑO_PLENO", "REDUCCIÓN_DE_DAÑO_PORCENTUAL", "REDUCCION_DE_PENALIZADORES_PORCENTUAL", "REDUCCION_DE_PENALIZADORES_PLENO", "REEMPLAZAR_TIPO_DE_ATAQUE_CON_ARMA", "AÑADIR_CRÍTICO_AL_ARMA"};

    //Localizaciones
    public final static int LOCALIZACION_PECTORAL_IZQUIERDO = 0;
    public final static int LOCALIZACION_PECTORAL_DERECHO = 1;
    public final static int LOCALIZACION_HOMBRO_IZQUIERDO = 2;
    public final static int LOCALIZACION_HOMBRO_DERECHO = 3;
    public final static int LOCALIZACION_ESTOMAGO = 4;
    public final static int LOCALIZACION_COSTADO_DERECHO = 5;
    public final static int LOCALIZACION_COSTADO_IZQUIERDO = 6;

    public final static int LOCALIZACION_ANTEBRAZO_SUPERIOR_IZQUIERDO = 7;
    public final static int LOCALIZACION_ANTEBRAZO_INFERIOR_IZQUIERDO = 8;
    public final static int LOCALIZACION_MANO_IZQUIERDA = 9;
    public final static int LOCALIZACION_ANTEBRAZO_SUPERIOR_DERECHO = 10;
    public final static int LOCALIZACION_ANTEBRAZO_INFERIOR_DERECHO = 11;
    public final static int LOCALIZACION_MANO_DERECHA = 12;

    public final static int LOCALIZACION_MUSLO_IZQUIERDO = 13;
    public final static int LOCALIZACION_PANTORILLA_IZQUIERDA = 14;
    public final static int LOCALIZACION_PIE_IZQUIERDO = 15;
    public final static int LOCALIZACION_MUSLO_DERECHO = 16;
    public final static int LOCALIZACION_PANTORILLA_DERECHA = 17;
    public final static int LOCALIZACION_PIE_DERECHO = 18;

    public final static int LOCALIZACION_CABEZA = 19;
    public final static int LOCALIZACION_CUELLO = 20;

    //Armaduras

    public final static int CABEZA = 0; // 19 A 20
    public final static int PECHO = 1; // 0 A 6
    public final static int BRAZOS = 2;// 7 A 12
    public final static int PIERNAS = 3;// 13 A 18

    public static final int ARM_TA_FIL = 0;
    public static final int ARM_TA_CON = 1;
    public static final int ARM_TA_PEN = 2;
    public static final int ARM_TA_CAL = 3;
    public static final int ARM_TA_ELE = 4;
    public static final int ARM_TA_FRI = 5;
    public static final int ARM_TA_ENE = 6;
    public static final int ARM_TA_BARRERA = 7;
    public static final int ARM_REQUERIMIENTO = 8;
    public static final int ARM_PENALIZADOR_VISUAL = 9;
    public static final int ARM_PENALIZADOR_DESTREZA = 10;
    public static final int ARM_PENALIZADOR_MOVIMIENTO = 11;
    public static final int ARM_ENTEREZA = 12;
    public static final int ARM_PRESENCIA = 13;
    public static final int ARM_DAÑOS = 14;
    public static final int ARM_DUREZA = 15;
    public static final int ARM_ZONA_PROTEGIDA_0 = 16;
    public static final int ARM_ZONA_PROTEGIDA_1 = 17;
    public static final int ARM_ZONA_PROTEGIDA_2 = 18;
    public static final int ARM_ZONA_PROTEGIDA_3 = 19;
    public static final int ARM_ACTIVA = 20;


    public static final int[] ARMADURA_LOCALIZACION_GENERAL = {1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 0, 0, -1, -1};

    public static final int Divisor_crítico(int localizacion) {

        if (localizacion == 1 | localizacion == 19 | localizacion == 20) {
            return 10;
        }

        if (localizacion == 4) {
            return 5;
        }

        return 2;
    }

    public static final double[] CLASE_ARMADURA_COSTES = {5.36, 5.36, 2.34, 2.34, 8.13, 2.93, 2.93, 3.22, 2.67, 2.93, 3.22, 2.67, 2.93, 8.32, 5.01, 2.28, 8.32, 5.01, 2.28, 19.51, 2.28};
    public static final int[] DIFICULTADES = {-60, -60, -30, -30, -20, -30, -30, -20, -20, -40, -20, -20, -40, -20, -10, -50, -20, -10, -50, -60, -80, -100};
    public static final double[] CLASE_ARMADURA_PENALIZADORES = {6.93, 6.93, 3.03, 3.03, 8.15, 3.78, 3.78, 4.16, 3.45, 1.89, 4.16, 3.45, 1.89, 10.76, 6.47, 1.26, 10.76, 6.47, 1.26, 6.72, 1.68};

    //Armas



    public static final int localizacion(int i) {
        if (i == -1) {
            int localizacion = (int) (Math.random() * 100);
            int[] tabla = {0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8, 8, 9, 9, 10, 10, 10, 10, 11, 11, 11, 11, 12, 12, 13, 13, 13, 13, 14, 14, 14, 14, 15, 15, 16, 16, 16, 16, 17, 17, 17, 17, 18, 18, 19, 19, 19, 19, 19, 19, 19, 20, 20, 20};
            return tabla[localizacion];
        } else {
            return i;
        }
    }

    public String tipoAtaque(int tipo) {
        String[] tabla = {"Fil", "Con", "Pen", "Cal", "Ele", "Fri", "Ene"};
        return tabla[tipo];
    }

    public final static String localización(int pos) {
        String[] tabla = {"el pectoral izquierdo", "el corazón", "el hombro izquierdo", "el hombro derecho", "el estomago ", "el costado abdominal derecho", "el costado abdominal izquierdo", "el antebrazo superior izquierdo", "el antebrazo inferior izquierdo", "la mano izquierda", "el antebrazo superior derecho", "el antebrazo inferior derecho", "la mano derecha", "el muslo izquierdo", "la pantorilla izquierda", "el pie izquierdo", "el muslo derecho", "la pantorilla derecha", "el pie derecho", "la cabeza ", "el cuello", "el ojo", "sin apuntar"};
        if (pos == -1) {
            return "sin apuntar";
        }
        return tabla[pos];
    }

    /*
       -1	 sin apuntar
        0	 el pectoral izquierdo
        1	 el corazón
        2	 el hombro izquierdo
        3	 el hombro derecho
        4	 el estomago
        5	 el costado abdominal derecho
        6	 el costado abdominal izquierdo
        7	 el antebrazo superior izquierdo
        8	 el antebrazo inferior izquierdo
        9	 la mano izquierda
        10	 el antebrazo superior derecho
        11	 el antebrazo inferior derecho
        12	 la mano derecha
        13	 el muslo izquierdo
        14	 la pantorilla izquierda
        15	 el pie izquierdo
        16	 el muslo derecho
        17	 la pantorilla derecha
        18	 el pie derecho
        19	 la cabeza
        20	 el cuello
        21	 el ojo

     */


    //Tiradas
    public static int tirada(int tipoDeEntidad, int nivelPifia) {
        return tirada(nivelPifia, 90, tipoDeEntidad);
    }

    public static int tirada(int tipoDeEntidad) {
        return tirada(-1, 999, tipoDeEntidad);
    }

    public static int tirada(int tipoDeEntidad, boolean atributo) {
        return (tirada(0, 999, tipoDeEntidad) / 10);
    }


    public static boolean abiertaDemigod(int comp) {

        String valor = String.valueOf(comp);

        if (valor.length() > 1) {
            if (valor.charAt(0) == valor.charAt(1)) {
                return true;
            }
        }
        return false;
    }

    public static int tirada() {
        return tirada(0, 3, 90);
    }

    public static int tirada(int tipoDeEntidad, int pifia, int abierta) {

        double resultDouble = Math.random() * 100;

        int result = (int) resultDouble;

        if (tipoDeEntidad == 3 & abiertaDemigod((int) result)) {
            result = result + tirada(tipoDeEntidad, 0, abierta);
        } else {
            if (result > abierta - 1) {
                mostrar("Abierta!");
                result = result + tirada(tipoDeEntidad, 0, abierta + 1);
            }
            if (result < pifia + 1) {
                mostrar("Pifia!");
                result = -((pifia + 1) - result) * (int) (Math.random() * 100);
            }
        }

        return (int) result;

    }

    //Modificadores

    public final static int calcularModificador(int valor_atributo) {
        try {
            int[] tabla = {-100, -30, -20, -10, -5, 0, 5, 5, 10, 10, 15, 20, 20, 25, 25, 30, 35, 35, 40, 40, 45};
            return tabla[valor_atributo];
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            return 45;
        }
    }

    //Dificultades:

    //Controles
    public static int ControlCaracteristica(int valor) {

        if (valor < 6) {
            return 0;
        }
        if (valor < 10 & valor >= 6) {
            return 1;
        }
        if (valor < 15 & valor >= 10) {
            return 2;
        }
        if (valor < 20 & valor >= 15) {
            return 3;
        }
        if (valor < 20) {
            return 4;
        }

        return -1;
    }

    public static final int[] dificultades = {20, 40, 80, 120, 140, 180, 240, 280, 320, 440};
    public static final int[] dificultades_atr = {6, 8, 10, 12, 15, 16, 18, 20, 22, 25};

    public static String DificultadStr(int resultado) {
        String[] tabla = {"Rutinario", "Fácil", "Media", "Difícil", "Muy difícil", "Absurdo", "Casí imposible ", "Imposible", "Inhumano", "Zen"};
        if (resultado < 20) {
            return "desastroso";
        }
        if (resultado < 40) {
            return tabla[0];
        }
        if (resultado < 80) {
            return tabla[1];
        }
        if (resultado < 120) {
            return tabla[2];
        }
        if (resultado < 140) {
            return tabla[3];
        }
        if (resultado < 180) {
            return tabla[4];
        }
        if (resultado < 240) {
            return tabla[5];
        }
        if (resultado < 280) {
            return tabla[6];
        }
        if (resultado < 320) {
            return tabla[7];
        }
        if (resultado < 440) {
            return tabla[8];
        } else {
            return tabla[9];
        }
    }

    public static int ControlHabilidad(int resultado) {
        if (resultado < 20) {
            return 0;
        }
        if (resultado < 40) {
            return 1;
        }
        if (resultado < 80) {
            return 2;
        }
        if (resultado < 120) {
            return 3;
        }
        if (resultado < 140) {
            return 4;
        }
        if (resultado < 180) {
            return 5;
        }
        if (resultado < 240) {
            return 6;
        }
        if (resultado < 280) {
            return 7;
        }
        if (resultado < 320) {
            return 8;
        }
        if (resultado < 440) {
            return 9;
        } else {
            return 10;
        }
    }


    public static int Velocidad(int tipoMov) {
        int[] tabla = {0, 1, 4, 8, 15, 20, 22, 25, 28, 32, 35, 40, 50, 80, 150, 250, 500, 1000, 5000, 25000, 100000, 100000, 100000, 100000};
        return tabla[tipoMov];
    }

    public static int CargaMax(int fuerza) {
        int[] tabla = {1, 10, 20, 40, 60, 120, 180, 260, 350, 420, 600, 1000, 3000, 25000, 100000, 500000, 2500000, 10000000, 150000000, 1500000000};
        return tabla[fuerza];

    }

    public static int Carga(int fuerza) {
        int[] tabla = {1, 5, 10, 15, 25, 40, 60, 80, 100, 150, 200, 350, 1000, 5000, 15000, 100000, 500000, 1000000, 10000000, 100000000};
        return tabla[fuerza];
    }

    public static int Tope(int x, int tope) {
        if (x > tope) {
            return tope;
        } else {
            return x;
        }
    }

    public static boolean inRange(int min, int max, int valor) {
        if (valor > min & max > valor) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean inRangeEq(int min, int max, int valor) {
        if (valor >= min & max >= valor) {
            return true;
        } else {
            return false;
        }
    }

    public static int Entre(int x, int min, int max) {

        if (x > max) {
            return max;
        }
        if (x < min) {
            return min;
        }
        return x;
    }


    public static TriplaVectores dificultades(ArrayList<Personaje> personajes_actuales, ArrayList<Personaje> personajes_disponibles, int indice_caracteristica, int dificultad, boolean dificultad_directa) {
        ArrayList<Integer> resultados = new ArrayList<>();
        ArrayList<String> descripciones = new ArrayList<>();
        ArrayList<String> nombres = new ArrayList<>();
        ArrayList<Integer> tiradas = new ArrayList<>();

        for (Personaje pj : personajes_actuales) {


            Personaje pjActual = pj;

            nombres.add(pj.getNombres());

            int habilidad = pjActual.resultado(indice_caracteristica, 0);

            resultados.add(habilidad);
            int dificultadAsuperar = 0;
            if (dificultad_directa) {
                dificultadAsuperar = dificultad;
            } else {
                if (indice_caracteristica > 7) {
                    dificultadAsuperar = dificultades[dificultad] - habilidad;
                } else {
                    dificultadAsuperar = dificultades_atr[dificultad] - habilidad;
                }
            }


            tiradas.add(dificultadAsuperar);

            descripciones.add("Posee una habilidad base de " + habilidad + ". \n" + pjActual.descripción(indice_caracteristica, dificultadAsuperar));

        }

        Integer[] resultadosArray = new Integer[resultados.size()];
        String[] descripcionesArray = new String[resultados.size()];
        String[] nombresArray = new String[resultados.size()];
        Integer[] tiradasArray = new Integer[resultados.size()];

        for (int i = 0; i < resultados.size(); i++) {
            resultadosArray[i] = resultados.get(i);
            descripcionesArray[i] = descripciones.get(i);
            nombresArray[i] = nombres.get(i);
            tiradasArray[i] = tiradas.get(i);
        }

        return new TriplaVectores(resultadosArray, descripcionesArray, nombresArray, tiradasArray);
    }

    public static TriplaVectores tiradas(ArrayList<Personaje> personajes_actuales, ArrayList<Personaje> personajes_disponibles, int indice_caracteristica) {
        ArrayList<Integer> resultados = new ArrayList<>();
        ArrayList<Integer> tiradas = new ArrayList<>();
        ArrayList<String> descripciones = new ArrayList<>();
        ArrayList<String> nombres = new ArrayList<>();


        for (Personaje pj : personajes_actuales) {

            Personaje pjActual = pj;
            int tirada = 0;
            if (indice_caracteristica > 7) {
                tirada = tirada(pjActual.getTipoDeEntidad(), pjActual.getNivelPifia(), 90);
            } else {
                tirada = (int) (Math.random() * 10) + 1;
            }

            tiradas.add(tirada);
            nombres.add(pj.getNombres());
            descripciones.add(pjActual.descripción(indice_caracteristica, tirada));
            resultados.add(pjActual.resultado(indice_caracteristica, tirada));
        }

        Integer[] resultadosArray = new Integer[resultados.size()];
        Integer[] tiradasArray = new Integer[resultados.size()];
        String[] descripcionesArray = new String[resultados.size()];
        String[] nombresArray = new String[resultados.size()];

        for (int i = 0; i < resultados.size(); i++) {
            resultadosArray[i] = resultados.get(i);
            descripcionesArray[i] = descripciones.get(i);
            nombresArray[i] = nombres.get(i);
            tiradasArray[i] = tiradas.get(i);
        }

        return new TriplaVectores(resultadosArray, descripcionesArray, nombresArray, tiradasArray);
    }

    public static final int DATOS_ARMA_ATACANTE = 0;
    public static final int DATOS_ARMA_DEFENSOR = 1;
    public static final int DATOS_TIPO_ATAQUE = 2;
    public static final int DATOS_TIPO_DEFENSA = 3;
    public static final int DATOS_APUNTADO = 4;
    public static final int DATOS_DISTANCIA = 5;
    public static final int DATOS_TIRADA_ATACANTE = 6;
    public static final int DATOS_TIRADA_DEFENSOR = 7;
    public static final int DATOS_TIRADA_BONIFICADOR_ATACANTE = 8;
    public static final int DATOS_TIRADA_BONIFICADOR_DEFENSOR = 9;
    public static final int NPC_PLUS_ATACANTE = 10;
    public static final int NPC_PLUS_DEFENSOR = 11;


    public static final int RESULTADO_TEXTO = 0;
    public static final int RESULTADO_DAÑO = 1;
    public static final int RESULTADO_LUGAR_DAÑADO = 2;
    public static final int RESULTADO_PENALIZADOR_DOLOR = 3;
    public static final int RESULTADO_BONIFICACION_CONTRAATAQUE = 4;

    public static final int[] Penalizadores_cantidad_Defensas = {0, -30, -50, -70, -90};

    public static final int SIN_APUNTAR = -1;
    public static final int EL_PECTORAL_IZQUIERDO = 0;
    public static final int EL_CORAZÓN = 1;
    public static final int EL_HOMBRO_IZQUIERDO = 2;
    public static final int EL_HOMBRO_DERECHO = 3;
    public static final int EL_ESTOMAGO = 4;
    public static final int EL_COSTADO_ABDOMINAL_DERECHO = 5;
    public static final int EL_COSTADO_ABDOMINAL_IZQUIERDO = 6;
    public static final int EL_ANTEBRAZO_SUPERIOR_IZQUIERDO = 7;
    public static final int EL_ANTEBRAZO_INFERIOR_IZQUIERDO = 8;
    public static final int LA_MANO_IZQUIERDA = 9;
    public static final int EL_ANTEBRAZO_SUPERIOR_DERECHO = 10;
    public static final int EL_ANTEBRAZO_INFERIOR_DERECHO = 11;
    public static final int LA_MANO_DERECHA = 12;
    public static final int EL_MUSLO_IZQUIERDO = 13;
    public static final int LA_PANTORILLA_IZQUIERDA = 14;
    public static final int EL_PIE_IZQUIERDO = 15;
    public static final int EL_MUSLO_DERECHO = 16;
    public static final int LA_PANTORILLA_DERECHA = 17;
    public static final int EL_PIE_DERECHO = 18;
    public static final int LA_CABEZA = 19;
    public static final int EL_CUELLO = 20;
    public static final int EL_OJO = 21;


    public static final boolean isExtremidad(int localizacion) {
        switch (localizacion) {
            default:
            case EL_CORAZÓN:
            case LA_CABEZA:
            case EL_CUELLO:
                return false;
            case EL_ANTEBRAZO_INFERIOR_DERECHO:
            case EL_ANTEBRAZO_INFERIOR_IZQUIERDO:
            case EL_ANTEBRAZO_SUPERIOR_DERECHO:
            case EL_ANTEBRAZO_SUPERIOR_IZQUIERDO:
            case LA_MANO_DERECHA:
            case LA_MANO_IZQUIERDA:
            case LA_PANTORILLA_DERECHA:
            case LA_PANTORILLA_IZQUIERDA:
            case EL_PIE_DERECHO:
            case EL_PIE_IZQUIERDO:
                return true;

        }
    }

    private static final int Penalizador_cantidad_defensas(int tipo) {//Devuelve el penalizador dependiendo de la cantidad de defensas que deba hacer en el turno

        switch (tipo) {
            case 0:
            case 5:
                return 0;

            case 1:
            case 6:
                return -30;

            case 2:
            case 7:
                return -50;

            case 3:
            case 8:
                return -70;

            case 4:
            case 9:
                return -90;
        }
        return 0;
    }


    public static String[] Ataque(Personaje atacante, Personaje defensor, int[] datos, ArrayList<Modificador> modificadoresAtacante, ArrayList<Modificador> modificadoresDefensor) {

        mostrar(Arrays.toString(datos));

        boolean defensorNPC = false;

        if (datos[NPC_PLUS_DEFENSOR] == 1) {
            defensorNPC = true;
        }

        boolean atacanteNPC = false;

        if (datos[NPC_PLUS_ATACANTE] == 1) {
            atacanteNPC = true;
        }

        String[] resultado = new String[5];
        Arrays.fill(resultado, "0");
        resultado[RESULTADO_TEXTO] = "";

        int ataqueFinal = datos[DATOS_TIRADA_BONIFICADOR_ATACANTE] + datos[DATOS_TIRADA_ATACANTE];
        int defensaFinal = datos[DATOS_TIRADA_BONIFICADOR_DEFENSOR] + datos[DATOS_TIRADA_DEFENSOR];

        mostrar("Modificadores atacante: " + Arrays.toString(modificadoresAtacante.toArray()));

        mostrar("Modificadores defensor: " + Arrays.toString(modificadoresDefensor.toArray()));

        for (Modificador modificador : modificadoresAtacante) {

            ataqueFinal = ataqueFinal + modificador.getCaracteristicas()[Modificador.MODIFICADOR.ATAQUE];

        }
        mostrar("Ataque = " + ataqueFinal + "\r\n" + "Defensa = " + defensaFinal + " (617)");
        int tipoDeAtaque = 0;
        int porcentualDaño = 0;
        int dañoProducido = 0;
        Arma arma_atacante = new Arma();
        Arma arma_defensor = new Arma();
        int TA_defensor = 0;
        int RF = 0;
        int vida_actual_defensor = 150;
        boolean ataqueSinTA = false;

        boolean parada = false;

        switch (datos[DATOS_TIPO_ATAQUE]) {
            case 2: //Ignora armadura
                ataqueSinTA = true;
            case 0:
            case 1:


                if (datos[DATOS_APUNTADO] == 21) {//TODO REARMAR ESTOS LISTADOS
                    datos[DATOS_APUNTADO] = localizacion(-1);
                } else if (datos[DATOS_APUNTADO] == 22) {
                    ataqueSinTA = true;
                    ataqueFinal = ataqueFinal - 100;
                } else {
                    ataqueFinal = ataqueFinal + DIFICULTADES[datos[DATOS_APUNTADO]];
                }

                mostrar("Ataque = " + ataqueFinal + "\r\n" + "Defensa = " + defensaFinal + " (639)");

                resultado[RESULTADO_LUGAR_DAÑADO] = String.valueOf(datos[DATOS_APUNTADO]);

                int zonaAtacada = ARMADURA_LOCALIZACION_GENERAL[datos[DATOS_APUNTADO]];

                if (datos[NPC_PLUS_DEFENSOR] != 1) {
                    arma_defensor = defensor.arma(datos[DATOS_ARMA_DEFENSOR]);

                    if (datos[DATOS_TIPO_DEFENSA] <= 4) {
                        parada = true;
                    }

                    for (Modificador modificador : modificadoresDefensor) {

                        if (parada) {
                            defensaFinal = defensaFinal + modificador.getCaracteristicas()[Modificador.MODIFICADOR.PARADA];
                        } else {
                            defensaFinal = defensaFinal + modificador.getCaracteristicas()[Modificador.MODIFICADOR.ESQUIVA];
                        }

                    }


                    defensaFinal = defensaFinal + Penalizador_cantidad_defensas(datos[DATOS_TIPO_DEFENSA]) + defensor.Penalizadores(Personaje.PENALIZADORES.TODA_ACCION);

                    RF = defensor.Caracteristica(RESISTENCIA_FISICA) + tirada(defensor.getTipoDeEntidad(), defensor.getNivelPifia(), 90);

                    vida_actual_defensor = defensor.getCaracteristicas(VIDA_ACT);

                    defensaFinal = defensaFinal + defensor.mejorDefensa(parada, datos[DATOS_ARMA_DEFENSOR]);

                    if ((arma_atacante.getCaracteristicas()[Arma.ROTURA_ARMA] + (tirada() / 10)) - (arma_defensor.getCaracteristicas()[Arma.ENTEREZA_ARMA]) > 0 & parada) {
                        resultado[0] = "El arma del defensor se rompe en el proceso y ";
                    }

                    mostrar("Ataque = " + ataqueFinal + "\r\n" + "Defensa = " + defensaFinal + " (665)");

                } else {

                    if (datos[DATOS_TIPO_DEFENSA] < 100) {
                        vida_actual_defensor = (datos[DATOS_TIPO_DEFENSA] * 10) + 10;
                    } else {
                        vida_actual_defensor = ((datos[DATOS_TIPO_DEFENSA] - 100) * 100) + 1000;
                    }
                    if (ataqueSinTA) {
                        TA_defensor = 0;
                    } else {
                        TA_defensor = datos[DATOS_ARMA_DEFENSOR];
                    }

                    RF = tirada(0, 3, 90) + (datos[DATOS_ARMA_DEFENSOR] * 15);

                    mostrar("Ataque = " + ataqueFinal + "\r\n" + "Defensa = " + defensaFinal + " (683)");
                }

                if (!atacanteNPC) {
                    ataqueFinal = ataqueFinal + atacante.Penalizadores(Personaje.PENALIZADORES.TODA_ACCION) + atacante.Caracteristica(HABILIDAD_ATAQUE);

                    arma_atacante.reemplazar(atacante.arma(datos[DATOS_ARMA_ATACANTE]));

                    switch (datos[DATOS_TIPO_ATAQUE]) {//TODO COMPLETAR EL RESTO DE LOS TIPOS DE ATAQUE
                        case 0:
                            tipoDeAtaque = arma_atacante.getCaracteristicas()[Arma.CRIT_PRINCIPAL];
                            break;
                        case 1:
                            tipoDeAtaque = arma_atacante.getCaracteristicas()[Arma.CRIT_SECUNDARIO];
                            ataqueFinal = ataqueFinal - 10;
                            break;
                    }

                    dañoProducido = atacante.dañoBaseArma(datos[DATOS_ARMA_ATACANTE]);

                    if (datos[NPC_PLUS_DEFENSOR] != 1 & !ataqueSinTA) {
                        TA_defensor = defensor.TA(tipoDeAtaque, zonaAtacada);
                    } else {
                        TA_defensor = 0;
                    }

                    if (TA_defensor > arma_atacante.getCaracteristicas()[Arma.TA_IGNORADA]) {
                        TA_defensor = TA_defensor - arma_atacante.getCaracteristicas()[Arma.TA_IGNORADA];
                    } else {
                        TA_defensor = 0;
                    }

                    mostrar("Ataque = " + ataqueFinal + "\r\n" + "Defensa = " + defensaFinal + " (717)");

                } else {
                    arma_atacante.setCaracteristicas((datos[DATOS_ARMA_ATACANTE] * 10) + 10, Arma.DAÑO_BASE);
                    tipoDeAtaque = datos[DATOS_TIPO_ATAQUE];
                    dañoProducido = (datos[DATOS_ARMA_ATACANTE] * 10) + 10;

                    if (!defensorNPC) {
                        TA_defensor = defensor.TA(tipoDeAtaque, zonaAtacada);
                    }

                    mostrar("Ataque = " + ataqueFinal + "\r\n" + "Defensa = " + defensaFinal + " (729)");

                }

                mostrar("Resultados: " + ataqueFinal + " vs " + defensaFinal);

                int resultadoContienda = ataqueFinal - defensaFinal;

                mostrar("Ataque = " + ataqueFinal + "\r\n" + "Defensa = " + defensaFinal + " (739)");

                if (barreraDeDaño(defensor, dañoProducido)) {
                    if (resultadoContienda > 10) {
                        resultado[0] = resultado[0] + "el ataque impacta (" + resultadoContienda + ")";
                        if (ataqueSinTA) {
                            porcentualDaño = resultadoContienda;
                        } else {
                            porcentualDaño = resultadoContienda - (TA_defensor * 10);
                        }
                        if (porcentualDaño < 10) {
                            resultado[0] = resultado[0] + ", pero no produce daños gracias a la TA (" + TA_defensor + ")";
                        } else {
                            mostrar(arma_atacante.toString());
                            mostrar("daño del arma: " + dañoProducido);

                            dañoProducido = PorcentualAtaque(porcentualDaño, dañoProducido);

                            mostrar("Porcentual: " + porcentualDaño + " entonces el daño será: " + dañoProducido);
                            resultado[RESULTADO_DAÑO] = String.valueOf(dañoProducido);

                            resultado[0] = resultado[0] + " produciendo daños de " + resultado[RESULTADO_DAÑO] + " PV en " + localización(datos[DATOS_APUNTADO]) + ".";

                            String[] critico = critico(dañoProducido, RF, vida_actual_defensor, datos[DATOS_APUNTADO]);

                            resultado[0] = resultado[0] + critico[0];
                            resultado[RESULTADO_PENALIZADOR_DOLOR] = critico[1];

                            if (!defensorNPC & !critico[1].equals("0")) {

                                int resultado_Res_dolor = defensor.resultado(HABILIDAD_RESDOLOR);

                                int ResDolor = Integer.valueOf(critico[1]) + resultado_Res_dolor;

                                if (resultado_Res_dolor != 0) {
                                    mostrar("Resistir el dolor: " + resultado_Res_dolor + " (" + defensor.Caracteristica(HABILIDAD_RESDOLOR) + ") vs " + critico[1]);

                                    if (ResDolor > 0) {
                                        resultado[RESULTADO_PENALIZADOR_DOLOR] = "0";
                                    } else {
                                        resultado[RESULTADO_PENALIZADOR_DOLOR] = String.valueOf(ResDolor);
                                    }

                                    resultado[0] = resultado[0] + " Los penalizadores por dolor se ven reducidos a " + resultado[RESULTADO_PENALIZADOR_DOLOR];
                                }
                            }
                        }

                        if (!defensorNPC)


                            if (vida_actual_defensor - dañoProducido < -defensor.getCaracteristicas(ATRIBUTO_CONSTITUCIÓN) * 5) {
                                resultado[0] = resultado[0] + " El personaje muere, su alma queda por " + defensor.getCaracteristicas(ATRIBUTO_PODER) + " horas antes volcarse al flujo.";
                            }


                    } else if (resultadoContienda < 0) {
                        resultado[0] = resultado[0] + "el defensor contraataca (" + resultadoContienda + ")";
                        int contra = (resultadoContienda / 2);
                        resultado[0] = resultado[0] + " obteniendo un bono de " + -redondeo5(contra) + ".";

                        resultado[RESULTADO_BONIFICACION_CONTRAATAQUE] = String.valueOf(-redondeo5(contra));
                    } else {
                        resultado[0] = resultado[0] + "los personajes quedan enfrascados en combate, el defensor pierde capacidad de ejecutar acciones activas este turno";
                    }
                } else {
                    resultado[0] = resultado[0] + " el defensor ni parece inmutarse por los ataques (barrera de daño)";
                }
                break;
            case 3: //Derribo
            case 4: //Inutilizar
            case 5: //Desarmar
            case 6: //Presa
            case 7: //inconsciencia
            case 8: //engatillar


        }
        return resultado;
    }


    public static final int floor(int base, int divisor) {

        return (int) Math.floor(base / divisor);

    }

    private static final String[] critico(int daño, int resistFisica, int vida, int localizacion) {
        String[] resultado = new String[2];
        resultado[0] = "";
        resultado[1] = "0";
        resistFisica = resistFisica + tirada(0, 3, 90);

        if (daño > vida / Divisor_crítico(localizacion)) {
            resultado[0] = resultado[0] + " Produce un daño crítico";

            int difCritico = daño + tirada(0, 0, 999);

            if (difCritico > 200) {
                difCritico = 200 + ((difCritico - 200) / 2);
            }

            int diferencia = redondeo5(resistFisica - difCritico);

            mostrar("Diferencia: " + diferencia + " (" + resistFisica + " - " + difCritico + ") ");

            if (diferencia > 0) {
                resultado[0] = resultado[0] + ", pero queda anulado gracias a la RF.";
            } else if (diferencia > -100) {
                resultado[0] = resultado[0] + ", que es un doloroso impacto que produce penalizadores de " + (diferencia);
                resultado[1] = String.valueOf(diferencia);
            } else if (diferencia < -150) {
                resultado[0] = resultado[0] + ", que destruye el lugar impactado, si es un miembro queda amputado, y si es el corazón o la cabeza, el personaje muere. Aparte produce penalizadores de " + (diferencia);
                resultado[1] = String.valueOf(diferencia);
            } else {
                resultado[0] = resultado[0] + ", que destruye el lugar impactado, si es un miembro queda amputado y el personaje cae inconsciente sin importar donde haya pegado. Si es el corazón o la cabeza, el personaje muere. Aparte produce penalizadores de " + (diferencia);
                resultado[1] = String.valueOf(diferencia);
            }
        } else {
            resultado[0] = "";
            resultado[1] = "0";
        }
        mostrar("resultado: " + Arrays.toString(resultado));
        return resultado;
    }


    public static final int redondeo5(int valor) {
        int resultado = 0;
        float temp = (float) valor;

        temp = temp / 5;

        resultado = (Math.round(temp)) * 5;

        return resultado;
    }

    private static final void mostrar(String texto) {
        if (MODO_DE_PRUEBAS) {
            System.out.println(texto);
        }
    }

    private static final int PorcentualAtaque(int porcentual, int dañoBase) {
        float temp = porcentual * dañoBase;
        temp = temp / 100;
        return Math.round(temp);

    }

    private static final boolean barreraDeDaño(Personaje personaje, int dañoBase) {

        if (personaje != null) {
            if (dañoBase < personaje.getCaracteristicas(BARRERA_DE_DAÑO)) {
                return false;
            }
        }
        return true;
    }

    public static final int[] generadorAtributos(int tipo) {
        int[] atributos = new int[8];
        switch (tipo) {
            case 0:// tradicional
                for (int i = 0; i < 8; i++) {
                    atributos[i] = tiradaAtrib(tipo);
                }
                Arrays.sort(atributos);

                atributos[0] = 9;
                break;
            case 1: // doble tirada
                for (int i = 0; i < 8; i++) {
                    atributos[i] = tiradaAtrib(tipo);
                }
                Arrays.sort(atributos);
            case 2: // tirada normal
                for (int i = 0; i < 8; i++) {
                    atributos[i] = tiradaAtrib(tipo);
                }
                break;
            case 3: //sumatoria
                for (int i = 0; i < 7; i++) {
                    atributos[0] = tiradaAtrib(tipo) + atributos[0];
                }
        }

        mostrar(Arrays.toString(atributos));

        return atributos;
    }

    private static final int tiradaAtrib(int tipo) {

        int resultado = (tirada(0, -100, 999) / 10);
        int resultadoAux = (tirada(0, -100, 999) / 10);
        if (resultado == 0) {
            resultado = 10;
        }

        if (resultadoAux == 0) {
            resultadoAux = 10;
        }
        switch (tipo) {
            case 0:
                if (resultado < 4 | resultado > 10) {
                    resultado = tiradaAtrib(tipo);
                }
                break;
            case 1:
                if (resultadoAux > resultado) {
                    resultado = resultadoAux;
                }
            case 2:
            case 3:
                break;


        }


        return resultado;
    }


    public static final String intToTextFormat(int entrada, int tamaño) {

        String salida = String.valueOf(entrada);

        while (salida.length() < tamaño) {
            salida = " " + salida;
        }

        return salida;
    }

    public static final int intToindexPorcentualCinco(int valor) {

        return Math.round((float) valor / 5);
    }

    public static final int StringToInt(String entrada) {
        return Integer.valueOf(entrada);
    }

}
