package com.nugganet.arget.mastertoolkit.Controladores;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.android.material.snackbar.Snackbar;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Armadura;
import com.nugganet.arget.mastertoolkit.Principal;
import com.nugganet.arget.mastertoolkit.R;

public class Controlador_creación_armaduras implements View.OnClickListener {

    final Principal p;
    private Armadura armadura_delete;

    public Controlador_creación_armaduras(Principal p) {
        this.p = p;
    }

    public void initListeners() {
        p.findViewById(R.id.button_Cargar_armadura).setOnClickListener(this);
        p.findViewById(R.id.button_Eliminar_armadura).setOnClickListener(this);
        p.findViewById(R.id.button_GuardarNuevo_armadura).setOnClickListener(this);
        p.findViewById(R.id.button_Modificar_armadura).setOnClickListener(this);
    }


    public void initArmaduras() {

        String[] nombres_armaduras = p.getResources().getStringArray(R.array.Datos_Creación_Nombres_Armaduras);
        int[] valores_armaduras = p.getResources().getIntArray(R.array.Datos_Creación_Bases_Armaduras);

        for (int i = 1; i < nombres_armaduras.length + 1; i++) {

            String ViewID = "cr_pj_armaduras_7_" + i;

            int resID = p.getResources().getIdentifier(ViewID, "id", p.getPackageName());

            ((TextView) p.findViewById(resID).findViewById(R.id.cr_pj_c_nombre)).setText(nombres_armaduras[i - 1]);
            ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_temp)).setVisibility(View.GONE);

            if (valores_armaduras[i - 1] == -6 | valores_armaduras[i - 1] == -4 | valores_armaduras[i - 1] == -5) {//spinner

                ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setVisibility(View.GONE);
                ((Spinner) p.findViewById(resID).findViewById(R.id.cr_pj_c_spinner)).setVisibility(View.VISIBLE);

                switch (valores_armaduras[i - 1]) {
                    case -4:
                        ((Spinner) p.findViewById(resID).findViewById(R.id.cr_pj_c_spinner)).setAdapter(ArrayAdapter.createFromResource(p, R.array.Tipos_de_armas, android.R.layout.simple_spinner_dropdown_item));
                        break;
                    case -5:
                        ((Spinner) p.findViewById(resID).findViewById(R.id.cr_pj_c_spinner)).setAdapter(ArrayAdapter.createFromResource(p, R.array.Tipos_de_ataque, android.R.layout.simple_spinner_dropdown_item));
                        break;
                    case -6:
                        ((Spinner) p.findViewById(resID).findViewById(R.id.cr_pj_c_spinner)).setAdapter(ArrayAdapter.createFromResource(p, R.array.Efectos_armas, android.R.layout.simple_spinner_dropdown_item));
                        break;

                }


            } else if (valores_armaduras[i - 1] == -3) {//checkbox

                ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setVisibility(View.GONE);
                ((ToggleButton) p.findViewById(resID).findViewById(R.id.cr_pj_c_toggleButton)).setVisibility(View.VISIBLE);

            } else {


                ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setText(String.valueOf(valores_armaduras[i - 1]));


            }
        }
    }

    @Override
    public void onClick(View v) {

        int seleccionado = ((Spinner) p.findViewById(R.id.spinner_seleccion_armadura)).getSelectedItemPosition();
        Snackbar mensajeConfirmación;
        View view;
        TextView tv;

        switch (v.getId()) {
            case R.id.button_Cargar_armadura:

                if (((Spinner) p.findViewById(R.id.spinner_seleccion_armadura)).getSelectedItemPosition() != -1) {

                    Armadura temp = p.getArrayArmaduras().get(seleccionado);

                    p.getControlador_creac_pj().setArmadura(7, temp);

                } else {
                    p.showSnack(Snackbar.make(v, "No se pudo cargar.", Snackbar.LENGTH_SHORT));
                }

                break;

            case R.id.button_GuardarNuevo_armadura:

                Armadura nueva_armadura = p.getControlador_creac_pj().getArmadura(7);

                nueva_armadura.setID(p.getDataBase().AñadirArmadura(nueva_armadura));

                p.getArrayArmaduras().add(nueva_armadura);

                p.showSnack(Snackbar.make(v, "Armadura " + nueva_armadura.getNombre() + " guardada.", Snackbar.LENGTH_SHORT));

                break;

            case R.id.button_Modificar_armadura:

                final int index_armadura = seleccionado;

                if (p.getArrayArmaduras().size() != 0 & seleccionado != -1) {

                    if (seleccionado < p.getArrayArmaduras().size()) {

                        Armadura armadura_modificada = p.getControlador_creac_pj().getArmadura(7);

                        armadura_delete = new Armadura().fromJson(armadura_modificada.toJson());

                        mensajeConfirmación = Snackbar
                                .make(v, "Armadura " + p.getArrayArmaduras().get(seleccionado).getNombre() + " modificada a " + armadura_modificada.getNombre(), Snackbar.LENGTH_LONG)
                                .setAction("DESHACER", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        p.getArrayArmaduras().get(index_armadura).reemplazar(armadura_delete);

                                        p.getDataBase().ModificarArmadura(armadura_delete);

                                        p.actualizarArrayArmaduras(R.id.spinner_seleccion_armadura);

                                        ((Spinner) p.findViewById(R.id.spinner_seleccion_armadura)).setSelection(index_armadura);

                                        p.showSnack(Snackbar.make(view, "Restaurado", Snackbar.LENGTH_SHORT));
                                    }

                                });

                        p.getArrayArmaduras().get(seleccionado).reemplazar(armadura_modificada);

                        p.getDataBase().ModificarArmadura(p.getArrayArmaduras().get(seleccionado));

                        p.actualizarArrayArmaduras(R.id.spinner_seleccion_armadura);

                        ((Spinner) p.findViewById(R.id.spinner_seleccion_armadura)).setSelection(seleccionado);

                        view = mensajeConfirmación.getView();

                        tv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                        tv.setTextColor(p.getResources().getColor(R.color.colorWhite));

                        mensajeConfirmación.setActionTextColor(p.getResources().getColor(R.color.colorWhite));

                        mensajeConfirmación.show();
                    }
                } else {
                    p.showSnack(Snackbar.make(v, "No hay ningún elemento que modificar", Snackbar.LENGTH_SHORT));
                }

                break;

            case R.id.button_Eliminar_armadura:

                if (p.getArrayArmaduras().size() != 0 & seleccionado != -1) {

                    if (seleccionado < p.getArrayArmaduras().size()) {

                        mensajeConfirmación = Snackbar
                                .make(v, "Armadura " + p.getArrayArmaduras().get(seleccionado).getNombre() + " eliminada", Snackbar.LENGTH_LONG)
                                .setAction("DESHACER", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        p.getArrayArmaduras().add(armadura_delete);

                                        p.getDataBase().AñadirArmadura(armadura_delete);

                                        p.actualizarArrayArmaduras(R.id.spinner_seleccion_armadura);

                                        p.showSnack(Snackbar.make(view, "Restaurado", Snackbar.LENGTH_SHORT))
                                        ;
                                    }

                                });

                        armadura_delete = p.getArrayArmaduras().get(seleccionado);

                        p.getDataBase().EliminarArmadura(p.getArrayArmaduras().get(seleccionado).getID());

                        p.getArrayArmaduras().remove(seleccionado);

                        p.actualizarArrayArmaduras(R.id.spinner_seleccion_armadura);

                        view = mensajeConfirmación.getView();

                        tv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                        tv.setTextColor(p.getResources().getColor(R.color.colorWhite));

                        mensajeConfirmación.setActionTextColor(p.getResources().getColor(R.color.colorWhite));

                        mensajeConfirmación.show();
                    }
                } else {
                    p.showSnack(Snackbar.make(v, "No hay ningún elemento que eliminar", Snackbar.LENGTH_SHORT));

                }

                break;
        }

        p.actualizarArrayArmaduras(R.id.spinner_seleccion_armadura);

        if (seleccionado < p.getArrayArmaduras().size()) {
            ((Spinner) p.findViewById(R.id.spinner_seleccion_armadura)).setSelection(seleccionado);
        }
    }

}
