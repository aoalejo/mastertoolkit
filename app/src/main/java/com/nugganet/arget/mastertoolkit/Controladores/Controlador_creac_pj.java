package com.nugganet.arget.mastertoolkit.Controladores;

import android.app.FragmentManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import androidx.core.widget.NestedScrollView;
import android.util.TypedValue;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.android.material.snackbar.Snackbar;
import com.nugganet.arget.mastertoolkit.Base;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Arma;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Armadura;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Personaje;
import com.nugganet.arget.mastertoolkit.Custom_adapters.Fragment_classes;
import com.nugganet.arget.mastertoolkit.Principal;
import com.nugganet.arget.mastertoolkit.R;

import java.util.Arrays;

import Utils.DbBitmapUtility;

public class Controlador_creac_pj implements NestedScrollView.OnScrollChangeListener, View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    private final int alturaBase_customs;
    private final int alturaBase_pad;
    private final int alturaBase_buttons;
    private final Principal p;
    private final int[] radioGroup_armas = {R.id.radioButton_cr_pj_armas_arma1, R.id.radioButton_cr_pj_armas_arma2, R.id.radioButton_cr_pj_armas_arma3, R.id.radioButton_cr_pj_armas_arma4, R.id.radioButton_cr_pj_armas_arma5, R.id.radioButton_cr_pj_armas_arma6};

    private final int[] EditText_Nombres_armas = {R.id.cr_pj_arma_1_nombre, R.id.cr_pj_arma_2_nombre, R.id.cr_pj_arma_3_nombre, R.id.cr_pj_arma_4_nombre, R.id.cr_pj_arma_5_nombre, R.id.cr_pj_arma_6_nombre, R.id.cr_pj_arma_7_nombre};

    private final int[] radioGroup_armaduras = {R.id.radioButton_cr_pj_armaduras_1, R.id.radioButton_cr_pj_armaduras_2, R.id.radioButton_cr_pj_armaduras_3, R.id.radioButton_cr_pj_armaduras_4, R.id.radioButton_cr_pj_armaduras_5, R.id.radioButton_cr_pj_armaduras_6};

    private final int[] EditText_Nombres_armaduras = {R.id.cr_pj_armadura_1_nombre, R.id.cr_pj_armadura_2_nombre, R.id.cr_pj_armadura_3_nombre, R.id.cr_pj_armadura_4_nombre, R.id.cr_pj_armadura_5_nombre, R.id.cr_pj_armadura_6_nombre, R.id.cr_pj_armadura_7_nombre};

    private final int[] buttons_puntos = {R.id.cr_pj_puntos_p1, R.id.cr_pj_puntos_p2, R.id.cr_pj_puntos_p3, R.id.cr_pj_puntos_p4, R.id.cr_pj_puntos_p5, R.id.cr_pj_puntos_p6, R.id.cr_pj_puntos_p7, R.id.cr_pj_puntos_p8, R.id.cr_pj_puntos_p9, R.id.cr_pj_puntos_p10, R.id.cr_pj_puntos_p11, R.id.cr_pj_puntos_p12, R.id.cr_pj_puntos_p13};
    private final int[] buttons_psquicos = {R.id.cr_pj_psiquicos_0, R.id.cr_pj_psiquicos_1, R.id.cr_pj_psiquicos_2, R.id.cr_pj_psiquicos_3, R.id.cr_pj_psiquicos_4, R.id.cr_pj_psiquicos_5, R.id.cr_pj_psiquicos_6, R.id.cr_pj_psiquicos_7, R.id.cr_pj_psiquicos_8, R.id.cr_pj_psiquicos_9, R.id.cr_pj_psiquicos_10, R.id.cr_pj_psiquicos_11, R.id.cr_pj_psiquicos_12, R.id.cr_pj_psiquicos_13, R.id.cr_pj_psiquicos_14};
    private final int[] buttons_armas = {R.id.cr_pj_armas_cargar, R.id.cr_pj_armas_borrar_todo};
    private final int[] alturas_puntos = {-1, 10, 17, 26, 39, 47, 55, 63, 67, 80, 84, 92, 101, 150, 999};
    private final int[] alturas_psiquicos = {-1, 4, 20, 35, 47, 61, 75, 87, 98, 103, 109, 119, 127, 134, 138, 999};

    private long lastUpdateNestedScroll = System.currentTimeMillis();

    private Personaje personaje_delete = new Personaje();

    private int seleccionado = -1;

    private String[] nombres_pen = {"A toda acción", "Temporales", "Visuales"};

    private Personaje dummyTest() {
        Personaje temp = new Personaje();

        temp.setNombre_personaje("Nombre personaje");
        temp.setNombre_jugador("Nombre jugador");

        int[] temp_vector = temp.getCaracteristicas();

        for (int i = 0; i < temp_vector.length; i++) {
            temp_vector[i] = i + 200;
        }

        temp.setCaracteristicas(temp_vector);

        temp_vector = temp.getCaracteristicas_temporales();

        for (int i = 0; i < temp_vector.length; i++) {
            temp_vector[i] = i + 100;
        }

        temp.setCaracteristicas_temporales(temp_vector);

        for (int i = 0; i < 6; i++) {
            Arma a = new Arma("Slot de arma " + (i + 1));
            temp.setArma(a, i);
        }

        for (int i = 0; i < 6; i++) {
            Armadura a = new Armadura("Slot de armadura " + (i + 1));
            temp.setArmadura(a, i);
        }

        temp_vector = temp.getPenalizadores();
        for (int i = 0; i < temp_vector.length; i++) {
            temp_vector[i] = i + 300;
        }

        temp.setPenalizadores(temp_vector);

        temp_vector = temp.getEspeciales();
        for (int i = 0; i < temp_vector.length; i++) {
            temp_vector[i] = i + 400;
        }

        temp.setToken(p.getToken_raw(1));

        temp.setTipoDeEntidad(1);
        temp.setTipoDePersonaje(1);

        temp.setNivelPifia(40);

        return temp;
    }

    public void setArma(int index, Arma arma) {

        int[] valores_armas = p.getResources().getIntArray(R.array.Datos_Creación_Bases_Arma);

        if (index < 7) {
            ((RadioButton) p.findViewById(radioGroup_armas[index - 1])).setText(arma.getNombre());
        }

        ((EditText) p.findViewById(EditText_Nombres_armas[index - 1])).setText(arma.getNombre());

        for (int i = 1; i < valores_armas.length + 1; i++) {

            String ViewID = "cr_pj_armas_" + index + "_" + i;

            int resID = p.getResources().getIdentifier(ViewID, "id", p.getPackageName());

            int value = arma.getCaracteristicas()[i - 1];

            if (valores_armas[i - 1] == -6 | valores_armas[i - 1] == -4 | valores_armas[i - 1] == -5 | valores_armas[i - 1] == -7) {//spinner

                ((Spinner) p.findViewById(resID).findViewById(R.id.cr_pj_c_spinner)).setSelection(value);

            } else if (valores_armas[i - 1] == -3) {//checkbox

                ((ToggleButton) p.findViewById(resID).findViewById(R.id.cr_pj_c_toggleButton)).setChecked(p.intToBool(value));

            } else {

                ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setText(String.valueOf(value));

            }
        }
    }

    public Arma getArma(int index) {

        Arma temp = new Arma();

        int[] valores_armas = p.getResources().getIntArray(R.array.Datos_Creación_Bases_Arma);

        temp.setNombre(((EditText) p.findViewById(EditText_Nombres_armas[index - 1])).getText().toString());

        for (int i = 1; i < valores_armas.length + 1; i++) {
            try {
                String ViewID = "cr_pj_armas_" + index + "_" + i;

                int resID = p.getResources().getIdentifier(ViewID, "id", p.getPackageName());

                if (valores_armas[i - 1] == -6 | valores_armas[i - 1] == -4 | valores_armas[i - 1] == -5 | valores_armas[i - 1] == -7) {//spinner

                    temp.reemplazar_valor_caracteristica(i - 1, ((Spinner) p.findViewById(resID).findViewById(R.id.cr_pj_c_spinner)).getSelectedItemPosition());

                } else if (valores_armas[i - 1] == -3) {//checkbox

                    temp.reemplazar_valor_caracteristica(i - 1, p.boolToInt(((ToggleButton) p.findViewById(resID).findViewById(R.id.cr_pj_c_toggleButton)).isChecked()));

                } else {
                    temp.reemplazar_valor_caracteristica(i - 1, Integer.parseInt(((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).getText().toString()));
                }
            } catch (Exception e) {
                e.printStackTrace();
                temp.reemplazar_valor_caracteristica(i - 1, 0);
            }
        }
        return temp;

    }

    public void setArmadura(int index, Armadura armadura) {

        int[] valores_armaduras = p.getResources().getIntArray(R.array.Datos_Creación_Bases_Armaduras);

        if (index < 7) {
            ((RadioButton) p.findViewById(radioGroup_armaduras[index - 1])).setText(armadura.getNombre());
        }

        ((EditText) p.findViewById(EditText_Nombres_armaduras[index - 1])).setText(armadura.getNombre());

        for (int i = 1; i < valores_armaduras.length + 1; i++) {

            String ViewID = "cr_pj_armaduras_" + index + "_" + i;

            int resID = p.getResources().getIdentifier(ViewID, "id", p.getPackageName());

            int value = armadura.getCaracteristicas()[i - 1];

            if (valores_armaduras[i - 1] == -6 | valores_armaduras[i - 1] == -4 | valores_armaduras[i - 1] == -5) {//spinner

                ((Spinner) p.findViewById(resID).findViewById(R.id.cr_pj_c_spinner)).setSelection(value);

            } else if (valores_armaduras[i - 1] == -3) {//checkbox

                ((ToggleButton) p.findViewById(resID).findViewById(R.id.cr_pj_c_toggleButton)).setChecked(p.intToBool(value));

            } else {

                ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setText(String.valueOf(value));

            }

        }
    }

    public Armadura getArmadura(int index) {

        Armadura temp = new Armadura();

        int[] valores_armaduras = p.getResources().getIntArray(R.array.Datos_Creación_Bases_Armaduras);

        temp.setNombre(((EditText) p.findViewById(EditText_Nombres_armaduras[index - 1])).getText().toString());

        for (int i = 1; i < valores_armaduras.length + 1; i++) {
            try {
                String ViewID = "cr_pj_armaduras_" + index + "_" + i;

                int resID = p.getResources().getIdentifier(ViewID, "id", p.getPackageName());

                if (valores_armaduras[i - 1] == -6 | valores_armaduras[i - 1] == -4 | valores_armaduras[i - 1] == -5) {//spinner

                    temp.reemplazar_valor_caracteristica(i - 1, ((Spinner) p.findViewById(resID).findViewById(R.id.cr_pj_c_spinner)).getSelectedItemPosition());

                } else if (valores_armaduras[i - 1] == -3) {//checkbox

                    temp.reemplazar_valor_caracteristica(i - 1, p.boolToInt(((ToggleButton) p.findViewById(resID).findViewById(R.id.cr_pj_c_toggleButton)).isChecked()));

                } else {

                    temp.reemplazar_valor_caracteristica(i - 1, Integer.parseInt(((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).getText().toString()));

                }
            } catch (Exception e) {
                e.printStackTrace();
                temp.reemplazar_valor_caracteristica(i - 1, 0);

            }

        }

        return temp;

    }

    public Personaje getPj() {

        Personaje temp = new Personaje();

        try {

            //PRINCIPAL

            temp.setNombre_jugador(((EditText) p.findViewById(R.id.EditText_Nombre_Jugador)).getText().toString());

            temp.setNombre_personaje(((EditText) p.findViewById(R.id.EditText_Nombre_Personaje)).getText().toString());

            try {
                Bitmap bitmap = ((BitmapDrawable) ((ImageButton) p.findViewById(R.id.imageButton_cambiar_imagen)).getDrawable()).getBitmap();

                temp.setToken(DbBitmapUtility.getBytes(bitmap));
            } catch (NullPointerException e) {
                e.printStackTrace();
            }


            temp.setDescripción(((EditText) p.findViewById(R.id.EditText_Descripción_pj)).getText().toString());

            temp.setNivelPifia(((Spinner) p.findViewById(R.id.spinner_cr_pj_ppal_pifia)).getSelectedItemPosition() + 1);

            if (((RadioButton) p.findViewById(R.id.radioButton_cr_pj_ppal_clasePj_1)).isChecked()) {
                temp.setTipoDeEntidad(0);
            }
            if (((RadioButton) p.findViewById(R.id.radioButton_cr_pj_ppal_clasePj_2)).isChecked()) {
                temp.setTipoDeEntidad(1);
            }
            if (((RadioButton) p.findViewById(R.id.radioButton_cr_pj_ppal_clasePj_3)).isChecked()) {
                temp.setTipoDeEntidad(2);
            }

            if (((RadioButton) p.findViewById(R.id.radioButton_cr_pj_ppal_tipoPj_1)).isChecked()) {
                temp.setTipoDePersonaje(0);
            }
            if (((RadioButton) p.findViewById(R.id.radioButton_cr_pj_ppal_tipoPj_2)).isChecked()) {
                temp.setTipoDePersonaje(1);
            }
            if (((RadioButton) p.findViewById(R.id.radioButton_cr_pj_ppal_tipoPj_3)).isChecked()) {
                temp.setTipoDePersonaje(2);
            }

            //PUNTOS

            int[] valores_lugares = p.getResources().getIntArray(R.array.Datos_creac_lugares);

            for (int i = 1; i < valores_lugares.length + 1; i++) {

                String buttonID = "cr_pj_c_" + (i);

                int resID = p.getResources().getIdentifier(buttonID, "id", p.getPackageName());

                if (valores_lugares[i - 1] != -1) {

                    temp.reemplazar_valor_caracteristica(valores_lugares[i - 1], Integer.valueOf(((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).getText().toString()));

                    temp.reemplazar_valor_caracteristica_temp(valores_lugares[i - 1], Integer.valueOf(((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_temp)).getText().toString()));

                }

            }


            for (int n = 1; n < 7; n++) {

                //ARMAS
                temp.setArma(getArma(n), n - 1);

                //ARMADURAS
                temp.setArmadura(getArmadura(n), n - 1);
            }


            //Penalizadores

            for (int i = 1; i < nombres_pen.length + 1; i++) {

                String buttonID = "cr_pj_p_" + (i);

                int resID = p.getResources().getIdentifier(buttonID, "id", p.getPackageName());

                temp.reemplazar_valor_penalizador(i - 1, Integer.parseInt(((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).getText().toString()));

            }

            //extras

            for (int i = 1; i < p.getResources().getStringArray(R.array.Datos_Creación_Nombres_Extras).length + 1; i++) {

                String buttonID = "cr_pj_ee_" + (i);

                int resID = p.getResources().getIdentifier(buttonID, "id", p.getPackageName());

                temp.reemplazar_valor_extra(i - 1, Integer.parseInt(((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).getText().toString()));

            }

            //PSIQUICOS

            int[] valores_psiquicos = p.getResources().getIntArray(R.array.Datos_Psiquicos_Bases);
            try {
                int contador_psi = 0;
                for (int i = 1; i < valores_psiquicos.length; i++) {

                    String buttonID = "cr_pj_ps_" + (i);

                    int resID = p.getResources().getIdentifier(buttonID, "id", p.getPackageName());

                    if (valores_psiquicos[i - 1] != -1) {

                        temp.reemplazar_valor_psiquico(contador_psi, Integer.valueOf(((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).getText().toString()));
                        contador_psi = contador_psi + 1;

                    }

                }
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
            }

            System.out.println(Arrays.toString(temp.getHabilidades_psiquicas()));


        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println(temp.toJson());

        return temp;

    }

    public void setPj(Personaje pj) {

        try {

            //PRINCIPAL

            ((EditText) p.findViewById(R.id.EditText_Nombre_Personaje)).setText(pj.getNombre_personaje());
            ((EditText) p.findViewById(R.id.EditText_Nombre_Jugador)).setText(pj.getNombre_jugador());

            ((ImageButton) p.findViewById(R.id.imageButton_cambiar_imagen)).setImageBitmap(DbBitmapUtility.getImage(pj.getToken()));

            ((EditText) p.findViewById(R.id.EditText_Descripción_pj)).setText(pj.getDescripción());

            ((Spinner) p.findViewById(R.id.spinner_cr_pj_ppal_pifia)).setSelection(pj.getNivelPifia() - 1);

            switch (pj.getTipoDeEntidad()) {
                case 0:
                    ((RadioButton) p.findViewById(R.id.radioButton_cr_pj_ppal_clasePj_1)).setChecked(true);
                    break;
                case 1:
                    ((RadioButton) p.findViewById(R.id.radioButton_cr_pj_ppal_clasePj_2)).setChecked(true);
                    break;
                case 2:
                    ((RadioButton) p.findViewById(R.id.radioButton_cr_pj_ppal_clasePj_3)).setChecked(true);
                    break;
            }

            switch (pj.getTipoDePersonaje()) {
                case 0:
                    ((RadioButton) p.findViewById(R.id.radioButton_cr_pj_ppal_tipoPj_1)).setChecked(true);
                    break;
                case 1:
                    ((RadioButton) p.findViewById(R.id.radioButton_cr_pj_ppal_tipoPj_2)).setChecked(true);
                    break;
                case 2:
                    ((RadioButton) p.findViewById(R.id.radioButton_cr_pj_ppal_tipoPj_3)).setChecked(true);
                    break;
            }

            //PUNTOS

            int[] valores_lugares = p.getResources().getIntArray(R.array.Datos_creac_lugares);

            for (int i = 1; i < valores_lugares.length + 1; i++) {

                String buttonID = "cr_pj_c_" + (i);

                int resID = p.getResources().getIdentifier(buttonID, "id", p.getPackageName());

                if (valores_lugares[i - 1] != -1) {

                    ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_temp)).setText(String.valueOf(pj.getCaracteristicas_temporales()[valores_lugares[i - 1]]));

                    ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setText(String.valueOf(pj.getCaracteristicas()[valores_lugares[i - 1]]));

                }

            }

            //PSIQUICOS

            int[] valores_psiquicos = p.getResources().getIntArray(R.array.Datos_Psiquicos_Bases);

            int contador_psi = 0;
            for (int i = 1; i < valores_psiquicos.length; i++) {
                String buttonID = "cr_pj_ps_" + (i);
                try {
                    int resID = p.getResources().getIdentifier(buttonID, "id", p.getPackageName());

                    if (valores_psiquicos[i - 1] != -1) {

                        ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setText(String.valueOf(pj.getHabilidades_psiquicas()[contador_psi]));

                        contador_psi = contador_psi + 1;
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    break;
                }


            }


            for (int n = 1; n < 7; n++) {

                //ARMAS
                setArma(n, pj.getArmas()[n - 1]);

                //ARMADURAS
                setArmadura(n, pj.getArmaduras()[n - 1]);
            }


            //Penalizadores

            for (int i = 1; i < nombres_pen.length + 1; i++) {

                String buttonID = "cr_pj_p_" + (i);

                int resID = p.getResources().getIdentifier(buttonID, "id", p.getPackageName());

                ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setText(String.valueOf(pj.getPenalizadores()[i - 1]));

            }

            //extras

            for (int i = 1; i < p.getResources().getStringArray(R.array.Datos_Creación_Nombres_Extras).length + 1; i++) {

                String buttonID = "cr_pj_ee_" + (i);

                int resID = p.getResources().getIdentifier(buttonID, "id", p.getPackageName());

                ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setText(String.valueOf(pj.getEspeciales()[i - 1]));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initListeners() {

        p.findViewById(R.id.button_Modificar_PJ).setOnClickListener(this);
        p.findViewById(R.id.button_GuardarNuevo_PJ).setOnClickListener(this);
        p.findViewById(R.id.button_Cargar_PJ).setOnClickListener(this);
        p.findViewById(R.id.button_Eliminar_PJ).setOnClickListener(this);

        p.findViewById(R.id.cr_pj_armas_cargar).setOnClickListener(this);
        p.findViewById(R.id.cr_pj_armas_borrar_todo).setOnClickListener(this);

        p.findViewById(R.id.cr_pj_armaduras_cargar).setOnClickListener(this);
        p.findViewById(R.id.cr_pj_armaduras_borrar_todo).setOnClickListener(this);


        ((NestedScrollView) p.findViewById(R.id.NestedScrollView_puntos)).setOnScrollChangeListener(this);
        ((NestedScrollView) p.findViewById(R.id.NestedScrollView_psiquicos)).setOnScrollChangeListener(this);

        (p.findViewById(R.id.button_cr_pj_generar_attr)).setOnClickListener(this);

        p.findViewById(R.id.imageButton_info_puntos).setOnClickListener(this);

        ((RadioGroup) p.findViewById(R.id.radioGroup_cr_pj_armas)).setOnCheckedChangeListener(this);

        ((RadioGroup) p.findViewById(R.id.radioGroup_cr_pj_armaduras)).setOnCheckedChangeListener(this);

        for (int id : buttons_armas) {
            (p.findViewById(id)).setOnClickListener(this);
        }

        for (int id : buttons_armas) {
            (p.findViewById(id)).setOnClickListener(this);
        }

        for (int id : buttons_puntos) {
            (p.findViewById(id)).setOnClickListener(this);
        }

        for (int id : buttons_psquicos) {
            (p.findViewById(id)).setOnClickListener(this);
        }

    }

    private void initBonificadores() {


        String[] nombres_extras = p.getResources().getStringArray(R.array.Datos_Creación_Nombres_Extras);

        for (int i = 1; i < nombres_extras.length + 1; i++) {

            String buttonID = "cr_pj_ee_" + (i);

            int resID = p.getResources().getIdentifier(buttonID, "id", p.getPackageName());

            (p.findViewById(resID).findViewById(R.id.cr_pj_c_temp)).setVisibility(View.GONE);
            ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setText("0");
            ((TextView) p.findViewById(resID).findViewById(R.id.cr_pj_c_nombre)).setText(nombres_extras[i - 1]);

        }

        for (int i = 1; i < nombres_pen.length + 1; i++) {

            String buttonID = "cr_pj_p_" + (i);

            int resID = p.getResources().getIdentifier(buttonID, "id", p.getPackageName());

            (p.findViewById(resID).findViewById(R.id.cr_pj_c_temp)).setVisibility(View.GONE);
            ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setText("0");
            ((TextView) p.findViewById(resID).findViewById(R.id.cr_pj_c_nombre)).setText(nombres_pen[i - 1]);

        }
    }

    private void initPsiquicos() {

        String[] nombres_psiquicos = p.getResources().getStringArray(R.array.Datos_Psiquicos_Nombres);
        int[] valores_psiquicos = p.getResources().getIntArray(R.array.Datos_Psiquicos_Bases);

        for (int i = 1; i < nombres_psiquicos.length + 1; i++) {

            String buttonID = "cr_pj_ps_" + (i);

            int resID = p.getResources().getIdentifier(buttonID, "id", p.getPackageName());

            if (valores_psiquicos[i - 1] == -1) {

                TextView nombre = ((TextView) p.findViewById(resID).findViewById(R.id.cr_pj_c_nombre));
                ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_temp)).setVisibility(View.GONE);
                ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setVisibility(View.GONE);
                nombre.setText(nombres_psiquicos[i - 1]);
                nombre.setTextSize(20);
                nombre.setTextColor(Color.parseColor("#ffffff"));
                nombre.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                p.findViewById(resID).setBackground(p.getDrawable(R.drawable.round_rect_button));
                nombre.setTypeface(null, Typeface.BOLD);


            } else {

                ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_temp)).setVisibility(View.GONE);
                ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setText(String.valueOf(valores_psiquicos[i - 1]));
                ((TextView) p.findViewById(resID).findViewById(R.id.cr_pj_c_nombre)).setText(nombres_psiquicos[i - 1]);

            }


        }
    }

    private void initPuntos() {

        String[] nombres_puntos = p.getResources().getStringArray(R.array.Datos_Creación_Nombres);
        int[] valores_puntos = p.getResources().getIntArray(R.array.Datos_Creación_Bases);

        for (int i = 1; i < nombres_puntos.length + 1; i++) {

            String buttonID = "cr_pj_c_" + (i);

            int resID = p.getResources().getIdentifier(buttonID, "id", p.getPackageName());

            if (valores_puntos[i - 1] == -1 | valores_puntos[i - 1] == -2) {

                TextView nombre = ((TextView) p.findViewById(resID).findViewById(R.id.cr_pj_c_nombre));
                ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_temp)).setVisibility(View.GONE);
                ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setVisibility(View.GONE);
                nombre.setText(nombres_puntos[i - 1]);
                nombre.setTextSize(20);
                nombre.setTextColor(Color.parseColor("#ffffff"));
                nombre.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                p.findViewById(resID).setBackground(p.getDrawable(R.drawable.round_rect_button));
                nombre.setTypeface(null, Typeface.BOLD);


            } else {

                ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_temp)).setText("0");
                ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setText(String.valueOf(valores_puntos[i - 1]));
                ((TextView) p.findViewById(resID).findViewById(R.id.cr_pj_c_nombre)).setText(nombres_puntos[i - 1]);

            }


        }
    }

    private void initArmas() {
        String[] nombres_armas = p.getResources().getStringArray(R.array.Datos_Creación_Nombres_Arma);
        int[] valores_armas = p.getResources().getIntArray(R.array.Datos_Creación_Bases_Arma);

        for (int n = 1; n < 7; n++) {

            for (int i = 1; i < nombres_armas.length + 1; i++) {

                String ViewID = "cr_pj_armas_" + n + "_" + i;

                int resID = p.getResources().getIdentifier(ViewID, "id", p.getPackageName());

                ((TextView) p.findViewById(resID).findViewById(R.id.cr_pj_c_nombre)).setText(nombres_armas[i - 1]);
                ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_temp)).setVisibility(View.GONE);

                if (valores_armas[i - 1] == -6 | valores_armas[i - 1] == -4 | valores_armas[i - 1] == -5 | valores_armas[i - 1] == -7) {//spinner

                    ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setVisibility(View.GONE);
                    ((Spinner) p.findViewById(resID).findViewById(R.id.cr_pj_c_spinner)).setVisibility(View.VISIBLE);

                    switch (valores_armas[i - 1]) {
                        case -4:
                            ((Spinner) p.findViewById(resID).findViewById(R.id.cr_pj_c_spinner)).setAdapter(ArrayAdapter.createFromResource(p, R.array.Tipos_de_armas, android.R.layout.simple_spinner_dropdown_item));
                            break;
                        case -5:
                            ((Spinner) p.findViewById(resID).findViewById(R.id.cr_pj_c_spinner)).setAdapter(ArrayAdapter.createFromResource(p, R.array.Tipos_de_ataque, android.R.layout.simple_spinner_dropdown_item));
                            break;
                        case -6:
                            ((Spinner) p.findViewById(resID).findViewById(R.id.cr_pj_c_spinner)).setAdapter(ArrayAdapter.createFromResource(p, R.array.Efectos_armas, android.R.layout.simple_spinner_dropdown_item));
                            break;
                        case -7:
                            ((Spinner) p.findViewById(resID).findViewById(R.id.cr_pj_c_spinner)).setAdapter(ArrayAdapter.createFromResource(p, R.array.Habilidades_para_armas, android.R.layout.simple_spinner_dropdown_item));
                            break;
                    }


                } else if (valores_armas[i - 1] == -3) {//checkbox

                    ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setVisibility(View.GONE);
                    ((ToggleButton) p.findViewById(resID).findViewById(R.id.cr_pj_c_toggleButton)).setVisibility(View.VISIBLE);

                } else {


                    ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setText(String.valueOf(valores_armas[i - 1]));


                }
            }
        }
    }

    private void initArmaduras() {

        String[] nombres_armaduras = p.getResources().getStringArray(R.array.Datos_Creación_Nombres_Armaduras);
        int[] valores_armaduras = p.getResources().getIntArray(R.array.Datos_Creación_Bases_Armaduras);

        for (int n = 1; n < 7; n++) {

            for (int i = 1; i < nombres_armaduras.length + 1; i++) {

                String ViewID = "cr_pj_armaduras_" + n + "_" + i;

                int resID = p.getResources().getIdentifier(ViewID, "id", p.getPackageName());

                ((TextView) p.findViewById(resID).findViewById(R.id.cr_pj_c_nombre)).setText(nombres_armaduras[i - 1]);
                ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_temp)).setVisibility(View.GONE);

                if (valores_armaduras[i - 1] == -6 | valores_armaduras[i - 1] == -4 | valores_armaduras[i - 1] == -5) {//spinner

                    ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setVisibility(View.GONE);
                    ((Spinner) p.findViewById(resID).findViewById(R.id.cr_pj_c_spinner)).setVisibility(View.VISIBLE);

                    switch (valores_armaduras[i - 1]) {
                        case -4:
                            ((Spinner) p.findViewById(resID).findViewById(R.id.cr_pj_c_spinner)).setAdapter(ArrayAdapter.createFromResource(p, R.array.Tipos_de_armas, android.R.layout.simple_spinner_dropdown_item));
                            break;
                        case -5:
                            ((Spinner) p.findViewById(resID).findViewById(R.id.cr_pj_c_spinner)).setAdapter(ArrayAdapter.createFromResource(p, R.array.Tipos_de_ataque, android.R.layout.simple_spinner_dropdown_item));
                            break;
                        case -6:
                            ((Spinner) p.findViewById(resID).findViewById(R.id.cr_pj_c_spinner)).setAdapter(ArrayAdapter.createFromResource(p, R.array.Efectos_armas, android.R.layout.simple_spinner_dropdown_item));
                            break;

                    }


                } else if (valores_armaduras[i - 1] == -3) {//checkbox

                    ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setVisibility(View.GONE);
                    ((ToggleButton) p.findViewById(resID).findViewById(R.id.cr_pj_c_toggleButton)).setVisibility(View.VISIBLE);

                } else {


                    ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setText(String.valueOf(valores_armaduras[i - 1]));


                }
            }
        }
    }

    public Controlador_creac_pj(Principal p) {
        alturaBase_buttons = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, p.getResources().getDisplayMetrics());
        alturaBase_customs = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 59, p.getResources().getDisplayMetrics());
        alturaBase_pad = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, p.getResources().getDisplayMetrics());

        this.p = p;



        //setPj(dummyTest());

    }

    public void inicializar(){
        initListeners();

        initPuntos();

        initArmas();

        initArmaduras();

        initBonificadores();

        initPsiquicos();
    }

    private boolean pos(int tope_superior, int tope_inferior, int scrollY, int position, int alturaBase, int b) {
        if ((scrollY >= (tope_inferior * alturaBase) - (alturaBase_pad)) & (scrollY <= (tope_superior * alturaBase) - (alturaBase_pad))) {

            if ((p.findViewById(b)).getBackground() != p.getDrawable(R.drawable.button_selected)) {

                (p.findViewById(b)).setBackground(p.getDrawable(R.drawable.button_selected));

            }
            return true;
        }

        ((Button) p.findViewById(b)).setBackground(p.getDrawable(R.drawable.round_rect_button));
        return false;
    }

    private void restoreButtons_puntos(View button) {
        lastUpdateNestedScroll = System.currentTimeMillis();
        for (int id : buttons_puntos) {
            if (id != button.getId()) {
                ((Button) p.findViewById(id)).setBackground(p.getDrawable(R.drawable.round_rect_button));
            }
        }
    }

    private void restoreButtons_psiquicos(View button) {
        lastUpdateNestedScroll = System.currentTimeMillis();
        for (int id : buttons_psquicos) {
            if (id != button.getId()) {
                ((Button) p.findViewById(id)).setBackground(p.getDrawable(R.drawable.round_rect_button));
            }
        }
    }


    private int posButton(View vista, int[] matriz_de_vistas) {
        for (int i = 0; i < matriz_de_vistas.length; i++) {
            if (vista.getId() == matriz_de_vistas[i]) {
                return i;
            }
        }
        return -1;
    }


    @Override
    public void onClick(View v) {
        Arma temp_arma;
        Armadura temp_armadura;
        FragmentManager fm;

        Snackbar mensajeConfirmacion;
        View view;
        TextView tv;

        switch (v.getId()) {
            case R.id.cr_pj_psiquicos_0:
            case R.id.cr_pj_psiquicos_1:
            case R.id.cr_pj_psiquicos_3:
            case R.id.cr_pj_psiquicos_4:
            case R.id.cr_pj_psiquicos_5:
            case R.id.cr_pj_psiquicos_6:
            case R.id.cr_pj_psiquicos_7:
            case R.id.cr_pj_psiquicos_8:
            case R.id.cr_pj_psiquicos_9:
            case R.id.cr_pj_psiquicos_10:
            case R.id.cr_pj_psiquicos_11:
            case R.id.cr_pj_psiquicos_12:
            case R.id.cr_pj_psiquicos_13:
            case R.id.cr_pj_psiquicos_14:

                restoreButtons_psiquicos(v);
                ((NestedScrollView) p.findViewById(R.id.NestedScrollView_psiquicos)).scrollTo(0, 0);
                ((NestedScrollView) p.findViewById(R.id.NestedScrollView_psiquicos)).smoothScrollTo(0, (alturaBase_pad + (alturaBase_customs * alturas_psiquicos[posButton(v, buttons_psquicos)])));
                v.setBackground(p.getDrawable(R.drawable.button_selected));
                break;

            case R.id.cr_pj_puntos_p1:
            case R.id.cr_pj_puntos_p2:
            case R.id.cr_pj_puntos_p3:
            case R.id.cr_pj_puntos_p4:
            case R.id.cr_pj_puntos_p5:
            case R.id.cr_pj_puntos_p6:
            case R.id.cr_pj_puntos_p7:
            case R.id.cr_pj_puntos_p8:
            case R.id.cr_pj_puntos_p9:
            case R.id.cr_pj_puntos_p10:
            case R.id.cr_pj_puntos_p11:
            case R.id.cr_pj_puntos_p12:
            case R.id.cr_pj_puntos_p13:
                restoreButtons_puntos(v);
                ((NestedScrollView) p.findViewById(R.id.NestedScrollView_puntos)).scrollTo(0, 0);
                ((NestedScrollView) p.findViewById(R.id.NestedScrollView_puntos)).smoothScrollTo(0, (alturaBase_pad + (alturaBase_customs * alturas_puntos[posButton(v, buttons_puntos)])));
                v.setBackground(p.getDrawable(R.drawable.button_selected));
                break;

            case R.id.imageButton_info_puntos:
                fm = p.getFragmentManager();
                Fragment_classes.Dialog_info dialogFragment = new Fragment_classes.Dialog_info();

                dialogFragment.setFragment_info_cuerpo("El valor 'FIJO' sería el que vemos en la planilla, mientras que el 'TEMP' es un valor temporal que permitirá a la aplicación aplicar diferentes bonificadores o penalizadores situacionales.");
                dialogFragment.setFragment_info_titulo("Valores FIJO y TEMP");

                dialogFragment.show(fm, "Información: ");

                break;
            case R.id.button_cr_pj_generar_attr:

                int[] gen = Base.generadorAtributos(((Spinner) p.findViewById(R.id.spinner_cr_pj_tipo_gen)).getSelectedItemPosition());

                for (int i = 0; i < 8; i++) {

                    String buttonID = "cr_pj_c_" + (i + 2);

                    int resID = p.getResources().getIdentifier(buttonID, "id", p.getPackageName());

                    System.out.println(Arrays.toString(gen));

                    ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setText(String.valueOf(gen[i]));
                }
            case R.id.cr_pj_armas_cargar:

                temp_arma = p.getArrayArmas().get(((Spinner) p.findViewById(R.id.spinner_Arma_Para_Añadir)).getSelectedItemPosition());

                setArma(getArmaSelected(), temp_arma);


                break;
            case R.id.cr_pj_armas_borrar_todo:

                temp_arma = new Arma();
                temp_arma.setNombre("Vacio");

                setArma(getArmaSelected(), temp_arma);

                break;
            case R.id.cr_pj_armaduras_cargar:

                temp_armadura = p.getArrayArmaduras().get(((Spinner) p.findViewById(R.id.spinner_Armaduras_Para_Añadir)).getSelectedItemPosition());

                setArmadura(getArmaduraSelected(), temp_armadura);


                break;
            case R.id.cr_pj_armaduras_borrar_todo:

                temp_armadura = new Armadura();
                temp_armadura.setNombre("Vacio");

                setArmadura(getArmaduraSelected(), temp_armadura);

                break;

            case R.id.button_Modificar_PJ:

                seleccionado = ((Spinner) p.findViewById(R.id.spinner_seleccionPj)).getSelectedItemPosition();

                final int index_personaje = seleccionado;

                if (p.getDatos().ArrayPersonajes().size() != 0 & seleccionado != -1) {
                    if (seleccionado < p.getDatos().ArrayPersonajes().size()) {

                        Personaje personaje_modificado = getPj();

                        personaje_delete = new Personaje().fromJson(p.getDatos().ArrayPersonajes().get(seleccionado).toJson());

                        mensajeConfirmacion = Snackbar
                                .make(v, "Personaje " + personaje_delete.getNombre_personaje() + " modificado a " + personaje_modificado.getNombre_personaje(), Snackbar.LENGTH_LONG)
                                .setAction("DESHACER", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        p.getDatos().ArrayPersonajes().get(index_personaje).reemplazar(personaje_delete);

                                        p.getDataBase().ModificarPersonaje(personaje_delete);

                                        p.actualizarArrayPersonajes(R.id.spinner_seleccionPj, false);

                                        ((Spinner) p.findViewById(R.id.spinner_seleccionPj)).setSelection(index_personaje);

                                        p.showSnack(Snackbar.make(view, "Restaurado", Snackbar.LENGTH_SHORT));
                                    }

                                });

                        personaje_modificado.setID(p.getDatos().ArrayPersonajes().get(seleccionado).getID());

                        p.getDatos().ArrayPersonajes().get(seleccionado).reemplazar(personaje_modificado);

                        p.getDataBase().ModificarPersonaje(personaje_modificado);

                        p.actualizarArrayPersonajes(R.id.spinner_seleccionPj, false);

                        ((Spinner) p.findViewById(R.id.spinner_seleccionPj)).setSelection(seleccionado);

                        view = mensajeConfirmacion.getView();

                        tv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                        tv.setTextColor(p.getResources().getColor(R.color.colorWhite));

                        mensajeConfirmacion.setActionTextColor(p.getResources().getColor(R.color.colorWhite));

                        mensajeConfirmacion.show();
                    }
                } else {
                    p.showSnack(Snackbar.make(v, "No hay ningún elemento que modificar", Snackbar.LENGTH_SHORT));

                    p.actualizarArrayPersonajes(R.id.spinner_seleccionPj, false);
                }


                break;
            case R.id.button_Cargar_PJ:

                if (((Spinner) p.findViewById(R.id.spinner_seleccionPj)).getSelectedItemPosition() != -1) {

                    seleccionado = ((Spinner) p.findViewById(R.id.spinner_seleccionPj)).getSelectedItemPosition();

                    Personaje temp = p.getDatos().ArrayPersonajes().get(seleccionado);

                    setPj(temp);

                } else {
                    p.showSnack(Snackbar.make(v, "No se pudo cargar.", Snackbar.LENGTH_SHORT));

                    p.actualizarArrayPersonajes(R.id.spinner_seleccionPj, false);
                }


                break;
            case R.id.button_Eliminar_PJ:

                seleccionado = ((Spinner) p.findViewById(R.id.spinner_seleccionPj)).getSelectedItemPosition();

                if (p.getDatos().ArrayPersonajes().size() != 0 & seleccionado != -1) {
                    if (seleccionado < p.getDatos().ArrayPersonajes().size()) {

                        mensajeConfirmacion = Snackbar
                                .make(v, "Personaje " + p.getDatos().ArrayPersonajes().get(seleccionado).getNombre_personaje() + " eliminado", Snackbar.LENGTH_LONG)
                                .setAction("DESHACER", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        personaje_delete.setID(p.getDataBase().AñadirPersonaje(personaje_delete));

                                        p.getDatos().Añadir_Personaje(personaje_delete);

                                        p.actualizarArrayPersonajes(R.id.spinner_seleccionPj, false);

                                        p.showSnack(Snackbar.make(view, "Restaurado", Snackbar.LENGTH_SHORT));
                                    }

                                });

                        personaje_delete = p.getDatos().ArrayPersonajes().get(seleccionado);

                        p.getDataBase().EliminarPersonaje(p.getDatos().ArrayPersonajes().get(seleccionado).getID());

                        p.getDatos().Eliminar_Personaje(seleccionado);

                        p.actualizarArrayPersonajes(R.id.spinner_seleccionPj, false);

                        view = mensajeConfirmacion.getView();

                        tv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                        tv.setTextColor(p.getResources().getColor(R.color.colorWhite));

                        mensajeConfirmacion.setActionTextColor(p.getResources().getColor(R.color.colorWhite));

                        mensajeConfirmacion.show();
                    }
                } else {

                    p.showSnack(Snackbar.make(v, "No hay ningún elemento que eliminar", Snackbar.LENGTH_SHORT));

                    p.actualizarArrayPersonajes(R.id.spinner_seleccionPj, false);
                }


                break;
            case R.id.button_GuardarNuevo_PJ:

                Personaje nuevo_Personaje = getPj();

                nuevo_Personaje.setID(p.getDataBase().AñadirPersonaje(nuevo_Personaje));

                p.getDatos().Añadir_Personaje(nuevo_Personaje);

                p.showSnack(Snackbar.make(v, "Personaje " + nuevo_Personaje.getNombre_personaje() + " guardado.", Snackbar.LENGTH_SHORT));

                p.actualizarArrayPersonajes(R.id.spinner_seleccionPj, false);

                ((Spinner) p.findViewById(R.id.spinner_seleccionPj)).setSelection(p.getDatos().ArrayPersonajes().size() - 1);

                break;

        }
    }

    private int getArmaSelected() {
        if (((RadioButton) p.findViewById(R.id.radioButton_cr_pj_armas_arma1)).isChecked()) {
            return 1;
        }
        if (((RadioButton) p.findViewById(R.id.radioButton_cr_pj_armas_arma2)).isChecked()) {
            return 2;
        }
        if (((RadioButton) p.findViewById(R.id.radioButton_cr_pj_armas_arma3)).isChecked()) {
            return 3;
        }
        if (((RadioButton) p.findViewById(R.id.radioButton_cr_pj_armas_arma4)).isChecked()) {
            return 4;
        }
        if (((RadioButton) p.findViewById(R.id.radioButton_cr_pj_armas_arma5)).isChecked()) {
            return 5;
        }
        if (((RadioButton) p.findViewById(R.id.radioButton_cr_pj_armas_arma6)).isChecked()) {
            return 6;
        }
        return 0;
    }

    private int getArmaduraSelected() {
        if (((RadioButton) p.findViewById(R.id.radioButton_cr_pj_armaduras_1)).isChecked()) {
            return 1;
        }
        if (((RadioButton) p.findViewById(R.id.radioButton_cr_pj_armaduras_2)).isChecked()) {
            return 2;
        }
        if (((RadioButton) p.findViewById(R.id.radioButton_cr_pj_armaduras_3)).isChecked()) {
            return 3;
        }
        if (((RadioButton) p.findViewById(R.id.radioButton_cr_pj_armaduras_4)).isChecked()) {
            return 4;
        }
        if (((RadioButton) p.findViewById(R.id.radioButton_cr_pj_armaduras_5)).isChecked()) {
            return 5;
        }
        if (((RadioButton) p.findViewById(R.id.radioButton_cr_pj_armaduras_6)).isChecked()) {
            return 6;
        }
        return 0;
    }

    @Override
    public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        long time = System.currentTimeMillis();
        if (time - lastUpdateNestedScroll > 500) {
            switch (v.getId()) {
                case R.id.NestedScrollView_psiquicos:
                    for (int i = 0; i < buttons_psquicos.length; i++) {
                        if (pos(alturas_psiquicos[i + 1], alturas_psiquicos[i], scrollY, i + 1, alturaBase_customs, buttons_psquicos[i])) {
                            ((NestedScrollView) p.findViewById(R.id.NestedScrollView_cr_pj_psiquicos)).scrollTo(0, (alturaBase_buttons * (i)));
                        }
                    }
                    break;
                case R.id.NestedScrollView_puntos:
                    for (int i = 0; i < buttons_puntos.length; i++) {
                        if (pos(alturas_puntos[i + 1], alturas_puntos[i], scrollY, i + 1, alturaBase_customs, buttons_puntos[i])) {
                            ((NestedScrollView) p.findViewById(R.id.NestedScrollView_cr_pj_buttons)).scrollTo(0, (alturaBase_buttons * (i)));
                        }
                    }
                    break;
            }
        } else {
            lastUpdateNestedScroll = time;
        }


    }


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        switch (group.getId()) {
            case R.id.radioGroup_cr_pj_armas:
                p.Hide(p.findViewById(R.id.LinearLayout_cr_pj_arma_1));
                p.Hide(p.findViewById(R.id.LinearLayout_cr_pj_arma_2));
                p.Hide(p.findViewById(R.id.LinearLayout_cr_pj_arma_3));
                p.Hide(p.findViewById(R.id.LinearLayout_cr_pj_arma_4));
                p.Hide(p.findViewById(R.id.LinearLayout_cr_pj_arma_5));
                p.Hide(p.findViewById(R.id.LinearLayout_cr_pj_arma_6));
                switch (checkedId) {
                    case R.id.radioButton_cr_pj_armas_arma1:
                        p.Show(p.findViewById(R.id.LinearLayout_cr_pj_arma_1));
                        break;
                    case R.id.radioButton_cr_pj_armas_arma2:
                        p.Show(p.findViewById(R.id.LinearLayout_cr_pj_arma_2));
                        break;
                    case R.id.radioButton_cr_pj_armas_arma3:
                        p.Show(p.findViewById(R.id.LinearLayout_cr_pj_arma_3));
                        break;
                    case R.id.radioButton_cr_pj_armas_arma4:
                        p.Show(p.findViewById(R.id.LinearLayout_cr_pj_arma_4));
                        break;
                    case R.id.radioButton_cr_pj_armas_arma5:
                        p.Show(p.findViewById(R.id.LinearLayout_cr_pj_arma_5));
                        break;
                    case R.id.radioButton_cr_pj_armas_arma6:
                        p.Show(p.findViewById(R.id.LinearLayout_cr_pj_arma_6));
                        break;

                }
                break;

            case R.id.radioGroup_cr_pj_armaduras:
                p.Hide(p.findViewById(R.id.LinearLayout_cr_pj_armadura_1));
                p.Hide(p.findViewById(R.id.LinearLayout_cr_pj_armadura_2));
                p.Hide(p.findViewById(R.id.LinearLayout_cr_pj_armadura_3));
                p.Hide(p.findViewById(R.id.LinearLayout_cr_pj_armadura_4));
                p.Hide(p.findViewById(R.id.LinearLayout_cr_pj_armadura_5));
                p.Hide(p.findViewById(R.id.LinearLayout_cr_pj_armadura_6));
                switch (checkedId) {
                    case R.id.radioButton_cr_pj_armaduras_1:
                        p.Show(p.findViewById(R.id.LinearLayout_cr_pj_armadura_1));
                        break;
                    case R.id.radioButton_cr_pj_armaduras_2:
                        p.Show(p.findViewById(R.id.LinearLayout_cr_pj_armadura_2));
                        break;
                    case R.id.radioButton_cr_pj_armaduras_3:
                        p.Show(p.findViewById(R.id.LinearLayout_cr_pj_armadura_3));
                        break;
                    case R.id.radioButton_cr_pj_armaduras_4:
                        p.Show(p.findViewById(R.id.LinearLayout_cr_pj_armadura_4));
                        break;
                    case R.id.radioButton_cr_pj_armaduras_5:
                        p.Show(p.findViewById(R.id.LinearLayout_cr_pj_armadura_5));
                        break;
                    case R.id.radioButton_cr_pj_armaduras_6:
                        p.Show(p.findViewById(R.id.LinearLayout_cr_pj_armadura_6));
                        break;
                }
                break;
        }


    }

}
