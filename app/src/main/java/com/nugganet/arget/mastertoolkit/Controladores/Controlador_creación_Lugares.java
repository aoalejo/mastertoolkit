package com.nugganet.arget.mastertoolkit.Controladores;

import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Lugar;
import com.nugganet.arget.mastertoolkit.Principal;
import com.nugganet.arget.mastertoolkit.R;

public class Controlador_creación_Lugares implements View.OnClickListener {

    final Principal p;
    private Lugar lugar_Delete;
    private String[] nombres_modif = {"Modificador al Ataque", "Modificador a la Parada", "Modificador a la Esquiva", "Modificador al Turno"};

    public Controlador_creación_Lugares(Principal p) {
        this.p = p;



    }

    public void initListeners() {
        p.findViewById(R.id.button_Cargar_lugar).setOnClickListener(this);
        p.findViewById(R.id.button_Eliminar_lugar).setOnClickListener(this);
        p.findViewById(R.id.button_GuardarNuevo_lugar).setOnClickListener(this);
        p.findViewById(R.id.button_Modificar_lugar).setOnClickListener(this);
    }


    public void initLugar() {

        for (int i = 1; i < nombres_modif.length + 1; i++) {

            String ViewID = "cr_pj_modif_" + i;

            int resID = p.getResources().getIdentifier(ViewID, "id", p.getPackageName());

            ((TextView) p.findViewById(resID).findViewById(R.id.cr_pj_c_nombre)).setText(nombres_modif[i - 1]);
            ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_temp)).setVisibility(View.GONE);
            ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setText("0");

        }
    }

    private int TxtToInt(String text) {
        try {
            int temp = Integer.parseInt(text);
            return temp;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }


    @Override
    public void onClick(View v) {

        int seleccionado = ((Spinner) p.findViewById(R.id.spinner_seleccion_lugar)).getSelectedItemPosition();
        Snackbar mensajeConfirmación;
        View view;
        TextView tv;

        switch (v.getId()) {
            case R.id.button_Cargar_lugar:

                if (((Spinner) p.findViewById(R.id.spinner_seleccion_lugar)).getSelectedItemPosition() != -1) {

                    Lugar temp = p.getArraylugares().get(seleccionado);

                    setLugar(temp);

                } else {
                    p.showSnack(Snackbar.make(v, "No se pudo cargar.", Snackbar.LENGTH_SHORT));
                }

                break;

            case R.id.button_GuardarNuevo_lugar:

                Lugar nuevo_lugar = getLugar();

                nuevo_lugar.setId(p.getDataBase().AñadirLugar(nuevo_lugar));

                p.getArraylugares().add(nuevo_lugar);

                p.showSnack(Snackbar.make(v, "Lugar " + nuevo_lugar.getNombre() + " guardado.", Snackbar.LENGTH_SHORT));


                break;

            case R.id.button_Modificar_lugar:

                final int index_lugar = seleccionado;

                if (p.getDatos().ArrayModificadores().size() != 0 & seleccionado != -1) {

                    if (seleccionado < p.getArraylugares().size()) {

                        Lugar lugar_modificado = getLugar();

                        lugar_Delete = new Lugar().fromJson(lugar_modificado.toJson());

                        mensajeConfirmación = Snackbar
                                .make(v, "Lugar " + p.getArraylugares().get(seleccionado).getNombre() + " modificado a " + lugar_modificado.getNombre(), Snackbar.LENGTH_LONG)
                                .setAction("DESHACER", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        p.getArraylugares().get(index_lugar).reemplazar(lugar_Delete);

                                        p.getDataBase().ModificarLugar(lugar_Delete);

                                        p.actualizarArrayLugares(R.id.spinner_seleccion_lugar, false, false);

                                        p.actualizarArrayLugares(R.id.spinner_lugar_padre, true, false);

                                        p.actualizarArrayLugares(R.id.spinner_ubicación, true, true);

                                        ((Spinner) p.findViewById(R.id.spinner_seleccion_modif)).setSelection(index_lugar);

                                        p.showSnack(Snackbar.make(view, "Restaurado", Snackbar.LENGTH_SHORT));
                                    }

                                });

                        p.getArraylugares().get(seleccionado).reemplazar(lugar_modificado);

                        p.getDataBase().ModificarLugar(p.getArraylugares().get(seleccionado));

                        view = mensajeConfirmación.getView();

                        tv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                        tv.setTextColor(p.getResources().getColor(R.color.colorWhite));

                        mensajeConfirmación.setActionTextColor(p.getResources().getColor(R.color.colorWhite));

                        mensajeConfirmación.show();
                    }
                } else {
                    p.showSnack(Snackbar.make(v, "No hay ningún elemento que modificar", Snackbar.LENGTH_SHORT));
                }


                break;

            case R.id.button_Eliminar_lugar:

                if (p.getArraylugares().size() != 0 & seleccionado != -1) {

                    if (seleccionado < p.getArraylugares().size()) {

                        mensajeConfirmación = Snackbar
                                .make(v, "Lugar " + p.getArraylugares().get(seleccionado).getNombre() + " eliminado", Snackbar.LENGTH_LONG)
                                .setAction("DESHACER", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        p.getArraylugares().add(lugar_Delete);

                                        p.getDataBase().AñadirLugar(lugar_Delete);

                                        p.actualizarArrayLugares(R.id.spinner_seleccion_lugar, false, false);

                                        p.actualizarArrayLugares(R.id.spinner_lugar_padre, true, false);

                                        p.actualizarArrayLugares(R.id.spinner_ubicación, true, true);

                                        p.showSnack(Snackbar.make(view, "Restaurado", Snackbar.LENGTH_SHORT))
                                        ;
                                    }

                                });

                        lugar_Delete = p.getArraylugares().get(seleccionado);

                        p.getDataBase().EliminarLugar(p.getArraylugares().get(seleccionado).getId());

                        p.getArraylugares().remove(seleccionado);

                        view = mensajeConfirmación.getView();

                        tv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                        tv.setTextColor(p.getResources().getColor(R.color.colorWhite));

                        mensajeConfirmación.setActionTextColor(p.getResources().getColor(R.color.colorWhite));

                        mensajeConfirmación.show();
                    }
                } else {
                    p.showSnack(Snackbar.make(v, "No hay ningún elemento que eliminar", Snackbar.LENGTH_SHORT));
                }

                break;
        }

        p.actualizarArrayLugares(R.id.spinner_seleccion_lugar, false, false);

        p.actualizarArrayLugares(R.id.spinner_lugar_padre, true, false);

        p.actualizarArrayLugares(R.id.spinner_ubicación, true, true);

        if (seleccionado < p.getArraylugares().size()) {
            ((Spinner) p.findViewById(R.id.spinner_seleccion_lugar)).setSelection(seleccionado);
        }
    }

    public Lugar getLugar() {
        String nombre = ((EditText) p.findViewById(R.id.cr_pj_Lugar_nombre)).getText().toString();
        String desc = ((EditText) p.findViewById(R.id.cr_pj_Lugar_desc)).getText().toString();
        String padre = ((Spinner) p.findViewById(R.id.spinner_lugar_padre)).getSelectedItem().toString();

        if (((Spinner) p.findViewById(R.id.spinner_lugar_padre)).getSelectedItemPosition() == 0) {
            padre = "";
        }
        if (nombre == padre) {
            padre = "";
        }

        Lugar l = new Lugar(nombre, desc, padre);

        return l;
    }

    public void setLugar(Lugar lugar) {

        ((EditText) p.findViewById(R.id.cr_pj_Lugar_nombre)).setText(lugar.getNombre());
        ((EditText) p.findViewById(R.id.cr_pj_Lugar_desc)).setText(lugar.getDescripción());

        int index = -1;

        for (int i = 0; i < p.getArraylugares().size(); i++) {

            if (lugar.getPadre().equals(p.getArraylugares().get(i).getNombre())) {
                index = i + 1;
                break;
            }
        }

        ((Spinner) p.findViewById(R.id.spinner_lugar_padre)).setSelection(index);

    }


}
