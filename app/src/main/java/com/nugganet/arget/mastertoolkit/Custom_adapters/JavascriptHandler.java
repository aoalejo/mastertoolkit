package com.nugganet.arget.mastertoolkit.Custom_adapters;

import android.app.FragmentManager;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import com.nugganet.arget.mastertoolkit.Principal;

public  class  JavascriptHandler {
    Principal mContext;

    /**
     * Instantiate the interface and set the context
     */
    public JavascriptHandler(Principal c) {
        mContext = c;
    }

    /**
     * Show a toast from the web page
     */
    @JavascriptInterface
     public void showToast(String toast) {
        System.out.println("en toast javascript");
        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
    }

  /*  @JavascriptInterface
    public void scrollHeight(int scroll){

        final int viewSize = Base.Entre(scroll,75, 200);

        System.out.println("max view: "+scroll+"\nResized: "+viewSize);
        mContext.ajusteWebText(viewSize,false);

    }*/

    @JavascriptInterface
    public void showHistorial(String id) {

        FragmentManager fm = mContext.getFragmentManager();
        Fragment_classes.Dialog_historial Dialog_historial = new Fragment_classes.Dialog_historial();

        Dialog_historial.setWebContent(mContext.getHistorial().getHistorial_id(id));

        Dialog_historial.show(fm, "Entrada del historial: ");
    }
}
