package com.nugganet.arget.mastertoolkit.Custom_adapters;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.nugganet.arget.mastertoolkit.Principal;
import com.nugganet.arget.mastertoolkit.R;
import com.nugganet.arget.mastertoolkit.WebBuilder;

import net.gotev.speech.SpeechUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by Arget on 21/6/2018.
 */

public class Fragment_classes {


    public static class DialogError extends DialogFragment {

        private String text = "";

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_error, container, false);

            Button dismiss = (Button) rootView.findViewById(R.id.dismiss);

            dismiss.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {


                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, text);
                    sendIntent.setType("WebContent/plain");
                    startActivity(sendIntent);


                    dismiss();
                }
            });


            return rootView;
        }
    }

    public static class Dialog_date_picker extends DialogFragment {

        private Principal p;

        public Principal getP() {
            return p;
        }

        public void setP(Principal p) {
            this.p = p;
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            final View rootView = inflater.inflate(R.layout.fragment_date_picker, container, false);

            ((TextView) rootView.findViewById(R.id.text_encabezado_datepicker)).setText(this.getTag());

            Button seleccionar = (Button) rootView.findViewById(R.id.button_aceptar_date_picker);

            Button cancelar = (Button) rootView.findViewById(R.id.button_cancelar_date_picker);

            final DatePicker date = (DatePicker) rootView.findViewById(R.id.datePicker_fragment);


            seleccionar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String dia = String.valueOf(((DatePicker) date).getDayOfMonth());
                    String mes = String.valueOf(((DatePicker) date).getMonth() + 1);
                    String año = String.valueOf(((DatePicker) date).getYear());

                    if (mes.length() == 1) {
                        mes = "0" + mes;
                    }

                    if (dia.length() == 1) {
                        dia = "0" + dia;
                    }
                    Long millis = (long) 0;
                    try {
                        millis = new SimpleDateFormat("yyyy/MM/dd").parse(año + "/" + mes + "/" + dia).getTime();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    String titulo = "";
                    String cuerpo = "";
                    String lugar = "";
                    String jugador_pri = "";
                    String jugador_sec = "";

                    millis = millis / 1000;

                    int rango = ((Spinner) rootView.findViewById(R.id.spinner_rango_date_picker)).getSelectedItemPosition() + 1;


                    try {
                        titulo = ((EditText) p.findViewById(R.id.editText_Título_logger)).getText().toString();
                        cuerpo = ((EditText) p.findViewById(R.id.editText_Cuerpo_logger)).getText().toString();
                        jugador_pri = ((Spinner) p.findViewById(R.id.spinner_jugadores_log_primario)).getSelectedItem().toString();
                        jugador_sec = ((Spinner) p.findViewById(R.id.spinner_jugadores_log_secundario)).getSelectedItem().toString();
                        lugar = ((Spinner) p.findViewById(R.id.spinner_ubicación_log)).getSelectedItem().toString();
                    } catch (NullPointerException e) {
                        System.out.println(e.toString());
                    }

                    boolean inclusivo = ((ToggleButton) p.findViewById(R.id.toggleButton_metodo_lugar)).isChecked();
                    p.getHistorial().buscar(titulo, jugador_pri, jugador_sec, cuerpo, lugar, millis, rango, inclusivo);
                    //((TextView) p.findViewById(R.id.textView_logger)).setWebContent();


                    dismiss();
                }
            });

            cancelar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });


            return rootView;
        }
    }

    public static class Dialog_tuto_importar extends DialogFragment {

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.fragment_tuto_importar, container, false);

            Button dismiss = (Button) rootView.findViewById(R.id.fragment_info_cerrar);

            dismiss.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            return rootView;
        }
    }

    public static class Dialog_tuto_speech extends DialogFragment {

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.fragment_tuto_speech, container, false);

            Button dismiss = (Button) rootView.findViewById(R.id.fragment_info_cerrar);

            dismiss.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            return rootView;
        }
    }

    public static class Dialog_config_speech extends DialogFragment {

        private Principal p;

        public void setP(Principal p) {
            this.p = p;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.fragment_config_speech, container, false);

            Button dismiss = (Button) rootView.findViewById(R.id.fragment_info_cerrar);
            Button playStore = (Button) rootView.findViewById(R.id.fragment_PlayStore);

            playStore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SpeechUtil.redirectUserToGoogleAppOnPlayStore(p);
                }
            });
            dismiss.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            return rootView;
        }
    }

    public static class Dialog_info extends DialogFragment {

        private String fragment_info_titulo = "";
        private String fragment_info_cuerpo = "";

        public String getFragment_info_titulo() {
            return fragment_info_titulo;
        }

        public void setFragment_info_titulo(String fragment_info_titulo) {
            this.fragment_info_titulo = fragment_info_titulo;
        }

        public String getFragment_info_cuerpo() {
            return fragment_info_cuerpo;
        }

        public void setFragment_info_cuerpo(String fragment_info_cuerpo) {
            this.fragment_info_cuerpo = fragment_info_cuerpo;
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.fragment_info, container, false);

            Button dismiss = (Button) rootView.findViewById(R.id.fragment_info_cerrar);

            TextView título = (TextView) rootView.findViewById(R.id.fragment_info_titulo);
            TextView cuerpo = (TextView) rootView.findViewById(R.id.fragment_info_cuerpo);

            título.setText(fragment_info_titulo);
            cuerpo.setText(fragment_info_cuerpo);


            dismiss.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });


            return rootView;
        }
    }

    public static class Dialog_confirmación extends DialogFragment {

        private String fragment_info_titulo = "";
        private String fragment_info_cuerpo = "";
        View.OnClickListener onClickListener;
        View rootView;
        private boolean showSpinner = false;
        SpinnerAdapter adapter_spinner;

        public String getFragment_info_titulo() {
            return fragment_info_titulo;
        }

        public void setFragment_info_titulo(String fragment_info_titulo) {
            this.fragment_info_titulo = fragment_info_titulo;
        }

        public String getFragment_info_cuerpo() {
            return fragment_info_cuerpo;
        }

        public void setFragment_info_cuerpo(String fragment_info_cuerpo) {
            this.fragment_info_cuerpo = fragment_info_cuerpo;
        }

        public void setOnClickListener(View.OnClickListener onClickListener) {
            this.onClickListener = onClickListener;
        }

        public int objSpinner(){
            return ((Spinner)(rootView.findViewById(R.id.spinner_confirm_opciones))).getSelectedItemPosition();
        }

        public void set_spinner(SpinnerAdapter adapter_spinner) {

            showSpinner = true;
            this.adapter_spinner = adapter_spinner;

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            rootView = inflater.inflate(R.layout.fragment_confirmacion, container, false);

            Button cancelar = (Button) rootView.findViewById(R.id.fragment_conf_cerrar);
            Button aplicar = (Button) rootView.findViewById(R.id.fragment_conf_aplicar);

            aplicar.setOnClickListener(onClickListener);

            TextView título = (TextView) rootView.findViewById(R.id.fragment_conf_titulo);
            TextView cuerpo = (TextView) rootView.findViewById(R.id.fragment_conf_cuerpo);

            título.setText(fragment_info_titulo);
            cuerpo.setText(fragment_info_cuerpo);

            if (showSpinner) {
                rootView.findViewById(R.id.spinner_confirm_opciones).setVisibility(View.VISIBLE);

                ((Spinner) rootView.findViewById(R.id.spinner_confirm_opciones)).setAdapter(adapter_spinner);

            }

            cancelar.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            return rootView;
        }
    }


    public static class Dialog_historial extends DialogFragment {

        private String WebContent = "";

        public String getWebContent() {
            return WebContent;
        }

        public void setWebContent(String webContent) {
            this.WebContent = webContent;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_historial, container, false);

            Button dismiss = (Button) rootView.findViewById(R.id.button_cerrar_hist);

            WebView webp = (WebView) rootView.findViewById(R.id.WebView_Historial_detalle);

            webp.getSettings().setJavaScriptEnabled(true);

            // webp.loadUrl(WebBuilder.add(WebContent));
            WebContent = WebContent.replace("<hr>", "");
            WebContent = WebContent.replace("</details>", "");
            WebContent = WebContent.replace("<details>", "<hr>");
            WebContent = WebContent.replace("<summary>", "");
            WebContent = WebContent.replace("</summary>", "");

            webp.loadData(WebBuilder.DETALLE + WebContent + WebBuilder.FinDoc, "text/html; charset=UTF-8", null);

            dismiss.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });


            return rootView;
        }
    }
}
