package com.nugganet.arget.mastertoolkit.Clases_objetos;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.nugganet.arget.mastertoolkit.Base;

import java.util.Arrays;

/**
 * Created by Arget on 11/4/2017.
 */

public class Arma {

    public static final int TIPO_ARMA_A_UNA_MANO = 0;
    public static final int TIPO_ARMA_A_DOS_MANOS = 1;
    public static final int TIPO_ARMA_LANZABLE = 2;
    public static final int TIPO_ARMA_PROYECTIL = 3;
    public static final int TIPO_ARMA_DISPARO = 4;


    public static final int DAÑO_BASE = 0;
    public static final int TURNO_ARMA = 1;
    public static final int TIPO_DE_ARMA = 2;    //0 = 1 mano 1=2 manos 2=lanzable 3=proyectil 4=disparo
    public static final int CRIT_PRINCIPAL = 3;
    public static final int CRIT_SECUNDARIO = 4;
    public static final int DAÑA_ENERGÍA = 5;
    public static final int ENTEREZA_ARMA = 6;
    public static final int ROTURA_ARMA = 7;
    public static final int PRESENCIA_ARMA = 8;
    public static final int BONO_ATAQUE = 9;
    public static final int BONO_PARADA = 10;
    public static final int BONO_ESQUIVA = 11;
    public static final int TA_IGNORADA = 12;
    public static final int ALCANCE = 13;
    public static final int CADENCIA = 14;
    public static final int RECARGA = 15;
    public static final int ENTEREZA_PROYECTIL = 16;
    public static final int ROTURA_PROYECTIL = 17;
    public static final int PRESENCIA_PROYECTIL = 18;
    public static final int EFECTO_A = 19;
    public static final int DIF_EFECTO_A = 20;
    public static final int EFECTO_B = 21;
    public static final int DIF_EFECTO_B = 22;
    public static final int EFECTO_C = 23;
    public static final int DIF_EFECTO_C = 24;
    public static final int CALIDAD = 25;
    public static final int DESARMADO = 26;
    public static final int PRESA = 27;
    public static final int TAMAÑO = 28;
    public static final int HABILIDAD_USADA = 29;

    public static final int HABILIDAD_ATAQUE =0;
    public static final int HABILIDAD_P_MAGICA =1;
    public static final int HABILIDAD_P_PSIQUICA =2;


    public static final int TAMAÑO_CHICO = 0;
    public static final int TAMAÑO_MEDIANO = 1;
    public static final int TAMAÑO_GRANDE = 2;

    @Expose
    private String Nombre = "";
    @Expose
    private int[] caracteristicas = new int[30];
    @Expose
    private int ID;

    public Arma() {
        Nombre = "Vacio";
    }

    public String toJson() {
        return new GsonBuilder().create().toJson(this, this.getClass());
    }

    public Arma fromJson(String json) {
        return new Gson().fromJson(json, this.getClass());
    }

    public Arma(boolean aleatorio) {
        String[] nombres = {"Maza", "Alabarda", "Mandoble", "Hacha", "Arco", "Ballesta", "Espada"};

        Nombre = nombres[((int) (Math.random() * 10) % 6)];
        caracteristicas[DAÑO_BASE] = (int) (Math.random() * 150);
    }

    public void setCaracteristica(int index,int valorNuevo){
        caracteristicas[index] = valorNuevo;
    }

    public void reemplazar(Arma nuevo) {
        this.Nombre = nuevo.getNombre();
        this.caracteristicas = nuevo.getCaracteristicas();
    }

    public Arma(String nombre) {
        Nombre = nombre;
    }


    public Arma(String nombre, int[] caracteristicas) {
        Nombre = nombre;
        this.caracteristicas = caracteristicas;
    }


    public void reemplazar_valor_caracteristica(int index, int nuevo) {
        this.caracteristicas[index] = nuevo;
    }


    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public int[] getCaracteristicas() {
        return caracteristicas;
    }

    public void setCaracteristicas(int[] caracteristicas) {
        this.caracteristicas = caracteristicas;
    }

    public void setCaracteristicas(int nuevo, int index) {
        this.caracteristicas[index] = nuevo;
    }


    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public Arma(String nombre, int[] caracteristicas, int ID) {
        Nombre = nombre;
        this.caracteristicas = caracteristicas;
        this.ID = ID;
    }

    public Arma(Arma nueva_arma) {
        Nombre = nueva_arma.getNombre();
        this.caracteristicas = nueva_arma.getCaracteristicas();
        this.ID = nueva_arma.getID();
    }

    @Override
    public String toString() {
        return "Arma{" +
                "Nombre='" + Nombre + '\'' +
                ", caracteristicas=" + Arrays.toString(caracteristicas) +
                ", ID_TABLE=" + ID +
                '}';
    }
}
