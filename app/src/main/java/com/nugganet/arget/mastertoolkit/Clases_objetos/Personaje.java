package com.nugganet.arget.mastertoolkit.Clases_objetos;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.nugganet.arget.mastertoolkit.Base;
import com.nugganet.arget.mastertoolkit.Combate;

import java.util.Arrays;
import java.util.Collections;

/**
 * Created by Arget on 19/4/2017.
 */

public class Personaje {
    //Tipos de personajes
    public final static int TIPO_PJ_HUMANO = 0;
    public final static int TIPO_PJ_INHUMANO = 1;
    public final static int TIPO_PJ_ZEN = 2;
    public final static int TIPO_PJ_DEMIGOD = 3;

    //Atributos base

    private transient byte[] token;
    private transient byte[] imagen;

    @Expose
    private int ID;

    @Expose
    private String nombre_jugador = "";

    @Expose
    private String nombre_personaje = "NPC";

    @Expose
    private String descripción = "";

    @Expose
    private int nivelPifia = 3;

    @Expose
    private int tipoDeEntidad = 0; //0= humano 1= inhumano 2 = zen

    @Expose
    private int tipoDePersonaje = 0; // 0= jugador 1=NPC 2= Generico

    @Expose
    private int[] caracteristicas = new int[100];

    @Expose
    private int[] habilidades_psiquicas = new int[200];

    @Expose
    private int[] caracteristicas_temporales = new int[100];

    @Expose
    private boolean[] Conocimientos_especiales = new boolean[10];

    @Expose
    private int[] penalizadores = new int[5];

    @Expose
    private int[] especiales = new int[10];

    @Expose
    private Armadura[] armaduras = new Armadura[6];

    @Expose
    private Arma[] armas = new Arma[6];

    public static final class TIPO {
        public final static int JUGADOR = 0;
        public final static int NPC = 1;
        public final static int GENERICO = 2;
    }


    public static final class PENALIZADORES {
        public final static int MOVIMIENTO = 0;
        public final static int VISUAL = 1;
        public final static int TODA_ACCION = 2;
        public final static int ACTUALES = 3;
        public final static int ARMADURA = 4;
        public final static int FATIGA = 5;
    }

    public static final class PSIQUICOS {
        public final static int POTENCIAL_BASE = 0;
        public final static int CVS_MAX = 1;
        public final static int CVS_ACT = 2;
        public final static int RECUP_CVS = 3;
    }


    //Preparar merge de Personaje lite
    @Expose
    private int index_última_arma_usada = 0;
    @Expose
    private int último_turno_final = 0;
    @Expose
    private boolean[] modificadores = new boolean[100];
    @Expose
    private int cantidad = 0;
    @Expose
    private int[] últimas_tiradas = new int[100];

    //Getter y setter de valores temporales


    public int[] getÚltimas_tiradas() {
        return últimas_tiradas;
    }

    public void setÚltimas_tiradas(int[] últimas_tiradas) {
        this.últimas_tiradas = últimas_tiradas;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getIndex_última_arma_usada() {
        return index_última_arma_usada;
    }

    public void setIndex_última_arma_usada(int index_última_arma_usada) {
        this.index_última_arma_usada = index_última_arma_usada;
    }

    public int getÚltimo_turno_final() {
        return último_turno_final;
    }

    public void setÚltimo_turno_final(int último_turno_final) {
        this.último_turno_final = último_turno_final;
    }

    public boolean[] getModificadores() {
        return modificadores;
    }

    public void setModificadores(boolean[] modificadores) {
        this.modificadores = modificadores;
    }

    //metodos relacionados con caracteristicas temporales

    public void modif_combate(int index, boolean estado) {
        this.modificadores[index] = estado;
    }

    public Personaje(int ID, String nombre_jugador, String nombre_personaje) {
        this.ID = ID;
        this.nombre_jugador = nombre_jugador;
        this.nombre_personaje = nombre_personaje;
    }

    //Obtener valores

    public byte[] getToken() {
        return token;
    }

    public String getNombre_jugador() {
        return nombre_jugador;
    }

    public String getNombre_personaje() {
        return nombre_personaje;
    }

    public int getNivelPifia() {
        return nivelPifia;
    }

    public int getTipoDeEntidad() {
        return tipoDeEntidad;
    }

    public int getTipoDePersonaje() {
        return tipoDePersonaje;
    }

    public int[] getCaracteristicas() {
        return caracteristicas;
    }


    public int[] getCaracteristicas_temporales() {
        return caracteristicas_temporales;
    }

    public boolean[] getConocimientos_especiales() {
        return Conocimientos_especiales;
    }

    public int[] getPenalizadores() {
        return penalizadores;
    }

    public int[] getEspeciales() {
        return especiales;
    }

    public int getID() {
        return ID;
    }

    public String getNombres() {
        return nombre_personaje + " - " + nombre_jugador;
    }

    public String getDescripción() {
        return descripción;
    }

    public byte[] getImagen() {
        return imagen;
    }

    public int[] getHabilidades_psiquicas() {
        return habilidades_psiquicas;
    }

    public Armadura[] getArmaduras() {
        return armaduras;
    }

    public Arma[] getArmas() {
        return armas;
    }

    //Escribir valores


    public void setHabilidades_psiquicas(int[] habilidades_psiquicas) {
        this.habilidades_psiquicas = habilidades_psiquicas;
    }

    public void setArmaduras(Armadura[] armaduras) {
        this.armaduras = armaduras;
    }

    public void setArmadura(Armadura armadura, int i) {
        this.armaduras[i] = armadura;
    }

    public void setArmas(Arma[] armas) {
        this.armas = armas;
    }

    public void setArma(Arma arma, int i) {
        this.armas[i] = arma;
    }

    public void setImagen(byte[] imagen) {
        this.imagen = imagen;
    }

    public void setDescripción(String descripción) {
        this.descripción = descripción;
    }

    public void setToken(byte[] token) {
        this.token = token;
    }

    public void setNombre_jugador(String nombre_jugador) {
        this.nombre_jugador = nombre_jugador;
    }

    public void setNombre_personaje(String nombre_personaje) {
        this.nombre_personaje = nombre_personaje;
    }

    public void setNivelPifia(int nivelPifia) {
        this.nivelPifia = nivelPifia;
    }

    public void setTipoDeEntidad(int tipoDeEntidad) {
        this.tipoDeEntidad = tipoDeEntidad;
    }

    public void setTipoDePersonaje(int tipoDePersonaje) {
        this.tipoDePersonaje = tipoDePersonaje;
    }

    public void setCaracteristicas(int[] caracteristicas) {
        this.caracteristicas = caracteristicas;
    }

    public void setCaracteristicas_temporales(int[] caracteristicas_temporales) {
        this.caracteristicas_temporales = caracteristicas_temporales;
    }

    public void setConocimientos_especiales(boolean[] conocimientos_especiales) {
        Conocimientos_especiales = conocimientos_especiales;
    }

    public void setPenalizadores(int[] penalizadores) {
        this.penalizadores = penalizadores;
    }

    public void setEspeciales(int[] especiales) {
        this.especiales = especiales;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    //   Escribir reemplazando

    public void reemplazar_valor_caracteristica(int index, int nuevo) {
        this.caracteristicas[index] = nuevo;
    }

    public void reemplazar_valor_psiquico(int index, int nuevo) {
        this.habilidades_psiquicas[index] = nuevo;
    }

    public void reemplazar_valor_extra(int index, int nuevo) {
        this.especiales[index] = nuevo;
    }

    public void reemplazar_valor_caracteristica_temp(int index, int nuevo) {
        this.caracteristicas_temporales[index] = nuevo;
    }

    public void reemplazar_valor_penalizador(int index, int nuevo) {
        this.penalizadores[index] = nuevo;
    }

    //Escribir sumando

    public void caracteristicasSum(int index, int valor) {
        this.caracteristicas[index] = this.caracteristicas[index] + valor;
    }

    public void penalizadoresSum(int index, int valor) {
        this.penalizadores[index] = this.penalizadores[index] + valor;
    }

    //Métodos generales

    public void randomize(int porcentaje) {

        for (int i = 0; i < caracteristicas.length; i++) {

            double porcentaje_float = ((((Math.random() * porcentaje * 2) - porcentaje) / 100) * caracteristicas[i]) + caracteristicas[i];

            caracteristicas[i] = (int) (porcentaje_float);

        }

        if (caracteristicas[Base.VIDA_ACT] > caracteristicas[Base.VIDA_MAX]) {
            caracteristicas[Base.VIDA_MAX] = caracteristicas[Base.VIDA_ACT];
        }
    }


    public int resultado(int indice, int tirada) {
        int resultado = 0;

        if (indice > Base.ATRIBUTO_VOLUNTAD) {
            resultado = caracteristicas[indice] + caracteristicas_temporales[indice] + tirada - Penalizadores(Base.TODA_ACCIÓN);
        } else {
            resultado = caracteristicas[indice] + caracteristicas_temporales[indice] + tirada - (Penalizadores(Base.TODA_ACCIÓN) / 40);
        }

        return resultado;
    }

    public int tirada() {
        return Base.tirada(this.tipoDeEntidad, this.getNivelPifia(), 90);
    }

    public int resultado(int indice) {
        int resultado = 0;

        int dificultadSuperada = 0;

        int tipoMovim = 0;
        int velocidad_normal = 0;
        int velocidad_maxima = 0;

        int fuerza = 0;
        int carga_normal = 0;
        int carga_maxima = 0;
        int carga_movimiento = 0;

        String texto = "";

        int bono = 0;

        int tirada = Base.tirada(this.tipoDeEntidad, this.getNivelPifia(), 90);

        resultado = Caracteristica(indice) + tirada;

        switch (indice) {
            case Base.HABILIDAD_PFUERZA:// pfuerza

                resultado = resultado + Penalizadores(PENALIZADORES.TODA_ACCION);

                dificultadSuperada = (Base.ControlHabilidad(resultado));
                if (Base.inRangeEq(0, 3, dificultadSuperada)) {
                    return 0;
                }
                if (Base.inRangeEq(4, 5, dificultadSuperada)) {
                    return 1;
                }
                if (Base.inRangeEq(6, 6, dificultadSuperada)) {
                    return 2;
                }
                if (Base.inRangeEq(7, 7, dificultadSuperada)) {
                    return 3;
                }
                if (Base.inRangeEq(8, 100, dificultadSuperada)) {
                    return 4;
                }
                break;
            case Base.HABILIDAD_RESDOLOR:// resdolor
                dificultadSuperada = (Base.ControlHabilidad(resultado));
                switch (dificultadSuperada) {
                    case 0:
                    case 1:
                    case 2:
                        bono = 0;
                        break;
                    case 3:
                        bono = 10;
                        break;
                    case 4:
                        bono = 20;
                        break;
                    case 5:
                        bono = 30;
                        break;
                    case 6:
                        bono = 40;
                        break;
                    case 7:
                        bono = 50;
                        break;
                    case 8:
                        bono = 60;
                        break;
                    case 9:
                        bono = 70;
                        break;
                    case 10:
                        bono = 80;
                        break;
                }
                return bono;


        }


        return 0;

    }

    public String descripción(int indice, int tirada) {
        int resultado = 0;

        int dificultadSuperada = 0;

        int tipoMovim = 0;
        int velocidad_normal = 0;
        int velocidad_maxima = 0;

        int fuerza = 0;
        int carga_normal = 0;
        int carga_maxima = 0;
        int carga_movimiento = 0;

        String texto = "";

        int bono = 0;

        if (Base.inRangeEq(0, 7, indice)) {

            resultado = caracteristicas[indice] + caracteristicas_temporales[indice] + (tirada);
            switch (Base.ControlCaracteristica(resultado)) {
                case 0:
                    return "No supera ni la dificultad 'facil'...";

                case 1:
                    return "Logra solamente cosas que un campesino lograría.";

                case 2:
                    return "Logra cosas que alguien un poco entrenado lograría.";

                case 3:
                    return "Logra solamente cosas que una persona entrenada podría hacer.";

                case 4:
                    return "Supera casi cualquier eventualidad, cosas extremas.";
            }
        }

        if (Base.inRangeEq(8, 13, indice)) {
            resultado = caracteristicas[indice] + caracteristicas_temporales[indice] + tirada;
            switch (indice) {
                case 8://RF
                    return "Supera una Resistencia Física contra " + resultado + ".";
                case 9://RE
                    return "Supera una Resistencia a Enfermedades contra " + resultado + ".";
                case 10://RV
                    return "Supera una Resistencia a Venenos contra " + resultado + ".";
                case 11://RM
                    return "Supera una Resistencia Mágica contra " + resultado + ".";
                case 12://RP
                    return "Supera una Resistencia Psiquica contra " + resultado + ".";
                case 13://PRES
                    return "Supera una Presencia contra " + resultado + ".";
            }
        }

        if (Base.inRangeEq(14, 89, indice)) {

            resultado = caracteristicas[indice] + caracteristicas_temporales[indice] + tirada;

            switch (indice) {
                case Base.HABILIDAD_ATAQUE:// ataque
                    resultado = resultado + Penalizadores(PENALIZADORES.TODA_ACCION);
                    return "Resultado del ataque: " + resultado + ".";
                case Base.HABILIDAD_PARADA:// parada
                    resultado = resultado + Penalizadores(PENALIZADORES.TODA_ACCION);
                    return "Resultado de la parada: " + resultado + ".";
                case Arma.BONO_ESQUIVA:// esquiva
                    resultado = resultado + Penalizadores(PENALIZADORES.TODA_ACCION);
                    return "Resultado de la esquiva: " + resultado + ".";
                case Base.HABILIDAD_ARMADURA:// armadura
                    return "Resultado de llevar armadura (????????): " + resultado + ".";
                case Base.HABILIDAD_TURNO:// turno
                    resultado = resultado + Penalizadores(PENALIZADORES.TODA_ACCION);
                    texto = "";
                    for (int i = 0; i < 6; i++) {
                        texto = texto + "con " + arma(i).getNombre() + ": " + (resultado + arma(i).getCaracteristicas()[Arma.TURNO_ARMA]);
                        if (i != 5) {
                            texto = texto + ", ";
                        }
                    }
                    return resultado + " y armado " + texto + ".";
                case Base.HABILIDAD_PROYECCION_MAGICA:// presiciónmagica
                    switch (Base.ControlHabilidad(resultado)) {
                        case 0:
                            texto = "0";
                            break;
                        case 1:
                            texto = "5";
                            break;
                        case 2:
                            texto = "25";
                            break;
                        case 3:
                            texto = "100";
                            break;
                        case 4:
                            texto = "250";
                            break;
                        case 5:
                            texto = "500";
                            break;
                        case 6:
                            texto = "1000 (necesita localizar exactamente el objetivo)";
                            break;
                        case 7:
                            texto = "5000 (solo necesita saber su ubicación aproximada)";
                            break;
                    }

                    return "Resultado de la presición mágica: " + resultado + ", logrando a una distancia de hasta " + texto + " metros.";

                case Base.HABILIDAD_PROYECCION_PSIQUICA:// presiciónpsiquica
                    switch (Base.ControlHabilidad(resultado)) {
                        case 0:
                            texto = "0";
                            break;
                        case 1:
                            texto = "5";
                            break;
                        case 2:
                            texto = "25";
                            break;
                        case 3:
                            texto = "100";
                            break;
                        case 4:
                            texto = "250";
                            break;
                        case 5:
                            texto = "500";
                            break;
                        case 6:
                            texto = "1000 (necesita localizar exactamente el objetivo)";
                            break;
                        case 7:
                            texto = "5000 (solo necesita saber su ubicación aproximada)";
                            break;
                    }

                    return "Resultado de la presición psiquica: " + resultado + ", logrando a una distancia de hasta " + texto + " metros.";

                case Base.HABILIDAD_ACROBACIAS:// acrobacias

                    return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";

                case Base.HABILIDAD_ATLETISMO:// atletismo

                    tipoMovim = 0;

                    dificultadSuperada = (Base.ControlHabilidad(resultado));

                    if (dificultadSuperada > 5) {
                        tipoMovim = 1;
                    }
                    if (dificultadSuperada > 7) {
                        tipoMovim = 2;
                    }
                    if (dificultadSuperada > 9) {
                        tipoMovim = 3;
                    }

                    tipoMovim = tipoMovim - Penalizadores(Base.MOVIMIENTO);

                    tipoMovim = tipoMovim + caracteristicas[Base.ATRIBUTO_AGILIDAD] + caracteristicas[Base.ATRIBUTO_AGILIDAD];

                    velocidad_normal = Base.Velocidad(tipoMovim);
                    velocidad_maxima = (int) (velocidad_normal * 0.6);


                    String[] tabla = {"no puede moverse", "Corriendo (can) 1 Minuto(vel:" + velocidad_normal + " m/t).\r\nMov. Maximo (2 can) 1 asalto(vel:" + velocidad_maxima + " m/t).", "Corriendo (can) 5 minutos(vel:" + velocidad_normal + " m/t).\r\nMov. Maximo (2 can) 2 asaltos(vel:" + velocidad_maxima + " m/t).", "Corriendo (can) 10 minutos(vel:" + velocidad_normal + " m/t).\r\nMov. Maximo (2 can) 5 asaltos(vel:" + velocidad_maxima + " m/t).", "Corriendo (can) 20 minutos(vel:" + velocidad_normal + " m/t).\r\nMov. Maximo (2 can) 10 asaltos(vel:" + velocidad_maxima + " m/t).", "Corriendo (can) 40 minutos(vel:" + velocidad_normal + " m/t).\r\nMov. Maximo (2 can) 15 asaltos(vel:" + velocidad_maxima + " m/t).", "Corriendo (can) 1 hora(vel:" + velocidad_normal + " m/t).\r\nMov. Maximo (2 can) 1 minuto(vel:" + velocidad_maxima + " m/t).", "Corriendo (can) 2 horas(vel:" + velocidad_normal + " m/t).\r\nMov. Maximo (2 can) 2 minutos(vel:" + velocidad_maxima + " m/t).", "Corriendo (can) 5 horas(vel:" + velocidad_normal + " m/t).\r\nMov. Maximo (2 can) 3 minutos(vel:" + velocidad_maxima + " m/t).", "Corriendo (can) 1 día(vel:" + velocidad_normal + " m/t).\r\nMov. Maximo (2 can) 5 minutos(vel:" + velocidad_maxima + " m/t).", "Corriendo (can) 2 dias(vel:" + velocidad_normal + " m/t).\r\nMov. Maximo (2 can) 10 minutos(vel:" + velocidad_maxima + " m/t).", "Corriendo (can) 5 día(vel:" + velocidad_normal + " m/t).\r\nMov. Maximo (2 can) 20 minutos(vel:" + velocidad_maxima + " m/t)."};

                    return tabla[dificultadSuperada];

                case Base.HABILIDAD_MONTAR:// montar

                    return "Cualquier habilidad podrá usar como maximo este valor " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_NADAR:// nadar

                    dificultadSuperada = (Base.ControlHabilidad(resultado));

                    switch (dificultadSuperada) {
                        case 0:
                            tipoMovim = -8;
                            break;
                        case 1:
                            tipoMovim = -5;
                            break;
                        case 2:
                            tipoMovim = -4;
                            break;
                        case 3:
                            tipoMovim = -3;
                            break;
                        case 4:
                            tipoMovim = -3;
                            break;
                        case 5:
                            tipoMovim = -2;
                            break;
                        case 6:
                            tipoMovim = -2;
                            break;
                        case 7:
                            tipoMovim = -1;
                            break;
                        case 8:
                            tipoMovim = -0;
                            break;
                        case 9:
                            tipoMovim = -0;
                            break;
                        case 10:
                            tipoMovim = -0;
                            break;
                    }


                    tipoMovim = tipoMovim + caracteristicas[Base.ATRIBUTO_AGILIDAD] + caracteristicas[Base.ATRIBUTO_AGILIDAD];

                    if (tipoMovim < 1) {
                        return "el personaje se hunde";
                    } else {
                        return "velocidad:" + Base.Velocidad(tipoMovim) + " m/t";
                    }

                case Base.HABILIDAD_TREPAR:// trepar
                    return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_SALTAR:// saltar

                    dificultadSuperada = (Base.ControlHabilidad(resultado));
                    switch (dificultadSuperada) {
                        case 0:
                            tipoMovim = -1;
                            break;
                        case 1:
                            tipoMovim = -0;
                            break;
                        case 2:
                            tipoMovim = -0;
                            break;
                        case 3:
                            tipoMovim = 1;
                            break;
                        case 4:
                            tipoMovim = 1;
                            break;
                        case 5:
                            tipoMovim = 2;
                            break;
                        case 6:
                            tipoMovim = 2;
                            break;
                        case 7:
                            tipoMovim = 3;
                            break;
                        case 8:
                            tipoMovim = 4;
                            break;
                        case 9:
                            tipoMovim = 5;
                            break;
                        case 10:
                            tipoMovim = 5;
                            break;
                    }

                    tipoMovim = tipoMovim + caracteristicas[Base.ATRIBUTO_AGILIDAD] + caracteristicas[Base.ATRIBUTO_AGILIDAD];

                    if (tipoMovim > 20) {
                        tipoMovim = 20;
                    }

                    return "salta " + Base.Velocidad(tipoMovim) + " metros.";

                case Base.HABILIDAD_ESTILO:// estilo

                    return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_INTIMIDAR:// imidar

                    return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_LIDERAZGO:// liderazgo

                    return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_PERSUASIÓN:// persuasión

                    return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_COMERCIO:// comercio

                    return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_CALLEJEO:// callejeo

                    return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_ETIQUETA:// etiqueta

                    return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_ADVERTIR:// advertir
                    resultado = resultado - Penalizadores(Base.VISUAL);
                    return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_BUSCAR:// buscar
                    resultado = resultado - Penalizadores(Base.VISUAL);
                    return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_RASTREAR:// rastrear
                    resultado = resultado - Penalizadores(Base.VISUAL);
                    return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_ANIMALES:// animales

                    return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_CIENCIA:// ciencia

                    return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_LEY:// ley

                    return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_HERBOLARIA:// herbolaria

                    return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_HISTORIA:// historia

                    return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_TACTICA:// tactica

                    return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_MEDICINA:// medicina
                    dificultadSuperada = (Base.ControlHabilidad(resultado));
                    switch (dificultadSuperada) {
                        case 0:
                            texto = "la herida no mejora";
                            break;
                        case 1:
                            texto = "la herida no mejora";
                            break;
                        case 2:
                            texto = "se detiene la hemorragia";
                            break;
                        case 3:
                            texto = "se detiene la hemorragia y estabiliza al objetivo";
                            break;
                        case 4:
                            texto = "se detiene la hemorragia, estabiliza al objetivo y cura un 10% del daño";
                            break;
                        case 5:
                            texto = "se detiene la hemorragia, estabiliza al objetivo y cura un 20% del daño";
                            break;
                        case 6:
                            texto = "se detiene la hemorragia, estabiliza al objetivo y cura un 30% del daño";
                            break;
                        case 7:
                            texto = "se detiene la hemorragia, estabiliza al objetivo y cura un 40% del daño";
                            break;
                        case 8:
                            texto = "se detiene la hemorragia, estabiliza al objetivo y cura un 50% del daño";
                            break;
                        case 9:
                            texto = "se detiene la hemorragia, estabiliza al objetivo y cura un 70% del daño";
                            break;
                        case 10:
                            texto = "se detiene la hemorragia, estabiliza al objetivo y cura un 100% del daño";
                            break;
                    }
                    return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + "). " + texto;

                case Base.HABILIDAD_MEMORIZAR:// memorizar

                    return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_NAVEGACIÓN:// navegación

                    return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_OCULTISMO:// ocultismo

                    return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_TASACIÓN:// tasación
                    return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_MÁGICA:// mágica
                    return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_FRIALDAD:// frialdad
                    dificultadSuperada = (Base.ControlHabilidad(resultado));
                    switch (dificultadSuperada) {
                        case 0:
                            bono = 5;
                            break;
                        case 1:
                            bono = 10;
                            break;
                        case 2:
                            bono = 15;
                            break;
                        case 3:
                            bono = 20;
                            break;
                        case 4:
                            bono = 25;
                            break;
                        case 5:
                            bono = 30;
                            break;
                        case 6:
                            bono = 35;
                            break;
                        case 7:
                            bono = 40;
                            break;
                        case 8:
                            bono = 45;
                            break;
                        case 9:
                            bono = 50;
                            break;
                        case 10:
                            bono = 55;
                            break;
                    }

                    return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ") Obtiene un bono de " + bono + " a ataques psiquicos.";


                case Base.HABILIDAD_PFUERZA:// pfuerza

                    dificultadSuperada = (Base.ControlHabilidad(resultado));
                    if (Base.inRangeEq(0, 3, dificultadSuperada)) {
                        bono = 0;
                    }
                    if (Base.inRangeEq(4, 5, dificultadSuperada)) {
                        bono = 1;
                    }
                    if (Base.inRangeEq(6, 6, dificultadSuperada)) {
                        bono = 2;
                    }
                    if (Base.inRangeEq(7, 7, dificultadSuperada)) {
                        bono = 3;
                    }
                    if (Base.inRangeEq(8, 100, dificultadSuperada)) {
                        bono = 4;
                    }

                    fuerza = caracteristicas[Base.ATRIBUTO_FUERZA] + caracteristicas_temporales[Base.ATRIBUTO_FUERZA] + bono;

                    carga_maxima = Base.CargaMax(fuerza);
                    carga_normal = Base.CargaMax(Caracteristica(Base.ATRIBUTO_FUERZA));
                    carga_movimiento = Base.Carga(Caracteristica(Base.ATRIBUTO_FUERZA));


                    return "El personaje puede cargar " + carga_maxima + "Kg instantaneamente. Habitualmente podría " + carga_normal + "Kg / " + carga_movimiento + "Kg.";
                case Base.HABILIDAD_RESDOLOR:// resdolor
                    dificultadSuperada = (Base.ControlHabilidad(resultado));
                    switch (dificultadSuperada) {
                        case 0:
                        case 1:
                        case 2:
                            bono = 0;
                            break;
                        case 3:
                            bono = 10;
                            break;
                        case 4:
                            bono = 20;
                            break;
                        case 5:
                            bono = 30;
                            break;
                        case 6:
                            bono = 40;
                            break;
                        case 7:
                            bono = 50;
                            break;
                        case 8:
                            bono = 60;
                            break;
                        case 9:
                            bono = 70;
                            break;
                        case 10:
                            bono = 80;
                            break;
                    }


                    return "Los penalizadores por dolor/cansancio se reducen en " + bono;


                case Base.HABILIDAD_CERRAJERÍA:// cerrajería
                    return "(cerrajería) Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_DISFRAZ:// disfraz
                    return "(disfraz) Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_OCULTARSE:// ocultarse
                    return "(ocultarse) Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_ROBO:// robo
                    return "(robo) Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_SIGILO:// sigilo
                    return "(sigilo) Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_TRAMPERÍA:// trampería
                    dificultadSuperada = (Base.ControlHabilidad(resultado));
                    return "(trampería) Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + "), creando/desarmando una trampa de nivel " + dificultadSuperada + "0 /" + (dificultadSuperada - 4) + "0 .";
                case Base.HABILIDAD_VENENOS:// venenos
                    return "(venenos) Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + "), creando un veneno de nivel " + dificultadSuperada + "0.";
                case Base.HABILIDAD_ARTE:// arte
                    return "(arte) Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_BAILE:// baile
                    return "(baile) Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_FORJA:// forja
                    return "(forja) Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_RUNAS:// runas
                    return "(runas) Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_ALQUIMIA:// alquimia
                    return "(alquimia) Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_ANIMISMO:// animismo
                    return "(animismo) Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_MÚSICA:// música
                    return "(música) Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                case Base.HABILIDAD_TMANOS:// tmanos
                    return "(tmanos) Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";

            }
        }


        return "";
    }


    public int Penalizadores(int tipo) {

        final class PENALIZADORES {
            public final static int MOVIMIENTO = 0;
            public final static int VISUAL = 1;
            public final static int TODA_ACCION = 2;
            public final static int ACTUALES = 3;
            public final static int ARMADURA = 4;
            public final static int FATIGA = 5;

        }

        switch (tipo) {
            case PENALIZADORES.MOVIMIENTO:

                return Penalizadores_old(0);

            case PENALIZADORES.VISUAL:

                return Penalizadores_old(1);

            case PENALIZADORES.TODA_ACCION:

                return Penalizadores_old(2) + Penalizadores(PENALIZADORES.FATIGA);

            case PENALIZADORES.ACTUALES:

                return Penalizadores_old(3);

            case PENALIZADORES.ARMADURA:

                return Penalizadores_old(4);

            case PENALIZADORES.FATIGA:

                if (caracteristicas[Base.ENERGÍA_ACT] != caracteristicas[Base.ENERGÍA_MAX] & caracteristicas[Base.ENERGÍA_ACT] < 6) {

                    int[] penalizadores_energía = {-120, -80, -40, -20, -10, 0, 0, 0, 0, 0, 0, 0, 0, 0};

                    return penalizadores_energía[caracteristicas[Base.ENERGÍA_ACT]];

                }

                return 0;

        }
        return 0;
    }

    public int Penalizadores_old(int tipo) {


        int pen = 0;
        /*TIPO:

        0 = Movimiento
        1 = Visual
        2 = Toda Acción Fisicos
        3 = Toda Acción Actuales
        4 = por armadura;

         */
        int llArm = caracteristicas[Base.HABILIDAD_ARMADURA] + caracteristicas_temporales[Base.HABILIDAD_ARMADURA];
        int requerimiento = 0;
        int penalizadores_actuales = 0;
        int reducción = 0;

        switch (tipo) {
            case 0:

                for (Armadura armadura : armaduras) {
                    if (armadura.getCaracteristicas()[Base.ARM_ACTIVA] == 1) {
                        reducción = reducción + armadura.getCaracteristicas()[Base.ARM_PENALIZADOR_MOVIMIENTO];
                        requerimiento = requerimiento + armadura.getCaracteristicas()[Base.ARM_REQUERIMIENTO];
                    }
                }

                pen = (int) Math.floor((requerimiento + penalizadores_actuales - llArm) / 50);
                pen = reducción + pen;
                return pen;

            case 1:

                for (Armadura armadura : armaduras) {
                    if (armadura.getCaracteristicas()[Base.ARM_ACTIVA] == 1) {
                        reducción = reducción + armadura.getCaracteristicas()[Base.ARM_PENALIZADOR_VISUAL];
                        requerimiento = requerimiento + armadura.getCaracteristicas()[Base.ARM_REQUERIMIENTO];
                    }
                }

                pen = llArm - requerimiento + Penalizadores_old(3) + this.penalizadores[Base.PEN_VISUAL];

                if (pen >= 0) {
                    return 0;
                } else {
                    return pen;
                }

            case 2:

                return Penalizadores_old(3) + Penalizadores_old(4);

            case 3:
                return penalizadores[0] + penalizadores[1];

            case 4:
                for (Armadura armadura : armaduras) {
                    if (armadura.getCaracteristicas()[Base.ARM_ACTIVA] == 1) {
                        reducción = reducción + armadura.getCaracteristicas()[Base.ARM_PENALIZADOR_DESTREZA];
                        requerimiento = requerimiento + armadura.getCaracteristicas()[Base.ARM_REQUERIMIENTO];
                    }
                }

                if (llArm - requerimiento > -reducción) {
                    return 0;
                } else {
                    return reducción + (llArm - requerimiento);
                }

        }

        return pen;
    }

    public String getEstado(int tipo) {
        switch (tipo) {
            case 0:
                return caracteristicas[Base.VIDA_ACT] + " / " + caracteristicas[Base.VIDA_MAX];
            case 1:
                return caracteristicas[Base.ZEON_ACT] + " / " + caracteristicas[Base.ZEON_MAX];
            case 2:
                return caracteristicas[Base.ENERGÍA_ACT] + " / " + caracteristicas[Base.ENERGÍA_MAX];
            case 3:
                return String.valueOf(Penalizadores(PENALIZADORES.TODA_ACCION));
        }
        return "";
    }

    public int getCaracteristicas(int index) {
        return caracteristicas[index];
    }

    public String toJson() {
        String pjJson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(this, this.getClass());
        return pjJson;

    }

    public Personaje fromJson(String json) {
        return new Gson().fromJson(json, this.getClass());
    }

    @Override
    public String toString() {
        return toJson();
    }

//Métodos combate

    public int dañoBaseArma(int arma) {
        Arma temp = this.arma(arma);
        int dañoProducido = 0;

        switch (temp.getCaracteristicas()[Arma.HABILIDAD_USADA]) {
            case Arma.HABILIDAD_P_MAGICA:
                return Base.calcularModificador(Caracteristica(Base.ATRIBUTO_PODER)) + temp.getCaracteristicas()[Arma.DAÑO_BASE];

            case Arma.HABILIDAD_P_PSIQUICA:
                return Base.calcularModificador(Caracteristica(Base.ATRIBUTO_VOLUNTAD)) + temp.getCaracteristicas()[Arma.DAÑO_BASE];

        }

        if (this.Caracteristica(Base.HABILIDAD_ATAQUE) == 0 & temp.getCaracteristicas()[Arma.BONO_ATAQUE] != 0) {
            return temp.getCaracteristicas()[Arma.DAÑO_BASE];
        }

        switch (temp.getCaracteristicas()[Arma.TIPO_DE_ARMA]) {
            case Arma.TIPO_ARMA_A_UNA_MANO:
                dañoProducido = Base.calcularModificador(Caracteristica(Base.ATRIBUTO_FUERZA) + resultado(Base.HABILIDAD_PFUERZA)) + temp.getCaracteristicas()[Arma.DAÑO_BASE];
                break;
            case Arma.TIPO_ARMA_A_DOS_MANOS:
                dañoProducido = (Base.calcularModificador(Caracteristica(Base.ATRIBUTO_FUERZA) + resultado(Base.HABILIDAD_PFUERZA)) * 2) + temp.getCaracteristicas()[Arma.DAÑO_BASE];
                break;
            default:
                dañoProducido = temp.getCaracteristicas()[Arma.DAÑO_BASE];
                break;
        }
        return dañoProducido;
    }

    public int mejorDefensa(boolean parada, int indexArma) {
        int defensaFinal = 0;

        int bonoArmaParada = arma(indexArma).getCaracteristicas()[Arma.BONO_PARADA];
        int bonoArmaesquiva = arma(indexArma).getCaracteristicas()[Arma.BONO_ESQUIVA];

        int totalParada = bonoArmaParada + Caracteristica(Base.HABILIDAD_PARADA);
        int totalEsquiva = bonoArmaesquiva + Caracteristica(Base.HABILIDAD_ESQUIVA);

        if (parada) {

            if (totalParada > totalEsquiva) {
                defensaFinal = totalParada;
            } else {
                if (totalEsquiva - 60 > totalParada) {
                    defensaFinal = totalEsquiva - 60;
                } else {
                    defensaFinal = totalParada;
                }
            }

        } else {

            if (totalParada < totalEsquiva) {
                defensaFinal = totalEsquiva;
            } else {
                if (totalParada - 60 > totalEsquiva) {
                    defensaFinal = totalParada - 60;
                } else {
                    defensaFinal = totalEsquiva;
                }
            }
        }
        return defensaFinal;
    }

    public int TA(int tipo, int sector) {
        int TA = 0;

        TA = TA + this.Caracteristica(Base.TA_FIL + tipo);

        Integer[] TAtemp = new Integer[10];

        Arrays.fill(TAtemp, 0);

        int pos = 0;

        for (Armadura armadura : armaduras) {
            if (armadura.getCaracteristicas()[Base.ARM_ACTIVA] == 1 & armadura.getCaracteristicas()[Base.ARM_ZONA_PROTEGIDA_0 + sector] == 1) {

                TAtemp[pos] = armadura.getCaracteristicas()[Base.ARM_TA_FIL + tipo];
                pos = pos + 1;

            }
        }

        Arrays.sort(TAtemp, Collections.reverseOrder());

        TA = TA + TAtemp[0] + Base.floor(TAtemp[1], 2) + Base.floor(TAtemp[2], 2) + Base.floor(TAtemp[3], 2) + Base.floor(TAtemp[4], 2) + Base.floor(TAtemp[5], 2) + Base.floor(TAtemp[6], 2);

        return TA;
    }

    public int[] datos_combate(int index_arma) {
        /*
   public static final int ID_PERSONAJE = 0;
    public static final int ATAQUE = 1;
    public static final int ESQUIVA = 2;
    public static final int PARADA = 3;
    public static final int DAÑO_BASE = 4;
    public static final int VIDA_ACTUAL = 5;
    public static final int TIRADA = 6;
    public static final int ARMADURA = 8;
    public static final int BARRERA_DE_DAÑO = 9;
    public static final int ESCUDO = 10;
    public static final int ROBO_VIDA = 11;
    public static final int RED_PEN_PORC = 12;
    public static final int RED_PEN_PLENO = 13;
    public static final int RED_DAÑO_PORC = 14;
    public static final int RED_DAÑO_PLENO = 15;
    public static final int RESISTENCIA_FISICA = 16;
    public static final int FUERZA = 17;
    public static final int DESTREZA = 18;
    public static final int RESISTIR_EL_DOLOR = 20;
    public static final int TRUCOS_DE_MANOS = 21;
    public static final int ENTEREZA = 22;
    public static final int ROTURA = 23;
    public static final int ALCANCE = 24;
    public static final int CALIDAD = 25;
    public static final int AGILIDAD = 28;
         */


        int[] datos = new int[30];
        Arrays.fill(datos, 0);


        datos[Combate.ID_PERSONAJE] = this.getID();

        datos[Combate.AGILIDAD] = this.Caracteristica(Base.ATRIBUTO_AGILIDAD);
        datos[Combate.FUERZA] = this.Caracteristica(Base.ATRIBUTO_FUERZA);
        datos[Combate.DESTREZA] = this.Caracteristica(Base.ATRIBUTO_DESTREZA);

        datos[Combate.VIDA_ACTUAL] = this.Caracteristica(Base.VIDA_ACT);

        if (index_arma == -1) {
            datos[Combate.ATAQUE] = this.Caracteristica(Base.HABILIDAD_ATAQUE);
            datos[Combate.ESQUIVA] = this.mejorDefensa(false, 0);
            datos[Combate.PARADA] = this.mejorDefensa(true, 0);
            datos[Combate.DAÑO_BASE] = 0;
            datos[Combate.ENTEREZA] = 0;
            datos[Combate.ROTURA] = 0;
            datos[Combate.ALCANCE] = 0;
            datos[Combate.CALIDAD] = 0;
        } else {

            switch (arma(index_arma).getCaracteristicas()[Arma.HABILIDAD_USADA]) {
                case 0:
                    datos[Combate.ATAQUE] = this.Caracteristica(Base.HABILIDAD_ATAQUE) + arma(index_arma).getCaracteristicas()[Arma.BONO_ATAQUE];

                    break;
                case 1:
                    datos[Combate.ATAQUE] = this.Caracteristica(Base.HABILIDAD_PROYECCION_MAGICA) + arma(index_arma).getCaracteristicas()[Arma.BONO_ATAQUE];
                    break;
                case 2:
                    datos[Combate.ATAQUE] = this.Caracteristica(Base.HABILIDAD_PROYECCION_PSIQUICA) + arma(index_arma).getCaracteristicas()[Arma.BONO_ATAQUE];
                    break;
            }

            datos[Combate.ESQUIVA] = this.mejorDefensa(false, index_arma);
            datos[Combate.PARADA] = this.mejorDefensa(true, index_arma);
            datos[Combate.DAÑO_BASE] = this.dañoBaseArma(index_arma);

            datos[Combate.ENTEREZA] = this.arma(index_arma).getCaracteristicas()[Arma.ENTEREZA_ARMA];
            datos[Combate.ROTURA] = this.arma(index_arma).getCaracteristicas()[Arma.ROTURA_ARMA];
            datos[Combate.ALCANCE] = this.arma(index_arma).getCaracteristicas()[Arma.ALCANCE];
            datos[Combate.CALIDAD] = this.arma(index_arma).getCaracteristicas()[Arma.CALIDAD];
        }


        datos[Combate.BARRERA_DE_DAÑO] = this.Caracteristica(Base.BARRERA_DE_DAÑO);
        datos[Combate.ESCUDO] = this.Caracteristica(Base.ESCUDO_ACT);
        datos[Combate.ROBO_VIDA] = this.especiales[Base.ROBO_DE_VIDA_PORCENTUAL];
        datos[Combate.RED_PEN_PORC] = this.especiales[Base.REDUCCION_DE_PENALIZADORES_PORCENTUAL];
        datos[Combate.RED_PEN_PLENO] = this.especiales[Base.REDUCCION_DE_PENALIZADORES_PLENO];
        datos[Combate.RED_DAÑO_PORC] = this.especiales[Base.REDUCCIÓN_DE_DAÑO_PORCENTUAL];
        datos[Combate.RED_DAÑO_PLENO] = this.especiales[Base.REDUCCIÓN_DE_DAÑO_PLENO];
        datos[Combate.RESISTENCIA_FISICA] = this.Caracteristica(Base.RESISTENCIA_FISICA);

        datos[Combate.RESISTIR_EL_DOLOR] = this.Caracteristica(Base.HABILIDAD_RESDOLOR);
        datos[Combate.TRUCOS_DE_MANOS] = this.Caracteristica(Base.HABILIDAD_TMANOS);

        return datos;

    }

    //Métodos obtener un dato

    public int Caracteristica(int index) {
        int base = this.caracteristicas[index];
        int temp = this.caracteristicas_temporales[index];
        return base + temp;
    }

    public Arma arma(int index) {
        return armas[index];
    }

    public Armadura armadura(int index) {
        return armaduras[index];
    }

    public String[] getArmas_nombres() {
        String[] nombres = new String[armas.length];
        for (int i = 0; i < armas.length; i++) {
            nombres[i] = armas[i].getNombre();
        }
        return nombres;
    }

    public String[] getArmaduras_nombres() {
        String[] nombres = new String[armaduras.length];
        for (int i = 0; i < armaduras.length; i++) {
            nombres[i] = armaduras[i].getNombre();
        }
        return nombres;
    }

    //CONSTRUCTORES

    //Constructor con caracteristicas

    public Personaje(String nombre_jugador, String nombre_personaje, String descripción, int nivelPifia, int tipoDeEntidad, int tipoDePersonaje, int[] caracteristicas, int[] habilidades_psiquicas, int[] caracteristicas_temporales, boolean[] conocimientos_especiales, int[] penalizadores, int[] especiales, Armadura[] armaduras, Arma[] armas) {
        this.nombre_jugador = nombre_jugador;
        this.nombre_personaje = nombre_personaje;
        this.descripción = descripción;
        this.nivelPifia = nivelPifia;
        this.tipoDeEntidad = tipoDeEntidad;
        this.tipoDePersonaje = tipoDePersonaje;
        this.caracteristicas = caracteristicas;
        this.habilidades_psiquicas = habilidades_psiquicas;
        this.caracteristicas_temporales = caracteristicas_temporales;
        Conocimientos_especiales = conocimientos_especiales;
        this.penalizadores = penalizadores;
        this.especiales = especiales;
        this.armaduras = armaduras;
        this.armas = armas;
    }


    //Constructor con caracteristicas e id

    public Personaje(int ID, String nombre_jugador, String nombre_personaje, String descripción, int nivelPifia, int tipoDeEntidad, int tipoDePersonaje, int[] caracteristicas, int[] habilidades_psiquicas, int[] caracteristicas_temporales, boolean[] conocimientos_especiales, int[] penalizadores, int[] especiales, Armadura[] armaduras, Arma[] armas) {
        this.ID = ID;
        this.nombre_jugador = nombre_jugador;
        this.nombre_personaje = nombre_personaje;
        this.descripción = descripción;
        this.nivelPifia = nivelPifia;
        this.tipoDeEntidad = tipoDeEntidad;
        this.tipoDePersonaje = tipoDePersonaje;
        this.caracteristicas = caracteristicas;
        this.habilidades_psiquicas = habilidades_psiquicas;
        this.caracteristicas_temporales = caracteristicas_temporales;
        Conocimientos_especiales = conocimientos_especiales;
        this.penalizadores = penalizadores;
        this.especiales = especiales;
        this.armaduras = armaduras;
        this.armas = armas;
    }

    //Constructor en base a otro pj (obsoleto){

    public Personaje(Personaje_old NuevoPj) {
        this.ID = NuevoPj.getID();
        this.nombre_jugador = NuevoPj.getNombre_jugador();
        this.descripción = NuevoPj.getDescripción();
        this.nombre_personaje = NuevoPj.getNombre_personaje();
        this.nivelPifia = NuevoPj.getNivelPifia();
        this.tipoDeEntidad = NuevoPj.getTipoDeEntidad();
        this.tipoDePersonaje = NuevoPj.getTipoDePersonaje();

        this.caracteristicas = NuevoPj.getCaracteristicas();
        this.caracteristicas_temporales = NuevoPj.getCaracteristicas_temporales();
        Conocimientos_especiales = NuevoPj.getConocimientos_especiales();
        this.penalizadores = NuevoPj.getPenalizadores();
        this.especiales = NuevoPj.getEspeciales();

        armaduras[0] = new Armadura();
        armaduras[1] = new Armadura();
        armaduras[2] = new Armadura();
        armaduras[3] = new Armadura();
        armaduras[4] = new Armadura();
        armaduras[5] = new Armadura();

        armas[0] = new Arma();
        armas[1] = new Arma();
        armas[2] = new Arma();
        armas[3] = new Arma();
        armas[4] = new Arma();
        armas[5] = new Arma();

        try {
            for (int i = 0; i < NuevoPj.getArmaduras_nombres().length; i++) {
                armaduras[i] = NuevoPj.armadura(i);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            for (int i = 0; i < NuevoPj.getArmas_nombres().length; i++) {
                armas[i] = NuevoPj.arma(i);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.token = NuevoPj.getToken();
        this.cantidad = cantidad;
        this.imagen = NuevoPj.getImagen();

    }

    //Constructor en base a otro pj (usar JSON)


    public Personaje(Personaje NuevoPj, int cantidad) {
        this.ID = NuevoPj.getID();
        this.nombre_jugador = NuevoPj.getNombre_jugador();
        this.descripción = NuevoPj.getDescripción();
        this.nombre_personaje = NuevoPj.getNombre_personaje();
        this.nivelPifia = NuevoPj.getNivelPifia();
        this.tipoDeEntidad = NuevoPj.getTipoDeEntidad();
        this.tipoDePersonaje = NuevoPj.getTipoDePersonaje();
        this.caracteristicas = NuevoPj.getCaracteristicas();
        this.caracteristicas_temporales = NuevoPj.getCaracteristicas_temporales();
        Conocimientos_especiales = NuevoPj.getConocimientos_especiales();
        this.penalizadores = NuevoPj.getPenalizadores();
        this.especiales = NuevoPj.getEspeciales();
        this.armaduras = NuevoPj.getArmaduras();
        this.armas = NuevoPj.getArmas();
        this.token = NuevoPj.getToken();
        this.cantidad = cantidad;
        this.imagen = NuevoPj.getImagen();

    }

    //Constructor vacio
    public Personaje() {
    }

    //Copia los valores de otro pj
    public void reemplazar(Personaje NuevoPj) {
        this.nombre_jugador = NuevoPj.getNombre_jugador();
        this.nombre_personaje = NuevoPj.getNombre_personaje();
        this.nivelPifia = NuevoPj.getNivelPifia();
        this.tipoDeEntidad = NuevoPj.getTipoDeEntidad();
        this.tipoDePersonaje = NuevoPj.getTipoDePersonaje();
        this.caracteristicas = NuevoPj.getCaracteristicas();
        this.caracteristicas_temporales = NuevoPj.getCaracteristicas_temporales();
        Conocimientos_especiales = NuevoPj.getConocimientos_especiales();
        this.penalizadores = NuevoPj.getPenalizadores();
        this.especiales = NuevoPj.getEspeciales();
        this.armaduras = NuevoPj.getArmaduras();
        this.armas = NuevoPj.getArmas();
        this.descripción = NuevoPj.getDescripción();
        this.token = NuevoPj.getToken();
        this.habilidades_psiquicas = NuevoPj.getHabilidades_psiquicas();
        this.imagen = NuevoPj.getImagen();
    }


}
