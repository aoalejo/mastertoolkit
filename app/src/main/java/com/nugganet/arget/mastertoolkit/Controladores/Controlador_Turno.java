package com.nugganet.arget.mastertoolkit.Controladores;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.nugganet.arget.mastertoolkit.Base;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Arma;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Personaje;
import com.nugganet.arget.mastertoolkit.Principal;
import com.nugganet.arget.mastertoolkit.R;

import java.util.ArrayList;

import Utils.DbBitmapUtility;

public class Controlador_Turno extends BaseAdapter {

    Principal p;

    private static LayoutInflater inflater = null;

    Turnos t;


    public Controlador_Turno(Principal mainActivity) {
        p = mainActivity;
        inflater = (LayoutInflater) p.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        t = new Turnos((ArrayList<Personaje>) p.getDatos().Array_Personajes_En_Mision().clone());

        t.calcularTurnos(true);

        //t.Añadir_NPC("Fin de la ronda", -999999);
    }


    @Override
    public int getCount() {
        return t.personajes.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder {
        TextView TextView_NombrePj;
        TextView TextView_ResultadoTirada;
        TextView TextView_Arma_usada;
        ImageView ImageView_token;
        ImageView ImageView_sorprendido;
        LinearLayout linearLayout_turno;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Holder holder = new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.list_turno, null, true);

        holder.linearLayout_turno = rowView.findViewById(R.id.linearLayout_turno);
        holder.ImageView_sorprendido = rowView.findViewById(R.id.imageView_sorprendido);
        holder.TextView_NombrePj = (TextView) rowView.findViewById(R.id.TextView_NombrePj);
        holder.TextView_ResultadoTirada = (TextView) rowView.findViewById(R.id.TextView_ResultadoTirada);
        holder.TextView_Arma_usada = (TextView) rowView.findViewById(R.id.TextView_Arma_usada);
        holder.ImageView_token = (ImageView) rowView.findViewById(R.id.ImageView_token);

        holder.ImageView_token.setImageDrawable(new BitmapDrawable(p.getResources(), t.Token(position)));

        holder.TextView_NombrePj.setText(t.Nombre(position));
        holder.TextView_ResultadoTirada.setText(t.Resultado(position));

        if (t.turno_final(t.turnoActual) > t.turno_final(position) + 150) {
            holder.ImageView_sorprendido.setVisibility(View.VISIBLE);
            holder.TextView_Arma_usada.setText("¡El personaje está sorprendido!");
            holder.linearLayout_turno.setBackground(p.getDrawable(R.drawable.round_rect_red));
            holder.TextView_Arma_usada.setTextColor(p.getColor(R.color.colorWhite));
            holder.TextView_NombrePj.setTextColor(p.getColor(R.color.colorWhite));
            holder.TextView_ResultadoTirada.setTextColor(p.getColor(R.color.colorWhite));

        } else {
            holder.ImageView_sorprendido.setVisibility(View.GONE);
            holder.TextView_Arma_usada.setText("");
        }

        if (position == t.turnoActual) {
            holder.linearLayout_turno.setBackground(p.getDrawable(R.drawable.round_rect_button));
            holder.TextView_Arma_usada.setTextColor(p.getColor(R.color.colorWhite));
            holder.TextView_NombrePj.setTextColor(p.getColor(R.color.colorWhite));
            holder.TextView_ResultadoTirada.setTextColor(p.getColor(R.color.colorWhite));
        }


        rowView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                t.Quitar_NPC(position);
                return true;
            }
        });

        return rowView;
    }

    private void actualizarArray(final Boolean adelanta) {

        ListView iniciativas = ((ListView) p.findViewById(R.id.List_view_Iniciativas));

        iniciativas.setAdapter(this);

        // iniciativas.setSelection(t.getTurnoActual());

        if (adelanta != null) {
            if (adelanta) {
                if (t.getTurnoActual() > 1) {
                    iniciativas.setSelection(t.getTurnoActual() - 2);
                } else if (t.getTurnoActual() == 1) {
                    iniciativas.setSelection(t.getTurnoActual() - 1);
                }
            } else {
                if (t.getTurnoActual() - 2 < t.cantidad) {
                    iniciativas.setSelection(t.getTurnoActual() - 1);
                }
            }
        }


        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                ListView iniciativas = ((ListView) p.findViewById(R.id.List_view_Iniciativas));

                if (iniciativas.getChildCount() != 0 & iniciativas.getAdapter() != null & t.getTurnoActual() > 1) {
                    int h1 = iniciativas.getHeight();
                    int h2 = iniciativas.getChildAt(0).getHeight();
                    if (adelanta != null) {
                        if (adelanta) {
                            iniciativas.smoothScrollBy(h2, 1000);
                        } else {
                            iniciativas.smoothScrollBy(-h2, 1000);
                        }
                    }
                }
            }
        }, 100);

    }

    public void Calcular_turnos() {
        t.calcularTurnos(true);
        t.Ordenar(false);
        actualizarArray(null);
    }

    public Turnos turnos() {
        return t;
    }


    public void Controlador_botones(int button_id) {
        boolean ascendente = ((ToggleButton) p.findViewById(R.id.toggleButton_orden)).isChecked();
        switch (button_id) {
            case R.id.button_turno_siguiente:
                boolean calculo_todos = ((ToggleButton) p.findViewById(R.id.toggleButton_actualizar_ronda)).isChecked();
                t.Siguiente(calculo_todos, ascendente);
                break;
            case R.id.button_turno_anterior:
                t.Anterior();
                break;
            case R.id.Button_calcular_iniciativa:
                t.setTurnoActual(0);
                t.calcularTurnos(true);
                t.Ordenar(ascendente);
                actualizarArray(null);
                break;

        }
    }

    private class Turnos {
        private ArrayList<Personaje> personajes = new ArrayList<>();
        private ArrayList<Integer> modificadores;
        private int cantidad;
        private int turnoActual = 0;

        public Turnos(ArrayList<Personaje> pj_lite) {

            personajes = pj_lite;

            cantidad = pj_lite.size();

            modificadores = new ArrayList<>();

            for (Personaje pj : pj_lite) {
                modificadores.add(0);
            }

        }

        public Bitmap Token(int pos) {
            return DbBitmapUtility.getImage(personajes.get(pos).getToken());
        }


        public boolean fin_De_ronda(int pos) {
            try {
                if (personajes.get(pos).getCaracteristicas(Base.HABILIDAD_TURNO) == -999999) {
                    return true;
                } else {
                    return false;
                }
            } catch (java.lang.IndexOutOfBoundsException e) {
                e.printStackTrace();
            }
            return false;
        }

        public void calcularTurnos(boolean tirada) {

            int última_tirada = 0;
            int penalizador_arma = 0;
            int habilidad_base = 0;

            for (int i = 0; i < cantidad; i++) {
                try {

                    if (tirada) {

                        última_tirada = personajes.get(i).tirada();
                        penalizador_arma = personajes.get(i).arma(personajes.get(i).getIndex_última_arma_usada()).getCaracteristicas()[Arma.TURNO_ARMA];
                        habilidad_base = personajes.get(i).getCaracteristicas(Base.HABILIDAD_TURNO);

                        personajes.get(i).setÚltimo_turno_final(última_tirada + penalizador_arma + habilidad_base + modificadores.get(i));


                    } else {

                        última_tirada = personajes.get(i).getÚltimas_tiradas()[Base.HABILIDAD_TURNO];
                        penalizador_arma = personajes.get(i).arma(personajes.get(i).getIndex_última_arma_usada()).getCaracteristicas()[Arma.TURNO_ARMA];
                        habilidad_base = personajes.get(i).getCaracteristicas(Base.HABILIDAD_TURNO);

                        personajes.get(i).setÚltimo_turno_final(última_tirada + penalizador_arma + habilidad_base + modificadores.get(i));
                    }

                } catch (java.lang.NullPointerException e) {
                    e.printStackTrace();

                }
            }
        }

        public void Siguiente(boolean recalcular_turnos, boolean ascendente) {

            t.setTurnoActual(t.getTurnoActual() + 1);

            if (t.getTurnoActual() > t.cantidad - 1) {

                if (recalcular_turnos) {
                    calcularTurnos(true);
                    Ordenar(ascendente);
                }

                t.setTurnoActual(0);

            }


            if (personajes.get(t.getTurnoActual()).getID() != -1) {

                int pj_actual_index = personajes.get(t.getTurnoActual()).getID();

                ((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_asaltante)).setSelection(pj_actual_index);

            }

            actualizarArray(true);

        }

        public void Anterior() {

            t.setTurnoActual(t.getTurnoActual() - 1);

            if (t.getTurnoActual() < 0) {
                t.setTurnoActual(0);
            }

            /*

            Personaje aux_lite = personajes.get(cantidad);
            Personaje aux_full = personajes.get(cantidad);
            Integer aux_modificador = modificadores.get(cantidad);

            for (int i = cantidad; i > 0; i--) {

                personajes.set(i, personajes.get(i - 1));
                personajes.set(i, personajes.get(i - 1));
                modificadores.set(i, modificadores.get(i - 1));

                if (i == 0 + 1) {

                    personajes.set(0, aux_lite);
                    personajes.set(0, aux_full);
                    modificadores.set(0, aux_modificador);

                }
            }

            if (personajes.get(0).getID() != -1) {

                int pj_actual_index = personajes.get(0).getID();

                ((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_asaltante)).setSelection(pj_actual_index);

            }*/

            actualizarArray(false);

        }

        public void Ordenar(boolean ascendente) {

            if (ascendente) {
                quickSortInAscendingOrder(0, cantidad - 1);

                Siguiente(false, ascendente);

            } else {
                quickSortInDescendingOrder(0, cantidad - 1);
            }
            actualizarArray(null);
        }

        private void quickSortInDescendingOrder(int low, int high) {

            int i = low;
            int j = high;

            Personaje aux_lite;
            Personaje aux_full;
            Integer aux_modificador;

            int middle = turno_final((low + high) / 2);

            while (i < j) {
                while (turno_final(i) > middle) {
                    i++;
                }
                while (turno_final(j) < middle) {
                    j--;
                }
                if (j >= i) {

                    aux_lite = personajes.get(i);
                    aux_full = personajes.get(i);
                    aux_modificador = modificadores.get(i);

                    personajes.set(i, personajes.get(j));
                    personajes.set(i, personajes.get(j));
                    modificadores.set(i, modificadores.get(j));

                    personajes.set(j, aux_lite);
                    personajes.set(j, aux_full);
                    modificadores.set(j, aux_modificador);

                    i++;
                    j--;
                }
            }


            if (low < j) {
                quickSortInDescendingOrder(low, j);
            }
            if (i < high) {
                quickSortInDescendingOrder(i, high);
            }
        }

        private void quickSortInAscendingOrder(int low, int high) {

            int i = low;
            int j = high;

            Personaje aux_lite;
            Personaje aux_full;
            Integer aux_modificador;

            int middle = turno_final((low + high) / 2);

            while (i < j) {
                while (turno_final(i) < middle) {
                    i++;
                }
                while (turno_final(j) > middle) {
                    j--;
                }
                if (i <= j) {
                    aux_lite = personajes.get(i);
                    aux_full = personajes.get(i);
                    aux_modificador = modificadores.get(i);

                    personajes.set(i, personajes.get(j));
                    personajes.set(i, personajes.get(j));
                    modificadores.set(i, modificadores.get(j));

                    personajes.set(j, aux_lite);
                    personajes.set(j, aux_full);
                    modificadores.set(j, aux_modificador);


                    i++;
                    j--;
                }
            }


            if (low < j) {
                quickSortInAscendingOrder(low, j);
            }
            if (i < high) {
                quickSortInAscendingOrder(i, high);
            }


        }


        public int turno_final(int pos) {
            if (!personajes.isEmpty() & pos < personajes.size()) {
                return personajes.get(pos).getÚltimo_turno_final();
            }
            return 0;
        }

        public int getTurnoActual() {
            return turnoActual;
        }

        public void setTurnoActual(int turnoActual) {
            this.turnoActual = turnoActual;
        }

        public String Nombre(int pos) {
            return personajes.get(pos).getNombres();
        }


        public String Resultado(int pos) {
            return "Resultado " + turno_final(pos) + " utilizando: " + personajes.get(pos).arma(personajes.get(pos).getIndex_última_arma_usada()).getNombre() + ".";
        }

        public void Cambiar_modificador(int num, int nuevo) {

            this.modificadores.set(num, nuevo);

        }

        public void Añadir_NPC(String nombre, int turno) {
            modificadores.add(0);
            Personaje temp = new Personaje();

            temp.caracteristicasSum(Base.HABILIDAD_TURNO, turno);

            personajes.add(temp);

            personajes.add(new Personaje(-1, nombre, nombre));

            cantidad = cantidad + 1;

            actualizarArray(null);

        }

        public void Quitar_NPC(int position) {
            if (personajes.get(position).getID() == -1) {
                personajes.remove(position);
                personajes.remove(position);
                modificadores.remove(position);
                cantidad = cantidad - 1;
                actualizarArray(null);
            }


        }


    }
/*
    public static class Creacion_NPC extends DialogFragment {



        private Principal p;

        public Principal getP() {
            return p;
        }

        public void setP(Principal p) {
            this.p = p;
        }

        private Turnos t;

        public Turnos getT() {
            return t;
        }

        public void setT(Turnos t) {
            this.t = t;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            final View rootView = inflater.inflate(R.layout.fragment_add_npc, container, false);



            Button seleccionar = (Button) rootView.findViewById(R.id.button_aceptar_npc);

            Button cancelar = (Button) rootView.findViewById(R.id.button_cancelar_npc);

            seleccionar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    dismiss();
                }
            });

            cancelar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });


            return rootView;
        }
    }

    public Turnos getT() {
        return t;
    }
*/

}