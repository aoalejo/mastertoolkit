package com.nugganet.arget.mastertoolkit;

import android.widget.Spinner;

import com.nugganet.arget.mastertoolkit.Clases_objetos.Arma;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Modificador;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Personaje;

import java.util.Arrays;

/**
 * Created by Arget on 26/3/2018.
 */

public abstract class Combate {


    public static final int[] DIFICULTADES_APUNTADO = {-60, -60, -30, -30, -20, -30, -30, -20, -20, -40, -20, -20, -40, -20, -10, -50, -20, -10, -50, -60, -80, -100};
    public static final int[] ARMADURA_LOCALIZACION_GENERAL = {1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 0, 0, -1, -1};

    public static final int SIN_APUNTAR = -1;
    public static final int EL_PECTORAL_IZQUIERDO = 0;
    public static final int EL_CORAZÓN = 1;
    public static final int EL_HOMBRO_IZQUIERDO = 2;
    public static final int EL_HOMBRO_DERECHO = 3;
    public static final int EL_ESTOMAGO = 4;
    public static final int EL_COSTADO_ABDOMINAL_DERECHO = 5;
    public static final int EL_COSTADO_ABDOMINAL_IZQUIERDO = 6;
    public static final int EL_ANTEBRAZO_SUPERIOR_IZQUIERDO = 7;
    public static final int EL_ANTEBRAZO_INFERIOR_IZQUIERDO = 8;
    public static final int LA_MANO_IZQUIERDA = 9;
    public static final int EL_ANTEBRAZO_SUPERIOR_DERECHO = 10;
    public static final int EL_ANTEBRAZO_INFERIOR_DERECHO = 11;
    public static final int LA_MANO_DERECHA = 12;
    public static final int EL_MUSLO_IZQUIERDO = 13;
    public static final int LA_PANTORILLA_IZQUIERDA = 14;
    public static final int EL_PIE_IZQUIERDO = 15;
    public static final int EL_MUSLO_DERECHO = 16;
    public static final int LA_PANTORILLA_DERECHA = 17;
    public static final int EL_PIE_DERECHO = 18;
    public static final int LA_CABEZA = 19;
    public static final int EL_CUELLO = 20;
    public static final int EL_OJO = 21;


    private static final Mostrar m = new Mostrar("Combate");

    private static final boolean MODO_DE_PRUEBAS = true;

    public static final int ID_PERSONAJE = 0;
    public static final int ATAQUE = 1;
    public static final int ESQUIVA = 2;
    public static final int PARADA = 3;
    public static final int DAÑO_BASE = 4;
    public static final int VIDA_ACTUAL = 5;
    public static final int TIRADA = 6;
    public static final int ARMADURA = 8;
    public static final int BARRERA_DE_DAÑO = 9;
    public static final int ESCUDO = 10;
    public static final int ROBO_VIDA = 11;
    public static final int RED_PEN_PORC = 12;
    public static final int RED_PEN_PLENO = 13;
    public static final int RED_DAÑO_PORC = 14;
    public static final int RED_DAÑO_PLENO = 15;
    public static final int RESISTENCIA_FISICA = 16;
    public static final int FUERZA = 17;
    public static final int DESTREZA = 18;
    public static final int RESISTIR_EL_DOLOR = 20;
    public static final int TRUCOS_DE_MANOS = 21;
    public static final int ENTEREZA = 22;
    public static final int ROTURA = 23;
    public static final int ALCANCE = 24;
    public static final int CALIDAD = 25;
    public static final int AGILIDAD = 28;


    public static class Detalle {

        int resultado;
        String Texto;
        String detalle;


        public Detalle() {
            this.resultado = 0;
            this.detalle = "";
            this.Texto = "";
        }

        public String getTexto() {
            return Texto;
        }

        public void setTexto(String texto) {
            Texto = texto;
        }

        public int getResultado() {
            return resultado;
        }

        public void setResultado(int resultado) {
            this.resultado = resultado;
        }

        public String getDetalle() {
            return detalle;
        }

        public void setDetalle(String detalle) {
            this.detalle = detalle;
        }

        public void append(String text) {
            detalle = detalle + text;
        }
    }

    public static class Resultado {

        String descripción = "";
        int DAÑOS_PV = 0;
        int PENALIZADORES = 0;
        int BONO_CONTRAATAQUE = 0;
        int ID_ATACANTE = 0;
        int ID_DEFENSOR = 0;
        int ESCUDO_RESULTADO = 0;
        int NIVEL_CRITICO = 0;
        int Robo_de_vida = 0;
        String datos_combate = "";
        String detalle = "";

        public String getDatos_combate() {
            return datos_combate;
        }

        public void setDatos_combate(String datos_combate) {
            this.datos_combate = datos_combate;
        }

        public int Robo_de_vida() {
            return Robo_de_vida;
        }

        public void Robo_de_vida(int robo_de_vida) {
            Robo_de_vida = robo_de_vida;
        }

        public void actualizar(Resultado nuevo) {
            this.descripción = nuevo.descripción;
            this.DAÑOS_PV = nuevo.DAÑOS_PV();
            this.PENALIZADORES = nuevo.PENALIZADORES();
            this.BONO_CONTRAATAQUE = nuevo.BONO_CONTRAATAQUE();
            this.ID_ATACANTE = nuevo.ID_ATACANTE();
            this.ID_DEFENSOR = nuevo.ID_DEFENSOR();
            this.ESCUDO_RESULTADO = nuevo.ESCUDO_RESULTADO();
            this.NIVEL_CRITICO = nuevo.NIVEL_CRITICO();
            this.Robo_de_vida = nuevo.Robo_de_vida();
            this.datos_combate = nuevo.getDatos_combate();
            this.DetalleAppend(nuevo.detalle);
        }

        public Resultado(Resultado nuevo) {

            this.descripción = nuevo.descripción;
            this.DAÑOS_PV = nuevo.DAÑOS_PV();
            this.PENALIZADORES = nuevo.PENALIZADORES();
            this.BONO_CONTRAATAQUE = nuevo.BONO_CONTRAATAQUE();
            this.ID_ATACANTE = nuevo.ID_ATACANTE();
            this.ID_DEFENSOR = nuevo.ID_DEFENSOR();
            this.ESCUDO_RESULTADO = nuevo.ESCUDO_RESULTADO();
            this.NIVEL_CRITICO = nuevo.NIVEL_CRITICO();
            this.Robo_de_vida = nuevo.Robo_de_vida();
            this.datos_combate = nuevo.getDatos_combate();
            this.DetalleAppend(nuevo.detalle);

        }

        public Resultado() {
        }

        public Resultado(String descripción, int DAÑOS_PV, int PENALIZADORES, int BONO_CONTRAATAQUE, int ID_ATACANTE, int ID_DEFENSOR, int ESCUDO_RESULTADO, int NIVEL_CRITICO) {
            this.descripción = descripción;
            this.DAÑOS_PV = DAÑOS_PV;
            this.PENALIZADORES = PENALIZADORES;
            this.BONO_CONTRAATAQUE = BONO_CONTRAATAQUE;
            this.ID_ATACANTE = ID_ATACANTE;
            this.ID_DEFENSOR = ID_DEFENSOR;
            this.ESCUDO_RESULTADO = ESCUDO_RESULTADO;
            this.NIVEL_CRITICO = NIVEL_CRITICO;
        }

        public Resultado(String descripción, int DAÑOS_PV, int PENALIZADORES, int BONO_CONTRAATAQUE, int ID_ATACANTE, int ID_DEFENSOR, int ESCUDO_RESULTADO, int NIVEL_CRITICO, int robo_de_vida) {
            this.descripción = descripción;
            this.DAÑOS_PV = DAÑOS_PV;
            this.PENALIZADORES = PENALIZADORES;
            this.BONO_CONTRAATAQUE = BONO_CONTRAATAQUE;
            this.ID_ATACANTE = ID_ATACANTE;
            this.ID_DEFENSOR = ID_DEFENSOR;
            this.ESCUDO_RESULTADO = ESCUDO_RESULTADO;
            this.NIVEL_CRITICO = NIVEL_CRITICO;
            Robo_de_vida = robo_de_vida;
        }

        public Resultado(String descripción, int DAÑOS_PV, int NIVEL_CRITICO) {
            this.descripción = descripción;
            this.DAÑOS_PV = DAÑOS_PV;
            this.NIVEL_CRITICO = NIVEL_CRITICO;
        }

        public void DetalleAppend(String text) {
            this.detalle = detalle + " " + text;
        }

        public String getDetalle() {
            return detalle;
        }

        public void setDetalle(String detalle) {
            this.detalle = detalle;
        }


        public String Descripción() {
            return descripción;
        }

        public void Descripción(String text) {
            this.descripción = text;
        }

        public void DescripciónAppend(String text) {
            this.descripción = descripción + text;
        }

        public int DAÑOS_PV() {
            return DAÑOS_PV;
        }

        public void DAÑOS_PV(int nuevo) {
            DAÑOS_PV = nuevo;
        }


        public int PENALIZADORES() {
            return PENALIZADORES;
        }

        public void PENALIZADORES(int nuevo) {
            PENALIZADORES = nuevo;
        }

        public int BONO_CONTRAATAQUE() {
            return PENALIZADORES;
        }

        public void BONO_CONTRAATAQUE(int nuevo) {
            BONO_CONTRAATAQUE = nuevo;
        }


        public int ID_ATACANTE() {
            return ID_ATACANTE;
        }

        public void ID_ATACANTE(int nuevo) {
            ID_ATACANTE = nuevo;
        }


        public int ID_DEFENSOR() {
            return ID_DEFENSOR;
        }

        public void ID_DEFENSOR(int nuevo) {
            ID_DEFENSOR = nuevo;
        }

        public int ESCUDO_RESULTADO() {
            return ESCUDO_RESULTADO;
        }

        public void ESCUDO_RESULTADO(int nuevo) {
            ESCUDO_RESULTADO = nuevo;
        }

        public int NIVEL_CRITICO() {
            return NIVEL_CRITICO;
        }

        public void NIVEL_CRITICO(int nuevo) {
            NIVEL_CRITICO = nuevo;
        }

        @Override
        public String toString() {
            return "Resultado{" +
                    "descripción='" + descripción + '\'' +
                    ", DAÑOS_PV=" + DAÑOS_PV +
                    ", PENALIZADORES=" + PENALIZADORES +
                    ", BONO_CONTRAATAQUE=" + BONO_CONTRAATAQUE +
                    ", ID_ATACANTE=" + ID_ATACANTE +
                    ", ID_DEFENSOR=" + ID_DEFENSOR +
                    ", ESCUDO_RESULTADO=" + ESCUDO_RESULTADO +
                    ", NIVEL_CRITICO=" + NIVEL_CRITICO +
                    '}';
        }
    }

    public static final int DESCRIPCIÓN = 0;
    public static final int DAÑOS_PV = 1;
    public static final int PENALIZADORES = 2;
    public static final int BONO_CONTRAATAQUE = 3;
    public static final int ID_ATACANTE = 4;
    public static final int ID_DEFENSOR = 5;
    public static final int ESCUDO_RESULTADO = 6;
    public static final int NIVEL_CRITICO = 7;

    public static final String LANZADO = "LANZADO";
    public static final String DISPARADO_LENTO = "DISPARADO_LENTO";//BALLESTAS
    public static final String DISPARADO_RÁPIDO = "DISPARADO_RÁPIDO";//ARMAS DE FUEGO
    public static final String SOBRENATURAL = "SOBRENATURAL";//PSIQUICOS
    public static final String CONTUNDENTE = "CONTUNDENTE";//para dejar inconsciente


    public static final int ATAQUE_BÁSICO = 0;
    public static final int USANDO_CRITICO_SECUNDARIO = 1;
    public static final int IGNORA_ARMADURA = 2;
    public static final int DERRIBO = 3;
    public static final int INUTILIZAR = 4;
    public static final int DESARMAR = 5;
    public static final int PRESA = 6;
    public static final int INCONSCIENCIA = 7;
    public static final int ENGATILLAR = 8;
    public static final int ATAQUE_EN_AREA = 9;
    public static final int ATRAPAR_PROYECTIL = 10;


    public static final int NOMBRE_ATACANTE_ST = 8;
    public static final int NOMBRE_DEFENSOR_ST = 9;

    public static final int ARMA_ATACANTE_ST = 0;
    public static final int ARMA_DEFENSOR_ST = 1;
    public static final int TIPO_DE_ATAQUE_ST = 2;
    public static final int TIPO_DE_DEFENSA_ST = 3;
    public static final int TIPO_DE_APUNTADO_ST = 4;
    public static final int DISTANCIA_ST = 5;
    public static final int TIRADA_ATACANTE_ST = 6;
    public static final int TIRADA_DEFENSOR_ST = 7;


    public static final Resultado controlador(Principal p) {


        mostrar("En metodo: controlador \n");


        String[] datos_base = new String[10];

        int index_atacante = ((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_asaltante)).getSelectedItemPosition();
        datos_base[8] = ((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_asaltante)).getSelectedItem().toString();
        int index_defensor = ((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_defensor)).getSelectedItemPosition();
        datos_base[9] = ((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_defensor)).getSelectedItem().toString();

        Personaje asaltante = null;
        Personaje defensor = null;

        if (index_atacante < p.getDatos().Array_Personajes_En_Mision().size()) {
            asaltante = p.getDatos().Array_Personajes_En_Mision().get(index_atacante);
        }

        if (index_defensor < p.getDatos().Array_Personajes_En_Mision().size()) {
            defensor = p.getDatos().Array_Personajes_En_Mision().get(index_defensor);
        }

        int index_arma_atacante = ((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_arma_atk)).getSelectedItemPosition();
        datos_base[0] = ((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_arma_atk)).getSelectedItem().toString();
        int index_arma_defensor = ((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_arma_def)).getSelectedItemPosition();

        datos_base[1] = ((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_arma_def)).getSelectedItem().toString();

        int tipo_de_ataque = ((Spinner) p.findViewById(R.id.spinner_Combate_Tipo_de_ataque)).getSelectedItemPosition();
        datos_base[2] = ((Spinner) p.findViewById(R.id.spinner_Combate_Tipo_de_ataque)).getSelectedItem().toString();

        int cantidad_de_defensas = ((Spinner) p.findViewById(R.id.spinner_Combate_tipo_de_defensa)).getSelectedItemPosition();
        datos_base[3] = ((Spinner) p.findViewById(R.id.spinner_Combate_tipo_de_defensa)).getSelectedItem().toString();

        int localizacion = ((Spinner) p.findViewById(R.id.spinner_Combate_apuntado)).getSelectedItemPosition() - 1;
        datos_base[4] = ((Spinner) p.findViewById(R.id.spinner_Combate_apuntado)).getSelectedItem().toString();

        int distancia = ((Spinner) p.findViewById(R.id.spinner_Combate_distancia)).getSelectedItemPosition();
        datos_base[5] = ((Spinner) p.findViewById(R.id.spinner_Combate_distancia)).getSelectedItem().toString();

        boolean parada = true;


        if (cantidad_de_defensas > 4) {
            parada = false;
        }


        //añadir modificadores

        int modificador_a_tirada_defensa = 0;
        int modificador_a_tirada_ataque = 0;

        String listado_modificadores_defensa = "";
        String listado_modificadores_ataque = "";

        for (int i = 0; i < p.getDatos().arrayModificadores.size(); i++) {
            if (p.getModificador_defensa(i)) {
                listado_modificadores_defensa = listado_modificadores_defensa + p.getDatos().arrayModificadores.get(i).getNombre() + ": " + p.getDatos().arrayModificadores.get(i).getCaracteristicas()[Modificador.MODIFICADOR.PARADA] + "<br>";
                if (parada) {
                    modificador_a_tirada_defensa = modificador_a_tirada_defensa + p.getDatos().arrayModificadores.get(i).getCaracteristicas()[Modificador.MODIFICADOR.PARADA];
                } else {
                    modificador_a_tirada_defensa = modificador_a_tirada_defensa + p.getDatos().arrayModificadores.get(i).getCaracteristicas()[Modificador.MODIFICADOR.ESQUIVA];
                }
            }

            if (p.getModificadores_ataque(i)) {
                listado_modificadores_ataque = listado_modificadores_ataque + p.getDatos().arrayModificadores.get(i).getNombre() + ": " + p.getDatos().arrayModificadores.get(i).getCaracteristicas()[Modificador.MODIFICADOR.ATAQUE] + "<br>";
                modificador_a_tirada_ataque = modificador_a_tirada_ataque + p.getDatos().arrayModificadores.get(i).getCaracteristicas()[Modificador.MODIFICADOR.ATAQUE];
            }
        }


        int tirada_atacante = p.getEdInt(R.id.editText_Tirada_atacante) + p.getEdInt(R.id.editText_Modificador_atacante) + p.getEdInt(R.id.editText_Modificador_atacante_extra) + modificador_a_tirada_ataque;
        datos_base[6] = "Con tiradas de " + p.getEdInt(R.id.editText_Tirada_atacante) + "+" + p.getEdInt(R.id.editText_Modificador_atacante) + "+" + p.getEdInt(R.id.editText_Modificador_atacante_extra) + "+" + modificador_a_tirada_ataque;


        int tirada_defensor = p.getEdInt(R.id.editText_Tirada_defensor) + p.getEdInt(R.id.editText_Modificador_Defensor) + p.getEdInt(R.id.editText_Modificador_Defensor_Extra) + modificador_a_tirada_defensa;
        datos_base[7] = "Con tiradas de " + p.getEdInt(R.id.editText_Tirada_defensor) + "+" + p.getEdInt(R.id.editText_Modificador_Defensor) + "+" + p.getEdInt(R.id.editText_Modificador_Defensor_Extra) + "+" + modificador_a_tirada_defensa;
        ;


        if (asaltante != null && defensor != null) {
            mostrar("Index asaltante= " + index_atacante + "\n" +
                    "Index defensor = " + index_defensor + "\n" +
                    "asaltante: " + asaltante.getNombres() + "\n" +
                    "defensor: " + defensor.getNombres() + "\n" +
                    "arma asaltante: " + index_arma_atacante + "\n" +
                    "arma defensor: " + index_arma_defensor + "\n" +
                    "Tirada asaltante: " + tirada_atacante + "\n" +
                    "Tirada defensor: " + tirada_defensor + "\n" +
                    "tipo de ataque: " + tipo_de_ataque + "\n" +
                    "cantidad de defensas: " + cantidad_de_defensas + "\n" +
                    "localizacion: " + localizacion + "\n" +
                    "distancia: " + distancia);
        } else {
            mostrar("Index asaltante= " + index_atacante + "\n" +
                    "Index defensor = " + index_defensor + "\n" +
                    "arma asaltante: " + index_arma_atacante + "\n" +
                    "arma defensor: " + index_arma_defensor + "\n" +
                    "Tirada asaltante: " + tirada_atacante + "\n" +
                    "Tirada defensor: " + tirada_defensor + "\n" +
                    "tipo de ataque: " + tipo_de_ataque + "\n" +
                    "cantidad de defensas: " + cantidad_de_defensas + "\n" +
                    "localizacion: " + localizacion + "\n" +
                    "distancia: " + distancia);
        }

        mostrar("Saliendo: controlador \n");

        return wrapper(asaltante, defensor, parada, localizacion, distancia, index_arma_atacante, index_arma_defensor, tipo_de_ataque, tirada_atacante, tirada_defensor, cantidad_de_defensas, datos_base, listado_modificadores_defensa, listado_modificadores_ataque, p);

    }

    private static final Detalle Penalizador_cantidad_defensas(int tipo) {//Devuelve el penalizador dependiendo de la cantidad de defensas que deba hacer en el turno
        Detalle resultado = new Detalle();
        switch (tipo) {
            case 0:
            case 5:
                resultado.setDetalle("tipo: Tirada de combate.<br>" + WebBuilder.detalle("No tiene penalizadores por multiples defensas", "Por ser la primera defensa en el mismo turno, no posee penalizadores" + WebBuilder.página(89, "CE")));

                resultado.setResultado(0);
                break;
            case 1:
            case 6:
                resultado.setDetalle(WebBuilder.detalle("Tiene penalizadores por multiples defensas", "Por ser la segunda defensa en el mismo turno, posee un penalizador de 30 puntos a la defensa" + WebBuilder.página(89, "CE")));
                resultado.setResultado(-30);
                break;
            case 2:
            case 7:
                resultado.setDetalle(WebBuilder.detalle("Tiene penalizadores por multiples defensas", "Por ser la tercera defensa en el mismo turno, posee un penalizador de 50 puntos a la defensa" + WebBuilder.página(89, "CE")));
                resultado.setResultado(-50);
                break;
            case 3:
            case 8:
                resultado.setDetalle(WebBuilder.detalle("Tiene penalizadores por multiples defensas", "Por ser la cuarta defensa en el mismo turno, posee un penalizador de 70 puntos a la defensa" + WebBuilder.página(89, "CE")));
                resultado.setResultado(-70);
                break;
            case 4:
            case 9:
                resultado.setDetalle(WebBuilder.detalle("Tiene penalizadores por multiples defensas", "Por ser la quinta o posterior defensa en el mismo turno, posee un penalizador de 90 puntos a la defensa" + WebBuilder.página(89, "CE")));

                resultado.setResultado(-90);
                break;
        }
        return resultado;
    }

    private static final Resultado wrapper(Personaje atacante,
                                           Personaje defensor,
                                           boolean parada,
                                           int localizacion,
                                           int distancia,
                                           int index_arma_atacante,
                                           int index_arma_defensor,
                                           int tipo_de_ataque,
                                           int tirada_atacante,
                                           int tirada_defensor,
                                           int cantidad_de_defensas,
                                           String[] descripción,
                                           String listado_modificadores_defensa,
                                           String listado_modificadores_ataque, Principal p) {
        mostrar("En metodo: wrapper");
           /*

            Este metodo controla el resto de los metodos de combate, obtiene los datos del combate y aplica todos los penalizadores y efectos pertinentes.

            */

        Resultado devolución = new Resultado();

        String arma_atacante = " - ";
        String arma_defensor = " - ";
        String string_distancia = " - ";
        String string_tipo_de_ataque = " - ";

        int[] array_atacante;
        int[] array_defensor;

        int critico_arma = 0;
        Arma arma_atancante;

        int[] array_distancias = {1, 10, 50, 150, 500, 1000, 999999};

        distancia = array_distancias[distancia];

        boolean sin_apuntado = false;
        if (localizacion == SIN_APUNTAR) {//Modificadores por apuntado
            localizacion = Base.localizacion(localizacion);
            sin_apuntado = true;
        } else {
            tirada_atacante = tirada_atacante + DIFICULTADES_APUNTADO[localizacion];
        }

        mostrar("Localizacion: " + localizacion + " Tirada atacante: " + tirada_atacante + " Tirada defensor: " + tirada_defensor);

        //determinar si está atacando desarmado
        boolean atacante_desarmado = false;
        boolean defensor_desarmado = false;


        if (atacante != null) {
            if (atacante.getTipoDePersonaje() != 2) {
                array_atacante = atacante.datos_combate(index_arma_atacante);

                arma_atancante = atacante.arma(index_arma_atacante);
                //Obtener tipo de ataque según el arma usada
                critico_arma = arma_atancante.getCaracteristicas()[Arma.CRIT_PRINCIPAL];
                if (tipo_de_ataque == USANDO_CRITICO_SECUNDARIO) {
                    critico_arma = arma_atancante.getCaracteristicas()[Arma.CRIT_SECUNDARIO];
                }

                if (arma_atancante.getCaracteristicas()[Arma.DESARMADO] == 1) {
                    atacante_desarmado = true;
                }
            } else {
                array_atacante = atacante.datos_combate(-1);

                arma_atancante = p.getArrayArmas().get(index_arma_atacante);

                //Obtener tipo de ataque según el arma usada
                critico_arma = arma_atancante.getCaracteristicas()[Arma.CRIT_PRINCIPAL];
                if (tipo_de_ataque == USANDO_CRITICO_SECUNDARIO) {
                    critico_arma = arma_atancante.getCaracteristicas()[Arma.CRIT_SECUNDARIO];
                }

                if (arma_atancante.getCaracteristicas()[Arma.DESARMADO] == 1) {
                    atacante_desarmado = true;
                }

                array_atacante[Combate.DAÑO_BASE] = p.getArrayArmas().get(index_arma_atacante).getCaracteristicas()[Arma.DAÑO_BASE];
                array_atacante[Combate.ENTEREZA] = p.getArrayArmas().get(index_arma_atacante).getCaracteristicas()[Arma.ENTEREZA_ARMA];
                array_atacante[Combate.ROTURA] = p.getArrayArmas().get(index_arma_atacante).getCaracteristicas()[Arma.ROTURA_ARMA];
                array_atacante[Combate.ALCANCE] = p.getArrayArmas().get(index_arma_atacante).getCaracteristicas()[Arma.ALCANCE];
                array_atacante[Combate.CALIDAD] = 0;


            }


        } else {


            atacante = new Personaje();
            array_atacante = new int[30];
            array_atacante[Combate.ID_PERSONAJE] = -1;

            array_atacante[Combate.AGILIDAD] = 6;
            array_atacante[Combate.FUERZA] = 6;
            array_atacante[Combate.DESTREZA] = 6;

            array_atacante[Combate.VIDA_ACTUAL] = 999;

            array_atacante[Combate.ATAQUE] = 0;
            array_atacante[Combate.ESQUIVA] = 0;
            array_atacante[Combate.PARADA] = 0;

            array_atacante[Combate.DAÑO_BASE] = p.getArrayArmas().get(index_arma_atacante).getCaracteristicas()[Arma.DAÑO_BASE];
            array_atacante[Combate.ENTEREZA] = p.getArrayArmas().get(index_arma_atacante).getCaracteristicas()[Arma.ENTEREZA_ARMA];
            array_atacante[Combate.ROTURA] = p.getArrayArmas().get(index_arma_atacante).getCaracteristicas()[Arma.ROTURA_ARMA];
            array_atacante[Combate.ALCANCE] = p.getArrayArmas().get(index_arma_atacante).getCaracteristicas()[Arma.ALCANCE];
            array_atacante[Combate.CALIDAD] = 0;

            array_atacante[Combate.BARRERA_DE_DAÑO] = 0;
            array_atacante[Combate.ESCUDO] = 0;
            array_atacante[Combate.ROBO_VIDA] = 0;
            array_atacante[Combate.RED_PEN_PORC] = 0;
            array_atacante[Combate.RED_PEN_PLENO] = 0;
            array_atacante[Combate.RED_DAÑO_PORC] = 0;
            array_atacante[Combate.RED_DAÑO_PLENO] = 0;
            array_atacante[Combate.RESISTENCIA_FISICA] = 0;

            array_atacante[Combate.RESISTIR_EL_DOLOR] = 50;
            array_atacante[Combate.TRUCOS_DE_MANOS] = 50;

            critico_arma = tipo_de_ataque;

            arma_atancante = p.getArrayArmas().get(index_arma_atacante);

            index_arma_atacante = 0;
            tipo_de_ataque = 0;

        }

        if (defensor != null) {
            array_defensor = defensor.datos_combate(index_arma_defensor);

            //obtener armadura para el tipo de daño y la ubicación
            array_defensor[ARMADURA] = defensor.TA(critico_arma, ARMADURA_LOCALIZACION_GENERAL[localizacion]) * 10;

            Detalle cantidad_defensas_detalle = Penalizador_cantidad_defensas(cantidad_de_defensas);

            devolución.DetalleAppend(cantidad_defensas_detalle.getDetalle());

            tirada_defensor = tirada_defensor + cantidad_defensas_detalle.getResultado();

            if (defensor.arma(index_arma_defensor).getCaracteristicas()[Arma.DESARMADO] == 1) {
                defensor_desarmado = true;
            }


        } else {

            defensor = new Personaje();
            array_defensor = new int[30];

            array_defensor[Combate.AGILIDAD] = 6;
            array_defensor[Combate.FUERZA] = 6;
            array_defensor[Combate.DESTREZA] = 6;

            array_defensor[Combate.VIDA_ACTUAL] = 0;

            array_defensor[Combate.ATAQUE] = 0;
            array_defensor[Combate.ESQUIVA] = 0;
            array_defensor[Combate.PARADA] = 0;

            array_defensor[Combate.DAÑO_BASE] = 0;
            array_defensor[Combate.ENTEREZA] = (cantidad_de_defensas + 1);
            array_defensor[Combate.ROTURA] = (cantidad_de_defensas + 1);
            array_defensor[Combate.ALCANCE] = 9999999;
            array_defensor[Combate.CALIDAD] = 0;

            array_defensor[Combate.BARRERA_DE_DAÑO] = 0;
            array_defensor[Combate.ESCUDO] = 0;
            array_defensor[Combate.ROBO_VIDA] = 0;
            array_defensor[Combate.RED_PEN_PORC] = 0;
            array_defensor[Combate.RED_PEN_PLENO] = 0;
            array_defensor[Combate.RED_DAÑO_PORC] = 0;
            array_defensor[Combate.RED_DAÑO_PLENO] = 0;
            array_defensor[Combate.RESISTENCIA_FISICA] = 0;

            array_defensor[Combate.RESISTIR_EL_DOLOR] = 50;
            array_defensor[Combate.TRUCOS_DE_MANOS] = 50;

            if (cantidad_de_defensas < 100) {
                array_defensor[Combate.VIDA_ACTUAL] = (cantidad_de_defensas * 10) + 10;
            } else {
                array_defensor[Combate.VIDA_ACTUAL] = ((cantidad_de_defensas - 100) * 100) + 1000;
            }

            array_defensor[Combate.ARMADURA] = index_arma_defensor * 10;

            index_arma_defensor = 0;
            cantidad_de_defensas = 0;

        }

        array_atacante[TIRADA] = tirada_atacante;
        array_defensor[TIRADA] = tirada_defensor;





        /*
         public static final int ID_PERSONAJE = 0;
    public static final int ATAQUE = 1;
    public static final int ESQUIVA = 2;
    public static final int PARADA = 3;
    public static final int DAÑO_BASE = 4;
    public static final int VIDA_ACTUAL = 5;
    public static final int TIRADA = 6;
    public static final int ARMADURA = 8;
    public static final int BARRERA_DE_DAÑO = 9;
    public static final int ESCUDO = 10;
    public static final int ROBO_VIDA = 11;
    public static final int RED_PEN_PORC = 12;
    public static final int RED_PEN_PLENO = 13;
    public static final int RED_DAÑO_PORC = 14;
    public static final int RED_DAÑO_PLENO = 15;
    public static final int RESISTENCIA_FISICA = 16;
    public static final int FUERZA = 17;
    public static final int DESTREZA = 18;
    public static final int RESISTIR_EL_DOLOR = 20;
    public static final int TRUCOS_DE_MANOS = 21;
    public static final int ENTEREZA = 22;
    public static final int ROTURA = 23;
    public static final int ALCANCE = 24;
    public static final int CALIDAD = 25;
    public static final int AGILIDAD = 28;
         */

        mostrar("           ID_TABLE ATK ESQ PAR DMG PV TIR TA BD ESC LS P% PP D% DP RF FUE DES RDOL TMAN ENT ROT ALC CAL AGI");
        mostrar("Atacante: " + Arrays.toString(array_atacante) + "\n" + "Defensor: " + Arrays.toString(array_defensor) + "\n Tipo de ataque: " + tipo_de_ataque);

        if (!(listado_modificadores_ataque == "") & !listado_modificadores_ataque.isEmpty()) {
            devolución.DetalleAppend(WebBuilder.detalle("Listado de modificadores que tiene el asaltante", listado_modificadores_ataque));
        }
        if (!(listado_modificadores_defensa == "") & !listado_modificadores_defensa.isEmpty()) {
            devolución.DetalleAppend(WebBuilder.detalle("Listado de modificadores que tiene el defensor", listado_modificadores_defensa));
        }


        switch (tipo_de_ataque) {
            case IGNORA_ARMADURA:
                array_defensor[ARMADURA] = 0;

                if (arma_atancante.getCaracteristicas()[Arma.ALCANCE] > 1) {
                    devolución.DetalleAppend(WebBuilder.detalle("Ataque a distancia " + WebBuilder.habilidad_usada("IGNORANDO ARMADURA") + " contra " + WebBuilder.nombres(defensor.getNombre_personaje()), WebBuilder.nombres(atacante.getNombre_personaje()) + " golpea en " + Base.localización(localizacion) + " a " + WebBuilder.nombres(defensor.getNombre_personaje()) + " con " + WebBuilder.nombres(((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_arma_atk)).getItemAtPosition(index_arma_atacante).toString()) + WebBuilder.lb() + "El defensor se encuentra a " + distancia + " metros."))
                    ;
                    devolución.actualizar(new Resultado(ataque_a_distancia(array_atacante, array_defensor, parada, localizacion, distancia, defensor_desarmado, false)));
                } else {
                    devolución.DetalleAppend(WebBuilder.detalle("Ataque cuerpo a cuerpo " + WebBuilder.habilidad_usada("IGNORANDO ARMADURA") + " contra " + WebBuilder.nombres(defensor.getNombre_personaje()), WebBuilder.nombres(atacante.getNombre_personaje()) + " golpea en " + Base.localización(localizacion) + " a " + WebBuilder.nombres(defensor.getNombre_personaje()) + " con " + WebBuilder.nombres(((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_arma_atk)).getItemAtPosition(index_arma_atacante).toString())));
                    devolución.actualizar(new Resultado(ataque_básico(array_atacante, array_defensor, parada, localizacion, distancia, defensor_desarmado, false, -1)));
                }
                break;
            case USANDO_CRITICO_SECUNDARIO:
                array_atacante[TIRADA] = array_atacante[TIRADA] - 10;
            case ATAQUE_BÁSICO:
                if (arma_atancante.getCaracteristicas()[Arma.ALCANCE] > 1) {
                    devolución.DetalleAppend(WebBuilder.detalle("Ataque a distancia contra " + WebBuilder.nombres(defensor.getNombre_personaje()), WebBuilder.nombres(atacante.getNombre_personaje()) + " golpea en " + Base.localización(localizacion) + " a " + WebBuilder.nombres(defensor.getNombre_personaje() + " con " + WebBuilder.nombres(((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_arma_atk)).getItemAtPosition(index_arma_atacante).toString()) + WebBuilder.lb() + "El defensor se encuentra a " + distancia + " metros.")));
                    devolución.actualizar(new Resultado(ataque_a_distancia(array_atacante, array_defensor, parada, localizacion, distancia, defensor_desarmado, false)));
                } else {
                    devolución.DetalleAppend(WebBuilder.detalle("Ataque cuerpo a cuerpo contra " + WebBuilder.nombres(defensor.getNombre_personaje()), WebBuilder.nombres(atacante.getNombre_personaje()) + " golpea en " + Base.localización(localizacion) + " a " + WebBuilder.nombres(defensor.getNombre_personaje())) + " con " + WebBuilder.nombres(((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_arma_atk)).getItemAtPosition(index_arma_atacante).toString()));
                    devolución.actualizar(new Resultado(ataque_básico(array_atacante, array_defensor, parada, localizacion, distancia, defensor_desarmado, false, -1)));
                }
                break;
            case DERRIBO:
                devolución.DetalleAppend(WebBuilder.detalle("Se intenta derribar a " + WebBuilder.nombres(defensor.getNombre_personaje()), "El objetivo de este ataque es derribar al adversario. Esta maniobra específica sufre un penalizador de a la habilidad, pero la armadura no influye en su finalidad. Si se consigue un daño mínimo, el impacto ha logrado alcanzar a su blanco y podrá tratar de derribarlo. Para ello, ambos deben realizar un control enfrentado de características. En el caso de que gane quien ejecutó el derribo, su antagonista caerá de inmediato al suelo. Un derribo puede producir daño sólo si el atacante lo desea, pero aun así, la base queda reducida a la mitad y sí se aplica la armadura del defensor." + WebBuilder.página(90, "CE")));

                if (arma_atancante.getCaracteristicas()[Arma.TAMAÑO] == 0 && !atacante_desarmado) {
                    array_atacante[TIRADA] = array_atacante[TIRADA] - 60;
                } else {
                    array_atacante[TIRADA] = array_atacante[TIRADA] - 30;
                }

                devolución.actualizar(new Resultado(derribo_desarmar_presa(array_atacante, array_defensor, parada, localizacion, distancia, defensor_desarmado, false, DERRIBO)));

                break;
            case INUTILIZAR:

                if (sin_apuntado | !Base.isExtremidad(localizacion)) {
                    devolución.DetalleAppend(WebBuilder.detalle("Este tipo de ataque debe ser apuntado a una extremidad", "Esta maniobra tiene como fin inutilizar a un individuo sin matarlo, rompiéndole alguna extremidad o sajando sus tendones. Si un personaje realiza un golpe apuntado a un brazo o una pierna, declarando que sólo quiere inutilizar a su adversario, reduce el daño final que produce su ataque a la mitad. Inutilizar no funciona contra criaturas con acumulación de daño." + WebBuilder.página(90, "CE")));
                } else {
                    if (arma_atancante.getCaracteristicas()[Arma.ALCANCE] > 1) {
                        devolución.DetalleAppend(WebBuilder.detalle("Se intenta inutilizar " + WebBuilder.nombres(defensor.getNombre_personaje()) + " con un ataque a distancia", "Esta maniobra tiene como fin inutilizar a un individuo sin matarlo, rompiéndole alguna extremidad o sajando sus tendones. Si un personaje realiza un golpe apuntado a un brazo o una pierna, declarando que sólo quiere inutilizar a su adversario, reduce el daño final que produce su ataque a la mitad. Inutilizar no funciona contra criaturas con acumulación de daño." + WebBuilder.página(90, "CE")));
                        devolución.actualizar(new Resultado(ataque_a_distancia(array_atacante, array_defensor, parada, localizacion, distancia, defensor_desarmado, true)));
                    } else {
                        devolución.DetalleAppend(WebBuilder.detalle("Se intenta inutilizar " + WebBuilder.nombres(defensor.getNombre_personaje()) + " con un ataque cuerpo a cuerpo", "Esta maniobra tiene como fin inutilizar a un individuo sin matarlo, rompiéndole alguna extremidad o sajando sus tendones. Si un personaje realiza un golpe apuntado a un brazo o una pierna, declarando que sólo quiere inutilizar a su adversario, reduce el daño final que produce su ataque a la mitad. Inutilizar no funciona contra criaturas con acumulación de daño." + WebBuilder.página(90, "CE")));
                        devolución.actualizar(new Resultado(ataque_básico(array_atacante, array_defensor, parada, localizacion, distancia, defensor_desarmado, true, -1)));
                    }
                }
                break;
            case DESARMAR:

                array_atacante[TIRADA] = array_atacante[TIRADA] - 40;

                devolución.DetalleAppend(WebBuilder.detalle("Se intenta desarmar a " + WebBuilder.nombres(defensor.getNombre_personaje()), "Mediante esta maniobra, un personaje puede arrebatar el arma de las manos de su adversario, utilizando para ello su destreza o la mera fuerza bruta. Una maniobra de Desarmar no produce daños sobre el defensor." + WebBuilder.página(90, "CE")));

                devolución.actualizar(new Resultado(derribo_desarmar_presa(array_atacante, array_defensor, parada, localizacion, distancia, defensor_desarmado, false, DESARMAR)));

                break;
            case PRESA:
                array_atacante[TIRADA] = array_atacante[TIRADA] - 40;

                devolución.DetalleAppend(WebBuilder.detalle(WebBuilder.nombres(atacante.getNombre_personaje()) + " intenta apresar a " + WebBuilder.nombres(defensor.getNombre_personaje()), "El objetivo de la Presa es inmovilizar a un adversario. Sólo puede utilizarse empleando ataques desarmados, o con aquellas armas que permitan específicamente Presar. Una Presa puede producir daño sólo si el atacante lo desea, pero aun así queda reducido a la mitad, y en dicho caso sí se aplicaría la armadura del defensor. Para liberarse de una Presa que no sea completa, un personaje puede realizar un nuevo control en turnos posteriores, aunque con un –3 al valor de su característica. Mientras sujete a su adversario, el personaje que realiza la presa está el mismo sometido a parálisis menor." + WebBuilder.página(90, "CE")));


                devolución.actualizar(new Resultado(derribo_desarmar_presa(array_atacante, array_defensor, parada, localizacion, distancia, defensor_desarmado, false, PRESA)));

                if (!atacante_desarmado) {
                    devolución.Descripción("El atacante suelta sus armas antes de proceder. " + devolución.Descripción());
                    devolución.DetalleAppend(WebBuilder.lb() + "El atacante suelta sus armas antes de proceder. ");
                }


                break;
            case INCONSCIENCIA:
                String tipo_de_arma = "";
                String detalle_arma = "";

                if (arma_atancante.getCaracteristicas()[Arma.CRIT_PRINCIPAL] == Base.ARM_TA_CON) {
                    tipo_de_arma = CONTUNDENTE;
                    detalle_arma = ("El arma permite este tipo de ataque");
                } else if (arma_atancante.getCaracteristicas()[Arma.CRIT_SECUNDARIO] == Base.ARM_TA_CON) {
                    detalle_arma = ("El arma permite este tipo de ataque, pero recibe penalizadores (ataque secundario)");
                    tipo_de_arma = CONTUNDENTE;
                    array_atacante[TIRADA] = array_atacante[TIRADA] - 10;
                }

                devolución.DetalleAppend(WebBuilder.detalle("Se intenta dejar inconsciente a " + WebBuilder.nombres(defensor.getNombre_personaje()), "Si lo desea, un luchador puede tratar de dejar fuera de combate a su adversario, en lugar de arrebatarle la vida por completo. Para ello es necesario utilizar un ataque con un crítico Contundente y realizar un golpe apuntado a la cabeza. Si el Nivel de Crítico es superior a 50, el personaje golpeado queda automáticamente inconsciente. Con esta maniobra, el crítico no producirá otro efecto que causar negativos a la acción o la inconsciencia deseada, y nunca podrá provocar muerte automática." + WebBuilder.lb() + detalle_arma + WebBuilder.página(90, "CE")));


                devolución.actualizar(new Resultado(Inconsciencia(array_atacante, array_defensor, parada, localizacion, distancia, tipo_de_arma, defensor_desarmado)));
                break;
            case ATAQUE_EN_AREA:
                array_atacante[TIRADA] = array_atacante[TIRADA] - 50;

                devolución.DetalleAppend(WebBuilder.detalle(WebBuilder.nombres(atacante.getNombre_personaje()) + " realiza una maniobra de ataque en área.", "Permite a un personaje abarcar a varios enemigos de un solo golpe, de manera que con un único ataque alcanza a multitud de blancos. Todos los individuos que sean afectados reciben un ataque con la habilidad final obtenida, y deberán defenderse en el orden en el que estén situados. Si uno de ellos consigue un contraataque utilizando su habilidad de parada, el ataque es interceptado y quienes aún no se han defendido no tienen la necesidad de hacerlo. Los sujetos han de encontrase juntos y al alcance del personaje. Se entiende que estas cifras son aplicables contra adversarios de tamaño humano, por lo que el Director de Juego podrá aumentar o disminuir su número si las dimensiones físicas de los antagonistas difieren." + WebBuilder.página(90, "CE")));
                devolución.DetalleAppend(WebBuilder.detalle("Cantidad de enemigos según el arma:", "Pequeñas = 3 personajes</li> <li>Medianas = 4 personajes</li> <li>Grandes = 5 personajes" + WebBuilder.página(90, "CE")));

                devolución.actualizar(new Resultado(ataque_básico(array_atacante, array_defensor, parada, localizacion, distancia, defensor_desarmado, false, -1)));

                break;
            case ENGATILLAR:
                array_atacante[TIRADA] = array_atacante[TIRADA] - 100;

                devolución.DetalleAppend(WebBuilder.detalle(WebBuilder.nombres(atacante.getNombre_personaje()) + " intenta engatillar a " + WebBuilder.nombres(defensor.getNombre_personaje()), "Engatillar es una acción con la que el personaje pretende poner a su adversario en una situación de desventaja, colocando en contacto con él algún tipo de arma, pero sin llegar realmente a causarle daño, como situar una daga en su cuello o la punta de una espada en el pecho. Si se consigue cualquier resultado que pudiera causar daños, el atacante no restará puntos de vida a su blanco, pero lo someterá a una situación de amenazado. Al contrario de lo que dictan las normas generales, realizar un ataque sobre un individuo engatillado se considera una acción pasiva (aunque sigue consumiendo un ataque), salvo en el caso de que alguien, incluyendo al mismo sujeto amenazado, logre sorpresa sobre quien lo amenaza. Si el Director lo permite, es posible declarar que se pretende Engatillar a alguien tras haber lanzado los dados para realizar su ataque, siempre y cuando el defensor aún no haya hecho su tirada." + WebBuilder.página(90, "CE")));

                devolución.actualizar(new Resultado(ataque_básico(array_atacante, array_defensor, parada, localizacion, distancia, defensor_desarmado, false, -1)));

                if (devolución.DAÑOS_PV() > 0) {
                    devolución.DetalleAppend(WebBuilder.resultado("<center>" + defensor.getNombre_personaje() + " queda en situación de amenazado.</center>"));
                    devolución.DAÑOS_PV(0);
                } else {
                    devolución.DetalleAppend(WebBuilder.resultado("<center>" + atacante.getNombre_personaje() + " no logra engatillar a " + defensor.getNombre_personaje() + "</center>"));
                    devolución.DAÑOS_PV(0);
                }

                break;
        }

        devolución.Robo_de_vida(((devolución.DAÑOS_PV * array_atacante[ROBO_VIDA]) / 100));

        if (array_atacante[ROBO_VIDA] > 0 & devolución.Robo_de_vida() > 0) {
            devolución.DetalleAppend(WebBuilder.detalle("El atacante roba " + devolución.Robo_de_vida() + " puntos de vida.", "El atacante posee un robo de vida del " + array_atacante[ROBO_VIDA] + "%."));
        }


       /* devolución.setDatos_combate(descripción[NOMBRE_ATACANTE_ST] + " usó " + descripción[ARMA_ATACANTE_ST]
                + " para realizar " + descripción[TIPO_DE_ATAQUE_ST] + " apuntando a "
                + descripción[TIPO_DE_APUNTADO_ST] + " a una distancia de " + descripción[DISTANCIA_ST] + " con una tirada de "
                + descripción[TIRADA_ATACANTE_ST] + ". "
                + descripción[NOMBRE_DEFENSOR_ST] + " se defiende con " + descripción[TIPO_DE_DEFENSA_ST] + " usando "
                + descripción[ARMA_DEFENSOR_ST] + " con una tirada de " + descripción[TIRADA_DEFENSOR_ST]);
*/
        devolución.setDatos_combate(devolución.getDetalle());

        mostrar(devolución.toString());

        return devolución;
    }

    public static Resultado Inconsciencia(int[] atacante, int[] defensor, boolean parada, int localizacion, int distancia, String tipo_de_arma, boolean desarmado) {
        mostrar("En metodo: Inconsciencia");
         /*
    En este método se considera que:

        El daño base ya tiene su modificador por proezas de fuerza calculado

        La localización es un spinner con el primer valor "sin ataque apuntado" y se pasa el valor "spinner.position -1"

        La tirada ya incluye todos los modificadores pertinentes

   La Inconsciencia es un posible efecto secundario de un impacto en la cabeza. Si lo desea, un luchador puede tratar de dejar fuera de combate a su adversario, en lugar de arrebatarle la vida por completo. Para ello es necesario utilizar un ataque con un crítico Contundente y realizar un golpe apuntado a la cabeza. Si el personaje declara previamente que su intención es dejar inconsciente a su enemigo, sólo producirá la mitad del daño que produciría con su ataque, pero se contará como daño pleno a efectos del cálculo de críticos. Si el Nivel de Crítico es superior a 50, el personaje golpeado queda automáticamente inconsciente. Con esta maniobra, el crítico no producirá otro efecto que causar negativos a la acción o la inconsciencia deseada, y nunca podrá provocar muerte automática. Si desea dejarse inconsciente a un adversario sin utilizar un arma Contundente, se aplicará un penalizador adicional de -40 al ataque.

   se aplica automaticamente el -40 si el arma no es contundente

    */
        Resultado devolución = new Resultado();

        if (localizacion != LA_CABEZA) {
            localizacion = LA_CABEZA;
            atacante[TIRADA] = atacante[TIRADA] - 60;
        }

        if (tipo_de_arma != CONTUNDENTE) {
            devolución.DetalleAppend("El arma no está preparada para realizar esta maniobra, por lo que recibe un penalizador de 40 puntos (" + atacante[TIRADA] + "-40=" + (atacante[TIRADA] - 40));
            devolución.DescripciónAppend("El arma no está preparada para realizar esta maniobra, por lo que recibe un penalizador de 40 puntos");
            atacante[TIRADA] = atacante[TIRADA] - 40;
        }

        devolución = ataque_básico(atacante, defensor, parada, localizacion, distancia, desarmado, false, -1);

        if ((int) devolución.NIVEL_CRITICO() > 50) {
            devolución.Descripción("El defensor queda inconsciente luego del ataque. Recibe " + ((int) devolución.DAÑOS_PV() / 2) + " puntos de daño (pero no puede causar muerte automatica)");
        }


        return devolución;

    }


    public static Resultado derribo_desarmar_presa(int[] atacante, int[] defensor, boolean parada, int localizacion, int distancia, boolean desarmado, boolean inutilizar, int tipo) {
        mostrar("En metodo: derribo_desarmar_presa");
         /*
    En este método se considera que:

        El daño base ya tiene su modificador por proezas de fuerza calculado

        La localización es un spinner con el primer valor "sin ataque apuntado" y se pasa el valor "spinner.position -1"

        La tirada ya incluye todos los modificadores pertinentes

      Derribo: Como su nombre indica, el objetivo de este ataque es derribar al adversario. Esta maniobra específica sufre un penalizador de -30 a la habilidad, por la dificultad que entraña (-60 si se emplea un arma corta que no sea combate desarmado), pero se ejecuta siempre contra una TA 0, ya que la armadura no influye en su finalidad. Si se consigue un daño mínimo del 10%, el impacto ha logrado alcanzar a su blanco y podrá tratar de derribarlo. Para ello, ambos deben realizar un control enfrentado de características, en el que el atacante podrá elegir entre emplear su Fuerza o Destreza, y el defensor optará entre su Fuerza o Agilidad. En el caso de que gane quien ejecutó el derribo, su antagonista caerá de inmediato al suelo. Si la diferencia de habilidades no ha logrado alcanzar un daño del 100%, el atacante aplicará un -3 a su tirada, mientras que si supera un daño del 200%, el modificador aumentará a +3. Las criaturas cuadrúpedas gozan de un bono natural de +3 a sus controles a causa de la estabilidad de sus patas. Un derribo puede producir daño sólo si el atacante lo desea, pero aun así, la base queda reducida a la mitad y sí se aplica la armadura del defensor.

       Desarmar: Mediante esta maniobra, un personaje puede arrebatar el arma de las manos de su adversario, utilizando para ello su destreza o la mera fuerza bruta. Para realizarla aplicará un –40 a su habilidad de ataque, que como en el caso del Derribo, se ejecutará siempre contra una TA 0. Si logra al menos un resultado de 10% de daño deberá realizarse un control enfrentado de características entre ambos, en el que cada uno podrá elegir indistintamente entre su Fuerza o su Destreza. Si la diferencia de habilidades no ha logrado alcanzar un daño del 100%, el atacante aplicará un –3 al valor de su tirada, mientras que si supera el 200%, aumentará en +3. Si el agresor gana el control, habrá desarmado a su adversario. Una maniobra de Desarmar no produce daños sobre el defensor.

       Presa: El objetivo de la Presa es inmovilizar a un adversario. Esta maniobra sufre un penalizador de –40 a la habilidad de ataque, pero al igual que las maniobras anteriores se ejecuta contra una TA 0. Sólo puede utilizarse empleando ataques desarmados, o con aquellas armas que permitan específicamente Presar. Si se alcanza un daño mínimo del 10%, ambos personajes realizarán un control enfrentado de características. El atacante puede elegir entre su Fuerza o Destreza, y el defensor entre su Fuerza o Agilidad. De nuevo, si el Resultado del asalto no ha logrado alcanzar un daño del 100%, el agresor aplicará un –3 al valor de su control, mientras que si es superior al 200%, obtendrá un +3. Si quien realiza la presa gana el control por una diferencia inferior a 3 puntos, el defensor quedará en una condición de parálisis menor, mientras que si es mayor, será de parálisis parcial. Si la diferencia fuese superior a 10, la presa es perfecta y queda sometido a parálisis completa, por lo que ya no le es posible librarse de ella posteriormente. Una Presa puede producir daño sólo si el atacante lo desea, pero aun así queda reducido a la mitad, y en dicho caso sí se aplicaría la armadura del defensor. Para liberarse de una Presa que no sea completa, un personaje puede realizar un nuevo control en turnos posteriores cada vez que le toque actuar, aunque con un –3 al valor de su característica. Si por alguna causa no puede actuar en ese asalto, tiene la capacidad de intentarlo igualmente antes de que acabe el turno, cuando todos los demás ya hayan ejecutado sus acciones. Mientras sujete a su adversario, el personaje que realiza la presa está el mismo sometido a parálisis menor.
    */

        Resultado devolución = new Resultado();

        devolución.ID_ATACANTE(atacante[ID_PERSONAJE]);
        devolución.ID_DEFENSOR(defensor[ID_PERSONAJE]);

        int ataque = atacante[ATAQUE] + atacante[TIRADA];

        String attr_atacante = "DESTREZA";
        String attr_defensor = "AGILIDAD";
        String bonos = "";
        int tirada_atacante = tiradaD10();


        int atributo_atacante = atacante[DESTREZA];
        if (atacante[FUERZA] > atacante[DESTREZA]) {
            attr_atacante = "FUERZA";
            atributo_atacante = atacante[FUERZA];
        }

        int atributo_defensor = 0;

        switch (tipo) {
            case DERRIBO:
            case PRESA:
                atributo_defensor = defensor[AGILIDAD];
                if (defensor[FUERZA] > defensor[AGILIDAD]) {
                    attr_defensor = "FUERZA";
                    atributo_defensor = defensor[FUERZA];
                }
                break;
            case DESARMAR:
                atributo_defensor = defensor[DESTREZA];
                attr_defensor = "DESTREZA";
                if (defensor[FUERZA] > defensor[DESTREZA]) {
                    attr_defensor = "FUERZA";
                    atributo_defensor = defensor[FUERZA];
                }
                break;
        }

        int defensa = defensor[ESQUIVA] + defensor[TIRADA];
        if (parada) {
            defensa = defensor[PARADA] + defensor[TIRADA];
        }

        mostrar("Ataque: " + ataque + " Defensa: " + defensa);
        int tirada_Defensor = tiradaD10();
        if (ataque - defensa > 10) {

            mostrar("Antes bono: atributo_atacante: " + atributo_atacante + " atributo_defensor: " + atributo_defensor);

            if (ataque - defensa < 100) {
                bonos = "El atacante tiene un penalizador de 3 puntos por no haber superado una diferencia de 100 respecto a la defensa. (" + WebBuilder.resultado(ataque + "-" + defensa + "=" + (ataque - defensa)) + ")<br>";
                atributo_atacante = atributo_atacante - 3;

            }

            if (ataque - defensa > 200) {
                bonos = "El atacante tiene un bonificador de 3 puntos por haber superado una diferencia de 200 respecto a la defensa. (" + WebBuilder.resultado(ataque + "-" + defensa + "=" + (ataque - defensa)) + ")<br>";
                atributo_atacante = atributo_atacante + 3;
            }

            if (ataque - defensa >= 100 & ataque - defensa <= 200) {
                bonos = "Resultado de la contienda: " + WebBuilder.resultado(ataque + "-" + defensa + "=" + (ataque - defensa)) + "<br>";
            }

            mostrar("Antes tirada: atributo_atacante: " + atributo_atacante + " atributo_defensor: " + atributo_defensor);


            int atributo_atacante_final = atributo_atacante + tirada_atacante;
            int atributo_defensor_final = atributo_defensor + tirada_Defensor;

            mostrar("Final: atributo_atacante: " + atributo_atacante + " atributo_defensor: " + atributo_defensor);

            if (atributo_atacante_final > atributo_defensor_final) {

                atacante[DAÑO_BASE] = atacante[DAÑO_BASE] / 2;

                switch (tipo) {
                    case DESARMAR:
                        devolución.DetalleAppend(WebBuilder.detalle("El atacante logra desarmar al defensor.", bonos + "Tiradas de atributo:<br>Atacante: " + WebBuilder.formateo_tirada(attr_atacante, atributo_atacante, tirada_atacante) + "<br>Defensor: " + WebBuilder.formateo_tirada(attr_defensor, atributo_defensor, tirada_Defensor)));
                        devolución.Descripción("El atacante logra desarmar al defensor");
                        break;
                    case DERRIBO:
                        devolución.actualizar(ataque_básico(atacante, defensor, parada, localizacion, distancia, desarmado, inutilizar, -1));

                        devolución.DetalleAppend(WebBuilder.detalle("El atacante logra derribar al defensor.", bonos + "Tiradas de atributo:<br>Atacante: " + WebBuilder.formateo_tirada(attr_atacante, atributo_atacante, tirada_atacante) + "<br>Defensor: " + WebBuilder.formateo_tirada(attr_defensor, atributo_defensor, tirada_Defensor)) + WebBuilder.lb() + "Puede provocar " + WebBuilder.resultado(devolución.DAÑOS_PV()) + " puntos de daño.");

                        break;
                    case PRESA:

                        if (atributo_atacante_final - atributo_defensor_final < 3) {
                            devolución.DetalleAppend(WebBuilder.detalle("El defensor está sujeto a una paralisis menor.", bonos + "Tiradas de atributo:<br>Atacante: " + WebBuilder.formateo_tirada(attr_atacante, atributo_atacante, tirada_atacante) + "<br>Defensor: " + WebBuilder.formateo_tirada(attr_defensor, atributo_defensor, tirada_Defensor)));
                            devolución.Descripción("El defensor está sujeto a una paralisis menor. Para liberarse un personaje puede realizar un nuevo control en turnos posteriores cada vez que le toque actuar, aunque con un –3 al valor de su característica." + WebBuilder.lb() + " Mientras sujete a su adversario, el personaje que realiza la presa está sometido a parálisis menor.");
                        }
                        if (atributo_atacante_final - atributo_defensor_final >= 3) {
                            devolución.DetalleAppend(WebBuilder.detalle("El defensor está sujeto a una paralisis parcial.", bonos + "Tiradas de atributo:<br>Atacante: " + WebBuilder.formateo_tirada(attr_atacante, atributo_atacante, tirada_atacante) + "<br>Defensor: " + WebBuilder.formateo_tirada(attr_defensor, atributo_defensor, tirada_Defensor)));
                            devolución.Descripción("El defensor está sujeto a una paralisis parcial. Para liberarse un personaje puede realizar un nuevo control en turnos posteriores cada vez que le toque actuar, aunque con un –3 al valor de su característica." + WebBuilder.lb() + " Mientras sujete a su adversario, el personaje que realiza la presa está sometido a parálisis menor.");
                        }

                        if (atributo_atacante_final - atributo_defensor_final > 10) {
                            devolución.DetalleAppend(WebBuilder.detalle("El defensor está sujeto a una paralisis completa.", bonos + "Tiradas de atributo:<br>Atacante: " + WebBuilder.formateo_tirada(attr_atacante, atributo_atacante, tirada_atacante) + "<br>Defensor: " + WebBuilder.formateo_tirada(attr_defensor, atributo_defensor, tirada_Defensor)) + WebBuilder.lb() + "Esto significa que ya no le es posible librarse de ella posteriormente." + WebBuilder.lb() + " Mientras sujete a su adversario, el personaje que realiza la presa está sometido a parálisis menor.");
                            devolución.Descripción("El defensor está sujeto a una paralisis completa. Esto significa que ya no le es posible librarse de ella posteriormente.");
                        }

                        devolución.DescripciónAppend(WebBuilder.lb() + " Mientras sujete a su adversario, el personaje que realiza la presa está sometido a parálisis menor.");

                        break;
                }

            } else {
                devolución.DetalleAppend(WebBuilder.detalle("La maniobra no tiene éxito.", bonos + "Tiradas de atributo:<br>Atacante: " + WebBuilder.formateo_tirada(attr_atacante, atributo_atacante, tirada_atacante) + "<br>Defensor: " + WebBuilder.formateo_tirada(attr_defensor, atributo_defensor, tirada_Defensor)) + WebBuilder.lb() + "Los dos personajes quedan enfrascados en un forcejeo.");
                devolución.Descripción("Los dos personajes quedan enfrascados en un forcejeo.");
            }

            return devolución;

        } else {
            devolución.DetalleAppend(WebBuilder.detalle("La maniobra no tiene éxito.", bonos + "El atacante no logra superar el mínimo ataque para poder realizar la maniobra." + WebBuilder.lb() + "Los dos personajes quedan enfrascados en un forcejeo."));

            devolución.actualizar(ataque_básico(atacante, defensor, parada, localizacion, distancia, desarmado, inutilizar, -1));
            return devolución;
        }
    }

    public static Resultado atrapar_proyectil(int[] atacante, int[] defensor, boolean parada, int localizacion, int distancia, String tipo_de_arma, boolean desarmado, boolean inutilizar) {
        mostrar("En metodo: atrapar_proyectil");
        /*
    En este método se considera que:

        El daño base ya tiene su modificador por proezas de fuerza calculado

        La localización es un spinner con el primer valor "sin ataque apuntado" y se pasa el valor "spinner.position -1"

        La tirada ya incluye todos los modificadores pertinentes

        Incluyendo los modificadores especiales dependiendo si usa o no escudo y si tiene maestría en defensa.

        Debe incluir el penalizador acumulativo de -25 por control en el mismo asalto

        Los Trucos de manos como defensa contra proyectiles: La habilidad malabar, que se desarrolla con Trucos de manos, permite a un personaje atrapar proyectiles en el aire antes de que alcancen su blanco, por lo que puede usarse como defensa contra ellos. Los proyectiles lanzados de manera natural (es decir, sin medios mágicos, psíquicos o de dominio) podrán atraparse si se consigue superar una dificultad de Absurdo (ABS). En el caso de proyectiles disparados, dado que alcanzan una velocidad y potencia mayores, es contra Casi Imposible (CIM). Si se intenta detener un disparo realizado por un arma de fuego, el personaje deberá haber alcanzado la maestría y realizar un control contra Inhumano (INH). Para atrapar proyectiles lanzados o disparados sobrenaturalmente, se realizará una comparación enfrentada entre la habilidad de Trucos de manos del defensor y el ataque del lanzador. Si el control malabar es superior al ataque final, se considera que lo ha interceptado en el aire. Esta habilidad puede emplearse como una defensa pasiva, pero se aplica un penalizador de –25 adicional por cada control que se realice durante el mismo asalto. Naturalmente, ejecutar esta maniobra puede requerir mucha fortaleza para detener objetos de gran tamaño o lanzados con mucha potencia, por lo que está en manos del DJ exigir que el defensor posea una Fuerza determinada. Si alguien fracasa atrapando un proyectil, aún podrá tratar de esquivarlo aplicando un penalizador de –50 a su habilidad.
    */
        Resultado devolución = new Resultado();

        int tirada_Defensa = defensor[TRUCOS_DE_MANOS] + defensor[TIRADA];
        String tipo_arma = "";
        boolean atrapa = false;

        switch (tipo_de_arma) {
            case LANZADO:
                tipo_arma = "Debía superar una dificultad de " + WebBuilder.resultado(180) + " para realizar esta maniobra.";
                if (tirada_Defensa > 180) {
                    atrapa = true;
                }

                break;
            case DISPARADO_LENTO:
                tipo_arma = "Debía superar una dificultad de " + WebBuilder.resultado(240) + " para realizar esta maniobra.";
                if (tirada_Defensa > 240) {
                    atrapa = true;
                }

                break;

            case DISPARADO_RÁPIDO:
                tipo_arma = "Debía superar una dificultad de " + WebBuilder.resultado(320) + " para realizar esta maniobra.";
                if (tirada_Defensa > 320) {
                    atrapa = true;
                }

                break;
            case SOBRENATURAL:
                int tirada_Ataque = atacante[ATAQUE] + atacante[TIRADA];
                tipo_arma = "Debía superar una dificultad de " + WebBuilder.resultado(tirada_Ataque) + " para realizar esta maniobra.";
                if (tirada_Defensa > tirada_Ataque) {
                    atrapa = true;
                }
                break;
        }

        if (atrapa) {
            devolución.ID_ATACANTE(atacante[ID_PERSONAJE]);
            devolución.ID_DEFENSOR(defensor[ID_PERSONAJE]);

            devolución.DetalleAppend(WebBuilder.detalle("El defensor atrapa el proyectil.", tipo_arma + " Como sacó " + WebBuilder.formateo_tirada("TRUCOS DE MANOS", defensor[TRUCOS_DE_MANOS], defensor[TIRADA]) + " logra atrapar el proyectil. Pero sigue pudiendo intentar esquivarlo, pero con un penalizador de 50 puntos"));

            devolución.Descripción("El defensor logra atrapar el proyectil en pleno vuelo");

            return devolución;

        } else {

            devolución.DetalleAppend(WebBuilder.detalle("El defensor no atrapa el proyectil.", tipo_arma + " Como sacó " + WebBuilder.formateo_tirada("TRUCOS DE MANOS", defensor[TRUCOS_DE_MANOS], defensor[TIRADA]) + ", no logra atrapar el proyectil."));

            defensor[TIRADA] = defensor[TIRADA] - 50;

            devolución.actualizar(ataque_a_distancia(atacante, defensor, parada, localizacion, distancia, desarmado, inutilizar));

            return devolución;
        }
    }

    public static Detalle alcanceFinal(int alcance, int fuerza, int calidad) {
        mostrar("En metodo: alcanceFinal");

        Detalle resultado = new Detalle();
        int alcance_final = alcance;

        switch (fuerza) {
            case 1:
            case 2:
                alcance_final = 0;
                break;
            case 3:
                alcance_final = alcance - 30;
                break;
            case 4:
                alcance_final = alcance - 10;
                break;
            case 7:
                alcance_final = alcance + 10;
                break;
            case 8:
                alcance_final = alcance + 20;
                break;
            case 9:
                alcance_final = alcance + 30;
                break;
            case 10:
                alcance_final = alcance + 50;
                break;
            case 11:
                if (calidad >= 20) {
                    alcance_final = alcance + 100;
                } else if (calidad >= 15) {
                    alcance_final = alcance + 100;
                } else if (calidad >= 10) {
                    alcance_final = alcance + 100;
                } else if (calidad >= 5) {
                    alcance_final = alcance + 100;
                } else {
                    alcance_final = alcance + 50;
                }
                break;
            case 12:
                if (calidad >= 20) {
                    alcance_final = alcance + 250;
                } else if (calidad >= 15) {
                    alcance_final = alcance + 250;
                } else if (calidad >= 10) {
                    alcance_final = alcance + 250;
                } else if (calidad >= 5) {
                    alcance_final = alcance + 100;
                } else {
                    alcance_final = alcance + 50;
                }
                break;
            case 13:
                if (calidad >= 20) {
                    alcance_final = alcance + 500;
                } else if (calidad >= 15) {
                    alcance_final = alcance + 500;
                } else if (calidad >= 10) {
                    alcance_final = alcance + 500;
                } else if (calidad >= 5) {
                    alcance_final = alcance + 100;
                } else {
                    alcance_final = alcance + 50;
                }
                break;
            case 14:
                if (calidad >= 20) {
                    alcance_final = alcance + 1000;
                } else if (calidad >= 15) {
                    alcance_final = alcance + 1000;
                } else if (calidad >= 10) {
                    alcance_final = alcance + 500;
                } else if (calidad >= 5) {
                    alcance_final = alcance + 100;
                } else {
                    alcance_final = alcance + 50;
                }
                break;
            case 15:
                if (calidad >= 20) {
                    alcance_final = alcance + 5000;
                } else if (calidad >= 15) {
                    alcance_final = alcance + 5000;
                } else if (calidad >= 10) {
                    alcance_final = alcance + 500;
                } else if (calidad >= 5) {
                    alcance_final = alcance + 100;
                } else {
                    alcance_final = alcance + 50;
                }
                break;
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
                if (calidad >= 20) {
                    alcance_final = alcance + 10000;
                } else if (calidad >= 15) {
                    alcance_final = alcance + 5000;
                } else if (calidad >= 10) {
                    alcance_final = alcance + 500;
                } else if (calidad >= 5) {
                    alcance_final = alcance + 100;
                } else {
                    alcance_final = alcance + 50;
                }
                break;
        }

        resultado.setResultado(alcance_final);

        resultado.setDetalle(WebBuilder.detalle("El alcance máximo del arma será de " + alcance_final + " metros.", "El alcance efectivo será de " + (alcance_final / 2) + " (la mitad del máximo)." + WebBuilder.lb() + "El alcance máximo se calcula dependiendo de un bono a la fuerza del usuario, y la calidad del arma." + WebBuilder.lb() + "El alcance normal del arma es de " + alcance + " metros."));

        return resultado;
    }

    public static int Dificultad_de_distancia(int distancia) {
        mostrar("En metodo: Dificultad_de_distancia");
        if (distancia > 1000) {
            return 280;
        } else {
            if (distancia <= 1) {
                return 40;
            } else if (distancia <= 10) {
                return 80;
            } else if (distancia <= 50) {
                return 120;
            } else if (distancia <= 150) {
                return 140;
            } else if (distancia <= 500) {
                return 180;
            } else if (distancia <= 1000) {
                return 240;
            }
        }
        return 0;

    }

    public static Resultado ataque_a_distancia(int[] atacante, int[] defensor, boolean parada, int localizacion, int distancia, boolean desarmado, boolean inutilizar) {
        mostrar("En metodo: ataque_a_distancia");
         /*
    En este método se considera que:

        El daño base ya tiene su modificador por proezas de fuerza calculado

        La localización es un spinner con el primer valor "sin ataque apuntado" y se pasa el valor "spinner.position -1"

        La tirada ya incluye todos los modificadores pertinentes

        Incluyendo los modificadores especiales dependiendo si usa o no escudo y si tiene maestría en defensa.
    */

        Resultado devolución = new Resultado();

        //Al contrario de lo que ocurre en los ataques físicos, no importa lo bien que se consiga alcanzar a un objetivo, la velocidad máxima que pueden obtener los proyectiles será siempre la misma. Por esta causa, hay un límite a la habilidad de ataque final que son capaces de desarrollar, ya sean lanzados o disparados. Dicha restricción queda establecida en un resultado final de 280, o lo que es lo mismo, una dificultad de Imposible (IMP). Los penalizadores por ataques específicos se aplican antes de calcular este límite, por lo que perfectamente podría darse el caso de que la habilidad final fuera de 280, pero el disparo sea apuntado a la cabeza. Cuando el personaje puede extender su Ki al arma, es capaz de superar esta limitación.
        Detalle alcance_final = alcanceFinal(atacante[ALCANCE], atacante[FUERZA], atacante[CALIDAD]);
        int alcance = alcance_final.getResultado();

        devolución.DetalleAppend(alcance_final.getDetalle());

        //Existe la posibilidad de que un proyectil ni siquiera alcance a su blanco, incluso si el Resultado del Asalto es favorable a su favor. Puede ser que la distancia a la que se encuentra sea demasiado grande y el ataque simplemente falle. Para descubrirlo, existe una Tabla de dificultades (Tabla 48), que indica si la habilidad de disparo del personaje le ha permitido alcanzar su objetivo o no. Si el resultado final supera la dificultad que en ella se requiere, se considera que ha logrado impactarlo. El Director de Juego puede utilizar estas cantidades como referencia para el impacto de blancos que no están en combate.

        if (atacante[ATAQUE] + atacante[TIRADA] < Dificultad_de_distancia(distancia)) {
            devolución.ID_ATACANTE(atacante[ID_PERSONAJE]);
            devolución.ID_DEFENSOR(defensor[ID_PERSONAJE]);

            devolución.DetalleAppend(WebBuilder.detalle("El ataque falla automáticamente", "El atacante no logra superar la dificultad mínima para alcanzar el objetivo (" + WebBuilder.resultado((atacante[ATAQUE] + atacante[TIRADA]) + " vs " + Dificultad_de_distancia(distancia)) + ")."));

            devolución.Descripción("El ataque falla (el atacante no supera la dificultad de la distancia)");

            return devolución;
        }

        if (distancia > alcance / 2 & distancia < alcance) {
            atacante[ATAQUE] = atacante[ATAQUE] / 2;
            atacante[TIRADA] = atacante[TIRADA] - 30;
            devolución.DetalleAppend(WebBuilder.detalle("Como la distancia es mayor al alcance efectivo del arma, se aplican penalizadores", "La habilidad de ataque se ve reducida a la mitad " + WebBuilder.habilidad(atacante[ATAQUE] + 30 * 2) + " -> " + WebBuilder.resultado(atacante[ATAQUE]) + " y la tirada aplica un penalizador de 30 puntos " + WebBuilder.tirada(atacante[TIRADA] + 30) + " -> " + WebBuilder.tirada(atacante[TIRADA])));
            devolución.DetalleAppend(WebBuilder.detalle("Como la distancia es mayor al alcance efectivo del arma, se aplican penalizadores", "La habilidad de ataque se ve reducida a la mitad " + WebBuilder.habilidad(atacante[ATAQUE] + 30 * 2) + " -> " + WebBuilder.resultado(atacante[ATAQUE]) + " y la tirada aplica un penalizador de 30 puntos " + WebBuilder.tirada(atacante[TIRADA] + 30) + " -> " + WebBuilder.tirada(atacante[TIRADA])));
        }

        if (distancia > alcance) {

            devolución.ID_ATACANTE(atacante[ID_PERSONAJE]);
            devolución.ID_DEFENSOR(defensor[ID_PERSONAJE]);

            devolución.DetalleAppend(WebBuilder.detalle("El ataque falla automáticamente", "Como la distancia es mayor al alcance total del arma, el ataque falla automáticamente. El alcance máximo del arma es de " + WebBuilder.resultado(alcance)));

            return devolución;

        }


        devolución.actualizar(ataque_básico(atacante, defensor, parada, localizacion, distancia, desarmado, inutilizar, 280));

        return devolución;

    }

    public static Resultado ataque_básico(int[] atacante, int[] defensor, boolean parada, int localizacion, int distancia, boolean desarmado, boolean inutilizar, int max_value) {
        mostrar("En metodo: ataque_básico");
    /*
    En este método se considera que:

        El daño base ya tiene su modificador por proezas de fuerza calculado

        La localización es un spinner con el primer valor "sin ataque apuntado" y se pasa el valor "spinner.position -1"

        La tirada ya incluye todos los modificadores pertinentes, incluyendo por apuntado
    */
        Resultado devolución = new Resultado();

        int resultado = 0;
        int daño = 0;
        int escudo = 0;

        devolución.ID_ATACANTE(atacante[ID_PERSONAJE]);
        devolución.ID_DEFENSOR(defensor[ID_PERSONAJE]);

        localizacion = Base.localizacion(localizacion);


        if (parada) {
            resultado = atacante[TIRADA] + atacante[ATAQUE] - defensor[TIRADA] - defensor[PARADA];
        } else {
            resultado = atacante[TIRADA] + atacante[ATAQUE] - defensor[TIRADA] - defensor[ESQUIVA];
        }


        if (resultado > max_value & max_value != -1) {
            resultado = max_value;
            devolución.DetalleAppend(WebBuilder.detalle("El ataque es reducido a " + WebBuilder.resultado(max_value), "Al contrario de lo que ocurre en los ataques físicos, no importa lo bien que se consiga alcanzar a un objetivo, la velocidad máxima que pueden obtener los proyectiles será siempre la misma. Por esta causa, hay un límite a la habilidad de ataque final que son capaces de desarrollar, ya sean lanzados o disparados. Dicha restricción queda establecida en un resultado final de Imposible (IMP)."));

            if (parada) {
                resultado = 280 - defensor[TIRADA] - defensor[PARADA];
            } else {
                resultado = 280 - defensor[TIRADA] - defensor[ESQUIVA];
            }

        }

        String defensa_text = WebBuilder.formateo_tirada("ESQUIVA", defensor[ESQUIVA], defensor[TIRADA]);
        if (parada) {
            defensa_text = WebBuilder.formateo_tirada("PARADA", defensor[PARADA], defensor[TIRADA]);
        }
        devolución.DetalleAppend(WebBuilder.detalle("Resultados de las tiradas", "Atacante: " + WebBuilder.formateo_tirada("ATAQUE", atacante[ATAQUE], atacante[TIRADA]) + WebBuilder.lb() + "Defensor: " + defensa_text + WebBuilder.lb() + "Resultado: " + resultado));

        if (resultado > 0) {//El ataque impacta

            resultado = resultado - defensor[ARMADURA];

            devolución.DetalleAppend(WebBuilder.detalle("Se resta la armadura del defensor (TA " + (defensor[ARMADURA] / 10) + ").", "Al resultado del ataque se le debe restar la tabla de armaduras que posea el defensor. Esto se hace restando el número de la Tabla correspondiente (dependerá del arma utilizada) multiplicada por diez al resultado: " + WebBuilder.resultado((resultado + defensor[ARMADURA]) + "-" + defensor[ARMADURA] + "=" + WebBuilder.resultado(resultado))));

            if (atacante[DAÑO_BASE] > defensor[BARRERA_DE_DAÑO]) {

                if (resultado > 10) { // Si el valor es positivo, el defensor sufre un daño porcentual equivalente a dicha cifra redondeada a la baja en grupos de 10, o lo que es lo mismo; cada 10 puntos que el atacante tenga a su favor, provoca a su vez un 10% de daño. Es decir, si el resultado fuera 27 sufriría un 20% de daño, mientras que si fuera 185, sufriría un 180%. Este porcentaje se aplica al daño f inal del ataque, y el resultado se resta directamente del total de PV que tenga el objetivo

                    Detalle Detalle_daño = calcular_daño(atacante[DAÑO_BASE], defensor[RED_DAÑO_PORC], defensor[RED_DAÑO_PLENO], resultado, defensor[ESCUDO]);

                    daño = Detalle_daño.getResultado();

                    escudo = calcular_escudo(atacante[DAÑO_BASE], defensor[RED_DAÑO_PORC], defensor[RED_DAÑO_PLENO], resultado, defensor[ESCUDO]);

                    devolución.DetalleAppend(Detalle_daño.getDetalle());

                    mostrar("Daños: " + daño + " Escudo: " + escudo + " Resultado:" + resultado + " Daño Base: " + atacante[DAÑO_BASE] + " Res * DB = " + ((atacante[DAÑO_BASE] * resultado) / 100));

                    Detalle rotura_entereza = rotura_arma(atacante[ROTURA], defensor[ENTEREZA], atacante[FUERZA]);

                    if (escudo == 0) {
                        devolución.Descripción("El ataque impacta en " + Base.localización(localizacion) + ", produciendo " + daño + " puntos de daño." + rotura_entereza.getDetalle());
                    } else {
                        devolución.Descripción("El ataque impacta en " + Base.localización(localizacion) + ", produciendo " + daño + " puntos de daño (el escudo queda con " + escudo + " puntos)." + rotura_entereza.getDetalle());
                    }

                    devolución.DetalleAppend(rotura_entereza.getDetalle());

                    if (desarmado && parada) {

                        Detalle Detalle_rotura = dañoRotura(atacante[ROTURA], defensor[ENTEREZA], atacante[FUERZA]);

                        daño = daño + Detalle_rotura.getResultado();

                        devolución.DetalleAppend(Detalle_rotura.getDetalle());
                    }

                    devolución.DAÑOS_PV(daño);
                    devolución.ESCUDO_RESULTADO(escudo);
                    //CRITICO -> el daño debe superar el 50% de la vida actual, o el 10% si es un punto debil


                    Resultado critico = crítico_calculos(daño, defensor[RESISTENCIA_FISICA], defensor[VIDA_ACTUAL], localizacion, inutilizar);

                    devolución.DescripciónAppend("\n" + critico.Descripción());
                    devolución.DetalleAppend(critico.getDetalle());


                    Detalle Detalle_penalizadores = calcular_pen(critico.NIVEL_CRITICO, defensor[RED_PEN_PORC], defensor[RED_PEN_PLENO], defensor[RESISTIR_EL_DOLOR], true);

                    devolución.PENALIZADORES(Detalle_penalizadores.getResultado());

                    devolución.DetalleAppend(Detalle_penalizadores.getDetalle());

                    if (critico.NIVEL_CRITICO() > 0 && critico.NIVEL_CRITICO() != devolución.PENALIZADORES()) {
                        devolución.DescripciónAppend(", que quedan reducidos a " + devolución.PENALIZADORES() + " puntos. ");
                    }

                    devolución.NIVEL_CRITICO(critico.NIVEL_CRITICO);

                } else {//Si tras restar la Absorción el valor final es inferior a 10, el defensor no sufre daños. Simboliza que el atacante, pese haber superado su guardia y haberle golpeado, no consigue provocar una verdadera herida; ha cortado únicamente ropa o su arma ha rebotado contra la armadura del defensor.

                    devolución.DetalleAppend(WebBuilder.detalle_sin("No produce daños", "A pesar de haber logrado impactar, el golpe no es lo suficientemente fuerte como para producir daños. Este simplemente ha cortado su ropa o rebotado en algún punto de la armadura."));

                }

            } else {
                devolución.DetalleAppend(WebBuilder.detalle_sin("No produce daños", "Gracias a la barrera de daño que posee el defensor, no se producen daños (barrera de " + WebBuilder.resultado(defensor[BARRERA_DE_DAÑO]) + ")."));
            }


        } else {//El ataque falla y es posible realizar una contra
            if (distancia <= 1) {

                devolución.actualizar(contraataque(resultado));

            } else {
                devolución.DetalleAppend(WebBuilder.detalle_sin("No produce daños (contraataque fuera de rango)", "El defensor logra defenderse de forma muy satisfactoria del ataque, pero al estar a distancia no puede realizar un contraataque. Queda a discreción del director permitir usar este bono de " + WebBuilder.resultado(contraataque(resultado).BONO_CONTRAATAQUE) + " puntos para otra acción."));
                devolución.Descripción("El atacante no produce daños en el objetivo");

            }

        }

        return devolución;
    }

    private static final int Divisor_crítico(int localizacion) {

        if (localizacion == EL_CORAZÓN | localizacion == LA_CABEZA | localizacion == EL_CUELLO | localizacion == EL_OJO) {
            return 10;
        }

        if (localizacion == EL_ESTOMAGO) {
            return 5;
        }

        return 2;
    }

    private static final Detalle nivelCritico(int daño, int resFis) {

        Detalle resultado = new Detalle();
        int resFis_total = 0;
        int tirada_crit = Base.tirada(0, 0, 999);
        int nivel_De_Critico = daño + tirada_crit;

        int tirada_res_fis = Base.tirada();
        resFis_total = resFis + tirada_res_fis;


        mostrar("Critico: " + nivel_De_Critico + " vs " + resFis_total);

        if (nivel_De_Critico > 200) {
            nivel_De_Critico = 200 + ((nivel_De_Critico - 200) / 2);
        }


        resultado.setResultado(Base.redondeo5(nivel_De_Critico - resFis_total));

        resultado.append("Con un resultado en " + WebBuilder.formateo_tirada("CRITICO", daño, tirada_crit) + " <small>(Si la cifra es mayor de 200, cualquier exceso por encima de dicha cantidad debe de reducirse a la mitad)</small> y en " + WebBuilder.formateo_tirada("RESISTENCIA FÍSICA", resFis, tirada_res_fis) + " el nivel de crítico queda en " + WebBuilder.resultado(resultado.getResultado()) + WebBuilder.página(96, "CE"));


        return resultado;
    }

    private static final int claseCritico(int nivelCritico) {

        if (nivelCritico > 150) {
            return 3;
        }
        if (nivelCritico > 100) {
            return 2;
        }
        if (nivelCritico > 50) {
            return 1;
        }
        if (nivelCritico > 0) {
            return 0;
        }
        if (nivelCritico <= 0) {
            return -1;
        }
        return 0;

    }

    private static final Resultado crítico_calculos(int daño, int resFis, int vida, int localizacion, boolean inutilizar) {
        mostrar("En metodo: crítico_calculos");
        Resultado resultado = new Resultado();

        int nivel_De_Critico;
        int divisor_crítico = Divisor_crítico(localizacion);
        mostrar("Divisor: " + divisor_crítico + " Daño: " + daño + " Res fis: " + resFis + " vida: " + vida + "inutilizar? " + inutilizar + " Localizacion: " + localizacion);

        if (inutilizar) {
            divisor_crítico = 10;
        }

        mostrar(daño + " > " + vida + " / " + divisor_crítico + " = " + (vida / divisor_crítico));

        if (daño > vida / divisor_crítico) {
            resultado.Descripción("Produce un daño crítico");

            Detalle nivel_De_Critico_detalle = nivelCritico(daño, resFis);

            nivel_De_Critico = nivel_De_Critico_detalle.getResultado();

            resultado.DetalleAppend(nivel_De_Critico_detalle.getDetalle());

            mostrar("Nivel de crit: " + nivel_De_Critico);
            mostrar("Diferencia: " + nivel_De_Critico + " (" + resFis + " - " + daño + ") ");

            switch (claseCritico(nivel_De_Critico)) {
                case 0: //Se recibe un negativo a toda acción equivalente al Nivel del Crítico. El penalizador se recupera a un ritmo de 5 puntos por asalto.
                    //resultado.DescripciónAppend(", pero queda anulado gracias a la RF.");
                    resultado.DescripciónAppend(", que es un doloroso impacto");
                    resultado.DetalleAppend(WebBuilder.lb() + "Produce penalizadores de " + WebBuilder.resultado(nivel_De_Critico) + " puntos");
                    break;
                case 1: //Se recibe un negativo a toda acción equivalente al Nivel del Crítico. El penalizador se recupera a un ritmo de 5 puntos por asalto hasta la mitad de su valor. Hay que localizar el punto donde se sufre el impacto.
                    resultado.DescripciónAppend(", que es un doloroso impacto");
                    resultado.DetalleAppend(WebBuilder.lb() + "Produce penalizadores de " + WebBuilder.resultado(nivel_De_Critico) + " puntos");

                    resultado.NIVEL_CRITICO(nivel_De_Critico);
                    break;
                case 2: //Se recibe un negativo a toda acción equivalente al Nivel del Crítico. El penalizador se recupera a un ritmo de 5 puntos por asalto hasta la mitad de su valor. Si la localización indica un miembro, este queda destrozado o amputado de manera irrecuperable. Si alcanza la cabeza o el corazón, el personaje muere.
                    switch (localizacion) {
                        case EL_CORAZÓN:
                        case LA_CABEZA:
                        case EL_CUELLO:
                            resultado.DescripciónAppend(", que destruye el lugar impactado y mata al personaje.");
                            resultado.DetalleAppend(WebBuilder.lb() + "(Produciría penalizadores de " + WebBuilder.resultado(nivel_De_Critico) + " puntos)");
                            break;
                        case EL_ANTEBRAZO_INFERIOR_DERECHO:
                        case EL_ANTEBRAZO_INFERIOR_IZQUIERDO:
                        case EL_ANTEBRAZO_SUPERIOR_DERECHO:
                        case EL_ANTEBRAZO_SUPERIOR_IZQUIERDO:
                        case LA_MANO_DERECHA:
                        case LA_MANO_IZQUIERDA:
                        case LA_PANTORILLA_DERECHA:
                        case LA_PANTORILLA_IZQUIERDA:
                        case EL_PIE_DERECHO:
                        case EL_PIE_IZQUIERDO:
                            resultado.DescripciónAppend(" " + Base.localización(localizacion) + " queda amputado.");
                            resultado.DetalleAppend(WebBuilder.lb() + "Produce penalizadores de " + WebBuilder.resultado(nivel_De_Critico) + " puntos");
                            break;
                        default:
                            resultado.DescripciónAppend(", que destruye el lugar impactado");
                            resultado.DetalleAppend(WebBuilder.lb() + "Produce penalizadores de " + WebBuilder.resultado(nivel_De_Critico) + " puntos");
                            break;
                    }

                    resultado.NIVEL_CRITICO(nivel_De_Critico);
                    break;
                case 3: //Igual que el anterior, sólo que el personaje queda además inconsciente automáticamente, y muere en un número de minutos equivalente a su Constitución si no recibe atención médica.
                    switch (localizacion) {
                        case EL_CORAZÓN:
                        case LA_CABEZA:
                        case EL_CUELLO:
                            resultado.DescripciónAppend(", que destruye el lugar impactado y mata al personaje.");
                            resultado.DetalleAppend(WebBuilder.lb() + "(Produciría penalizadores de " + WebBuilder.resultado(nivel_De_Critico) + " puntos)");
                            break;
                        case EL_ANTEBRAZO_INFERIOR_DERECHO:
                        case EL_ANTEBRAZO_INFERIOR_IZQUIERDO:
                        case EL_ANTEBRAZO_SUPERIOR_DERECHO:
                        case EL_ANTEBRAZO_SUPERIOR_IZQUIERDO:
                        case LA_MANO_DERECHA:
                        case LA_MANO_IZQUIERDA:
                        case LA_PANTORILLA_DERECHA:
                        case LA_PANTORILLA_IZQUIERDA:
                        case EL_PIE_DERECHO:
                        case EL_PIE_IZQUIERDO:
                            resultado.DescripciónAppend(" " + Base.localización(localizacion) + " queda amputado y el personaje cae inconsciente.");
                            resultado.DetalleAppend(WebBuilder.lb() + "Produce penalizadores de " + WebBuilder.resultado(nivel_De_Critico));
                            break;
                        default:
                            resultado.DescripciónAppend(", que destruye el lugar impactado y deja inconsciente al personaje.");
                            resultado.DetalleAppend(WebBuilder.lb() + "Produce penalizadores de " + WebBuilder.resultado(nivel_De_Critico));
                            break;
                    }

                    resultado.NIVEL_CRITICO(nivel_De_Critico);
                    break;
                default:
                    resultado.Descripción("Produce un crítico, pero este queda anulado.");

                    break;
            }
        }

        if (!resultado.Descripción().isEmpty()) {
            resultado.setDetalle(WebBuilder.detalle(resultado.Descripción(), resultado.getDetalle()));
        }

        return resultado;

    }

    private static Detalle dañoRotura(int rotura, int entereza, int fuerza) {
        Detalle result = new Detalle();


        int dev = 0;
        int bono = 0;
        switch (fuerza) {
            case 8:
            case 9:
                bono = 1;
                break;
            case 10:
                bono = 2;
                break;
            case 11:
            case 12:
                bono = 3;
                break;
            case 13:
            case 14:
                bono = 4;
                break;
            case 15:
                bono = 5;
                break;
        }
        if (fuerza > 15) {
            bono = 5;
        }

        rotura = rotura + bono;

        int diferencia = rotura + tiradaD10() - entereza + tiradaD10();

        if (diferencia > 0) {

            result.setResultado(diferencia * 5);

        }

        result.setDetalle(WebBuilder.detalle("Recibe " + result.getResultado() + " puntos de daño adicionales", "Este daño gue causado por intentar defenderse sin armas." + WebBuilder.página(92, "CE")));

        return result;
    }

    private static Detalle rotura_arma(int rotura, int entereza, int fuerza) {
        Detalle result = new Detalle();


        String título = "";
        String texto = "";

        int bono = 0;
        switch (fuerza) {
            case 8:
            case 9:
                bono = 1;
                break;
            case 10:
                bono = 2;
                break;
            case 11:
            case 12:
                bono = 3;
                break;
            case 13:
            case 14:
                bono = 4;
                break;
            case 15:
                bono = 5;
                break;
        }
        if (fuerza > 15) {
            bono = 5;
        }

        texto = ("Se determina si el arma del defensor se rompe comparando la rotura del arma atacante (que tendrá un bono debido a su fuerza) (" + rotura + "+" + bono + "=" + WebBuilder.resultado(rotura + bono) + ") y la entereza del arma defensora (" + WebBuilder.resultado(entereza) + ")." + WebBuilder.página(91, "CE"));


        rotura = rotura + bono;

        int tirada_rotura = tiradaD10();
        int tirada_entereza = tiradaD10();

        texto = texto + WebBuilder.lb() + WebBuilder.formateo_tirada("ROTURA", rotura, tirada_rotura) + " contra " + WebBuilder.formateo_tirada("ENTEREZA", entereza, tirada_entereza);

        if (rotura + tirada_rotura - entereza - tirada_entereza > 0) {

            texto = texto + ", como la rotura es mayor, el arma del defensor se rompe (o baja su calidad)" + WebBuilder.página(73, "CE");
            título = "El arma del defensor se rompe (o baja su calidad)";
        } else {
            texto = texto + ", como la entereza es mayor, el arma del defensor queda intacta" + WebBuilder.página(73, "CE");
            título = "El arma del defensor queda intacta";
        }

        result.setDetalle(WebBuilder.detalle(título, texto));

        return result;
    }

    private static int tiradaD10() {

        int res = (int) (Math.random() * 10) + 1;
        if (res > 10) {
            res = 10;
        }
        return res;
    }

    private static int calcular_escudo(int dañoBase, int redPORC, int redPLENO, int resultado, int escudo) {
        int daño;

        if (escudo > 0) {

            daño = (dañoBase * resultado) / 100;

            daño = base(daño - redPLENO, 0);

            daño = porcentaje(daño, redPORC);

            if (escudo < daño) {
                return 0;
            }

            return (escudo - daño);

        } else {
            return 0;
        }


    }

    private static Detalle calcular_daño(int dañoBase, int redPORC, int redPLENO, int resultado, int escudo) {
        int daño;
        Detalle result = new Detalle();

        daño = (dañoBase * resultado) / 100;

        result.append("Se calcula el daño como  (Daño Base * Resultado) / 100 (" + dañoBase + "*" + resultado + "/100 = " + WebBuilder.resultado(daño) + ").");

        if (redPLENO > 0 | redPORC > 0) {
            result.append(WebBuilder.lb() + "El defensor posee reducciónes a los daños de -" + redPLENO + " y " + redPORC + "% (se aplica primero la Plena y luego la porcentual).");
        }

        daño = base(daño - redPLENO, 0);

        daño = porcentaje(daño, redPORC);

        if (escudo > 0) {
            result.append(WebBuilder.lb() + "EL defensor posee " + escudo + " puntos de escudo, así que primero se descuenta a el, quedando ");
            if (escudo > daño) {
                result.append(escudo + "-" + daño + "=" + WebBuilder.resultado(escudo - daño) + ", por lo que el escudo todavía seguirá activo");
                daño = 0;
            } else {
                daño = daño - escudo;
                result.append(escudo + "-" + daño + "=" + WebBuilder.resultado(escudo - daño) + ", por lo que el escudo queda destruido y se aplican " + (daño - escudo) + " puntos de daño. (" + daño + "-" + escudo + "=" + WebBuilder.resultado(daño - escudo) + ").");
            }
        }
        String título = "Produce un daño de " + WebBuilder.resultado(daño) + " puntos";
        if (escudo > 0) {
            if (daño - escudo >= 0) {
                título = título + " y el escudo queda " + WebBuilder.resultado("destruido") + ".";
            } else {
                título = título + " y el escudo queda en " + WebBuilder.resultado(escudo - daño) + " puntos.";
            }
        }

        result.setDetalle(WebBuilder.detalle(título, result.getDetalle()));
        result.setResultado(daño);
        return result;
    }

    private static Detalle calcular_pen(int penBase, int redPORC, int redPLENO, int resistirAlDolor, boolean resiste) {

        Detalle result = new Detalle();

        int penalizador;

        penBase = base(penBase - redPLENO, 0);

        penalizador = porcentaje(penBase, redPORC);

        String detalle = "";


        if (resiste & penalizador > 0) {

            if (redPLENO > 0 | redPORC > 0) {
                detalle = "El defensor posee reducciónes a los penalizadores de -" + redPLENO + " y " + redPORC + "% (se aplica primero la Plena: " + (penBase + redPLENO) + "-" + redPLENO + "=" + WebBuilder.resultado(penBase) + " y luego la porcentual: " + penBase + "-" + (penBase - porcentaje(penBase, redPORC)) + "=" + WebBuilder.resultado(penalizador) + ")." + WebBuilder.lb();
            }

            Detalle resistir_dolor = resitir_el_Dolor(resistirAlDolor);

            penalizador = penalizador - resistir_dolor.getResultado();

            detalle = detalle + resistir_dolor.getDetalle();
        }


        if (penalizador < 0) {
            penalizador = 0;
        }

        if (!detalle.isEmpty()) {
            result.setDetalle(WebBuilder.detalle("Quedando con un penalizador de " + WebBuilder.resultado(penalizador) + ".", detalle));
        }

        return result;
    }

    private static Resultado contraataque(int resultado) {

        final Resultado devolución = new Resultado();

        devolución.setDetalle(" El defensor podría contraatacar con un bonificador de " + (-resultado) + " (limitado a 150, Pg 87 CE) ");

        resultado = tope(-resultado, 150);

        devolución.Descripción("El defensor puede contraatacar con un bonificador de: " + resultado + "(Pg 87 CE).");

        devolución.BONO_CONTRAATAQUE(resultado);

        devolución.setDetalle(WebBuilder.detalle("El defensor puede contraatacar", " El defensor podría contraatacar con un bonificador de " + (resultado) + " (limitado a 150)." + WebBuilder.página(87, "CE")));

        return devolución;

    }

    private static int porcentaje(int base, int porcentaje) {

        int reduccion = ((base * porcentaje) / 100);
        return base - reduccion;

    }

    private static int tope(int resultado, int tope) {
        if (resultado > tope) {
            return tope;
        } else {
            return resultado;
        }
    }

    private static int base(int resultado, int base) {
        if (resultado < base) {
            return base;
        } else {
            return resultado;
        }
    }

    private static final void mostrar(String texto) {
        if (MODO_DE_PRUEBAS) {
            System.out.println(texto);
        }
    }

    private static final Detalle resitir_el_Dolor(int habilidad) {

        Detalle resultado = new Detalle();

        mostrar("En metodo: resitir_el_Dolor");
        int tirada = Base.tirada();
        resultado.append("Realiza una tirada de " + WebBuilder.formateo_tirada("RESISTIR EL DOLOR", habilidad, tirada));

        tirada = habilidad + tirada;

        int dificultadSuperada = (Base.ControlHabilidad(tirada));
        mostrar("Habilidad: " + habilidad + " Dif Sup: " + dificultadSuperada + " Tirada: " + tirada);
        switch (dificultadSuperada) {
            case 0:
            case 1:
            case 2:
                resultado.setResultado(0);
                break;
            case 3:
                resultado.setResultado(10);
                break;
            case 4:
                resultado.setResultado(20);
                break;
            case 5:
                resultado.setResultado(30);
                break;
            case 6:
                resultado.setResultado(40);
                break;
            case 7:
                resultado.setResultado(50);
                break;
            case 8:
                resultado.setResultado(60);
                break;
            case 9:
                resultado.setResultado(70);
                break;
            case 10:
                resultado.setResultado(80);
                break;
            default:
                resultado.setResultado(0);
                break;
        }
        resultado.append(", superando la dificultad " + Base.DificultadStr(tirada) + "(" + WebBuilder.resultado(resultado.getResultado()) + ")" + WebBuilder.página(53, "CE"));

        resultado.setDetalle(resultado.getDetalle());

        return resultado;

    }

    public static class Mostrar {

        private final boolean EN_PRUEBAS = true;
        private final String clase;


        public Mostrar(String clase) {
            this.clase = clase;
        }

        public void p(String text, String metodo) {
            if (EN_PRUEBAS) {
                System.out.println("Clase: " + clase + ".- En " + metodo + ": " + text + "\n");
            }
        }

    }

}
