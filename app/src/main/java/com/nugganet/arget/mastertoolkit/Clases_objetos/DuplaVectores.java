package com.nugganet.arget.mastertoolkit.Clases_objetos;

/**
 * Created by Arget on 24/4/2017.
 */

public class DuplaVectores {

    private Integer[] numeros;
    private String[]  nombres;

    public DuplaVectores(Integer[] numeros,String[]  nombres){
        this.nombres = nombres;
        this.numeros = numeros;
    }



    public void ordenar(){
        quickSortInDescendingOrder(numeros,0,numeros.length-1);
    }

    private void quickSortInDescendingOrder (Integer[] numbers, int low, int high)
    {

        int i=low;
        int j=high;
        int temp;
        String tempS;
        int middle=numbers[(low+high)/2];

        while (i<j)
        {
            while (numbers[i]>middle)
            {
                i++;
            }
            while (numbers[j]<middle)
            {
                j--;
            }
            if (j>=i)
            {
                temp=numbers[i];
                tempS = nombres[i];

                numbers[i]=numbers[j];
                nombres[i]=nombres[j];

                numbers[j]=temp;
                nombres[j]=tempS;

                i++;
                j--;
            }
        }


        if (low<j)
        {
            quickSortInDescendingOrder(numbers, low, j);
        }
        if (i<high)
        {
            quickSortInDescendingOrder(numbers, i, high);
        }
    }

    public Integer[] getNumeros() {
        return numeros;
    }

    public void setNumeros(Integer[] numeros) {
        this.numeros = numeros;
    }

    public String[] getNombres() {
        return nombres;
    }

    public void setNombres(String[] nombres) {
        this.nombres = nombres;
    }
}
