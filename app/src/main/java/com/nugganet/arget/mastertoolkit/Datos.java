package com.nugganet.arget.mastertoolkit;

import com.nugganet.arget.mastertoolkit.Clases_objetos.Modificador;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Personaje;

import java.util.ArrayList;

/**
 * Created by Arget on 22/3/2018.
 */

public class Datos {

    private ArrayList<Personaje> array_Personajes_En_Mision = new ArrayList<>();
    private ArrayList<Personaje> arrayPersonajes = new ArrayList<>();
    ArrayList<Modificador> arrayModificadores = new ArrayList<>();

    private byte[] image_temp = new byte[5];


    private boolean modificado_pjs_en_mision = false;
    private boolean modificado_pjs = false;
    private boolean modificado_modificadores = false;


    public ArrayList<Modificador> ArrayModificadores() {
        return arrayModificadores;
    }

    public void ArrayModificadores(ArrayList<Modificador> arrayModificadores) {
        this.arrayModificadores = arrayModificadores;
    }

    public void AñadirModificador(Modificador modif) {
        this.arrayModificadores.add(modif);
        modificado_modificadores = true;
    }

    public void ElminarModificador(int index) {
        this.arrayModificadores.remove(index);
        modificado_modificadores = true;
    }

    public boolean isModificado_modificadores() {

        return modificado_modificadores;

    }

    public void setModificado_modificadores(boolean modificado_modificadores) {
        this.modificado_modificadores = modificado_modificadores;
        p.setParaGuardar(true);
    }

    public void modif_comb_pj_en_mision(int index_pj, int index_mod, boolean estado) {

        array_Personajes_En_Mision.get(index_pj).modif_combate(index_mod, estado);

    }

    public boolean isModificado_pjs_en_mision() {
        return modificado_pjs_en_mision;
    }

    public void setModificado_pjs_en_mision(boolean modificado_pjs_en_mision) {
        this.modificado_pjs_en_mision = modificado_pjs_en_mision;
        p.setParaGuardar(true);
    }

    public boolean isModificado_pjs() {
        return modificado_pjs;
    }

    public void setModificado_pjs(boolean modificado_pjs) {
        this.modificado_pjs = modificado_pjs;
        p.setParaGuardar(true);
    }

    public Personaje Busqueda_Personaje(int id) {

        for (Personaje pj : arrayPersonajes) {
            if (pj.getID() == id) {
                return pj;
            }
        }

        for (Personaje pj : array_Personajes_En_Mision) {
            if (pj.getID() == id) {
                return pj;
            }
        }

        return null;
    }

    public void reset_tamaño_modificadores() {
        for (Personaje pj : array_Personajes_En_Mision) {
            pj.setModificadores(new boolean[arrayModificadores.size() + 5]);
        }
    }

    public void Añadir_Personaje_en_misión(Personaje pj) {

        pj.setModificadores(new boolean[arrayModificadores.size() + 5]);

        Personaje temp = new Personaje().fromJson(pj.toJson());

        temp.setToken(pj.getToken());

        array_Personajes_En_Mision.add(temp);

        modificado_pjs_en_mision = true;

    }

    public void Añadir_Personaje(Personaje pj) {
        arrayPersonajes.add(pj);
        modificado_pjs = true;
    }

    public void Eliminar_Personaje(int index) {
        arrayPersonajes.remove(index);
        modificado_pjs = true;
    }

    public void Eliminar_Personaje_en_misión(int index) {
        array_Personajes_En_Mision.remove(index);
        modificado_pjs_en_mision = true;
    }


    public ArrayList<Personaje> ArrayPersonajes() {
        return arrayPersonajes;
    }

    public void ArrayPersonajes(ArrayList<Personaje> arrayPersonajes) {
        this.arrayPersonajes = arrayPersonajes;
    }

    public ArrayList<Personaje> Array_Personajes_En_Mision() {
        return array_Personajes_En_Mision;
    }

    public void Array_Personajes_En_Mision(ArrayList<Personaje> array_Personajes_En_Mision) {
        this.array_Personajes_En_Mision = array_Personajes_En_Mision;
    }

    public byte[] getImage_temp() {
        return image_temp;
    }

    public void setImage_temp(byte[] image_temp) {
        this.image_temp = image_temp;
    }

    public void Array_Personajes_En_Mision_add(Personaje Personaje_En_Mision) {
        this.array_Personajes_En_Mision.add(Personaje_En_Mision);
        modificado_pjs_en_mision = true;
    }


    private Principal p;

    private int[] bases = new int[100];
    private int[] bases_armas = new int[25];
    private int[] bases_armaduras = new int[25];
    private int[] temporales = new int[100];
    private int[] bases_Efectos = new int[100];
    private int[] bases_penalizadores = new int[10];

    private int[] armas_temporal = new int[160];
    private int[] armaduras_temporal = new int[160];
    private String[] armas_nombre_temporal = {"Vacio", "Vacio", "Vacio", "Vacio", "Vacio", "Vacio"};
    private String[] armaduras_nombre_temporal = {"Vacio", "Vacio", "Vacio", "Vacio", "Vacio", "Vacio"};

    public final static int PERSONAJE = 0;
    public final static int ARMAS = 1;
    public final static int ARMADURAS = 2;
    public final static int TEMPORALES = 3;
    public final static int EFECTOS = 4;
    public final static int PENALIZADORES = 5;
    public final static int ARMAS_TEMP = 6;
    public final static int ARMADURAS_TEMP = 7;

    public String[] obtener_nombres(int tipo) {
        switch (tipo) {
            case ARMAS:
                return armas_nombre_temporal;
            case ARMADURAS:
                return armaduras_nombre_temporal;
        }
        return new String[]{""};
    }

    public int[] obtener(int tipo) {

        switch (tipo) {
            case PERSONAJE:
                return bases;
            case ARMAS:
                return bases_armas;
            case ARMADURAS:
                return bases_armaduras;
            case TEMPORALES:
                return temporales;
            case PENALIZADORES:
                return bases_penalizadores;
            case EFECTOS:
                return bases_Efectos;
            case ARMAS_TEMP:
                return armas_temporal;
            case ARMADURAS_TEMP:
                return armaduras_temporal;
        }
        return new int[]{0};
    }

    public void editar(int[] nuevos, int tipo) {

        switch (tipo) {
            case PERSONAJE:
                bases = nuevos;
                break;
            case ARMAS:
                bases_armas = nuevos;
                break;
            case ARMADURAS:
                bases_armaduras = nuevos;
                break;
            case TEMPORALES:
                temporales = nuevos;
                break;
            case PENALIZADORES:
                bases_penalizadores = nuevos;
                break;
            case EFECTOS:
                bases_Efectos = nuevos;
                break;
            case ARMAS_TEMP:
                armas_temporal = nuevos;
                break;
            case ARMADURAS_TEMP:
                armaduras_temporal = nuevos;
                break;
        }

    }

    public void editar(int nuevos, int tipo, int index) {

        switch (tipo) {
            case PERSONAJE:
                bases[index] = nuevos;
                break;
            case ARMAS:
                bases_armas[index] = nuevos;
                break;
            case ARMADURAS:
                bases_armaduras[index] = nuevos;
                break;
            case TEMPORALES:
                temporales[index] = nuevos;
                break;
            case PENALIZADORES:
                bases_penalizadores[index] = nuevos;
                break;
            case EFECTOS:
                bases_Efectos[index] = nuevos;
                break;
            case ARMAS_TEMP:
                armas_temporal[index] = nuevos;
                break;
            case ARMADURAS_TEMP:
                armaduras_temporal[index] = nuevos;
                break;
        }

    }

    public void editarNombres(String nuevos, int tipo, int index) {

        switch (tipo) {
            case ARMAS:
                armas_nombre_temporal[index] = nuevos;
                break;
            case ARMADURAS:
                armaduras_nombre_temporal[index] = nuevos;
                break;
        }
    }

    public void editarNombres(String[] nuevos, int tipo) {

        switch (tipo) {
            case ARMAS:
                armas_nombre_temporal = nuevos;
                break;
            case ARMADURAS:
                armaduras_nombre_temporal = nuevos;
                break;
        }
    }

    public Datos(Principal p) {
        this.p = p;
    }

    public int[] getBases() {
        return bases;
    }

    public int getBases(int i) {
        return bases[i];
    }

    public void setBases(int[] bases) {
        this.bases = bases;
    }

    public void setBases(int bases, int i) {
        this.bases[i] = bases;
    }

    public int[] getBases_armas() {
        return bases_armas;
    }

    public int getBases_armas(int i) {
        return bases_armas[i];
    }

    public void setBases_armas(int[] bases_armas) {
        this.bases_armas = bases_armas;
    }

    public int[] getBases_armaduras() {
        return bases_armaduras;
    }

    public int getBases_armaduras(int i) {
        return bases_armaduras[i];
    }

    public void setBases_armaduras(int[] bases_armaduras) {
        this.bases_armaduras = bases_armaduras;
    }

    public int[] getTemporales() {
        return temporales;
    }

    public int getTemporales(int i) {
        return temporales[i];
    }

    public void setTemporales(int[] temporales) {
        this.temporales = temporales;
    }

    public void setTemporales(int bases_penalizadores, int i) {
        this.temporales[i] = bases_penalizadores;
    }

    public int[] getBases_Efectos() {
        return bases_Efectos;
    }

    public int getBases_Efectos(int i) {
        return bases_Efectos[i];
    }

    public void setBases_Efectos(int[] bases_Efectos) {
        this.bases_Efectos = bases_Efectos;
    }

    public void setBases_Efectos(int bases_penalizadores, int i) {
        this.bases_Efectos[i] = bases_penalizadores;
    }

    public int[] getBases_penalizadores() {
        return bases_penalizadores;
    }

    public int getBases_penalizadores(int i) {
        return bases_penalizadores[i];
    }

    public void setBases_penalizadores(int[] bases_penalizadores) {
        this.bases_penalizadores = bases_penalizadores;
    }

    public void setBases_penalizadores(int bases_penalizadores, int i) {
        this.bases_penalizadores[i] = bases_penalizadores;
    }

    public void setBases_armas(int bases_armas, int i) {
        this.bases_armas[i] = bases_armas;
    }

    public void setBases_armaduras(int bases_armadura, int i) {
        this.bases_armaduras[i] = bases_armadura;
    }

    public void reset_temporales() {
        editarNombres(new String[]{"Slot 1", "Slot 2", "Slot 3", "Slot 4", "Slot 5", "Slot 6"}, Datos.ARMADURAS);
        editarNombres(new String[]{"Slot 1", "Slot 2", "Slot 3", "Slot 4", "Slot 5", "Slot 6"}, Datos.ARMAS);

        editar(new int[160], Datos.ARMAS_TEMP);
        editar(new int[160], Datos.ARMADURAS_TEMP);
    }


}
