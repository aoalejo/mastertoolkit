package com.nugganet.arget.mastertoolkit.Controladores;

import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Modificador;
import com.nugganet.arget.mastertoolkit.Principal;
import com.nugganet.arget.mastertoolkit.R;

public class Controlador_creación_modificadores implements View.OnClickListener {

    final Principal p;
    private Modificador modificador_delete;
    private String[] nombres_modif = {"Modificador al Ataque", "Modificador a la Parada", "Modificador a la Esquiva", "Modificador al Turno"};

    public Controlador_creación_modificadores(Principal p) {
        this.p = p;



    }

    public void initListeners() {
        p.findViewById(R.id.button_Cargar_modif).setOnClickListener(this);
        p.findViewById(R.id.button_Eliminar_modif).setOnClickListener(this);
        p.findViewById(R.id.button_GuardarNuevo_modif).setOnClickListener(this);
        p.findViewById(R.id.button_Modificar_modif).setOnClickListener(this);
    }


    public void initModif() {

        for (int i = 1; i < nombres_modif.length + 1; i++) {

            String ViewID = "cr_pj_modif_" + i;

            int resID = p.getResources().getIdentifier(ViewID, "id", p.getPackageName());

            ((TextView) p.findViewById(resID).findViewById(R.id.cr_pj_c_nombre)).setText(nombres_modif[i - 1]);
            ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_temp)).setVisibility(View.GONE);
            ((EditText) p.findViewById(resID).findViewById(R.id.cr_pj_c_fijo)).setText("0");

        }
    }

    private void setModif(Modificador temp) {

        ((EditText) p.findViewById(R.id.cr_pj_modif_nombre)).setText(temp.getNombre());

        ((EditText) (p.findViewById(R.id.cr_pj_modif_1).findViewById(R.id.cr_pj_c_fijo))).setText(String.valueOf(temp.getCaracteristicas()[0]));
        ((EditText) (p.findViewById(R.id.cr_pj_modif_2).findViewById(R.id.cr_pj_c_fijo))).setText(String.valueOf(temp.getCaracteristicas()[1]));
        ((EditText) (p.findViewById(R.id.cr_pj_modif_3).findViewById(R.id.cr_pj_c_fijo))).setText(String.valueOf(temp.getCaracteristicas()[2]));
        ((EditText) (p.findViewById(R.id.cr_pj_modif_4).findViewById(R.id.cr_pj_c_fijo))).setText(String.valueOf(temp.getCaracteristicas()[3]));

    }

    private Modificador getModif() {
        Modificador temp = new Modificador();

        temp.setNombre(((EditText) p.findViewById(R.id.cr_pj_modif_nombre)).getText().toString());

        int[] caract = new int[4];

        caract[0] = TxtToInt(((EditText) (p.findViewById(R.id.cr_pj_modif_1).findViewById(R.id.cr_pj_c_fijo))).getText().toString());
        caract[1] = TxtToInt(((EditText) (p.findViewById(R.id.cr_pj_modif_2).findViewById(R.id.cr_pj_c_fijo))).getText().toString());
        caract[2] = TxtToInt(((EditText) (p.findViewById(R.id.cr_pj_modif_3).findViewById(R.id.cr_pj_c_fijo))).getText().toString());
        caract[3] = TxtToInt(((EditText) (p.findViewById(R.id.cr_pj_modif_4).findViewById(R.id.cr_pj_c_fijo))).getText().toString());

        temp.setCaracteristicas(caract);

        return temp;
    }

    private int TxtToInt(String text) {
        try {
            int temp = Integer.parseInt(text);
            return temp;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }


    @Override
    public void onClick(View v) {

        int seleccionado = ((Spinner) p.findViewById(R.id.spinner_seleccion_modif)).getSelectedItemPosition();
        Snackbar mensajeConfirmación;
        View view;
        TextView tv;

        switch (v.getId()) {
            case R.id.button_Cargar_modif:

                if (((Spinner) p.findViewById(R.id.spinner_seleccion_modif)).getSelectedItemPosition() != -1) {

                    Modificador temp = p.getDatos().ArrayModificadores().get(seleccionado);

                    setModif(temp);

                } else {
                    p.showSnack(Snackbar.make(v, "No se pudo cargar.", Snackbar.LENGTH_SHORT));
                }

                break;

            case R.id.button_GuardarNuevo_modif:

                Modificador nuevo_modif = getModif();

                nuevo_modif.setID(p.getDataBase().AñadirModificador(nuevo_modif));

                p.getDatos().ArrayModificadores().add(nuevo_modif);

                p.showSnack(Snackbar.make(v, "Modificador " + nuevo_modif.getNombre() + " guardado.", Snackbar.LENGTH_SHORT));

                break;

            case R.id.button_Modificar_modif:

                final int index_modif = seleccionado;

                if (p.getDatos().ArrayModificadores().size() != 0 & seleccionado != -1) {

                    if (seleccionado < p.getDatos().ArrayModificadores().size()) {

                        Modificador modif_modificada = getModif();

                        modificador_delete = new Modificador().fromJson(modif_modificada.toJson());

                        mensajeConfirmación = Snackbar
                                .make(v, "Modificador " + p.getDatos().ArrayModificadores().get(seleccionado).getNombre() + " modificado a " + modif_modificada.getNombre(), Snackbar.LENGTH_LONG)
                                .setAction("DESHACER", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        p.getDatos().ArrayModificadores().get(index_modif).reemplazar(modificador_delete);

                                        p.getDataBase().ModificarModif(modificador_delete);

                                        p.actualizarArrayModificadores();
                                        ((Spinner) p.findViewById(R.id.spinner_seleccion_modif)).setSelection(index_modif);

                                        p.showSnack(Snackbar.make(view, "Restaurado", Snackbar.LENGTH_SHORT));
                                    }

                                });

                        p.getDatos().ArrayModificadores().get(seleccionado).reemplazar(modif_modificada);

                        p.getDataBase().ModificarModif(p.getDatos().ArrayModificadores().get(seleccionado));

                        p.actualizarArrayModificadores();
                        ((Spinner) p.findViewById(R.id.spinner_seleccion_modif)).setSelection(seleccionado);

                        view = mensajeConfirmación.getView();

                        tv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                        tv.setTextColor(p.getResources().getColor(R.color.colorWhite));

                        mensajeConfirmación.setActionTextColor(p.getResources().getColor(R.color.colorWhite));

                        mensajeConfirmación.show();
                    }
                } else {
                    p.showSnack(Snackbar.make(v, "No hay ningún elemento que modificar", Snackbar.LENGTH_SHORT));
                }

                break;

            case R.id.button_Eliminar_modif:

                if (p.getDatos().ArrayModificadores().size() != 0 & seleccionado != -1) {

                    if (seleccionado < p.getDatos().ArrayModificadores().size()) {

                        mensajeConfirmación = Snackbar
                                .make(v, "Modificador " + p.getDatos().ArrayModificadores().get(seleccionado).getNombre() + " eliminado", Snackbar.LENGTH_LONG)
                                .setAction("DESHACER", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        p.getDatos().ArrayModificadores().add(modificador_delete);

                                        p.getDataBase().AñadirModificador(modificador_delete);

                                        p.actualizarArrayModificadores();
                                        p.showSnack(Snackbar.make(view, "Restaurado", Snackbar.LENGTH_SHORT))
                                        ;
                                    }

                                });

                        modificador_delete = p.getDatos().ArrayModificadores().get(seleccionado);

                        p.getDataBase().EliminarModif(p.getDatos().ArrayModificadores().get(seleccionado).getID());

                        p.getDatos().ArrayModificadores().remove(seleccionado);

                        p.actualizarArrayModificadores();

                        view = mensajeConfirmación.getView();

                        tv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                        tv.setTextColor(p.getResources().getColor(R.color.colorWhite));

                        mensajeConfirmación.setActionTextColor(p.getResources().getColor(R.color.colorWhite));

                        mensajeConfirmación.show();
                    }
                } else {
                    p.showSnack(Snackbar.make(v, "No hay ningún elemento que eliminar", Snackbar.LENGTH_SHORT));

                }

                break;
        }

        p.actualizarArrayModificadores();

        if (seleccionado < p.getDatos().ArrayModificadores().size()) {
            ((Spinner) p.findViewById(R.id.spinner_seleccion_modif)).setSelection(seleccionado);
        }
    }

}
