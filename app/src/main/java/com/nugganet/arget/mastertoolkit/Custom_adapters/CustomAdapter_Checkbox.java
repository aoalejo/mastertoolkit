package com.nugganet.arget.mastertoolkit.Custom_adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.nugganet.arget.mastertoolkit.Clases_objetos.Modificador;
import com.nugganet.arget.mastertoolkit.Principal;
import com.nugganet.arget.mastertoolkit.R;

import java.util.ArrayList;

public class CustomAdapter_Checkbox extends BaseAdapter {
    ArrayList<Modificador> modificadores;
    private int tipo;

    Context context;
    Principal main;
    private static LayoutInflater inflater = null;


    public CustomAdapter_Checkbox(Principal mainActivity, ArrayList<Modificador> modificadores) {

        this.modificadores = modificadores;
        this.tipo = tipo;
        context = mainActivity;
        main = mainActivity;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {

        return modificadores.size();
    }

    @Override
    public Object getItem(int position) {

        return position;
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    public class Holder {
        ToggleButton atacante;
        ToggleButton defensor;
        TextView nombre;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.list_modificadores_v2, null, true);

        holder.nombre = (TextView) rowView.findViewById(R.id.textView_nombre);
        holder.atacante = (ToggleButton) rowView.findViewById(R.id.Toggle_atacante);
        holder.defensor = (ToggleButton) rowView.findViewById(R.id.Toggle_defensor);

        holder.nombre.setText(modificadores.get(position).getNombre());

        try {
            holder.defensor.setChecked(main.getModificador_defensa(position));
            holder.atacante.setChecked(main.getModificadores_ataque(position));
        } catch (NullPointerException e) {
            System.out.println(e.toString() + "on position: " + position);
        }

        holder.atacante.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                main.setModificador_ataque(position, isChecked);

            }
        });

        holder.defensor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                main.setModificador_defensa(position, isChecked);

            }
        });

        return rowView;
    }


    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
}