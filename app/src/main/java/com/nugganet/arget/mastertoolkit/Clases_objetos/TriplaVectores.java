package com.nugganet.arget.mastertoolkit.Clases_objetos;

import java.util.Arrays;

/**
 * Created by Arget on 24/4/2017.
 */

public class TriplaVectores {

    private Integer[] numeros;
    private Integer[] tiradas;
    private String[] nombres;
    private String[] resultados;

    public TriplaVectores(Integer[] numeros, String[] resultados, String[] nombres) {
        this.nombres = nombres;
        this.resultados = resultados;
        this.numeros = numeros;
        tiradas = new Integer[numeros.length];
        Arrays.fill(tiradas,1);
    }

    public TriplaVectores(Integer[] numeros, String[] resultados, String[] nombres,Integer[] tiradas) {
        this.nombres = nombres;
        this.resultados = resultados;
        this.numeros = numeros;
        this.tiradas = tiradas;
    }


    public void ordenar() {
        quickSortInDescendingOrder(numeros, 0, numeros.length - 1);
    }

    public void quickSortInDescendingOrder(Integer[] numbers, int low, int high) {

        int i = low;
        int j = high;
        int temp;
        int tempt;
        String tempS;
        String tempR;
        int middle = numbers[(low + high) / 2];

        while (i < j) {
            while (numbers[i] > middle) {
                i++;
            }
            while (numbers[j] < middle) {
                j--;
            }
            if (j >= i) {
                temp = numbers[i];
                tempS = nombres[i];
                tempR = resultados[i];
                tempt = tiradas[i];

                numbers[i] = numbers[j];
                nombres[i] = nombres[j];
                resultados[i] = resultados[j];
                tiradas[i] = tiradas[j];

                numbers[j] = temp;
                nombres[j] = tempS;
                resultados[j] = tempR;
                tiradas[j] = tempt;

                i++;
                j--;
            }
        }


        if (low < j) {
            quickSortInDescendingOrder(numbers, low, j);
        }
        if (i < high) {
            quickSortInDescendingOrder(numbers, i, high);
        }
    }

    public Integer[] getNumeros() {
        return numeros;
    }

    public void setNumeros(Integer[] numeros) {
        this.numeros = numeros;
    }

    public String[] getNombres() {
        return nombres;
    }

    public void setNombres(String[] nombres) {
        this.nombres = nombres;
    }

    public String[] getResultados() {
        return resultados;
    }

    public void setResultados(String[] resultados) {
        this.resultados = resultados;
    }

    public Integer[] getTiradas() {
        return tiradas;
    }

    public void setTiradas(Integer[] tiradas) {
        this.tiradas = tiradas;
    }
}
