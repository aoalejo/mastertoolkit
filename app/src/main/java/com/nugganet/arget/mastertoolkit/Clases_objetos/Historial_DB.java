package com.nugganet.arget.mastertoolkit.Clases_objetos;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nugganet.arget.mastertoolkit.WebBuilder;

import org.threeten.bp.Instant;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;
import org.threeten.bp.format.DateTimeFormatter;

/**
 * Created by Arget on 20/3/2018.
 */

public class Historial_DB {

    private String título;
    private String cuerpo;
    private Long timestamp;
    private int id;
    private String lugar;

    public Historial_DB(String título, String cuerpo, Long timestamp, int id, String lugar) {
        this.título = título;
        this.cuerpo = cuerpo;
        this.timestamp = timestamp;
        this.id = id;
        this.lugar = lugar;
    }

    public Historial_DB(String título, String cuerpo, Long timestamp, String lugar) {
        this.título = título;
        this.cuerpo = cuerpo;
        this.timestamp = timestamp;
        this.lugar = lugar;
    }


    public String getTítulo() {
        return título;
    }

    public void setTítulo(String título) {
        this.título = título;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }


    public String toJson(){
        return new GsonBuilder().create().toJson(this,this.getClass());
    }

    public Historial_DB fromJson(String json){
        return new Gson().fromJson(json,this.getClass());
    }

    public Historial_DB() {
    }

    public String getEncabezado(){
        Instant i = Instant.ofEpochSecond(timestamp);
        ZonedDateTime actualTime = ZonedDateTime.ofInstant(i, ZoneId.systemDefault());

        String jugador_text="";
        if(!lugar.isEmpty()){
            jugador_text = " en "+ lugar;
        }

        String tiempo = "El "+ DateTimeFormatter.ofPattern("dd/MM/yyyy - HH:mm").format(actualTime);
        String text = "";

        text = título+"<br>"+tiempo+jugador_text+"<br>";

        return text;

    }



    public String getText(){
        Instant i = Instant.ofEpochSecond(timestamp);
        ZonedDateTime actualTime = ZonedDateTime.ofInstant(i, ZoneId.systemDefault());

        String jugador_text="";
        if(!lugar.isEmpty()){
            jugador_text = " en "+ lugar;
        }

        String tiempo = "El "+ DateTimeFormatter.ofPattern("dd/MM/yyyy - HH:mm").format(actualTime);
        String text = "";



        if(!cuerpo.isEmpty()){
            cuerpo.replace('\n',' ');
            text = WebBuilder.detalle_sin(título+"<br>"+tiempo+jugador_text,cuerpo)+"<br>";
        }else{
            text =  WebBuilder.detalle_sin(título,tiempo+jugador_text)+"<br>";
        }

        return text;

    }


    @Override
    public String toString() {
        return "Historial_DB{" +
                "título='" + título + '\'' +
                ", cuerpo='" + cuerpo + '\'' +
                ", timestamp=" + timestamp +
                ", id=" + id +
                ", lugar='" + lugar + '\'' +
                '}';
    }
}
