package com.nugganet.arget.mastertoolkit.Clases_objetos;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Arrays;

import Utils.BaseDeDatos;

/**
 * Created by Arget on 28/1/2018.
 */

public class Modificador {

    private int ID=0;
    private String nombre="";
    private int[] caracteristicas =new int[7];//0= ataque 1=parada 2=esquiva 3=turno 4=tipo 5-acc fisica

    public String toJson(){
        return new GsonBuilder().create().toJson(this,this.getClass());
    }

    public Modificador fromJson(String json){
        return new Gson().fromJson(json,this.getClass());
    }

    public Modificador() {
    }

    //tipo: 0=ataque 1=defensa 2=ambos

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int[] getCaracteristicas() {
        return caracteristicas;
    }

    public void setCaracteristicas(int[] caracteristicas) {
        this.caracteristicas = caracteristicas;
    }

    public Modificador(int ID, String nombre, int[] modificadores) {
        this.ID = ID;
        this.nombre = nombre;
        this.caracteristicas = modificadores;
    }

    public Modificador(Modificador nuevo) {
        this.nombre = nuevo.getNombre();
        this.caracteristicas = nuevo.getCaracteristicas();
    }

    public Modificador(String nuevo) {

        try {

            String[] modificador = BaseDeDatos.derialize(nuevo);

            this.setNombre(modificador[0]);

            for(int i=0;i<modificador.length;i++){
                this.caracteristicas[i]= BaseDeDatos.transformar_Texto(modificador[i+1]);
            }

        }catch (ArrayIndexOutOfBoundsException e){
            System.out.println(e.toString());
        }

    }

    public void reemplazar(Modificador nuevo) {
        this.nombre = nuevo.getNombre();
        this.caracteristicas = nuevo.getCaracteristicas();
    }


    @Override
    public String toString() {
        return "Modificador{" +
                "ID_TABLE=" + ID +
                ", nombre='" + nombre + '\'' +
                ", caracteristicas=" + Arrays.toString(caracteristicas) +
                '}';
    }


    public static final class MODIFICADOR { //Modificadores
        public final static int ATAQUE = 0;
        public final static int PARADA = 1;
        public final static int ESQUIVA = 2;
        public final static int TURNO = 3;
        public final static int TIPO = 4;
        public final static int ACC_FISICA = 5;

    }
}
