package com.nugganet.arget.mastertoolkit.Custom_adapters;

import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ScrollView;

import com.nugganet.arget.mastertoolkit.Principal;
import com.nugganet.arget.mastertoolkit.R;

import tourguide.tourguide.Overlay;
import tourguide.tourguide.Pointer;
import tourguide.tourguide.ToolTip;
import tourguide.tourguide.TourGuide;

public class Tutorial {


    private Principal p;
    private boolean onTutorial = false;
    private TourGuide mTutorialHandler;
    private boolean lock = false;

    private Animation enterAnimation = new AlphaAnimation(0f, 1f);

    private Animation exitAnimation = new AlphaAnimation(1f, 0f);

    private class tooltip {
        private String título;
        private String cuerpo;
        private int gravity;
        private int objetivo_id;
        private boolean noPointer = false;

        public tooltip() {
        }

        public tooltip(String título, String cuerpo) {
            this.título = título;
            this.cuerpo = "\n" + cuerpo;
            this.gravity = Gravity.CENTER;
            this.objetivo_id = -999;
            noPointer = true;
        }

        public tooltip(String título, String cuerpo, int objetivo, int gravity) {
            this.título = título;
            this.cuerpo = "\n" + cuerpo;
            this.gravity = gravity;
            this.objetivo_id = objetivo;
        }

        public tooltip(String título, String cuerpo, int objetivo) {
            this.título = título;
            this.cuerpo = "\n" + cuerpo;
            this.gravity = Gravity.CENTER;
            this.objetivo_id = objetivo;
        }

        public String getTítulo() {
            return título;
        }

        public void setTítulo(String título) {
            this.título = título;
        }

        public String getCuerpo() {
            return cuerpo;
        }

        public void setCuerpo(String cuerpo) {
            this.cuerpo = cuerpo;
        }

        public int getGravity() {
            return gravity;
        }

        public void setGravity(int gravity) {
            this.gravity = gravity;
        }

        public int getObjetivo_id() {
            return objetivo_id;
        }

        public void setObjetivo_id(int objetivo_id) {
            this.objetivo_id = objetivo_id;
        }


        public boolean isNoPointer() {
            return noPointer;
        }

        public void setNoPointer(boolean noPointer) {
            this.noPointer = noPointer;
        }
    }

    final private tooltip[] menu_inicio = {

            new tooltip("¡Bienvenido!", "Para empezar daremos un paseo por las principales funciones de la aplicación"),

            new tooltip("Seleccionar personajes", "En esta pestaña podremos indicar a la aplicación que personajes queremos usar en esta partida. Esto evita que los selectores luego se sobrecarguen de opciones, así no consumas tiempo en listados interminables de personajes en medio de un combate.", R.id.menú_sel_pjs),

            new tooltip("Seleccionar ubicaciónes", "Esta pestaña es totalmente opcional, pero podrá servir para mejorar el orden de nuestras notas en el historial ya que cada acción 'sucede' en una ubicación.\n\nLas ubicaciónes que aquí selecciones aparecerán en el selector de la esquina superior-derecha.", R.id.menú_sel_ubic),

            new tooltip("Habilidades secundarias", "Aquí podremos realizar acciones secundarias para todos los personajes seleccionados.\n\nSe puede usar con tiradas automáticas o seleccionando una dificultad y que la aplicación simplemente nos diga el control que debe superar cada personaje.", R.id.menú_part_sec),

            new tooltip("Combate", "Esta es la pestaña más completa de la aplicación, podremos resolver situaciones de combate comúnes, a distancia o maniobras especiales.\n\nTambién genera una lista con la iniciativa de cada personaje y puede manejar situaciones de combate para cada personaje.", R.id.menú_part_comb),

            new tooltip("Psiquicos", "En esta pestaña podremos controlar a los psiquicos de nuestra partida. \n\nSe puede ver el listado de poderes aprendidos por el personaje, así como la descripción y efectos del mismo.", R.id.menú_part_psiq),

            new tooltip("Estado de los jugadores", "Si querémos ver rápidamente como se encuentra cada personaje, en esta pestaña mostrará la vida, el cansancio, penalizadores y zeon de cada uno. Además, si seleccionas un personaje te permitirá cambiar de forma cómoda algunos valores, como ser los mencionados anteriormente y varios más.", R.id.menú_part_estado),

            new tooltip("Generador de tiradas", "Un simple pero tremendamente útil generador de tiradas. Puede personalizarse el rango y cantidad de dados, así como marcar un número mínimo de éxito para que nos indique la cantidad de aciertos, también cuenta automáticamente todos los resultados y posee un historial de las últimas tiradas.", R.id.menú_extr_tiradas),

            new tooltip("Historial", "El historial guarda todas las tiradas del generador, todas las acciones de combate que decidamos 'aplicar' y los controles de habilidades secundarias que creamos necesarios guardar.", R.id.menú_extr_historial),

            new tooltip("Creación", "En cada uno de estos ítems podremos crear, editar o eliminar personajes, armas, armaduras modificadores o estados.", R.id.menú_creación, Gravity.TOP),

            new tooltip("Importar", "Cabe destacar que los personajes pueden ser importados directamente desde la planilla excel creada por la comunidad \n(enlace y tutorial para hacerlo en la misma pestaña).", R.id.menú_cre_imp, Gravity.TOP),

            new tooltip("¡Esto es todo!", "Gracias por probar esta aplicación, es un trabajo en progreso que irá mutando y mejorando con el tiempo. \n\nSi quieres repasar el tutorial puedes ir a la ventana de herramientas.")
    };


    final private Overlay overlay_intro = new Overlay()
            .setBackgroundColor(Color.parseColor("#C8263238"))
            .setStyle(Overlay.Style.NO_HOLE)
            .setEnterAnimation(enterAnimation)
            .setExitAnimation(exitAnimation);

    final private Overlay overlay = new Overlay()
            .setBackgroundColor(Color.parseColor("#C8263238"))
            .setStyle(Overlay.Style.ROUNDED_RECTANGLE)
            .setExitAnimation(exitAnimation)
            .setEnterAnimation(enterAnimation);

    final private Pointer pointer = new Pointer()
            .setColor(Color.parseColor("#7cb342"));

    public Tutorial(Principal p) {
        this.p = p;
        enterAnimation.setDuration(300);
        enterAnimation.setFillAfter(true);
        exitAnimation.setDuration(300);
        exitAnimation.setFillAfter(true);
    }

    public boolean isOnTutorial() {
        return onTutorial;
    }

    private int contador_tuto = 0;
    private long mLastPress = 0;

    private void tuto_ppal() {
        long currentTime = System.currentTimeMillis();

        if (onTutorial) {
            if (currentTime - mLastPress > 500) {
                mLastPress = currentTime;
                if (contador_tuto >= menu_inicio.length) {
                    stop();
                } else {
                    try {
                        ScrollView scroll = p.findViewById(R.id.Extras_pantalla_principal);

                        View v = p.findViewById(R.id.Linear_Layout_prep);

                        switch (contador_tuto) {
                            case 0:
                                scroll.smoothScrollTo(0, 0);
                                break;
                            case 3:
                                scroll.smoothScrollTo(0, v.getHeight());
                                break;
                            case 6:
                                scroll.smoothScrollTo(0, v.getHeight() * 2);
                                break;
                        }

                        safeStart(menu_inicio[contador_tuto], 150);
                        contador_tuto = contador_tuto + 1;

                    } catch (java.lang.ArrayIndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            stop();
        }

    }

    public void handler_events(View v) {

        if (v instanceof Button | v instanceof ImageButton) {
            tuto_ppal();
        }


    }

    private void safeStart(tooltip tip, long milis) {
        if (!lock) {
            lock = true;
            p.findViewById(R.id.center).postDelayed(new RunTutorial(tip), milis);
        }
    }

    public class RunTutorial implements Runnable {
        private tooltip tooltip_instance;

        RunTutorial(tooltip parameter) {
            tooltip_instance = parameter;
            // store parameter for later user
        }

        public void run() {
            siguiente_paso(tooltip_instance);
        }
    }


    private void siguiente_paso(tooltip tip) {

        if (onTutorial) {

            try {
                mTutorialHandler.cleanUp();

                if (tip.isNoPointer()) {
                    mTutorialHandler.setOverlay(overlay_intro);

                    mTutorialHandler.setToolTip(new ToolTip()
                            .setTitle(tip.getTítulo())
                            .setDescription(tip.getCuerpo())
                            .setGravity(tip.getGravity())
                            .setBackgroundColor(Color.parseColor("#263238")));

                    mTutorialHandler.setPointer(null);
                    mTutorialHandler.playOn(p.findViewById(R.id.center));

                } else {

                    mTutorialHandler.setOverlay(overlay);

                    mTutorialHandler.setToolTip(new ToolTip()
                            .setTitle(tip.getTítulo())
                            .setDescription(tip.getCuerpo())
                            .setGravity(tip.getGravity())
                            .setBackgroundColor(Color.parseColor("#263238")));

                    mTutorialHandler.setPointer(null);
                    mTutorialHandler.playOn(p.findViewById(tip.getObjetivo_id()));

                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            lock = false;

        } else {
            stop();
        }
    }

    public void stop() {
        (p.findViewById(R.id.Button_overlay_tutorial)).setVisibility(View.GONE);
        onTutorial = false;
        try {
            mTutorialHandler.cleanUp();
        } catch (java.lang.NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void start_ppal(boolean force) {

        (p.findViewById(R.id.Button_overlay_tutorial)).setVisibility(View.VISIBLE);

        if (force | !p.obtener_estado("tutorial ppal", false)) {

            if (force) {
                p.cambio_de_pagina(null, -1, true, "Menu principal");

                ScrollView scroll = p.findViewById(R.id.Extras_pantalla_principal);

                scroll.scrollTo(0, 0);
            }

            if (!p.obtener_estado("tutorial ppal", false)) {
                p.guardarEstado("tutorial ppal", true);
            }
            lock = false;
            onTutorial = true;

            overlay_intro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tuto_ppal();
                }
            });

            overlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tuto_ppal();
                }
            });

            mTutorialHandler = TourGuide.init(p);

            mTutorialHandler.setOverlay(overlay_intro);

            tooltip tip = menu_inicio[0];

            mTutorialHandler.setToolTip(new ToolTip()
                    .setTitle(tip.getTítulo())
                    .setDescription(tip.getCuerpo())
                    .setGravity(tip.getGravity())
                    .setBackgroundColor(Color.parseColor("#263238")));

            mTutorialHandler.setPointer(null);
            mTutorialHandler.playOn(p.findViewById(R.id.center));

            contador_tuto = 1;

        } else {
            stop();
        }

    }
}
