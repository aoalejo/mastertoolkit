package com.nugganet.arget.mastertoolkit;

/**
 * Created by Arget on 17/6/2018.
 */

public class WebBuilder {

    public static final String WebPage = "<!DOCTYPE html> <html> <body bgcolor='#fafafa'> <font size='4'> <script> function appendText(extraStr) { document.getElementsByTagName('body')[0].innerHTML = document.getElementsByTagName('body')[0].innerHTML + extraStr; } </script> <script type='text/javascript'> function linkHistorial(link) { Android.showHistorial(link); } </script> <script> function appendText_list(extraStr) { document.getElementsByTagName('ul')[0].innerHTML = document.getElementsByTagName('ul')[0].innerHTML + extraStr; } </script> <ul> </ul> </font> </body> </html>";

    public static final String DETALLE = "<body  bgcolor=\"#fafafa\"> <center>" + nombres("NOMBRE") + " | <font color='607d8b'>BASE</font> | <font color='33691E'>[TIRADA]</font> | <font color='b71c1c'>RESULTADO</font> </center></font></body>";

    public static final String FinDoc = "</body> </html>";

    public static String add(String text){
        return  "javascript:appendText('"+text+"')";
    }

    public static String add_list(String text){
        return  "javascript:appendText_list('<li>"+text+"</li><br>')";
    }

    public static String linkto(String link,String text){

        return "<a onClick=\"linkHistorial("+link+");\" style=\"cursor: pointer; cursor: hand;\">"+text+"</a>";

    }


    public static final String ENCABEZADO = "<!DOCTYPE html>" +
            "<html>" +
            "<body  bgcolor=\"#fafafa\"><font size=\"4\">" +
            "<script>" +
            "function appendText(extraStr) {" +
            "     document.getElementsByTagName('body')[0].innerHTML = document.getElementsByTagName('body')[0].innerHTML + extraStr;" +
            " }  " +
            "</script>"+
            "<script>function openAll() {\n" +
            "    var x = document.getElementsByTagName(\"details\");\n" +
            "    var i;\n" +
            "    for (i = 0; i < x.length; i++) {\n" +
            "         x[i].setAttribute(\"open\", \"true\");\n" +
            "}\n" +
            "</script>"+
            "\n" +
            "<script type=\"text/javascript\">\n" +
            "    function showAndroidToast(toast) {\n" +
            "        Android.showToast(toast);\n" +
            "    }\n" +
            "</script>";

    public static final String generateBody(String text) {

        return ENCABEZADO + text +"<br><br>" +DETALLE + FinDoc;
    }


    public static final String bajo(String text) {
        return "<b><font color=\"#b71c1c\">" + text + "</font></b>";
    }

    public static final String result(String text) {
        return "<b><font color=\"#01579b\">" + text + "</font></b>";
    }

    public static final String alto(String text) {
        return "<b><font color=\"#7cb342\">" + text + "</font></b>";
    }

    public static final String nombres(String text) {
        return "<b><font color=\"01579b\">" + text + "</font></b>";
    }

    public static final String habilidad(int text) {
        return "<code><b><font color=\"607d8b\">" + text + "</font></b></code>";
    }

    public static final String habilidad_usada(String text) {
        return "<b>" + text + "</b>";
    }

    public static final String tirada(int text) {
        return "<code><b><font color=\"33691E\">[" + text + "]</font></b></code>";
    }

    public static final String resultado(int text) {
        return "<code><b><font color=\"b71c1c\">" + text + "</font></b></code>";
    }

    public static final String resultado(String text) {
        return "<code><b><font color=\"b71c1c\">" + text + "</font></b></code>";
    }

    public static final String formateo_tirada(String habilidad_nombre, int habilidad, int tirada) {
        return habilidad_usada(habilidad_nombre) + " <span style=\"white-space: nowrap\"> (" + WebBuilder.habilidad(habilidad) + " + " + WebBuilder.tirada(tirada) + " = " + WebBuilder.resultado(tirada + habilidad) + ")</span>";
    }

    public static final String lb() {
        return "</li><br> <li>";
    }

    public static final String página(int pagina, String libro) {
        return " <br><center><small>Página " + pagina + " " + libro + "</small></center>";
    }

    public static final String detalle_sin(String título, String detalle) {

        return "<details>" + "<summary>" + título + "</summary> <p style=\"margin-left:5px;\">" + detalle + "</p> <hr> </details>";
    }

    public static final String detalle(String título, String detalle) {

        return "<details>" +
                "<summary>" + título + "</summary>" +
                "<ul> <li>" + detalle + "</li></ul> <hr> " +
                "</details>";
    }

}
