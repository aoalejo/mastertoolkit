package com.nugganet.arget.mastertoolkit.Clases_objetos;

import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.nugganet.arget.mastertoolkit.Principal;
import com.nugganet.arget.mastertoolkit.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.drawerlayout.widget.DrawerLayout;

public class Historial_de_acciones {

    private class holder implements Cloneable {
        private int pageNumber = 0;
        private int tabNumber = 0;
        private MenuItem item;
        private String title = "Menú principal";

        public Object clone() {
            try {
                return super.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            return null;
        }

        public int getPageNumber() {
            return pageNumber;
        }

        public void setPageNumber(int pageNumber) {
            this.pageNumber = pageNumber;
        }

        public int getTabNumber() {
            return tabNumber;
        }

        public void setTabNumber(int tabNumber) {
            this.tabNumber = tabNumber;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public holder(int pageNumber, int tabNumber, MenuItem item, String title) {
            this.pageNumber = pageNumber;
            this.tabNumber = tabNumber;
            this.item = item;
            this.title = title;
        }

        public holder(holder nuevo) {
            this.pageNumber = nuevo.getPageNumber();
            this.tabNumber = nuevo.getTabNumber();
            this.item = nuevo.getItem();
            this.title = nuevo.getTitle();
        }

        public holder(holder nuevo, int newTab) {
            this.pageNumber = nuevo.getPageNumber();
            this.tabNumber = newTab;
            this.item = nuevo.getItem();
            this.title = nuevo.getTitle();
        }

        public MenuItem getItem() {
            return item;
        }

        public void setItem(MenuItem item) {
            this.item = item;
        }

        @Override
        public String toString() {
            return "holder{" +
                    "pageNumber=" + pageNumber +
                    ", tabNumber=" + tabNumber +
                    ", item=" + item +
                    '}';
        }
    }

    private boolean flag_auto_cambio = false;
    private boolean flagExit = false;
    private ArrayList<holder> historial = new ArrayList<>();
    private Principal p;
    private final static List<Integer> PÁGINAS_CON_PESTAÑAS = Arrays.asList(R.id.Creac_Pj, R.id.Part_Combate);

    private holder actual = new holder(-1, 0, null, "Menú principal");
    private holder anterior = new holder(-1, 0, null, "Menú principal");
    private holder principal = new holder(-1, 0, null, "Menú principal");

    private boolean sistemaNahuel = true;


    private long mLastPress = 0;

    int TOAST_DURATION = 5000;
    Toast onBackPressedToast;

    public Historial_de_acciones(Principal p) {
        this.p = p;
        // historial.add(new holder(-1,0,null));
    }

    public ArrayList<holder> getHistorial() {
        return historial;
    }

    public void setHistorial(ArrayList<holder> historial) {
        this.historial = historial;
    }

    public holder getLast() {
        holder temp;
        if (!historial.isEmpty()) {
            temp = historial.get(historial.size() - 1);
        } else {
            temp = new holder(-1, 0, null, "Menú principal");
        }
        return temp;
    }

    public int[] getActualPage() {
        return new int[]{getLast().getPageNumber(), getLast().getTabNumber()};
    }


    public void undo() {
        flag_auto_cambio = true;

        DrawerLayout drawer = p.findViewById(R.id.drawer_layout);

        if (sistemaNahuel) {
            System.out.println("\nact: " + actual.toString() + "\nant: " + anterior.toString() + "\nppl: " + principal.toString());

            if (actual.toString().equals(principal.toString())) {

                long currentTime = System.currentTimeMillis();
                if (drawer.isDrawerOpen(Gravity.START)) {
                    if (currentTime - mLastPress > TOAST_DURATION) {
                        onBackPressedToast = Toast.makeText(p, "Presiona otra vez para salir", Toast.LENGTH_SHORT);
                        onBackPressedToast.show();
                        mLastPress = currentTime;
                    } else {
                        if (onBackPressedToast != null) {
                            onBackPressedToast.cancel();  //Difference with previous answer. Prevent continuing showing toast after application exit.
                            onBackPressedToast = null;
                        }
                        System.exit(0);
                    }
                } else {
                    if (currentTime - mLastPress < TOAST_DURATION) {
                        if (onBackPressedToast != null) {
                            onBackPressedToast.cancel();  //Difference with previous answer. Prevent continuing showing toast after application exit.
                            onBackPressedToast = null;
                        }
                        System.exit(0);
                    } else {
                        drawer.openDrawer(Gravity.START);
                    }
                }

            } else {

                if (PÁGINAS_CON_PESTAÑAS.contains(anterior.pageNumber)) {

                    if (p.getControlador_ventanas().getToolkit_combate() != null) {
                        if (p.getControlador_ventanas().getToolkit_combate().getVisibility() == View.VISIBLE) {

                            ((TabLayout) p.findViewById(R.id.tab_layout_combate)).getTabAt(anterior.tabNumber).select();

                        }
                    } else if (p.getControlador_ventanas().getToolkit_creacion_pj() != null) {
                        if (p.getControlador_ventanas().getToolkit_creacion_pj().getVisibility() == View.VISIBLE) {

                            ((TabLayout) p.findViewById(R.id.tab_layout_pj)).getTabAt(anterior.tabNumber).select();
                        }
                    }


                } else {
                    p.cambio_de_pagina(anterior.getItem(), anterior.getPageNumber(), false, null);
                }


                actual = (holder) anterior.clone();
                anterior = principal;

            }


        } else {

            removelast();


            if (!flagExit) {

                if (drawer.isDrawerOpen(Gravity.START)) {
                    drawer.closeDrawer(Gravity.START);
                } else {


                    holder temp = getLast();

                    p.cambio_de_pagina(temp.getItem(), temp.getPageNumber(), false, null);

                    System.out.println("\n" + temp.toString() + " -> " + PÁGINAS_CON_PESTAÑAS.toString() + "\n");

                    if (PÁGINAS_CON_PESTAÑAS.contains(temp.pageNumber)) {

                        if (p.getControlador_ventanas().existsAndVisible(p.getControlador_ventanas().getToolkit_combate())) {

                            ((TabLayout) p.findViewById(R.id.tab_layout_combate)).getTabAt(temp.tabNumber).select();


                        } else if (p.getControlador_ventanas().existsAndVisible(p.getControlador_ventanas().getToolkit_creacion_pj())) {

                            ((TabLayout) p.findViewById(R.id.tab_layout_pj)).getTabAt(temp.tabNumber).select();
                        }
                    }
                }
            } else {
                long currentTime = System.currentTimeMillis();
                if (drawer.isDrawerOpen(Gravity.START)) {
                    if (currentTime - mLastPress > TOAST_DURATION) {
                        onBackPressedToast = Toast.makeText(p, "Presiona otra vez para salir", Toast.LENGTH_SHORT);
                        onBackPressedToast.show();
                        mLastPress = currentTime;
                    } else {
                        if (onBackPressedToast != null) {
                            onBackPressedToast.cancel();  //Difference with previous answer. Prevent continuing showing toast after application exit.
                            onBackPressedToast = null;
                        }
                        System.exit(0);
                    }
                } else {
                    if (currentTime - mLastPress < TOAST_DURATION) {
                        if (onBackPressedToast != null) {
                            onBackPressedToast.cancel();  //Difference with previous answer. Prevent continuing showing toast after application exit.
                            onBackPressedToast = null;
                        }
                        System.exit(0);
                    } else {
                        drawer.openDrawer(Gravity.START);
                    }
                }
            }
        }
        flag_auto_cambio = false;
    }

    public void añadirHistorial(MenuItem menú, int number, boolean pestaña, String title) {

        if (!flag_auto_cambio) {

            if (sistemaNahuel) {
                anterior = (holder) actual.clone();
                if (pestaña) {

                    actual = new holder(actual, number);

                } else {

                    actual = new holder(number, 0, menú, title);

                }

            } else {
                if (pestaña) {

                    historial.add(new holder(getLast(), number));

                } else {
                    historial.add(new holder(number, 0, menú, title));
                }
            }


        }


    }

    private void removelast() {
        if (!historial.isEmpty()) {
            historial.remove(historial.size() - 1);
            flagExit = false;
        } else {
            flagExit = true;
            p.cambio_de_pagina(null, -1, false, "Menú principal");
        }
    }


}
