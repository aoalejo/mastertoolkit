package com.nugganet.arget.mastertoolkit.Clases_objetos;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.Arrays;

import Utils.BaseDeDatos;

/**
 * Created by Arget on 11/4/2017.
 */

public class Armadura implements Serializable{
    @Expose
    private String nombre = "";
    @Expose
    private int[] caracteristicas = new int[25];
    @Expose
    private int ID;

    public String toJson(){
        return new GsonBuilder().create().toJson(this,this.getClass());
    }

    public Armadura fromJson(String json){
        return new Gson().fromJson(json,this.getClass());
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int[] getCaracteristicas() {
        return caracteristicas;
    }

    public void setCaracteristicas(int[] caracteristicas) {
        this.caracteristicas = caracteristicas;
    }

    public Armadura(String nombre, int[] caracteristicas) {
        this.nombre = nombre;
        this.caracteristicas = caracteristicas;
    }

    public Armadura() {
        nombre = "Vacio";
    }

    public void setCaracteristica(int index,int valorNuevo){
        caracteristicas[index] = valorNuevo;
    }

    public void reemplazar(Armadura nuevo) {
        this.nombre = nuevo.getNombre();
        this.caracteristicas = nuevo.getCaracteristicas();
    }

    public void reemplazar_valor_caracteristica(int index, int nuevo) {
        this.caracteristicas[index] = nuevo;
    }

    public Armadura(String nuevo) {
        String[] array = BaseDeDatos.derialize(nuevo);
        nombre = array[0];

        int[] caracteristicas = new int[25];
        for (int i = 1; i < array.length; i++) {
            caracteristicas[i - 1] = Integer.valueOf(array[i]);
        }

        setCaracteristicas(caracteristicas);
    }

    public Armadura(String nombre, int[] caracteristicas, int ID) {
        this.nombre = nombre;
        this.caracteristicas = caracteristicas;
        this.ID = ID;
    }

    public Armadura(Armadura Nueva_Armadura) {
        this.nombre = Nueva_Armadura.getNombre();
        this.caracteristicas = Nueva_Armadura.getCaracteristicas();
        this.ID = Nueva_Armadura.getID();
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    @Override
    public String toString() {
        return "Armadura{" +
                "nombre='" + nombre + '\'' +
                ", caracteristicas=" + Arrays.toString(caracteristicas) +
                ", ID_TABLE=" + ID +
                '}';
    }

    public void cambiarCaracteristica(int index, int valorNuevo) {
        this.caracteristicas[index] = valorNuevo;
    }

    public void cambiarCaracteristica(int[] nuevos) {
        this.caracteristicas = nuevos;
    }



}

