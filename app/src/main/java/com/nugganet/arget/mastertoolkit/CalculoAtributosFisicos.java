package com.nugganet.arget.mastertoolkit;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/**
 * Created by Arget on 1/4/2017.
 */

public class CalculoAtributosFisicos {

    private int fuerza;
    private int constitucion;
    private int agilidad;
    private int edad;
    private int altura;
    private int peso;

    public CalculoAtributosFisicos() {
    }


    void calculoNivel() {

        altura = fuerza + constitucion;

        peso = fuerza + constitucion - (agilidad-4);

    }

    public double multiplicadorEdad(double entrada) {

        int x = edad;

        double y;
        y = 0.4051343302555319 + x * (0.03139474576136056 +
                x * (0.02429260985738164 + x * (-0.007791210652873638 +
                        x * (0.0008301516658519457 + x * (3.778786316860987E-05 +
                                x * (-1.967182978441197E-05 + x * (2.485965091956676E-06 +
                                        x * (-1.848172280847336E-07 + x * (9.397073540500466E-09 +
                                                x * (-3.474765335161532E-10 + x * (9.644617823490309E-12 +
                                                        x * (-2.043609500112589E-13 + x * (3.329907038449701E-15 +
                                                                x * (-4.170282108776491E-17 + x * (3.980617691451300E-19 +
                                                                        x * (-2.843982818029300E-21 + x * (1.472470414519754E-23 +
                                                                                x * (-5.217084047853596E-26 + x * (1.131474696074304E-28 +
                                                                                        x * -1.132782402892083E-31)))))))))))))))))));
        return (y * entrada);
    }


    public void setAtrs(int FUE, int CON, int AGI, int EDAD) {
        fuerza = FUE;
        constitucion = CON;
        agilidad = AGI;
        edad = EDAD;

        calculoNivel();
    }

    public double alturaMax() {

        double y;

        y = (-0.1910297560525603) + 0.5723083071629069 * pow(altura, 0.5116889980569195);

        return (multiplicadorEdad(y));

    }

    public double alturaMin() {

        double y;

        y = (-1.153758174800276) + 1.081130691408648 * pow(altura, 0.3602447769203210);

        return (multiplicadorEdad(y));

    }


    public double altura() {

        double y;
        y = -0.4140304963099246 + (altura * 0.2664198338110730);
        return multiplicadorEdad(sqrt(y));

    }

    public double pesoMax() {

        double y;
        double x1;
        x1 = 1.0 / sqrt(peso);
        y = -0.01623804198258875 + 0.08502889098369874 * x1;
        return multiplicadorEdad(1.0 / y);

    }


    public double pesoMin() {

        double y;
        double x1;
        x1 = 1.0 / sqrt(peso);
        y = -0.02742042107987597 + 0.1639712219490064 * x1;
        return multiplicadorEdad(1.0 / y);

    }


}
