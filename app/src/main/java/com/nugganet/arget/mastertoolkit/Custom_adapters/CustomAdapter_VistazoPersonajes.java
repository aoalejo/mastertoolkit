package com.nugganet.arget.mastertoolkit.Custom_adapters;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.nugganet.arget.mastertoolkit.Base;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Personaje;
import com.nugganet.arget.mastertoolkit.Principal;
import com.nugganet.arget.mastertoolkit.R;

import java.util.ArrayList;

public class CustomAdapter_VistazoPersonajes extends BaseAdapter {
    private ArrayList<Personaje> personajes = new ArrayList<>();

    Context context;
    private static LayoutInflater inflater = null;
    private Principal principal;


    public CustomAdapter_VistazoPersonajes(Principal mainActivity, ArrayList<Personaje> personajes, Principal principal) {
        this.personajes = personajes;
        context = mainActivity;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.principal = principal;

    }

    @Override
    public int getCount() {
        return personajes.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder {
        TextView textView_Nombre;
        TextView textView_vida;
        TextView textView_zeon;
        TextView textView_energía;

        TextView textView_penalizadores;
        ProgressBar progressBar_vida;
        ProgressBar progressBar_zeon;
        ProgressBar progressBar_energía;

        ProgressBar progressBar_penalizadores;
        LinearLayout Layout_zeon;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.list_personajes, null, true);

        final Personaje temp = personajes.get(position);

        holder.Layout_zeon = (LinearLayout) rowView.findViewById(R.id.Layout_zeon);

        holder.textView_Nombre = (TextView) rowView.findViewById(R.id.textView_Nombre);
        holder.textView_vida = (TextView) rowView.findViewById(R.id.textView_vida);
        holder.textView_zeon = (TextView) rowView.findViewById(R.id.textView_zeon);
        holder.textView_energía = (TextView) rowView.findViewById(R.id.textView_energía);
        holder.textView_penalizadores = (TextView) rowView.findViewById(R.id.textView_penalizadores);

        holder.progressBar_vida = (ProgressBar) rowView.findViewById(R.id.progressBar_vida);
        holder.progressBar_zeon = (ProgressBar) rowView.findViewById(R.id.progressBar_zeon);
        holder.progressBar_energía = (ProgressBar) rowView.findViewById(R.id.progressBar_energía);
        holder.progressBar_penalizadores = (ProgressBar) rowView.findViewById(R.id.progressBar_penalizadores);

        holder.textView_Nombre.setText(temp.getNombres());
        holder.textView_vida.setText(temp.getEstado(0));
        holder.textView_zeon.setText(temp.getEstado(1));
        holder.textView_energía.setText(temp.getEstado(2));
        holder.textView_penalizadores.setText(temp.getEstado(3));

        holder.progressBar_vida.setMax(temp.getCaracteristicas(Base.VIDA_MAX));
        holder.progressBar_vida.setProgress(temp.getCaracteristicas(Base.VIDA_ACT));


        holder.progressBar_zeon.setMax(temp.getCaracteristicas(Base.ZEON_MAX));
        holder.progressBar_zeon.setProgress(temp.getCaracteristicas(Base.ZEON_ACT));

        if (temp.getCaracteristicas(Base.ZEON_MAX) == 0) {
            holder.Layout_zeon.setVisibility(View.GONE);
        }

        holder.progressBar_energía.setMax(temp.getCaracteristicas(Base.ENERGÍA_MAX));
        holder.progressBar_energía.setProgress(temp.getCaracteristicas(Base.ENERGÍA_ACT));

        holder.progressBar_penalizadores.setMax(200);
        holder.progressBar_penalizadores.setProgress(Math.abs(temp.Penalizadores(Base.TODA_ACCIÓN)));

        rowView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                principal.toCreacPj(temp);
                return true;
            }
        });

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = principal.getFragmentManager();
                EstadosFragment dialogFragment = new EstadosFragment();

                dialogFragment.SetPj(temp);
                dialogFragment.setPrincipal(principal);

                System.out.println(temp.toString());

                dialogFragment.show(fm, "");
            }
        });

        return rowView;
    }


    public static class EstadosFragment extends DialogFragment {

        private Personaje pj;

        private void SetPj(Personaje pj) {
            this.pj = pj;
        }

        Principal principal;

        private void setPrincipal(Principal p) {
            principal = p;
        }

        private String text = "";

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment_estados, container, false);

            Button button_guardar = (Button) rootView.findViewById(R.id.button_guardar);
            Button button_cancelar = (Button) rootView.findViewById(R.id.button_cancelar);

            int escudoMax = pj.getCaracteristicas(Base.ESCUDO_MAX);
            int escudoAct = pj.getCaracteristicas(Base.ESCUDO_ACT);

            if (escudoAct > escudoMax) {
                pj.reemplazar_valor_caracteristica(Base.ESCUDO_ACT, escudoMax);
            }

            SeekBar escudo_seek = ((SeekBar) rootView.findViewById(R.id.seekBar_escudo));
            final TextView escudo_act = ((TextView) rootView.findViewById(R.id.Text_escudo_actual));

            if (escudoMax > 100) {
                escudo_seek.setMax(escudoMax);
                escudo_seek.setProgress(escudoAct);
            } else {
                escudo_seek.setMax(1000);
                escudo_seek.setProgress(escudoAct);
            }

            escudo_act.setText(String.valueOf(escudoAct));

            escudo_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                    escudo_act.setText(String.valueOf(progress));

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });

            ((TextView) rootView.findViewById(R.id.Nombre)).setText(pj.getNombres());

            final int vidaMax = (pj.getCaracteristicas(Base.VIDA_MAX));
            final int Constit = (pj.getCaracteristicas(Base.ATRIBUTO_CONSTITUCIÓN));

            SeekBar vida = ((SeekBar) rootView.findViewById(R.id.seekBar_vida_actual));

            vida.setMax(vidaMax / 5 + Constit);

            vida.setProgress(pj.getCaracteristicas(Base.VIDA_ACT) / 5 + Constit);

            ((TextView) rootView.findViewById(R.id.Text_Vida_actual)).setText((pj.getCaracteristicas(Base.VIDA_ACT)) + " / " + pj.getCaracteristicas(Base.VIDA_MAX));

            vida.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                    int vida = (progress - (Constit)) * 5;

                    ((TextView) rootView.findViewById(R.id.Text_Vida_actual)).setText(vida + " / " + pj.getCaracteristicas(Base.VIDA_MAX));

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });

            SeekBar zeon = ((SeekBar) rootView.findViewById(R.id.seekBar_zeon_actual));

            zeon.setMax((pj.getCaracteristicas(Base.ZEON_MAX)) / 5);

            zeon.setProgress(pj.getCaracteristicas(Base.ZEON_ACT) / 5);

            ((TextView) rootView.findViewById(R.id.Text_Zeon_Actual)).setText((pj.getCaracteristicas(Base.ZEON_ACT)) + " / " + pj.getCaracteristicas(Base.ZEON_MAX));

            zeon.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                    int zeon = progress * 5;

                    ((TextView) rootView.findViewById(R.id.Text_Zeon_Actual)).setText(zeon + " / " + pj.getCaracteristicas(Base.ZEON_MAX));

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });

            SeekBar energia = ((SeekBar) rootView.findViewById(R.id.seekBar_energía_actual));

            energia.setMax((pj.getCaracteristicas(Base.ENERGÍA_MAX)));

            energia.setProgress(pj.getCaracteristicas(Base.ENERGÍA_ACT));

            ((TextView) rootView.findViewById(R.id.Text_Energía_actual)).setText((pj.getCaracteristicas(Base.ENERGÍA_ACT)) + " / " + pj.getCaracteristicas(Base.ENERGÍA_MAX));

            energia.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                    ((TextView) rootView.findViewById(R.id.Text_Energía_actual)).setText(progress + " / " + pj.getCaracteristicas(Base.ENERGÍA_MAX));

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });

            SeekBar cvs = ((SeekBar) rootView.findViewById(R.id.seekBar_cvs_libres_actuales));

            cvs.setMax(pj.getHabilidades_psiquicas()[Personaje.PSIQUICOS.CVS_MAX]);

            cvs.setProgress(pj.getHabilidades_psiquicas()[Personaje.PSIQUICOS.CVS_ACT]);

            ((TextView) rootView.findViewById(R.id.Text_cv_libre_actual)).setText((pj.getHabilidades_psiquicas()[Personaje.PSIQUICOS.CVS_ACT]) + " / " + (pj.getHabilidades_psiquicas()[Personaje.PSIQUICOS.CVS_MAX]));

            cvs.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                    ((TextView) rootView.findViewById(R.id.Text_cv_libre_actual)).setText(progress + " / " + (pj.getHabilidades_psiquicas()[Personaje.PSIQUICOS.CVS_MAX]));

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });


            try {

                ((Spinner) rootView.findViewById(R.id.spinner_añadirCritico)).setSelection(Base.intToindexPorcentualCinco(pj.getEspeciales()[Base.AÑADIR_CRÍTICO_AL_ARMA]));

                ((TextView) rootView.findViewById(R.id.textView_pen_armadura)).setText(String.valueOf(pj.Penalizadores(4)));

                ((TextView) rootView.findViewById(R.id.textView_pen_fatiga)).setText(String.valueOf(pj.Penalizadores(5)));

                ((Spinner) rootView.findViewById(R.id.spinner_pen_toda_acción)).setSelection(indexPenalizador(pj.getPenalizadores()[Base.PEN_A_TODA_ACCION]));
                ((Spinner) rootView.findViewById(R.id.spinner_pen_temporal)).setSelection(indexPenalizador(pj.getPenalizadores()[Base.PEN_TEMPORAL]));
                ((Spinner) rootView.findViewById(R.id.spinner_pen_visual)).setSelection(indexPenalizador(pj.getPenalizadores()[Base.PEN_VISUAL]));

                ((Spinner) rootView.findViewById(R.id.spinner_robo_de_vida)).setSelection(Base.intToindexPorcentualCinco(pj.getEspeciales()[Base.ROBO_DE_VIDA_PORCENTUAL]));

                ((Spinner) rootView.findViewById(R.id.spinner_redDañoPorc)).setSelection(Base.intToindexPorcentualCinco(pj.getEspeciales()[Base.REDUCCIÓN_DE_DAÑO_PORCENTUAL]));
                ((Spinner) rootView.findViewById(R.id.spinner_redPenPorc)).setSelection(Base.intToindexPorcentualCinco(pj.getEspeciales()[Base.REDUCCION_DE_PENALIZADORES_PORCENTUAL]));

                ((Spinner) rootView.findViewById(R.id.spinner_redDañoPleno)).setSelection(Base.intToindexPorcentualCinco(pj.getEspeciales()[Base.REDUCCIÓN_DE_DAÑO_PLENO]));
                ((Spinner) rootView.findViewById(R.id.spinner_redPenPleno)).setSelection(Base.intToindexPorcentualCinco(pj.getEspeciales()[Base.REDUCCION_DE_PENALIZADORES_PLENO]));


            } catch (ArrayIndexOutOfBoundsException exception) {
                System.out.println("Error en intento de poblar spinners: " + exception);
            }


            button_guardar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int vida = (((SeekBar) rootView.findViewById(R.id.seekBar_vida_actual)).getProgress() - (Constit)) * 5;
                    int zeon = (((SeekBar) rootView.findViewById(R.id.seekBar_zeon_actual)).getProgress() * 5);
                    int energia = ((SeekBar) rootView.findViewById(R.id.seekBar_energía_actual)).getProgress();
                    int escudo = ((SeekBar) rootView.findViewById(R.id.seekBar_escudo)).getProgress();
                    int cvs = ((SeekBar) rootView.findViewById(R.id.seekBar_cvs_libres_actuales)).getProgress();

                    int[] pen = new int[5];
                    pen[0] = (20 - ((Spinner) rootView.findViewById(R.id.spinner_pen_toda_acción)).getSelectedItemPosition()) * 5;
                    pen[1] = (20 - ((Spinner) rootView.findViewById(R.id.spinner_pen_temporal)).getSelectedItemPosition()) * 5;
                    pen[2] = (20 - ((Spinner) rootView.findViewById(R.id.spinner_pen_visual)).getSelectedItemPosition()) * 5;

                    pj.setPenalizadores(pen);

                    pj.reemplazar_valor_caracteristica(Base.VIDA_ACT, vida);
                    pj.reemplazar_valor_caracteristica(Base.ZEON_ACT, zeon);
                    pj.reemplazar_valor_caracteristica(Base.ENERGÍA_ACT, energia);
                    pj.reemplazar_valor_caracteristica(Base.ESCUDO_ACT, escudo);
                    pj.reemplazar_valor_psiquico(Personaje.PSIQUICOS.CVS_ACT, cvs);

                    if (pj.getCaracteristicas(Base.ESCUDO_MAX) < pj.getCaracteristicas(Base.ESCUDO_ACT)) {
                        pj.reemplazar_valor_caracteristica(Base.ESCUDO_MAX, escudo);
                    }

                    int[] esp = new int[10];

                    esp[Base.ROBO_DE_VIDA_PORCENTUAL] = (((Spinner) rootView.findViewById(R.id.spinner_robo_de_vida)).getSelectedItemPosition()) * 5;
                    esp[Base.AÑADIR_CRÍTICO_AL_ARMA] = (((Spinner) rootView.findViewById(R.id.spinner_añadirCritico)).getSelectedItemPosition()) * 5;
                    esp[Base.REDUCCION_DE_PENALIZADORES_PLENO] = (((Spinner) rootView.findViewById(R.id.spinner_redPenPleno)).getSelectedItemPosition()) * 5;
                    esp[Base.REDUCCION_DE_PENALIZADORES_PORCENTUAL] = (((Spinner) rootView.findViewById(R.id.spinner_redPenPorc)).getSelectedItemPosition()) * 5;
                    esp[Base.REDUCCIÓN_DE_DAÑO_PLENO] = (((Spinner) rootView.findViewById(R.id.spinner_redDañoPleno)).getSelectedItemPosition()) * 5;
                    esp[Base.REDUCCIÓN_DE_DAÑO_PORCENTUAL] = (((Spinner) rootView.findViewById(R.id.spinner_redDañoPorc)).getSelectedItemPosition()) * 5;

                    pj.setEspeciales(esp);

                    principal.reemplazarPj(pj);

                    principal.actualizarVistazo();

                    dismiss();
                }
            });


            button_cancelar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            return rootView;
        }
    }

    private static int indexPenalizador(int pen) {

        int index = 20 - (pen / 5);

        return index;
    }


}