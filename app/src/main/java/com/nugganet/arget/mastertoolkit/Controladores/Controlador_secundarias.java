package com.nugganet.arget.mastertoolkit.Controladores;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.nugganet.arget.mastertoolkit.Base;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Arma;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Personaje;
import com.nugganet.arget.mastertoolkit.Clases_objetos.TriplaVectores;
import com.nugganet.arget.mastertoolkit.Custom_adapters.CustomAdapter_TiradaSecundaria;
import com.nugganet.arget.mastertoolkit.Principal;
import com.nugganet.arget.mastertoolkit.R;

import java.util.ArrayList;

import Utils.DbBitmapUtility;

public class Controlador_secundarias extends BaseAdapter {

    Principal p;

    private static LayoutInflater inflater = null;
    private AutoCompleteTextView autoc;

    Turnos t;


    public Controlador_secundarias(Principal p) {
        this.p = p;

    }

    public void init() {
        inflater = (LayoutInflater) this.p.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        autoc = p.findViewById(R.id.autoCompleteTextView_habilidades_Secundarias);

        /*

        t = new Turnos((ArrayList<Personaje>) this.p.getDatos().Array_Personajes_En_Mision().clone());

        t.calcularTurnos(true);

        t.Añadir_NPC("Fin de la ronda", -999999);*/
    }


    @Override
    public int getCount() {
        return t.pj_full.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder {
        TextView TextView_NombrePj;
        TextView TextView_ResultadoTirada;
        TextView TextView_Arma_usada;
        ImageView ImageView_token;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Holder holder = new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.list_turno, null, true);

        holder.TextView_NombrePj = (TextView) rowView.findViewById(R.id.TextView_NombrePj);
        holder.TextView_ResultadoTirada = (TextView) rowView.findViewById(R.id.TextView_ResultadoTirada);
        holder.TextView_Arma_usada = (TextView) rowView.findViewById(R.id.TextView_Arma_usada);
        holder.ImageView_token = (ImageView) rowView.findViewById(R.id.ImageView_token);

        if (t.fin_De_ronda(position)) {
            holder.ImageView_token.setVisibility(View.GONE);
            holder.TextView_ResultadoTirada.setVisibility(View.INVISIBLE);
            holder.TextView_Arma_usada.setVisibility(View.INVISIBLE);

            holder.TextView_NombrePj.setText(t.Nombre(position));
            holder.TextView_NombrePj.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);


        } else {

            holder.ImageView_token.setImageDrawable(new BitmapDrawable(p.getResources(), t.Token(position)));

            holder.TextView_NombrePj.setText(t.Nombre(position));
            holder.TextView_ResultadoTirada.setText(t.Resultado(position));

            if (t.turno_final(0) > t.turno_final(position) + 150) {
                holder.TextView_Arma_usada.setText("¡El personaje está sorprendido!");
            } else {
                holder.TextView_Arma_usada.setText("");
            }


        }

        rowView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                t.Quitar_NPC(position);
                return true;
            }
        });

        return rowView;
    }

    private void actualizarArray() {
        ((ListView) p.findViewById(R.id.List_view_Iniciativas)).setAdapter(this);
    }

    public void Calcular_turnos() {
        t.calcularTurnos(true);
        t.Ordenar(false);
        actualizarArray();
    }

    public Turnos turnos() {
        return t;
    }


    public void Controlador_botones(int button_id) {
        boolean ascendente = ((ToggleButton) p.findViewById(R.id.toggleButton_orden)).isChecked();
        switch (button_id) {
            case R.id.button_turno_siguiente:
                boolean calculo_todos = ((ToggleButton) p.findViewById(R.id.toggleButton_actualizar_ronda)).isChecked();
                t.Siguiente(calculo_todos, ascendente);
                break;
            case R.id.button_turno_anterior:
                t.Anterior();
                break;
            case R.id.Button_calcular_iniciativa:
                t.calcularTurnos(true);
                t.Ordenar(ascendente);
                actualizarArray();
                break;

        }
    }

    private class Turnos {
        private ArrayList<Personaje> pj_full;
        private ArrayList<Personaje> pj_lite = new ArrayList<>();
        private ArrayList<Integer> modificadores;
        private int cantidad;

        public Bitmap Token(int pos) {
            return DbBitmapUtility.getImage(pj_full.get(pos).getToken());
        }

        public Turnos(ArrayList<Personaje> pj_lite) {

            this.pj_lite.addAll(pj_lite);

            cantidad = pj_lite.size() - 1;

            pj_full = new ArrayList<>();

            modificadores = new ArrayList<>();

            for (Personaje pj : pj_lite) {
                modificadores.add(0);
                pj_full.add(pj);
            }

        }

        public boolean fin_De_ronda(int pos) {
            try {
                if (pj_full.get(pos).getCaracteristicas(Base.HABILIDAD_TURNO) == -999999) {
                    return true;
                } else {
                    return false;
                }
            } catch (java.lang.IndexOutOfBoundsException e) {
                e.printStackTrace();
            }
            return false;
        }

        public void calcularTurnos(boolean tirada) {

            int última_tirada = 0;
            int penalizador_arma = 0;
            int habilidad_base = 0;

            for (int i = 0; i < cantidad; i++) {
                try {


                    if (pj_full.get(i).getCaracteristicas(Base.HABILIDAD_TURNO) == -999999) {

                        pj_lite.get(i).setÚltimo_turno_final(-999999);

                    } else {
                        if (tirada) {

                            última_tirada = pj_full.get(i).tirada();
                            penalizador_arma = pj_full.get(i).arma(pj_lite.get(i).getIndex_última_arma_usada()).getCaracteristicas()[Arma.TURNO_ARMA];
                            habilidad_base = pj_full.get(i).getCaracteristicas(Base.HABILIDAD_TURNO);

                            pj_lite.get(i).setÚltimo_turno_final(última_tirada + penalizador_arma + habilidad_base + modificadores.get(i));


                        } else {

                            última_tirada = pj_lite.get(i).getÚltimas_tiradas()[Base.HABILIDAD_TURNO];
                            penalizador_arma = pj_full.get(i).arma(pj_lite.get(i).getIndex_última_arma_usada()).getCaracteristicas()[Arma.TURNO_ARMA];
                            habilidad_base = pj_full.get(i).getCaracteristicas(Base.HABILIDAD_TURNO);

                            pj_lite.get(i).setÚltimo_turno_final(última_tirada + penalizador_arma + habilidad_base + modificadores.get(i));
                        }
                    }
                } catch (java.lang.NullPointerException e) {
                    e.printStackTrace();

                }
            }
        }

        public void Siguiente(boolean recalcular_turnos, boolean ascendente) {

            Personaje aux_lite = pj_lite.get(0);
            Personaje aux_full = pj_full.get(0);
            Integer aux_modificador = modificadores.get(0);

            for (int i = 0; i < cantidad; i++) {

                pj_lite.set(i, pj_lite.get(i + 1));
                pj_full.set(i, pj_full.get(i + 1));
                modificadores.set(i, modificadores.get(i + 1));

                if (i == cantidad - 1) {

                    pj_lite.set(cantidad, aux_lite);
                    pj_full.set(cantidad, aux_full);
                    modificadores.set(cantidad, aux_modificador);

                }
            }


            if (recalcular_turnos && fin_De_ronda(cantidad)) {
                calcularTurnos(true);
                Ordenar(ascendente);
            }

            if (pj_lite.get(0).getID() != -1) {

                int pj_actual_index = pj_lite.get(0).getID();

                ((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_asaltante)).setSelection(pj_actual_index);

            }

            actualizarArray();

        }

        public void Anterior() {

            Personaje aux_lite = pj_lite.get(cantidad);
            Personaje aux_full = pj_full.get(cantidad);
            Integer aux_modificador = modificadores.get(cantidad);

            for (int i = cantidad; i > 0; i--) {

                pj_lite.set(i, pj_lite.get(i - 1));
                pj_full.set(i, pj_full.get(i - 1));
                modificadores.set(i, modificadores.get(i - 1));

                if (i == 0 + 1) {

                    pj_lite.set(0, aux_lite);
                    pj_full.set(0, aux_full);
                    modificadores.set(0, aux_modificador);

                }
            }

            if (pj_lite.get(0).getID() != -1) {

                int pj_actual_index = pj_lite.get(0).getID();

                ((Spinner) p.findViewById(R.id.spinner_Combate_Jugador_asaltante)).setSelection(pj_actual_index);

            }

            actualizarArray();

        }

        public void Ordenar(boolean ascendente) {

            if (ascendente) {
                quickSortInAscendingOrder(0, cantidad);

                Siguiente(false, ascendente);

            } else {
                quickSortInDescendingOrder(0, cantidad);
            }
            actualizarArray();
        }

        private void quickSortInDescendingOrder(int low, int high) {

            int i = low;
            int j = high;

            Personaje aux_lite;
            Personaje aux_full;
            Integer aux_modificador;

            int middle = turno_final((low + high) / 2);

            while (i < j) {
                while (turno_final(i) > middle) {
                    i++;
                }
                while (turno_final(j) < middle) {
                    j--;
                }
                if (j >= i) {

                    aux_lite = pj_lite.get(i);
                    aux_full = pj_full.get(i);
                    aux_modificador = modificadores.get(i);

                    pj_lite.set(i, pj_lite.get(j));
                    pj_full.set(i, pj_full.get(j));
                    modificadores.set(i, modificadores.get(j));

                    pj_lite.set(j, aux_lite);
                    pj_full.set(j, aux_full);
                    modificadores.set(j, aux_modificador);

                    i++;
                    j--;
                }
            }


            if (low < j) {
                quickSortInDescendingOrder(low, j);
            }
            if (i < high) {
                quickSortInDescendingOrder(i, high);
            }
        }

        private void quickSortInAscendingOrder(int low, int high) {

            int i = low;
            int j = high;

            Personaje aux_lite;
            Personaje aux_full;
            Integer aux_modificador;

            int middle = turno_final((low + high) / 2);

            while (i < j) {
                while (turno_final(i) < middle) {
                    i++;
                }
                while (turno_final(j) > middle) {
                    j--;
                }
                if (i <= j) {
                    aux_lite = pj_lite.get(i);
                    aux_full = pj_full.get(i);
                    aux_modificador = modificadores.get(i);

                    pj_lite.set(i, pj_lite.get(j));
                    pj_full.set(i, pj_full.get(j));
                    modificadores.set(i, modificadores.get(j));

                    pj_lite.set(j, aux_lite);
                    pj_full.set(j, aux_full);
                    modificadores.set(j, aux_modificador);


                    i++;
                    j--;
                }
            }


            if (low < j) {
                quickSortInAscendingOrder(low, j);
            }
            if (i < high) {
                quickSortInAscendingOrder(i, high);
            }


        }


        public int turno_final(int pos) {
            return pj_lite.get(pos).getÚltimo_turno_final();
        }

        public String Nombre(int pos) {
            return pj_lite.get(pos).getNombre_personaje();
        }

        public String Resultado(int pos) {
            return "Resultado " + turno_final(pos) + " utilizando: " + pj_full.get(pos).arma(pj_lite.get(pos).getIndex_última_arma_usada()).getNombre() + ".";
        }

        public void Cambiar_modificador(int num, int nuevo) {

            this.modificadores.set(num, nuevo);

        }

        public void Añadir_NPC(String nombre, int turno) {
            modificadores.add(0);
            Personaje temp = new Personaje();

            temp.caracteristicasSum(Base.HABILIDAD_TURNO, turno);

            pj_full.add(temp);

            pj_lite.add(new Personaje(-1, nombre, nombre));

            cantidad = cantidad + 1;

            actualizarArray();

        }

        public void Quitar_NPC(int position) {
            if (pj_lite.get(position).getID() == -1) {
                pj_lite.remove(position);
                pj_full.remove(position);
                modificadores.remove(position);
                cantidad = cantidad - 1;
                actualizarArray();
            }


        }


    }

   

    public Turnos getT() {
        return t;
    }


    public void controlador_speech(String result) {

        boolean sumando = false;
        boolean restando = false;
        boolean ha_Dicho_muy = false;
        boolean ha_Dicho_casi = false;
        Integer base = null;

        ((EditText) p.findViewById(R.id.autoCompleteTextView_habilidades_Secundarias)).setText("");

        for (String str : result.split(" ")) {

            Integer ultimaCaptura = p.SpokenTextToInt(str);

            if (ultimaCaptura != null) {
                if (base == null) {
                    base = 0;
                }

                if (sumando) {

                    base = base + ultimaCaptura;

                } else if (restando) {

                    base = base - ultimaCaptura;

                } else {

                    base = ultimaCaptura;

                }
                sumando = false;
                restando = false;

            } else {
                System.out.println("Cadena: " + str);
                if (str.contains("+")) {
                    sumando = true;
                } else if (str.contains("-")) {
                    restando = true;
                    sumando = false;
                } else if (str.toLowerCase().contains("rutinario")) {
                    base = 20;
                } else if (str.toLowerCase().contains("facil") | str.toLowerCase().contains("fácil")) {
                    base = 40;
                } else if (str.toLowerCase().contains("media")) {
                    base = 80;
                } else if (str.toLowerCase().contains("dificil") | str.toLowerCase().contains("difícil")) {
                    if (ha_Dicho_muy) {
                        base = 140;
                    } else {
                        base = 120;
                    }
                } else if (str.toLowerCase().contains("absurdo")) {
                    base = 180;
                } else if (str.toLowerCase().contains("imposible")) {
                    if (ha_Dicho_casi) {
                        ha_Dicho_casi = false;
                        base = 240;
                    } else {
                        base = 280;
                    }
                } else if (str.toLowerCase().contains("inhumano")) {
                    base = 320;
                } else if (str.toLowerCase().contains("zen")) {
                    base = 440;
                } else if (str.toLowerCase().contains("casi")) {
                    ha_Dicho_casi = true;
                } else if (str.toLowerCase().contains("muy")) {
                    ha_Dicho_muy = true;

                } else {


                    autoc.setText(str.substring(0, str.length() - (str.length() / 3)));

                    autoc.showDropDown();

                }
            }
        }

        if (base != null) {

            if (p.getDatos().Array_Personajes_En_Mision() != null & p.getDatos().Array_Personajes_En_Mision().size() != 0) {

                int caracteristica = ((Spinner) p.findViewById(R.id.spinner_habilidad_secundaria)).getSelectedItemPosition();
                TriplaVectores resultados = Base.dificultades(p.getDatos().Array_Personajes_En_Mision(), p.getDatos().ArrayPersonajes(), caracteristica, base, true);
                resultados.ordenar();

                int tipo = p.getResources().getIntArray(R.array.Tipos_secundarias)[caracteristica];

                CustomAdapter_TiradaSecundaria adapter = new CustomAdapter_TiradaSecundaria(p, resultados.getNombres(), resultados.getNumeros(), resultados.getResultados(), tipo, resultados.getTiradas(), true);
                ((ListView) p.findViewById(R.id.ListView_Tiradas)).setAdapter(adapter);

                ((TextView) p.findViewById(R.id.textView_desc_secundaria)).setText("Los personajes intentan superar una dificultad de " + base + " (" + resultToString(base) + ") en " + ((Spinner) p.findViewById(R.id.spinner_habilidad_secundaria)).getSelectedItem().toString());
            }
        }

        runnable_act_dropdown(base);

    }

    private void runnable_act_dropdown(final Integer base) {

        autoc.postDelayed(new Runnable() {
            @Override
            public void run() {

                System.out.println("Base: " + base);

                if (autoc.getAdapter().getCount() == 1) {

                    autoc.setText(autoc.getAdapter().getItem(0).toString());

                    String seleccionado = ((AutoCompleteTextView) p.findViewById(R.id.autoCompleteTextView_habilidades_Secundarias)).getText().toString();

                    for (int i = 0; i < p.getResources().getStringArray(R.array.Secundarias).length; i++) {

                        if (seleccionado.equalsIgnoreCase(p.getResources().getStringArray(R.array.Secundarias)[i])) {
                            ((Spinner) p.findViewById(R.id.spinner_habilidad_secundaria)).setSelection(i);
                            break;
                        }
                    }

                    ((AutoCompleteTextView) p.findViewById(R.id.autoCompleteTextView_habilidades_Secundarias)).setText("");

                    autoc.dismissDropDown();


                    if (base != null) {

                        if (p.getDatos().Array_Personajes_En_Mision() != null & p.getDatos().Array_Personajes_En_Mision().size() != 0) {

                            autoc.postDelayed(new Runnable() {

                                @Override
                                public void run() {
                                    int caracteristica = ((Spinner) p.findViewById(R.id.spinner_habilidad_secundaria)).getSelectedItemPosition();
                                    TriplaVectores resultados = Base.dificultades(p.getDatos().Array_Personajes_En_Mision(), p.getDatos().ArrayPersonajes(), caracteristica, base, true);
                                    resultados.ordenar();

                                    int tipo = p.getResources().getIntArray(R.array.Tipos_secundarias)[caracteristica];

                                    CustomAdapter_TiradaSecundaria adapter = new CustomAdapter_TiradaSecundaria(p, resultados.getNombres(), resultados.getNumeros(), resultados.getResultados(), tipo, resultados.getTiradas(), true);
                                    ((ListView) p.findViewById(R.id.ListView_Tiradas)).setAdapter(adapter);

                                    ((TextView) p.findViewById(R.id.textView_desc_secundaria)).setText("Los personajes intentan superar una dificultad de " + base + " (" + resultToString(base) + ") en " + ((Spinner) p.findViewById(R.id.spinner_habilidad_secundaria)).getSelectedItem().toString());

                                }
                            }, 50);
                        }

                    } else {
                        ((TextView) p.findViewById(R.id.textView_desc_secundaria)).setText("Los personajes realizan todos tiradas de " + ((Spinner) p.findViewById(R.id.spinner_habilidad_secundaria)).getSelectedItem().toString());
                    }


                }
            }
        }, 50);
    }

    private String resultToString(int result) {

        if (result < 20) {
            return "Pifia";
        }
        if (result < 40) {
            return "Rutinario";
        }
        if (result < 80) {
            return "Fácil";
        }
        if (result < 120) {
            return "Media";
        }
        if (result < 140) {
            return "Difícil";
        }
        if (result < 180) {
            return "Muy difícil";
        }
        if (result < 240) {
            return "Absurdo";
        }
        if (result < 280) {
            return "Casi imposible";
        }
        if (result < 320) {
            return "imposible";
        }
        if (result < 440) {
            return "inhumano";
        }
        return "zen";
    }

}