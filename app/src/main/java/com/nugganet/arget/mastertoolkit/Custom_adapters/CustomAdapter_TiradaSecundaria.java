package com.nugganet.arget.mastertoolkit.Custom_adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.nugganet.arget.mastertoolkit.Principal;
import com.nugganet.arget.mastertoolkit.R;
import com.nugganet.arget.mastertoolkit.WebBuilder;

public class CustomAdapter_TiradaSecundaria extends BaseAdapter {
    private Integer[] resultados;
    private Integer[] tiradas;
    private String[] nombres;
    private String[] descripciones;
    private int tipo;
    private boolean dificultad;

    Principal context;
    private static LayoutInflater inflater = null;


    public CustomAdapter_TiradaSecundaria(Principal mainActivity, String[] nombres, Integer[] resultados, String[] descripciones, int tipo, Integer[] tiradas, boolean dificultad) {

        this.nombres = nombres;
        this.resultados = resultados;
        this.descripciones = descripciones;
        this.tipo = tipo;
        this.tiradas = tiradas;
        context = mainActivity;
        this.dificultad = dificultad;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {

        return nombres.length;
    }

    @Override
    public Object getItem(int position) {

        return position;
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    public class Holder {
        TextView NombrePj;
        TextView ResultadoTirada;
        TextView DificultadSuperada;
        TextView textView_Descripción_Tirada;
        LinearLayout Layout_tirada;
        ImageView imagen_tirada;
        int habilidad = 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.list_tiradas, null, true);

        holder.imagen_tirada = (ImageView) rowView.findViewById(R.id.imageView_tirada_sec);

        holder.NombrePj = (TextView) rowView.findViewById(R.id.NombrePj);
        holder.ResultadoTirada = (TextView) rowView.findViewById(R.id.ResultadoTirada);
        holder.DificultadSuperada = (TextView) rowView.findViewById(R.id.DificultadSuperada);
        holder.textView_Descripción_Tirada = (TextView) rowView.findViewById(R.id.textView_Descripción_Tirada);
        holder.Layout_tirada = (LinearLayout) rowView.findViewById(R.id.Layout_tirada);


        switch (tipo) {
            case 0:// tirada común
                holder.NombrePj.setText(nombres[position]);
                holder.ResultadoTirada.setText(String.valueOf(resultados[position]));
                holder.DificultadSuperada.setText(resultToString(resultados[position]));
                holder.textView_Descripción_Tirada.setText((descripciones[position]));
                holder.imagen_tirada.setImageDrawable(setImage((int) (tiradas[position] / 5)));

                if (dificultad) {
                    holder.ResultadoTirada.setText("D100: " + String.valueOf(tiradas[position]));
                    holder.DificultadSuperada.setText("D20: " + String.valueOf(tiradas[position] / 5));
                    holder.imagen_tirada.setVisibility(View.GONE);
                }

                break;
            case 1: //atributo
                holder.NombrePj.setText(nombres[position]);
                holder.ResultadoTirada.setText(String.valueOf(resultados[position]));
                holder.DificultadSuperada.setVisibility(View.GONE);
                holder.textView_Descripción_Tirada.setText((descripciones[position]));
                holder.imagen_tirada.setImageDrawable(setImage((int) (tiradas[position] * 2)));

                if (dificultad) {
                    holder.ResultadoTirada.setText("D10: " + String.valueOf(tiradas[position]));
                    holder.DificultadSuperada.setText("D20: " + String.valueOf(tiradas[position] * 2));
                    holder.DificultadSuperada.setVisibility(View.VISIBLE);
                    holder.imagen_tirada.setVisibility(View.GONE);
                }

                break;
            case 2://iniciativa
                holder.imagen_tirada.setVisibility(View.GONE);
                holder.NombrePj.setText(nombres[position]);
                holder.ResultadoTirada.setText(String.valueOf(resultados[position]));
                holder.DificultadSuperada.setVisibility(View.GONE);
                holder.textView_Descripción_Tirada.setText((descripciones[position]));
                holder.imagen_tirada.setImageDrawable(setImage((int) (tiradas[position] / 5)));
                break;

        }

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sec = "";
                try {
                    sec = ((Spinner) context.findViewById(R.id.spinner_habilidad_secundaria)).getSelectedItem().toString();
                } catch (java.lang.NullPointerException e) {

                }

                context.getHistorial().appendLog(WebBuilder.nombres(nombres[position]) + " ha lanzado " + WebBuilder.habilidad_usada(sec), "tipo: Tirada secundaria.<br>"+descripciones[position]+"<br>"+WebBuilder.formateo_tirada(sec,resultados[position]-tiradas[position],tiradas[position]) );

                Snackbar snackbar1 = Snackbar.make(v, "Guardado en el log: "+nombres[position] + " ha lanzado " + sec, Snackbar.LENGTH_SHORT);

                View viewSb = snackbar1.getView();

                TextView tv = (TextView) viewSb.findViewById(com.google.android.material.R.id.snackbar_text);
                tv.setTextColor(context.getResources().getColor(R.color.colorWhite));

                snackbar1.show();

            }
        });


        return rowView;
    }

    private Drawable setImage(int tirada) {

        switch (tirada) {
            case 1:
                return (context.getResources().getDrawable(R.drawable.dado_1));

            case 2:
                return (context.getResources().getDrawable(R.drawable.dado_2));

            case 3:
                return (context.getResources().getDrawable(R.drawable.dado_3));

            case 4:
                return (context.getResources().getDrawable(R.drawable.dado_4));

            case 5:
                return (context.getResources().getDrawable(R.drawable.dado_5));

            case 6:
                return (context.getResources().getDrawable(R.drawable.dado_6));

            case 7:
                return (context.getResources().getDrawable(R.drawable.dado_7));

            case 8:
                return (context.getResources().getDrawable(R.drawable.dado_8));

            case 9:
                return (context.getResources().getDrawable(R.drawable.dado_9));

            case 10:
                return (context.getResources().getDrawable(R.drawable.dado_10));

            case 11:
                return (context.getResources().getDrawable(R.drawable.dado_11));

            case 12:
                return (context.getResources().getDrawable(R.drawable.dado_12));

            case 13:
                return (context.getResources().getDrawable(R.drawable.dado_13));

            case 14:
                return (context.getResources().getDrawable(R.drawable.dado_14));

            case 15:
                return (context.getResources().getDrawable(R.drawable.dado_15));

            case 16:
                return (context.getResources().getDrawable(R.drawable.dado_16));

            case 17:
                return (context.getResources().getDrawable(R.drawable.dado_17));

            case 18:
                return (context.getResources().getDrawable(R.drawable.dado_18));

            case 19:
                return (context.getResources().getDrawable(R.drawable.dado_19));

            case 20:
                return (context.getResources().getDrawable(R.drawable.dado_20));

        }
        if (tirada > 20) {
            return (context.getResources().getDrawable(R.drawable.dado_20));
        } else {
            return (context.getResources().getDrawable(R.drawable.dado_1));
        }
    }

    private String resultToString(int result) {

        if (result < 20) {
            return "Pifia";
        }
        if (result < 40) {
            return "Rutinario";
        }
        if (result < 80) {
            return "Fácil";
        }
        if (result < 120) {
            return "Media";
        }
        if (result < 140) {
            return "Difícil";
        }
        if (result < 180) {
            return "Muy difícil";
        }
        if (result < 240) {
            return "Absurdo";
        }
        if (result < 280) {
            return "Casi imposible";
        }
        if (result < 320) {
            return "imposible";
        }
        if (result < 440) {
            return "inhumano";
        }
        return "zen";
    }


}