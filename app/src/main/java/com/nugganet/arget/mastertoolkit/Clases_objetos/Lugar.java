package com.nugganet.arget.mastertoolkit.Clases_objetos;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Arrays;

import Utils.BaseDeDatos;

/**
 * Created by Arget on 21/3/2018.
 */

public class Lugar {
    private String nombre;
    private String descripción;
    private String padre;
    private int id;

    public String toJson(){
        return new GsonBuilder().create().toJson(this,this.getClass());
    }

    public Lugar fromJson(String json){
        return new Gson().fromJson(json,this.getClass());
    }

    public Lugar(String nombre, String descripción, String padre) {
        this.nombre = nombre;
        this.descripción = descripción;
        this.padre = padre;
    }

    public Lugar() {
    }

    public Lugar(String nombre, String descripción, String padre, int id) {
        this.nombre = nombre;
        this.descripción = descripción;
        this.padre = padre;
        this.id = id;
    }

    public Lugar(Lugar nuevo) {
        this.nombre = nuevo.getNombre();
        this.descripción = nuevo.getDescripción();
        this.padre = nuevo.getPadre();
        this.id = nuevo.getId();
    }

    public Lugar(String nuevo) {

        String[] nuevo_lugar = BaseDeDatos.derialize(nuevo);

        System.out.println(Arrays.toString(nuevo_lugar));

        this.nombre = nuevo_lugar[0];
        this.descripción = nuevo_lugar[1];
        this.padre = nuevo_lugar[2];
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripción() {
        return descripción;
    }

    public void setDescripción(String descripción) {
        this.descripción = descripción;
    }

    public String getPadre() {
        return padre;
    }

    public void setPadre(String padre) {
        this.padre = padre;
    }

    public void reemplazar(Lugar lugar){
        this.nombre = lugar.getNombre();
        this.descripción = lugar.getDescripción();
        this.padre = lugar.getPadre();
    }

    @Override
    public String toString() {
        return nombre;
    }

}
