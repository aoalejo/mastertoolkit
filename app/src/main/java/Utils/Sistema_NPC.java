package Utils;

import com.nugganet.arget.mastertoolkit.Base;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Arma;

import java.util.ArrayList;

/**
 * Created by Arget on 10/5/2017.
 */

public class Sistema_NPC {
    private ArrayList<Integer> IDS_ARMAS= new ArrayList<>();
    private ArrayList<Arma> Armas = new ArrayList<>();

    public Arma indexArma(int armaID){
        return Armas.get(IDS_ARMAS.indexOf(armaID));
    }


    public static int indiceGeneral(int indice){
        if(Base.inRangeEq(0,7,indice))return 0;
        if(Base.inRangeEq(8,12,indice))return 1;
        if(Base.inRangeEq(13,999,indice))return 2;
        return -1;
    }

    public static int indiceEspecifico(int indice){
        if(Base.inRangeEq(0,7,indice))return indice - 0;
        if(Base.inRangeEq(8,12,indice))return indice - 8;
        if(Base.inRangeEq(13,999,indice))return indice - 13;
        return -1;
    }

    public static String descrpción(Integer[] valores) {

        //0 = indice general
        //1 = indice especifico
        //2 = tirada
        //3 = Habilidad
        //4 = atributo
        // siguientes, datos extra que se necesiten según la situación
        int resultado = 0;

        int dificultadSuperada = 0;

        int tipoMovim = 0;
        int velocidad_normal = 0;
        int velocidad_maxima = 0;

        int fuerza = 0;
        int carga_normal = 0;
        int carga_maxima = 0;
        int carga_movimiento = 0;

        int bono = 0;

        String texto = "";


        switch (valores[0]) {
            case 0://Atributos
                if (valores[1] > 999) {
                    return "excede el tamaño del vector";
                } else {
                    resultado = valores[2] + valores[3];
                    switch (Base.ControlCaracteristica(resultado)) {
                        case 0:
                            return "No supera ni la dificultad 'facil'...";

                        case 1:
                            return "Logra solamente cosas que un campesino lograría.";

                        case 2:
                            return "Logra cosas que alguien un poco entrenado lograría.";

                        case 3:
                            return "Logra solamente cosas que una persona entrenada podría hacer.";

                        case 4:
                            return "Supera casi cualquier eventualidad, cosas extremas.";
                    }
                }
                break;
            case 1://Resistencias
                if (valores[1] > 999) {
                    return "excede el tamaño del vector";
                } else {
                    resultado = valores[3] + valores[2];
                    switch (valores[1]) {
                       /*
                        private final static int RF=0;
                        private final static int RE=1;
                        private final static int RV=2;
                        private final static int RM=3;
                        private final static int RP=4;
                        private final static int Presencia=5;
                        */
                        case 0://RF
                            return "Supera una Resistencia Física contra " + resultado + ".";
                        case 1://RF
                            return "Supera una Resistencia a Enfermedades contra " + resultado + ".";
                        case 2://RF
                            return "Supera una Resistencia a Venenos contra " + resultado + ".";
                        case 3://RF
                            return "Supera una Resistencia Mágica contra " + resultado + ".";
                        case 4://RF
                            return "Supera una Resistencia Psiquica contra " + resultado + ".";
                        case 5://RF
                            return "Supera una Presencia contra " + resultado + ".";
                    }
                }
                break;
            case 2://Secundarias y combate
                if (valores[1] > 999) {
                    return "excede el tamaño del vector";
                } else {
                    resultado = valores[3] + valores[2];
                    switch (valores[1]) {
                        case 0:// ataque
                            return "Resultado del ataque: " + resultado + ".";
                        case 1:// parada
                            return "Resultado de la parada: " + resultado + ".";
                        case 2:// esquiva
                            return "Resultado de la esquiva: " + resultado + ".";
                        case 3:// armadura

                            return "Resultado de llevar armadura (????????): " + resultado + ".";
                        case 4:// turno

                            return "Resultado de iniciativa: " + resultado + ".";
                        case 5:// presiciónmagica

                            switch (Base.ControlHabilidad(resultado)){
                                case 0:
                                    texto = "0";
                                    break;
                                case 1:
                                    texto = "5";
                                    break;
                                case 2:
                                    texto = "25";
                                    break;
                                case 3:
                                    texto = "100";
                                    break;
                                case 4:
                                    texto = "250";
                                    break;
                                case 5:
                                    texto = "500";
                                    break;
                                case 6:
                                    texto = "1000 (necesita localizar exactamente el objetivo)";
                                    break;
                                case 7:
                                    texto = "5000 (solo necesita saber su ubicación aproximada)";
                                    break;
                            }

                            return "Resultado de la presición mágica: " + resultado + ", logrando a una distancia de hasta "+ texto +" metros.";


                        case 6:// presiciónpsiquica
                            switch (Base.ControlHabilidad(resultado)){
                                case 0:
                                    texto = "0";
                                    break;
                                case 1:
                                    texto = "5";
                                    break;
                                case 2:
                                    texto = "25";
                                    break;
                                case 3:
                                    texto = "100";
                                    break;
                                case 4:
                                    texto = "250";
                                    break;
                                case 5:
                                    texto = "500";
                                    break;
                                case 6:
                                    texto = "1000 (necesita localizar exactamente el objetivo)";
                                    break;
                                case 7:
                                    texto = "5000 (solo necesita saber su ubicación aproximada)";
                                    break;
                            }

                            return "Resultado de la presición psiquica: " + resultado + ", logrando a una distancia de hasta "+ texto +" metros.";

                        case 7:// acrobacias

                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";

                        case 8:// atletismo

                            tipoMovim = 0;

                            dificultadSuperada = (Base.ControlHabilidad(resultado));

                            if (dificultadSuperada > 5) {
                                tipoMovim = 1;
                            }
                            if (dificultadSuperada > 7) {
                                tipoMovim = 2;
                            }
                            if (dificultadSuperada > 9) {
                                tipoMovim = 3;
                            }


                            tipoMovim = tipoMovim + valores[4];

                            velocidad_normal = Base.Velocidad(tipoMovim);
                            velocidad_maxima = (int) (velocidad_normal * 0.6);


                            String[] tabla = {"no puede moverse", "Corriendo (can) 1 Minuto(vel:" + velocidad_normal + " m/t).\r\nMov. Maximo (2 can) 1 asalto(vel:" + velocidad_maxima + " m/t).", "Corriendo (can) 5 minutos(vel:" + velocidad_normal + " m/t).\r\nMov. Maximo (2 can) 2 asaltos(vel:" + velocidad_maxima + " m/t).", "Corriendo (can) 10 minutos(vel:" + velocidad_normal + " m/t).\r\nMov. Maximo (2 can) 5 asaltos(vel:" + velocidad_maxima + " m/t).", "Corriendo (can) 20 minutos(vel:" + velocidad_normal + " m/t).\r\nMov. Maximo (2 can) 10 asaltos(vel:" + velocidad_maxima + " m/t).", "Corriendo (can) 40 minutos(vel:" + velocidad_normal + " m/t).\r\nMov. Maximo (2 can) 15 asaltos(vel:" + velocidad_maxima + " m/t).", "Corriendo (can) 1 hora(vel:" + velocidad_normal + " m/t).\r\nMov. Maximo (2 can) 1 minuto(vel:" + velocidad_maxima + " m/t).", "Corriendo (can) 2 horas(vel:" + velocidad_normal + " m/t).\r\nMov. Maximo (2 can) 2 minutos(vel:" + velocidad_maxima + " m/t).", "Corriendo (can) 5 horas(vel:" + velocidad_normal + " m/t).\r\nMov. Maximo (2 can) 3 minutos(vel:" + velocidad_maxima + " m/t).", "Corriendo (can) 1 día(vel:" + velocidad_normal + " m/t).\r\nMov. Maximo (2 can) 5 minutos(vel:" + velocidad_maxima + " m/t).", "Corriendo (can) 2 dias(vel:" + velocidad_normal + " m/t).\r\nMov. Maximo (2 can) 10 minutos(vel:" + velocidad_maxima + " m/t).", "Corriendo (can) 5 día(vel:" + velocidad_normal + " m/t).\r\nMov. Maximo (2 can) 20 minutos(vel:" + velocidad_maxima + " m/t)."};

                            return tabla[dificultadSuperada];

                        case 9:// montar

                            return "Cualquier habilidad podrá usar como maximo este valor " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 10:// nadar

                            dificultadSuperada = (Base.ControlHabilidad(resultado));

                            switch (dificultadSuperada) {
                                case 0:
                                    tipoMovim = -8;
                                    break;
                                case 1:
                                    tipoMovim = -5;
                                    break;
                                case 2:
                                    tipoMovim = -4;
                                    break;
                                case 3:
                                    tipoMovim = -3;
                                    break;
                                case 4:
                                    tipoMovim = -3;
                                    break;
                                case 5:
                                    tipoMovim = -2;
                                    break;
                                case 6:
                                    tipoMovim = -2;
                                    break;
                                case 7:
                                    tipoMovim = -1;
                                    break;
                                case 8:
                                    tipoMovim = -0;
                                    break;
                                case 9:
                                    tipoMovim = -0;
                                    break;
                                case 10:
                                    tipoMovim = -0;
                                    break;
                            }



                            tipoMovim = tipoMovim +valores[4];

                            if (tipoMovim < 1) {
                                return "el personaje se hunde";
                            } else {
                                return "velocidad:" + Base.Velocidad(tipoMovim) + " m/t";
                            }

                        case 11:// trepar
                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 12:// saltar

                            dificultadSuperada = (Base.ControlHabilidad(resultado));
                            switch (dificultadSuperada) {
                                case 0:
                                    tipoMovim = -1;
                                    break;
                                case 1:
                                    tipoMovim = -0;
                                    break;
                                case 2:
                                    tipoMovim = -0;
                                    break;
                                case 3:
                                    tipoMovim = 1;
                                    break;
                                case 4:
                                    tipoMovim = 1;
                                    break;
                                case 5:
                                    tipoMovim = 2;
                                    break;
                                case 6:
                                    tipoMovim = 2;
                                    break;
                                case 7:
                                    tipoMovim = 3;
                                    break;
                                case 8:
                                    tipoMovim = 4;
                                    break;
                                case 9:
                                    tipoMovim = 5;
                                    break;
                                case 10:
                                    tipoMovim = 5;
                                    break;
                            }



                            tipoMovim = tipoMovim + valores[4];


                            if (tipoMovim > 20) {
                                tipoMovim = 20;
                            }

                            return "salta " + Base.Velocidad(tipoMovim) + " metros.";

                        case 13:// estilo

                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 14:// imidar

                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 15:// liderazgo

                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 16:// persuasión

                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 17:// comercio

                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 18:// callejeo

                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 19:// etiqueta

                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 20:// advertir

                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 21:// buscar

                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 22:// rastrear

                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 23:// animales

                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 24:// ciencia

                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 25:// ley

                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 26:// herbolaria

                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 27:// historia

                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 28:// tactica

                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 29:// medicina
                            dificultadSuperada = (Base.ControlHabilidad(resultado));
                            texto = "";
                            switch (dificultadSuperada) {
                                case 0:
                                    texto = "la herida no mejora";
                                    break;
                                case 1:
                                    texto = "la herida no mejora";
                                    break;
                                case 2:
                                    texto = "se detiene la hemorragia";
                                    break;
                                case 3:
                                    texto = "se detiene la hemorragia y estabiliza al objetivo";
                                    break;
                                case 4:
                                    texto = "se detiene la hemorragia, estabiliza al objetivo y cura un 10% del daño";
                                    break;
                                case 5:
                                    texto = "se detiene la hemorragia, estabiliza al objetivo y cura un 20% del daño";
                                    break;
                                case 6:
                                    texto = "se detiene la hemorragia, estabiliza al objetivo y cura un 30% del daño";
                                    break;
                                case 7:
                                    texto = "se detiene la hemorragia, estabiliza al objetivo y cura un 40% del daño";
                                    break;
                                case 8:
                                    texto = "se detiene la hemorragia, estabiliza al objetivo y cura un 50% del daño";
                                    break;
                                case 9:
                                    texto = "se detiene la hemorragia, estabiliza al objetivo y cura un 70% del daño";
                                    break;
                                case 10:
                                    texto = "se detiene la hemorragia, estabiliza al objetivo y cura un 100% del daño";
                                    break;
                            }


                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + "). " + texto;
                        case 30:// memorizar

                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 31:// navegación

                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 32:// ocultismo

                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 33:// tasación
                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 34:// mágica
                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 35:// frialdad
                            dificultadSuperada = (Base.ControlHabilidad(resultado));
                            switch (dificultadSuperada) {
                                case 0:
                                    bono = 5;
                                    break;
                                case 1:
                                    bono = 10;
                                    break;
                                case 2:
                                    bono = 15;
                                    break;
                                case 3:
                                    bono = 20;
                                    break;
                                case 4:
                                    bono = 25;
                                    break;
                                case 5:
                                    bono = 30;
                                    break;
                                case 6:
                                    bono = 35;
                                    break;
                                case 7:
                                    bono = 40;
                                    break;
                                case 8:
                                    bono = 45;
                                    break;
                                case 9:
                                    bono = 50;
                                    break;
                                case 10:
                                    bono = 55;
                                    break;
                            }

                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ") Obtiene un bono de " + bono + " a ataques psiquicos.";


                        case 36:// pfuerza

                            dificultadSuperada = (Base.ControlHabilidad(resultado));
                            switch (dificultadSuperada) {
                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                    bono = 0;
                                    break;
                                case 4:
                                case 5:
                                    bono = 1;
                                    break;
                                case 6:
                                    bono = 2;
                                    break;
                                case 7:
                                    bono = 3;
                                    break;
                                case 8:
                                case 9:
                                case 10:
                                    bono = 4;
                                    break;
                            }

                            fuerza = valores[4];

                            carga_maxima = Base.CargaMax(fuerza);
                            carga_normal = Base.CargaMax(valores[4]);
                            carga_movimiento = Base.Carga(valores[4]);


                            return "El personaje puede cargar " + carga_maxima + "Kg instantaneamente. Habitualmente podría " + carga_normal + "Kg / " + carga_movimiento + "Kg.";
                        case 37:// resdolor
                            dificultadSuperada = (Base.ControlHabilidad(resultado));
                            switch (dificultadSuperada) {
                                case 0:
                                case 1:
                                case 2:
                                    bono = 0;
                                    break;
                                case 3:
                                    bono = 10;
                                    break;
                                case 4:
                                    bono = 20;
                                    break;
                                case 5:
                                    bono = 30;
                                    break;
                                case 6:
                                    bono = 40;
                                    break;
                                case 7:
                                    bono = 50;
                                    break;
                                case 8:
                                    bono = 60;
                                    break;
                                case 9:
                                    bono = 70;
                                    break;
                                case 10:
                                    bono = 80;
                                    break;
                            }

                            return "Los penalizadores por dolor/cansancio se reducen en " + bono;


                        case 38:// cerrajería
                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 39:// disfraz
                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 40:// ocultarse
                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 41:// robo
                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 42:// sigilo
                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 43:// trampería
                            dificultadSuperada = (Base.ControlHabilidad(resultado));
                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + "), creando/desarmando una trampa de nivel " + dificultadSuperada + "0 /" + (dificultadSuperada - 4) + "0 .";
                        case 44:// venenos
                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + "), creando un veneno de nivel " + dificultadSuperada + "0.";
                        case 45:// arte
                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 46:// baile

                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 47:// forja
                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 48:// runas
                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 49:// alquimia
                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 50:// animismo
                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 51:// música
                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";
                        case 52:// tmanos
                            return "Obtiene un resultado de: " + resultado + " (" + Base.DificultadStr(resultado) + ").";

                    }

                }
                break;
        }

        return "";
    }



}
