package Utils;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.text.TextUtils;

import com.nugganet.arget.mastertoolkit.Clases_objetos.Arma;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Armadura;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Historial_DB;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Lugar;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Modificador;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Personaje;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Personaje_old;
import com.nugganet.arget.mastertoolkit.Principal;

import java.util.ArrayList;

/**
 * Created by Arget on 9/5/2017.
 */

public class BaseDeDatos_v2 extends SQLiteOpenHelper {

    public static final String TextNotNull = " TEXT NOT NULL, ";
    public static final String IntegerNotNull = " INTEGER NOT NULL, ";

    private final Principal p;

    public int getDatabaseVersion() {
        return DATABASE_VERSION;
    }

    private static final int DATABASE_VERSION = 5;
    private static final String DATABASE_NAME = "Base_De_Datos";
    public static final String ID_TABLE = "ID";
    public static final String DATOS = "DATOS";


    public static abstract class Personaje_Estructura implements BaseColumns {
        public static final String TABLE_NAME = "PERSONAJE";
        public static final String TOKEN = "TOKEN";
    }

    public static abstract class Arma_Estructura implements BaseColumns {
        public static final String TABLE_NAME = "ARMA";
    }

    public static abstract class Lugar_Estructura implements BaseColumns {
        public static final String TABLE_NAME = "LUGAR";
    }

    public static abstract class Historial_Estructura implements BaseColumns {
        public static final String TABLE_NAME = "Historial";
    }

    public static abstract class Modificador_Estructura implements BaseColumns {
        public static final String TABLE_NAME = "MODIFICADOR";
    }

    public static abstract class Armadura_Estructura implements BaseColumns {
        public static final String TABLE_NAME = "ARMADURA";
    }

    public BaseDeDatos_v2(Principal context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        p = context;
    }


    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE " + Modificador_Estructura.TABLE_NAME + " ("
                + ID_TABLE + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,"
                + DATOS + " TEXT NOT NULL)");

        db.execSQL("CREATE TABLE " + Arma_Estructura.TABLE_NAME + " ("
                + ID_TABLE + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,"
                + DATOS + " TEXT NOT NULL)");

        db.execSQL("CREATE TABLE " + Lugar_Estructura.TABLE_NAME + " ("
                + ID_TABLE + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,"
                + DATOS + " TEXT NOT NULL)");


        db.execSQL("CREATE TABLE " + Armadura_Estructura.TABLE_NAME + " (  "
                + ID_TABLE + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,"
                + DATOS + " TEXT NOT NULL)");

        db.execSQL("CREATE TABLE " + Historial_Estructura.TABLE_NAME + " (  "
                + ID_TABLE + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,"
                + DATOS + " TEXT NOT NULL)");


        db.execSQL("CREATE TABLE " + Personaje_Estructura.TABLE_NAME + " (  "
                + ID_TABLE + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,"
                + Personaje_Estructura.TOKEN + " BLOB" + ","
                + DATOS + " TEXT NOT NULL)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        p.setDbUpgrade(true);

        ArrayList<Arma> armas = GetArmas_OLD(db);
        ArrayList<Armadura> armaduras = GetArmaduras_OLD(db);
        ArrayList<Personaje> personajes = GetPersonajes_OLD(db);
        ArrayList<Modificador> modificadores = GetModif_OLD(db);
        ArrayList<Historial_DB> historial = GetHistorial_OLD(db);
        ArrayList<Lugar> lugares = GetLugares_OLD(db);

        db.execSQL("DROP TABLE IF EXISTS " + Arma_Estructura.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Armadura_Estructura.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Personaje_Estructura.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Modificador_Estructura.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Historial_Estructura.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Lugar_Estructura.TABLE_NAME);

        onCreate(db);

        for (Arma temp : armas) {
            AñadirArma(temp, db);
        }
        for (Armadura temp : armaduras) {
            AñadirArmadura(temp, db);
        }
        for (Personaje temp : personajes) {
            AñadirPersonaje(temp, db);
        }
        for (Modificador temp : modificadores) {
            AñadirModificador(temp, db);
        }
        for (Historial_DB temp : historial) {
            AñadirLog(temp, db);
        }
        for (Lugar temp : lugares) {
            AñadirLugar(temp, db);
        }

        p.cargaDB();
    }

    //Modificar
    public boolean ModificarArmadura(Armadura armadura) {
        return ModificarArmadura(armadura, null);
    }

    public boolean ModificarArmadura(Armadura armadura, SQLiteDatabase db) {
        if (db == null) {
            db = getWritableDatabase();
        }

        ContentValues values = new ContentValues();

        String[] ID = {String.valueOf(armadura.getID())};

        values.put(DATOS, armadura.toJson());

        db.update(Armadura_Estructura.TABLE_NAME, values, ID_TABLE + " = ?", ID);

        return true;
    }

    public boolean ModificarLugar(Lugar lugar) {
        return ModificarLugar(lugar, null);
    }


    public boolean ModificarLugar(Lugar lugar, SQLiteDatabase db) {

        if (db == null) {
            db = getWritableDatabase();
        }

        ContentValues values = new ContentValues();

        String[] ID = {String.valueOf(lugar.getId())};

        values.put(DATOS, lugar.toJson());

        db.update(Lugar_Estructura.TABLE_NAME, values, ID_TABLE + " = ?", ID);

        return true;
    }

    public boolean ModificarArma(Arma arma) {
        return ModificarArma(arma, null);
    }

    public boolean ModificarArma(Arma arma, SQLiteDatabase db) {

        if (db == null) {
            db = getWritableDatabase();
        }

        ContentValues values = new ContentValues();

        String[] ID = {String.valueOf(arma.getID())};

        values.put(DATOS, arma.toJson());

        db.update(Arma_Estructura.TABLE_NAME, values, ID_TABLE + " = ?", ID);

        return true;

    }

    public boolean ModificarModif(Modificador modif) {
        return ModificarModif(modif, null);
    }


    public boolean ModificarModif(Modificador modif, SQLiteDatabase db) {

        if (db == null) {
            db = getWritableDatabase();
        }

        ContentValues values = new ContentValues();

        String[] ID = {String.valueOf(modif.getID())};

        values.put(DATOS, modif.toJson());

        db.update(Modificador_Estructura.TABLE_NAME, values, ID_TABLE + " = ?", ID);

        return true;

    }

    public boolean ModificarPersonaje(Personaje pj) {
        return ModificarPersonaje(pj, null);
    }

    public boolean ModificarPersonaje(Personaje pj, SQLiteDatabase db) {

        if (db == null) {
            db = getWritableDatabase();
        }

        ContentValues values = new ContentValues();

        String[] ID = {String.valueOf(pj.getID())};

        values.put(DATOS, pj.toJson());

        values.put(Personaje_Estructura.TOKEN, pj.getToken());

        db.update(Personaje_Estructura.TABLE_NAME, values, ID_TABLE + " = ?", ID);

        return true;

    }

    public boolean ModificarPersonajes(ArrayList<Personaje> personajes) {
        return ModificarPersonajes(personajes, null);
    }

    public boolean ModificarPersonajes(ArrayList<Personaje> personajes, SQLiteDatabase db) {

        if (db == null) {
            db = getWritableDatabase();
        }

        for (Personaje pj : personajes) {

            ContentValues values = new ContentValues();

            String[] ID = {String.valueOf(pj.getID())};

            values.put(DATOS, pj.toJson());
            values.put(Personaje_Estructura.TOKEN, pj.getToken());


            db.update(Personaje_Estructura.TABLE_NAME, values, ID_TABLE + " = ?", ID);

        }

        return true;
    }

    //Añadir

    public int AñadirArma(Arma arma) {
        return AñadirArma(arma, null);
    }

    public int AñadirArma(Arma arma, SQLiteDatabase db) {
        if (db == null) {
            db = getWritableDatabase();
        }
        ContentValues values = new ContentValues();

        values.put(DATOS, arma.toJson());

        try {
            db.insertOrThrow(Arma_Estructura.TABLE_NAME, null, values);
        } catch (android.database.sqlite.SQLiteConstraintException e) {
            return 0;
        }

        String[] columna = {ID_TABLE};

        Cursor c = db.query(Arma_Estructura.TABLE_NAME, columna, null, null, null, null, ID_TABLE);

        c.moveToLast();
        int index = c.getInt(c.getColumnIndex(ID_TABLE));

        c.close();

        return index;
    }

    public int AñadirLugar(Lugar lugar) {
        return AñadirLugar(lugar, null);
    }

    public int AñadirLugar(Lugar lugar, SQLiteDatabase db) {
        if (db == null) {
            db = getWritableDatabase();
        }
        ContentValues values = new ContentValues();

        values.put(DATOS, lugar.toJson());

        try {
            db.insertOrThrow(Lugar_Estructura.TABLE_NAME, null, values);
        } catch (android.database.sqlite.SQLiteConstraintException e) {
            return 0;
        }

        String[] columna = {ID_TABLE};

        Cursor c = db.query(Lugar_Estructura.TABLE_NAME, columna, null, null, null, null, ID_TABLE);

        c.moveToLast();
        int index = c.getInt(c.getColumnIndex(ID_TABLE));

        c.close();

        return index;
    }

    public void AñadirLog(Historial_DB log) {
        AñadirLog(log, null);
    }

    public void AñadirLog(Historial_DB log, SQLiteDatabase db) {
        if (db == null) {
            db = getWritableDatabase();
        }
        ContentValues values = new ContentValues();

        values.put(DATOS, log.toJson());

        try {
            db.insertOrThrow(Historial_Estructura.TABLE_NAME, null, values);
        } catch (android.database.sqlite.SQLiteConstraintException e) {

        }
    }

    public int AñadirModificador(Modificador modif) {
        return AñadirModificador(modif, null);
    }

    public int AñadirModificador(Modificador modif, SQLiteDatabase db) {
        if (db == null) {
            db = getWritableDatabase();
        }
        ContentValues values = new ContentValues();

        values.put(DATOS, modif.toJson());

        try {
            db.insertOrThrow(Modificador_Estructura.TABLE_NAME, null, values);
        } catch (SQLiteConstraintException e) {
            return 0;
        }

        String[] columna = {ID_TABLE};

        Cursor c = db.query(Modificador_Estructura.TABLE_NAME, columna, null, null, null, null, ID_TABLE);
        c.moveToLast();
        int index = c.getInt(c.getColumnIndex(ID_TABLE));

        c.close();

        return index;
    }

    public int AñadirArmadura(Armadura armadura) {
        return AñadirArmadura(armadura, null);
    }

    public int AñadirArmadura(Armadura armadura, SQLiteDatabase db) {
        if (db == null) {
            db = getWritableDatabase();
        }
        ContentValues values = new ContentValues();

        values.put(DATOS, armadura.toJson());

        try {
            db.insertOrThrow(Armadura_Estructura.TABLE_NAME, null, values);
        } catch (android.database.sqlite.SQLiteConstraintException e) {
            return 0;
        }

        String[] columna = {ID_TABLE};

        Cursor c = db.query(Armadura_Estructura.TABLE_NAME, columna, null, null, null, null, ID_TABLE);
        c.moveToLast();
        int index = c.getInt(c.getColumnIndex(ID_TABLE));

        c.close();

        return index;
    }

    public int AñadirPersonaje(Personaje pj) {
        return AñadirPersonaje(pj, null);
    }

    public int AñadirPersonaje(Personaje pj, SQLiteDatabase db) {

        if (db == null) {
            db = getWritableDatabase();
        }

        ContentValues values = new ContentValues();

        values.put(DATOS, pj.toJson());
        values.put(Personaje_Estructura.TOKEN, pj.getToken());

        try {
            db.insertOrThrow(Personaje_Estructura.TABLE_NAME, null, values);
        } catch (android.database.sqlite.SQLiteConstraintException e) {
            System.out.println(e.toString());
            return -1;
        }


        String[] columna = {ID_TABLE};

        Cursor c = db.query(Personaje_Estructura.TABLE_NAME, columna, null, null, null, null, ID_TABLE);

        c.moveToLast();

        int index = c.getInt(c.getColumnIndex(ID_TABLE));

        c.close();

        return index;
    }

    //Eliminar

    public boolean EliminarArmadura(int ID) {
        return EliminarArmadura(ID, null);
    }

    public boolean EliminarArmadura(int ID, SQLiteDatabase db) {

        if (db == null) {
            db = getWritableDatabase();
        }

        String[] valor = {String.valueOf(ID)};

        db.delete(Armadura_Estructura.TABLE_NAME, ID_TABLE + " =?", valor);

        return true;
    }

    public boolean EliminarLugar(int ID) {
        return EliminarLugar(ID, null);
    }

    public boolean EliminarLugar(int ID, SQLiteDatabase db) {

        if (db == null) {
            db = getWritableDatabase();
        }

        String[] valor = {String.valueOf(ID)};

        db.delete(Lugar_Estructura.TABLE_NAME, ID_TABLE + " =?", valor);

        return true;
    }

    public boolean EliminarArma(int ID) {
        return EliminarArma(ID, null);
    }

    public boolean EliminarArma(int ID, SQLiteDatabase db) {

        if (db == null) {
            db = getWritableDatabase();
        }

        String[] valor = {String.valueOf(ID)};

        db.delete(Arma_Estructura.TABLE_NAME, ID_TABLE + " =?", valor);

        return true;
    }

    public boolean EliminarModif(int ID) {
        return EliminarModif(ID, null);
    }

    public boolean EliminarModif(int ID, SQLiteDatabase db) {

        if (db == null) {
            db = getWritableDatabase();
        }

        String[] valor = {String.valueOf(ID)};

        db.delete(Modificador_Estructura.TABLE_NAME, ID_TABLE + " =?", valor);

        return true;
    }

    public boolean EliminarPersonaje(int ID) {
        return EliminarPersonaje(ID, null);
    }

    public boolean EliminarPersonaje(int ID, SQLiteDatabase db) {

        if (db == null) {
            db = getWritableDatabase();
        }

        String[] valor = {String.valueOf(ID)};

        db.delete(Personaje_Estructura.TABLE_NAME, ID_TABLE + " =?", valor);

        return true;
    }

    //Obtener
    public ArrayList<Arma> GetArmas() {
        return GetArmas(null);
    }

    public ArrayList<Arma> GetArmas(SQLiteDatabase db) {

        if (db == null) {
            System.out.println("Is null!");
            db = getWritableDatabase();
        }

        ArrayList<Arma> Armas = new ArrayList<>();

        try {
            Cursor c = db.query(Arma_Estructura.TABLE_NAME, null, null, null, null, null, null);

            if (c.getCount() != 0) {
                c.moveToFirst();
                boolean flag = true;

                while (flag) {

                    Arma temp = new Arma().fromJson(c.getString(c.getColumnIndex(DATOS)));

                    temp.setID(c.getInt(c.getColumnIndex(ID_TABLE)));

                    Armas.add(temp);

                    if (c.isLast()) {
                        flag = false;
                    }
                    c.moveToNext();
                }
            }
            c.close();
        } catch (java.lang.IllegalStateException e) {
            e.printStackTrace();
        }
        return Armas;
    }

    public ArrayList<Historial_DB> GetHistorial() {
        return GetHistorial(null);
    }

    public ArrayList<Historial_DB> GetHistorial(SQLiteDatabase db) {

        if (db == null) {
            db = getWritableDatabase();
        }

        ArrayList<Historial_DB> historial = new ArrayList<>();
        try {
            Cursor c = db.query(Historial_Estructura.TABLE_NAME, null, null, null, null, null, null);

            if (c.getCount() != 0) {
                c.moveToFirst();
                boolean flag = true;

                while (flag) {

                    Historial_DB temp = new Historial_DB().fromJson(c.getString(c.getColumnIndex(DATOS)));

                    temp.setId(c.getInt(c.getColumnIndex(ID_TABLE)));

                    historial.add(temp);

                    if (c.isLast()) {
                        flag = false;
                    }
                    c.moveToNext();
                }
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return historial;
    }

    public ArrayList<Modificador> GetModif() {
        return GetModif(null);
    }

    public ArrayList<Modificador> GetModif(SQLiteDatabase db) {
        ArrayList<Modificador> Modificadores = new ArrayList<>();
        if (db == null) {
            db = getWritableDatabase();
        }
        try {
            Cursor c = db.query(Modificador_Estructura.TABLE_NAME, null, null, null, null, null, null);

            if (c.getCount() != 0) {
                c.moveToFirst();
                boolean flag = true;

                while (flag) {

                    Modificador temp = new Modificador().fromJson(c.getString(c.getColumnIndex(DATOS)));

                    temp.setID(c.getInt(c.getColumnIndex(ID_TABLE)));

                    Modificadores.add(temp);


                    if (c.isLast()) {
                        flag = false;
                    }
                    c.moveToNext();
                }
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Modificadores;
    }

    public ArrayList<Lugar> GetLugares() {
        return GetLugares(null);
    }

    public ArrayList<Lugar> GetLugares(SQLiteDatabase db) {
        if (db == null) {
            db = getWritableDatabase();
        }
        ArrayList<Lugar> Lugares = new ArrayList<>();
        try {
            Cursor c = db.query(Lugar_Estructura.TABLE_NAME, null, null, null, null, null, null);

            if (c.getCount() != 0) {
                c.moveToFirst();
                boolean flag = true;

                while (flag) {

                    Lugar temp = new Lugar().fromJson(c.getString(c.getColumnIndex(DATOS)));

                    temp.setId(c.getInt(c.getColumnIndex(ID_TABLE)));

                    Lugares.add(temp);

                    if (c.isLast()) {
                        flag = false;
                    }
                    c.moveToNext();
                }
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Lugares;
    }

    public ArrayList<Armadura> GetArmaduras() {
        return GetArmaduras(null);
    }

    public ArrayList<Armadura> GetArmaduras(SQLiteDatabase db) {
        if (db == null) {
            db = getWritableDatabase();
        }
        ArrayList<Armadura> Armaduras = new ArrayList<>();
        try {
            Cursor c = db.query(Armadura_Estructura.TABLE_NAME, null, null, null, null, null, null);

            if (c.getCount() != 0) {
                c.moveToFirst();
                boolean flag = true;

                while (flag) {

                    Armadura temp = new Armadura().fromJson(c.getString(c.getColumnIndex(DATOS)));

                    temp.setID(c.getInt(c.getColumnIndex(ID_TABLE)));

                    Armaduras.add(temp);

                    if (c.isLast()) {
                        flag = false;
                    }
                    c.moveToNext();
                }
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Armaduras;
    }

    public ArrayList<Personaje> GetPersonajes() {
        return GetPersonajes(null);
    }

    public ArrayList<Personaje> GetPersonajes(SQLiteDatabase db) {
        if (db == null) {
            db = getWritableDatabase();
        }
        ArrayList<Personaje> personajes = new ArrayList<>();
        try {
            Cursor c = db.query(Personaje_Estructura.TABLE_NAME, null, null, null, null, null, null);

            if (c.getCount() != 0) {
                c.moveToFirst();
                boolean flag = true;

                while (flag) {

                    String pj_str = c.getString(c.getColumnIndex(DATOS));

                    Personaje pj = new Personaje().fromJson(pj_str);

                    pj.setID(c.getInt(c.getColumnIndex(ID_TABLE)));


                    try {
                        byte[] token = c.getBlob(c.getColumnIndex(Personaje_Estructura.TOKEN));

                        pj.setToken(token);

                    } catch (java.lang.IllegalStateException e) {

                    } catch (java.lang.NullPointerException e) {
                        System.out.println(pj.getNombre_personaje() + " no posee imagen. ");
                    }

                    personajes.add(pj);

                    if (c.isLast()) {
                        flag = false;
                    }
                    c.moveToNext();
                }
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return personajes;
    }


    //Utiles varios


    public static final String ARRAY_DIVIDER = "#";

    public static final String[] FloatToText(float[] valores) {
        String[] cadena = new String[valores.length];
        for (int i = 0; i < valores.length; i++) {
            cadena[i] = String.valueOf(valores[i]);
        }
        return cadena;
    }

    public static final float[] TextToFloat(String[] cadena) {
        float[] valores = new float[cadena.length];
        for (int i = 0; i < cadena.length; i++) {
            valores[i] = Integer.parseInt(cadena[i]);
        }
        return valores;
    }

    public static final String[] BoolToText(boolean[] valores) {
        String[] cadena = new String[valores.length];
        for (int i = 0; i < valores.length; i++) {
            if (valores[i]) {
                cadena[i] = "1";
            } else {
                cadena[i] = "0";
            }
        }
        return cadena;
    }

    public static final boolean[] TextToBool(String[] cadena) {
        boolean[] valores = new boolean[cadena.length];
        for (int i = 0; i < cadena.length; i++) {
            valores[i] = IntToBool(Integer.parseInt(cadena[i]));
        }
        return valores;
    }

    public static final String[] IntToText(int[] valores) {
        String[] cadena = new String[valores.length];
        for (int i = 0; i < valores.length; i++) {
            cadena[i] = String.valueOf(valores[i]);
        }
        return cadena;
    }

    public static final String[] LongToText(long[] valores) {
        String[] cadena = new String[valores.length];
        for (int i = 0; i < valores.length; i++) {
            cadena[i] = String.valueOf(valores[i]);
        }
        return cadena;
    }

    public static final int[] TextToInt(String[] cadena) {
        int[] valores = new int[cadena.length];
        for (int i = 0; i < cadena.length; i++) {
            valores[i] = Integer.parseInt(cadena[i]);
        }
        return valores;
    }

    public static final long[] TextToLong(String[] cadena) {
        long[] valores = new long[cadena.length];
        for (int i = 0; i < cadena.length; i++) {
            valores[i] = Long.parseLong(cadena[i]);
        }
        return valores;
    }

    public static final String serialize(String content[]) {
        return TextUtils.join(ARRAY_DIVIDER, content);
    }

    public static final String[] derialize(String content) {
        return content.split(ARRAY_DIVIDER);
    }

    public static final boolean IntToBool(int i) {
        if (i == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static final int transformar_Texto(String text) {

        int resultado = 0;

        try {
            resultado = Integer.valueOf(text);
            return resultado;
        } catch (NumberFormatException e) {
            System.out.println(e.toString());
            return 0;
        }

    }


    //METODOS VIEJOS

    public ArrayList<Arma> GetArmas_OLD(SQLiteDatabase db) {
        ArrayList<Arma> Armas = new ArrayList<>();

        Cursor c = db.query(BaseDeDatos.Arma_Estructura.TABLE_NAME, null, null, null, null, null, null);

        if (c.getCount() != 0) {
            c.moveToFirst();
            boolean flag = true;

            while (flag) {

                Armas.add(new Arma(
                        c.getString(c.getColumnIndex(BaseDeDatos.Arma_Estructura.NOMBRE)),
                        (TextToInt(derialize(c.getString(c.getColumnIndex(BaseDeDatos.Arma_Estructura.CARACTERISTICAS))))),
                        c.getInt(c.getColumnIndex(BaseDeDatos.Arma_Estructura.ID_ARMA))
                ));
                if (c.isLast()) {
                    flag = false;
                }
                c.moveToNext();
            }
        }
        c.close();
        return Armas;
    }

    public ArrayList<Historial_DB> GetHistorial_OLD(SQLiteDatabase db) {
        ArrayList<Historial_DB> historial = new ArrayList<>();

        Cursor c = db.query(BaseDeDatos.Historial_Estructura.TABLE_NAME, null, null, null, null, null, null);

        if (c.getCount() != 0) {
            c.moveToFirst();
            boolean flag = true;

            while (flag) {

                historial.add(new Historial_DB(
                        c.getString(c.getColumnIndex(BaseDeDatos.Historial_Estructura.TITULO)),
                        c.getString(c.getColumnIndex(BaseDeDatos.Historial_Estructura.CUERPO)),
                        c.getLong(c.getColumnIndex(BaseDeDatos.Historial_Estructura.TIMESTAMP)),
                        c.getInt(c.getColumnIndex(BaseDeDatos.Historial_Estructura.ID_LOG)),
                        c.getString(c.getColumnIndex(BaseDeDatos.Historial_Estructura.PERSONAJE))
                ));
                if (c.isLast()) {
                    flag = false;
                }
                c.moveToNext();
            }
        }
        c.close();

        return historial;
    }

    public ArrayList<Modificador> GetModif_OLD(SQLiteDatabase db) {
        ArrayList<Modificador> Modificadores = new ArrayList<>();
        Cursor c = db.query(BaseDeDatos.Modificador_Estructura.TABLE_NAME, null, null, null, null, null, null);

        if (c.getCount() != 0) {
            c.moveToFirst();
            boolean flag = true;

            while (flag) {

                Modificadores.add(new Modificador(
                        c.getInt(c.getColumnIndex(BaseDeDatos.Modificador_Estructura.ID_MODIFICADOR)),
                        c.getString(c.getColumnIndex(BaseDeDatos.Modificador_Estructura.NOMBRE)),
                        (TextToInt(derialize(c.getString(c.getColumnIndex(BaseDeDatos.Modificador_Estructura.CARACTERISTICAS)))))
                ));
                if (c.isLast()) {
                    flag = false;
                }
                c.moveToNext();
            }
        }
        c.close();
        return Modificadores;
    }

    public ArrayList<Lugar> GetLugares_OLD(SQLiteDatabase db) {
        ArrayList<Lugar> Lugares = new ArrayList<>();
        Cursor c = db.query(BaseDeDatos.Lugar_Estructura.TABLE_NAME, null, null, null, null, null, null);

        if (c.getCount() != 0) {
            c.moveToFirst();
            boolean flag = true;

            while (flag) {

                Lugares.add(new Lugar(
                        c.getString(c.getColumnIndex(BaseDeDatos.Lugar_Estructura.NOMBRE)),
                        c.getString(c.getColumnIndex(BaseDeDatos.Lugar_Estructura.DESCRIPCION)),
                        c.getString(c.getColumnIndex(BaseDeDatos.Lugar_Estructura.LUGAR_PADRE)),
                        c.getInt(c.getColumnIndex(BaseDeDatos.Lugar_Estructura.ID))
                ));
                if (c.isLast()) {
                    flag = false;
                }
                c.moveToNext();
            }
        }
        c.close();
        return Lugares;
    }


    public ArrayList<Armadura> GetArmaduras_OLD(SQLiteDatabase db) {
        ArrayList<Armadura> Armaduras = new ArrayList<>();
        Cursor c = db.query(BaseDeDatos.Armadura_Estructura.TABLE_NAME, null, null, null, null, null, null);

        if (c.getCount() != 0) {
            c.moveToFirst();
            boolean flag = true;

            while (flag) {
                Armaduras.add(new Armadura(
                        c.getString(c.getColumnIndex(BaseDeDatos.Armadura_Estructura.NOMBRE)),
                        TextToInt(derialize(c.getString(c.getColumnIndex(BaseDeDatos.Armadura_Estructura.CARACTERISTICAS)))),
                        c.getInt(c.getColumnIndex(BaseDeDatos.Armadura_Estructura.ID_ARMADURA))
                ));
                if (c.isLast()) {
                    flag = false;
                }
                c.moveToNext();
            }
        }
        c.close();
        return Armaduras;
    }


    public ArrayList<Personaje> GetPersonajes_OLD(SQLiteDatabase db) {
        ArrayList<Personaje> personajes = new ArrayList<>();

        Cursor c = db.query(BaseDeDatos.Personaje_Estructura.TABLE_NAME, null, null, null, null, null, null);

        if (c.getCount() != 0) {
            c.moveToFirst();
            boolean flag = true;

            while (flag) {

                Personaje_old pj = new Personaje_old(
                        c.getInt(c.getColumnIndex(BaseDeDatos.Personaje_Estructura.ID_PERSONAJE)),
                        c.getString(c.getColumnIndex(BaseDeDatos.Personaje_Estructura.NOMBRE)),
                        c.getString(c.getColumnIndex(BaseDeDatos.Personaje_Estructura.PERSONAJE)),
                        c.getString(c.getColumnIndex(BaseDeDatos.Personaje_Estructura.DESCRIPCIÓN)),
                        c.getInt(c.getColumnIndex(BaseDeDatos.Personaje_Estructura.NIVEL_PIFIA)),
                        c.getInt(c.getColumnIndex(BaseDeDatos.Personaje_Estructura.TIPOENTIDAD)),
                        c.getInt(c.getColumnIndex(BaseDeDatos.Personaje_Estructura.TIPO_DE_PERSONAJE)),
                        TextToInt(derialize(c.getString(c.getColumnIndex(BaseDeDatos.Personaje_Estructura.CARACTERISTICAS)))),
                        TextToInt(derialize(c.getString(c.getColumnIndex(BaseDeDatos.Personaje_Estructura.CARACTERISTICAS_TEMPORAL)))),
                        TextToBool(derialize(c.getString(c.getColumnIndex(BaseDeDatos.Personaje_Estructura.CONOCIM_ESPECIALES)))),
                        TextToInt(derialize(c.getString(c.getColumnIndex(BaseDeDatos.Personaje_Estructura.PENALIZADORES)))),
                        TextToInt(derialize(c.getString(c.getColumnIndex(BaseDeDatos.Personaje_Estructura.ESPECIALES)))),
                        TextToInt(derialize(c.getString(c.getColumnIndex(BaseDeDatos.Personaje_Estructura.ARMADURAS)))),
                        (derialize(c.getString(c.getColumnIndex(BaseDeDatos.Personaje_Estructura.ARMADURAS_NOMBRES)))),
                        TextToInt(derialize(c.getString(c.getColumnIndex(BaseDeDatos.Personaje_Estructura.ARMAS)))),
                        (derialize(c.getString(c.getColumnIndex(BaseDeDatos.Personaje_Estructura.ARMAS_NOMBRES)))), null, null
                );

                try {
                    byte[] token = c.getBlob(c.getColumnIndex(BaseDeDatos.Personaje_Estructura.TOKEN));

                    pj.setToken(token);

                    byte[] image = c.getBlob(c.getColumnIndex(BaseDeDatos.Personaje_Estructura.IMAGEN));

                    pj.setImagen(image);

                } catch (java.lang.IllegalStateException e) {

                } catch (java.lang.NullPointerException e) {
                    System.out.println(pj.getNombre_personaje() + " no posee imagen. ");
                }

                personajes.add(new Personaje(pj));

                if (c.isLast()) {
                    flag = false;
                }
                c.moveToNext();
            }
        }
        c.close();
        return personajes;
    }


}
