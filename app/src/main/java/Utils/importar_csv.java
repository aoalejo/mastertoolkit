package Utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nugganet.arget.mastertoolkit.Base;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Arma;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Armadura;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Personaje;
import com.nugganet.arget.mastertoolkit.Principal;
import com.nugganet.arget.mastertoolkit.R;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class importar_csv {

    public void printExcel() {

    }


    public static String printLine(String[] line) {
        String file = "";
        for (int i = 0; i < line.length; i++) {

            if (!line[i].isEmpty()) {
                file = file + " |" + i + ": " + separate_number_start(line[i]).toString();
            }


        }
        return file;
    }

    public static String readStrFromCsv(List<String[]> csv, int fila, int columna) {
        try {
            return csv.get(fila)[columna];
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static int readIntFromCsv(List<String[]> csv, int fila, int columna) {
        try {
            // return separate_number_start(csv.get(fila)[columna]).getValor();
            return ToIntWT(csv.get(fila)[columna]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    private static int ToIntWT(String text) {
        int num = -999;

        try {
            num = Integer.parseInt(text);
            System.out.println(text + " -> " + " es número");
        } catch (NumberFormatException e) {
            System.out.println(text + " no es número");
        }

        if (num == -999) {
            String[] nuevo = text.split(" ");
            for (String parte : nuevo) {
                try {
                    num = Integer.parseInt(parte);
                    System.out.println(text + " -> " + parte + " es número");
                } catch (NumberFormatException e) {
                    System.out.println(text + " -> " + parte + " no es número");
                }
            }
        }
        if (num == -999) {
            num = 0;
        }

        return num;

    }

    private static Armadura findArmadura(String nombre, Principal p) {
        Armadura temp = null;
        if (!nombre.isEmpty()) {
            for (Armadura armadura : p.getArrayArmaduras()) {

                System.out.print("\nComparando: " + armadura.getNombre() + " vs " + nombre);

                if (armadura.getNombre().toLowerCase().equals(nombre.toLowerCase())) {
                    temp.fromJson(armadura.toJson());
                    System.out.println("Encontrado!");
                    break;
                }
                System.out.print(" -> no coinciden.");
            }
        }
        return temp;
    }

    public static Personaje readExcelFile(InputStream myInput, Principal p) {

        Personaje temp = new Personaje();

        try {

            final CSVParser parser =
                    new CSVParserBuilder()
                            .withSeparator(';')
                            .withIgnoreQuotations(true)
                            .build();
            final CSVReader reader =
                    new CSVReaderBuilder(new InputStreamReader(myInput))
                            .withSkipLines(1)
                            .withCSVParser(parser)
                            .build();

            List<String[]> csv = reader.readAll();

            System.out.println("CSV: ");

            String csv_string = "";
            for (int i = 0; i < csv.size(); i++) {
                csv_string = csv_string + "\nfila: " + i + "\n\n" + printLine(csv.get(i));
            }
            System.out.println(csv_string);
            System.out.println("FIN: ");

            temp.setNombre_personaje(readStrFromCsv(csv, 2, 10));
            temp.reemplazar_valor_caracteristica(Base.ATRIBUTO_FUERZA, readIntFromCsv(csv, 11, 3));
            temp.reemplazar_valor_caracteristica(Base.ATRIBUTO_FUERZA, readIntFromCsv(csv, 11, 3));
            temp.reemplazar_valor_caracteristica(Base.ATRIBUTO_DESTREZA, readIntFromCsv(csv, 11, 5));
            temp.reemplazar_valor_caracteristica(Base.ATRIBUTO_AGILIDAD, readIntFromCsv(csv, 11, 7));
            temp.reemplazar_valor_caracteristica(Base.ATRIBUTO_CONSTITUCIÓN, readIntFromCsv(csv, 11, 9));
            temp.reemplazar_valor_caracteristica(Base.ATRIBUTO_PODER, readIntFromCsv(csv, 11, 11));
            temp.reemplazar_valor_caracteristica(Base.ATRIBUTO_INTELIGENCIA, readIntFromCsv(csv, 11, 13));
            temp.reemplazar_valor_caracteristica(Base.ATRIBUTO_VOLUNTAD, readIntFromCsv(csv, 11, 15));
            temp.reemplazar_valor_caracteristica(Base.ATRIBUTO_PERCEPCIÓN, readIntFromCsv(csv, 11, 17));

            temp.reemplazar_valor_caracteristica(Base.RESISTENCIA_FISICA, readIntFromCsv(csv, 12, 3));
            temp.reemplazar_valor_caracteristica(Base.RESISTENCIA_MAGICA, readIntFromCsv(csv, 12, 5));
            temp.reemplazar_valor_caracteristica(Base.RESISTENCIA_PSIQUICA, readIntFromCsv(csv, 12, 7));
            temp.reemplazar_valor_caracteristica(Base.RESISTENCIA_VENENOS, readIntFromCsv(csv, 12, 9));
            temp.reemplazar_valor_caracteristica(Base.RESISTENCIA_ENFERMEDADES, readIntFromCsv(csv, 12, 11));
            temp.reemplazar_valor_caracteristica(Base.RESISTENCIA_PRESENCIA, readIntFromCsv(csv, 12, 15));

            temp.reemplazar_valor_caracteristica(Base.HABILIDAD_TURNO, readIntFromCsv(csv, 14, 3));

            temp.reemplazar_valor_caracteristica(Base.VIDA_ACT, readIntFromCsv(csv, 10, 5));

            temp.reemplazar_valor_caracteristica(Base.VIDA_MAX, readIntFromCsv(csv, 10, 5));

            temp.reemplazar_valor_caracteristica(Base.ENERGÍA_ACT, readIntFromCsv(csv, 56, 12));

            temp.reemplazar_valor_caracteristica(Base.ENERGÍA_MAX, readIntFromCsv(csv, 56, 12));

            temp.reemplazar_valor_caracteristica(Base.ENERGÍA_REGEN, readIntFromCsv(csv, 56, 12));

            temp.getHabilidades_psiquicas()[1] = readIntFromCsv(csv, 36, 4);
            temp.getHabilidades_psiquicas()[2] = readIntFromCsv(csv, 36, 4);
            temp.getHabilidades_psiquicas()[3] = 24;

            temp.reemplazar_valor_caracteristica(Base.ENERGÍA_MAX, readIntFromCsv(csv, 56, 12));

            temp.reemplazar_valor_caracteristica(Base.ENERGÍA_REGEN, readIntFromCsv(csv, 56, 12));

            temp.reemplazar_valor_caracteristica(Base.VIDA_REG, Base.tablaRegenVida[readIntFromCsv(csv, 55, 13) + 1]);

            temp.reemplazar_valor_caracteristica(Base.HABILIDAD_PROYECCION_PSIQUICA, readIntFromCsv(csv, 39, 6));

            temp.getHabilidades_psiquicas()[0] = readIntFromCsv(csv, 38, 6);

            Armadura[] armaduras = new Armadura[6];

            armaduras[0] = new Armadura();
            armaduras[1] = new Armadura();
            armaduras[2] = new Armadura();
            armaduras[3] = new Armadura();
            armaduras[4] = new Armadura();
            armaduras[5] = new Armadura();

            armaduras[0].setNombre(readStrFromCsv(csv, 18, 3));
            armaduras[0].setCaracteristicas(new int[]{
                    readIntFromCsv(csv, 19, 3),
                    readIntFromCsv(csv, 19, 5),
                    readIntFromCsv(csv, 19, 7),
                    readIntFromCsv(csv, 19, 9),
                    readIntFromCsv(csv, 19, 13),
                    readIntFromCsv(csv, 19, 11),
                    readIntFromCsv(csv, 19, 15),
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1
            });


            temp.setArmaduras(armaduras);

            ArrayList<Dupla> secundarias = proc_sec(readStrFromCsv(csv, 59, 2));

            int[] valores_lugares = p.getResources().getIntArray(R.array.Datos_creac_lugares);

            int[] valores_puntos = p.getResources().getIntArray(R.array.Datos_Creación_Bases);


            String[] nombres_puntos = p.getResources().getStringArray(R.array.Datos_Creación_Nombres);

            for (int i = 0; i < nombres_puntos.length; i++) {
                for (Dupla sec : secundarias) {

                    if (nombres_puntos[i].toLowerCase().trim().contentEquals(sec.getNombre().toLowerCase().trim())) {
                        if (valores_puntos[i] == -30) {

                            System.out.println("Encontrada coincidencia en " + i + ": " + nombres_puntos[i] + " -> " + sec.getNombre() + " (valor " + sec.getValor() + ").");
                            temp.reemplazar_valor_caracteristica(valores_lugares[i], sec.getValor());
                            secundarias.remove(sec);
                            break;
                        }
                    }
                }
            }

            String[] habilidades_psiquicas_del_pj = readStrFromCsv(csv, 41, 6).split(",");

            String[] Array_nombres_habilidades_vector = p.getResources().getStringArray(R.array.habilidades_psiquicas);

            for (String habilidad : habilidades_psiquicas_del_pj) {
                for (int i = 0; i < Array_nombres_habilidades_vector.length; i++)
                    if (habilidad.toLowerCase().replace(" ", "").equals(Array_nombres_habilidades_vector[i].toLowerCase().replace(" ", ""))) {
                        temp.getHabilidades_psiquicas()[i + 4] = 1;
                        break;
                    }
            }


            System.out.println("No encontradas : ");
            String desc = "";
            for (Dupla sec : secundarias) {
                System.out.println(sec.toString());
                desc = desc + "\n" + sec.getNombre() + ": " + sec.getValor();
            }
            System.out.println("No encontradas ^ ");

            if (!desc.isEmpty()) {
                temp.setDescripción("Habilidades secundarias no encontradas: " + desc);
            }


            Dupla defensa = separate_number_start(readStrFromCsv(csv, 16, 6));

            ArrayList<String> turnos = new ArrayList<>();
            ArrayList<String> defensas = new ArrayList<>();
            ArrayList<String> ataques = new ArrayList<>();
            ArrayList<String> daños = new ArrayList<>();

            for (String csv_strings : csv.get(14)) {
                if (!csv_strings.isEmpty() & !csv_strings.contains("Turno:")) {
                    turnos.add(csv_strings);
                }
            }

            for (String csv_strings : csv.get(15)) {
                if (!csv_strings.isEmpty() & !csv_strings.contains("Habilidad de ataque:")) {
                    ataques.add(csv_strings);
                }
            }

            for (String csv_strings : csv.get(16)) {
                if (!csv_strings.isEmpty() & !csv_strings.contains("Habilidad de defensa:")) {
                    defensas.add(csv_strings);
                }
            }

            for (String csv_strings : csv.get(17)) {
                if (!csv_strings.isEmpty() & !csv_strings.contains("Daño:")) {
                    daños.add(csv_strings);
                }
            }

            temp.setArmas(getArmas(defensas, ataques, daños, turnos, readIntFromCsv(csv, 14, 3)));


            if (!defensa.getNombre().toLowerCase().contains("esquiva")) {
                temp.reemplazar_valor_caracteristica(Base.HABILIDAD_ESQUIVA, 0);
                temp.reemplazar_valor_caracteristica(Base.HABILIDAD_PARADA, 0);

            } else {
                temp.reemplazar_valor_caracteristica(Base.HABILIDAD_ESQUIVA, defensa.getValor());
                temp.reemplazar_valor_caracteristica(Base.HABILIDAD_PARADA, (defensa.getValor() - 60));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        temp.fromJson(temp.toJson());

        System.out.println(temp.toJson());

        return temp;
    }

    private static Arma[] getArmas(ArrayList<String> defensas, ArrayList<String> ataques, ArrayList<String> daños, ArrayList<String> turnos, int turno) {
        Arma[] armas = new Arma[6];
        for (int i = 0; i < armas.length; i++) {
            armas[i] = new Arma();
        }

        System.out.println("Defensas: " + defensas.toString());
        System.out.println("Ataques: " + ataques.toString());
        System.out.println("Daños: " + daños.toString());
        System.out.println("Turnos: " + turnos.toString());

        int j = 0;
        for (int i = 0; i < ataques.size() | i < 5; i++) {
            try {
                Dupla[] temp = procesarArma(ataques.get(i));
                if (temp[1] == null) { //No es un arma doble

                    armas[j].setNombre(separate_number_start(ataques.get(i)).getNombre());
                    armas[j].setCaracteristica(Arma.DAÑO_BASE, separate_number_start(daños.get(i)).getValor());

                    armas[j].setCaracteristica(Arma.TURNO_ARMA, separate_number_start(turnos.get(i)).getValor() - turno);

                    armas[j].setCaracteristica(Arma.BONO_ATAQUE, separate_number_start(ataques.get(i)).getValor());

                    armas[j].setCaracteristica(Arma.BONO_PARADA, separate_number_start(defensas.get(i)).getValor());

                    armas[j].setCaracteristica(Arma.CRIT_PRINCIPAL, setTipoDaño(separate_number_start(daños.get(i)).getNombre()));
                    armas[j].setCaracteristica(Arma.CRIT_SECUNDARIO, setTipoDaño(separate_number_start(daños.get(i)).getNombre()));


                } else {  //Es un arma doble

                    if (ataques.size() > 4) {//El usuario tiene demasiadas armas, las dobles se uniran como una sola

                        armas[j].setNombre(separate_number_start(turnos.get(i)).getNombre());

                        armas[j].setCaracteristica(Arma.DAÑO_BASE, promedioDuplaArmas((daños.get(i))).getValor());

                        armas[j].setCaracteristica(Arma.TURNO_ARMA, separate_number_start(turnos.get(i)).getValor() - turno);

                        armas[j].setCaracteristica(Arma.BONO_ATAQUE, promedioDuplaArmas((ataques.get(i))).getValor());

                        armas[j].setCaracteristica(Arma.BONO_PARADA, promedioDuplaArmas((defensas.get(i))).getValor());

                        temp = procesarArma(daños.get(i));

                        armas[j].setCaracteristica(Arma.CRIT_PRINCIPAL, setTipoDaño(temp[0].getNombre()));
                        armas[j].setCaracteristica(Arma.CRIT_SECUNDARIO, setTipoDaño(temp[1].getNombre()));


                    } else { //las armas dobles se pueden desdoblar

                        armas[j].setCaracteristica(Arma.TURNO_ARMA, separate_number_start(turnos.get(i)).getValor() - turno);
                        armas[j + 1].setCaracteristica(Arma.TURNO_ARMA, separate_number_start(turnos.get(i)).getValor() - turno);

                        armas[j].setNombre(temp[0].getNombre());
                        armas[j + 1].setNombre(temp[1].getNombre());

                        armas[j].setCaracteristica(Arma.BONO_ATAQUE, temp[0].getValor());
                        armas[j + 1].setCaracteristica(Arma.BONO_ATAQUE, temp[1].getValor());

                        temp = procesarArma(defensas.get(i));

                        armas[j].setCaracteristica(Arma.BONO_PARADA, temp[0].getValor());
                        armas[j + 1].setCaracteristica(Arma.BONO_PARADA, temp[1].getValor());

                        temp = procesarArma(daños.get(i));

                        armas[j].setCaracteristica(Arma.DAÑO_BASE, temp[0].getValor());
                        armas[j + 1].setCaracteristica(Arma.DAÑO_BASE, temp[1].getValor());

                        armas[j].setCaracteristica(Arma.CRIT_PRINCIPAL, setTipoDaño(temp[0].getNombre()));
                        armas[j + 1].setCaracteristica(Arma.CRIT_SECUNDARIO, setTipoDaño(temp[1].getNombre()));


                        j = j + 1;
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println(armas[i].toString() + "\n\n");

            j = j + 1;
        }

        return armas;

    }


    private static int setTipoDaño(String nombre) {
        if (nombre.contains("(FIL)")) {
            return Base.ARM_TA_FIL;
        }
        if (nombre.contains("(CON)")) {
            return Base.ARM_TA_CON;
        }
        if (nombre.contains("(PEN)")) {
            return Base.ARM_TA_PEN;
        }
        if (nombre.contains("(CAL)")) {
            return Base.ARM_TA_CAL;
        }
        if (nombre.contains("(ELE)")) {
            return Base.ARM_TA_ELE;
        }
        if (nombre.contains("(FRI)")) {
            return Base.ARM_TA_FRI;
        }
        if (nombre.contains("(ENE)")) {
            return Base.ARM_TA_ENE;
        }
        return 0;
    }

    private static Dupla[] procesarArma(String text) {
        Dupla[] result = new Dupla[2];


        if (text.contains(" y ")) {
            result = separarDuplaArmas(separate_number_start(text));
        } else {
            result[0] = separate_number_start(text);
            result[1] = null;
        }
        return result;

    }

    private static Dupla promedioDuplaArmas(String text) {
        Dupla[] temp = procesarArma(text);

        Dupla result = new Dupla();

        result.setNombre(temp[0].getNombre());
        result.setValor((temp[0].getValor() + temp[1].getValor()) / 2);

        return result;
    }

    private static Dupla[] separarDuplaArmas(Dupla dupla) {
        Dupla[] result = new Dupla[2];

        result[0] = new Dupla().fromJson(dupla.toJson());
        result[1] = new Dupla().fromJson(dupla.toJson());

        if (dupla.getNombre().contains(" y ")) {
            String[] nombres = dupla.getNombre().split(" y ");
            result[0].setNombre(nombres[0] + " (Principal)");
            result[1].setNombre(separate_number_start(nombres[1]).getNombre() + " (Secundaria)");
            result[1].setValor(separate_number_start(nombres[1]).getValor());
        } else {
            result[1] = null;
        }
        return result;
    }

    public static class Dupla {
        private int valor = 0;
        private String nombre = "";

        public Dupla(int valor, String nombre) {
            this.valor = valor;
            this.nombre = nombre;
        }

        public Dupla() {
        }

        public String toJson() {
            return new GsonBuilder().create().toJson(this, this.getClass());
        }

        public Dupla fromJson(String json) {
            System.out.println("Parsing json: " + json);
            return new Gson().fromJson(json, this.getClass());
        }

        public int getValor() {
            return valor;
        }

        public void setValor(int valor) {
            this.valor = valor;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        @Override
        public String toString() {
            return "Dupla{" +
                    "valor=" + valor +
                    ", nombre='" + nombre + '\'' +
                    '}';
        }
    }

    private static ArrayList<Dupla> proc_sec(String text) {
        String[] hab_sec = text.split(",");
        ArrayList<Dupla> result = new ArrayList<>();

        for (int i = 0; i < hab_sec.length; i++) {
            result.add(separate_number_start(hab_sec[i]));
        }

        return result;
    }

    public static Dupla separate_number_start(String texto) {
        int contador = 0;
        Dupla result = new Dupla(0, texto);
        if (!texto.isEmpty()) {
            for (int i = 0; i < texto.length(); i++) {
                if (i == 0 | i == 1) {
                    if (texto.charAt(i) == ' ' | texto.charAt(i) == '-' | charIsNumber(texto.charAt(i))) {
                        contador = contador + 1;
                        if (texto.charAt(i) != ' ' & texto.charAt(i) != '-') {
                            try {
                                result.setValor(Integer.valueOf(texto.substring(0, contador).trim()));
                                result.setNombre(texto.substring(contador).trim());
                            } catch (Exception e) {
                                System.out.println("Error procesando " + texto + " en " + i);
                            }
                        }

                    } else {
                        return result;
                    }
                } else {
                    if (charIsNumber(texto.charAt(i))) {
                        contador = contador + 1;
                    } else {

                        try {
                            result.setValor(Integer.valueOf(texto.substring(0, contador).trim()));
                            result.setNombre(texto.substring(contador).trim());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return result;
                    }
                }
            }
        }
        return result;
    }


    private static boolean charIsNumber(char chara) {
        if (chara == '0' |
                chara == '1' |
                chara == '2' |
                chara == '3' |
                chara == '4' |
                chara == '5' |
                chara == '6' |
                chara == '7' |
                chara == '8' |
                chara == '9') {
            return true;
        }
        return false;
    }
}
