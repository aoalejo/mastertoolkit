package Utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.text.TextUtils;

import com.nugganet.arget.mastertoolkit.Clases_objetos.Arma;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Armadura;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Historial_DB;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Lugar;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Modificador;
import com.nugganet.arget.mastertoolkit.Clases_objetos.Personaje_old;

import java.util.ArrayList;

/**
 * Created by Arget on 9/5/2017.
 */

public class BaseDeDatos extends SQLiteOpenHelper {


    public static final String TextNotNull = " TEXT NOT NULL, ";
    public static final String IntegerNotNull = " INTEGER NOT NULL, ";

    public int getDatabaseVersion(){
        return DATABASE_VERSION;
    }

    private static final int DATABASE_VERSION = 4;
    private static final String DATABASE_NAME = "Base_De_Datos_old";


    public static abstract class Personaje_Estructura implements BaseColumns {

        public static final String TABLE_NAME = "PERSONAJE";
        public static final String ID_PERSONAJE = "ID_PERSONAJE";
        public static final String DESCRIPCIÓN = "DESCRIPCION";
        public static final String NOMBRE = "NOMBRE";
        public static final String PERSONAJE = "PERSONAJE";
        public static final String NIVEL_PIFIA = "NIVEL_PIFIA";
        public static final String TIPOENTIDAD = "TIPOENTIDAD";
        public static final String TIPO_DE_PERSONAJE = "TIPO_DE_PERSONAJE";
        public static final String CARACTERISTICAS = "CARACTERISTICAS";
        public static final String CARACTERISTICAS_TEMPORAL = "CARACTERISTICAS_TEMPORAL";
        public static final String CONOCIM_ESPECIALES = "CONOCIM_ESPECIALES";
        public static final String PENALIZADORES = "PENALIZADORES";
        public static final String ESPECIALES = "ESPECIALES";
        public static final String ARMADURAS = "ARMADURAS";
        public static final String ARMADURAS_NOMBRES = "ARMADURAS_NOMBRES";
        public static final String ARMAS = "ARMAS";
        public static final String ARMAS_NOMBRES = "ARMAS_NOMBRES";
        public static final String TOKEN = "TOKEN";
        public static final String IMAGEN = "IMAGEN";
    }

    public static abstract class Arma_Estructura implements BaseColumns {
        public static final String TABLE_NAME = "ARMA";
        public static final String ID_ARMA = "ID_ARMA";
        public static final String NOMBRE = "NOMBRE";
        public static final String CARACTERISTICAS = "CARACTERISTICAS";
    }

    public static abstract class Lugar_Estructura implements BaseColumns {
        public static final String TABLE_NAME = "LUGAR";
        public static final String ID = "ID_LUGAR";
        public static final String NOMBRE = "NOMBRE";
        public static final String DESCRIPCION = "DESCRIPCION";
        public static final String LUGAR_PADRE = "LUGAR_PADRE";
        public static final String IMAGEN = "IMAGEN";
    }

    public static abstract class Historial_Estructura implements BaseColumns {
        public static final String TABLE_NAME = "Historial";
        public static final String ID_LOG = "ID_HISTORIAL";
        public static final String TIMESTAMP = "FECHAYHORA";
        public static final String TITULO = "TITULO";
        public static final String CUERPO = "CUERPO";
        public static final String PERSONAJE = "PERSONAJE";
    }

    public static abstract class Modificador_Estructura implements BaseColumns {
        public static final String TABLE_NAME = "MODIFICADOR";
        public static final String ID_MODIFICADOR = "ID_MODIFICADOR";
        public static final String NOMBRE = "NOMBRE";
        public static final String CARACTERISTICAS = "CARACTERISTICAS";
    }

    public static abstract class Armadura_Estructura implements BaseColumns {
        public static final String TABLE_NAME = "ARMADURA";
        public static final String ID_ARMADURA = "ID_ARMADURA";
        public static final String NOMBRE = "NOMBRE";
        public static final String CARACTERISTICAS = "CARACTERISTICAS";
    }

    public BaseDeDatos(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE " + Modificador_Estructura.TABLE_NAME + " ("
                + Modificador_Estructura.ID_MODIFICADOR + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,"
                + Modificador_Estructura.NOMBRE + TextNotNull
                + Modificador_Estructura.CARACTERISTICAS + " TEXT NOT NULL "
                + ")");

        db.execSQL("CREATE TABLE " + Arma_Estructura.TABLE_NAME + " ("
                + Arma_Estructura.ID_ARMA + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,"
                + Arma_Estructura.NOMBRE + TextNotNull
                + Arma_Estructura.CARACTERISTICAS + " TEXT NOT NULL "
                + ")");

        db.execSQL("CREATE TABLE " + Lugar_Estructura.TABLE_NAME + " ("
                + Lugar_Estructura.ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,"
                + Lugar_Estructura.NOMBRE + TextNotNull
                + Lugar_Estructura.DESCRIPCION + TextNotNull
                + Lugar_Estructura.IMAGEN + " BLOB" + ","
                + Lugar_Estructura.LUGAR_PADRE + " TEXT NOT NULL "
                + ")");


        db.execSQL("CREATE TABLE " + Armadura_Estructura.TABLE_NAME + " (  "
                + Armadura_Estructura.ID_ARMADURA + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,"
                + Armadura_Estructura.NOMBRE + TextNotNull
                + Armadura_Estructura.CARACTERISTICAS + " TEXT NOT NULL "
                + ")");

        db.execSQL("CREATE TABLE " + Historial_Estructura.TABLE_NAME + " (  "
                + Historial_Estructura.ID_LOG + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,"
                + Historial_Estructura.TIMESTAMP + " LONG NOT NULL,"
                + Historial_Estructura.TITULO + " TEXT,"
                + Historial_Estructura.CUERPO + " TEXT,"
                + Historial_Estructura.PERSONAJE + " TEXT"
                + ")");


        db.execSQL("CREATE TABLE " + Personaje_Estructura.TABLE_NAME + " (  "
                + Personaje_Estructura.ID_PERSONAJE + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,"
                + Personaje_Estructura.DESCRIPCIÓN + TextNotNull
                + Personaje_Estructura.NOMBRE + TextNotNull
                + Personaje_Estructura.PERSONAJE + TextNotNull
                + Personaje_Estructura.NIVEL_PIFIA + IntegerNotNull
                + Personaje_Estructura.TIPOENTIDAD + IntegerNotNull
                + Personaje_Estructura.TIPO_DE_PERSONAJE + TextNotNull
                + Personaje_Estructura.CARACTERISTICAS + TextNotNull
                + Personaje_Estructura.CARACTERISTICAS_TEMPORAL + TextNotNull
                + Personaje_Estructura.CONOCIM_ESPECIALES + TextNotNull
                + Personaje_Estructura.PENALIZADORES + TextNotNull
                + Personaje_Estructura.ESPECIALES + TextNotNull
                + Personaje_Estructura.ARMADURAS + TextNotNull
                + Personaje_Estructura.ARMADURAS_NOMBRES + TextNotNull
                + Personaje_Estructura.ARMAS + TextNotNull
                + Personaje_Estructura.ARMAS_NOMBRES + TextNotNull
                + Personaje_Estructura.IMAGEN + " BLOB" + ","
                + Personaje_Estructura.TOKEN + " BLOB" + ")");
    }

    public void upgradeTest(){

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + Arma_Estructura.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Armadura_Estructura.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Personaje_Estructura.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Modificador_Estructura.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Historial_Estructura.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Lugar_Estructura.TABLE_NAME);

        onCreate(db);
    }

    //Modificar

    public boolean ModificarArmadura(Armadura armadura) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        String[] ID = {String.valueOf(armadura.getID())};

        values.put(Armadura_Estructura.NOMBRE, armadura.getNombre());
        values.put(Armadura_Estructura.CARACTERISTICAS, serialize(IntToText(armadura.getCaracteristicas())));

        db.update(Armadura_Estructura.TABLE_NAME, values, Armadura_Estructura.ID_ARMADURA + " = ?", ID);

        return true;
    }

    public boolean ModificarLugar(Lugar lugar) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        String[] ID = {String.valueOf(lugar.getId())};

        values.put(Lugar_Estructura.NOMBRE, lugar.getNombre());
        values.put(Lugar_Estructura.DESCRIPCION, lugar.getDescripción());
        values.put(Lugar_Estructura.LUGAR_PADRE, lugar.getPadre());

        db.update(Lugar_Estructura.TABLE_NAME, values, Lugar_Estructura.ID + " = ?", ID);

        return true;
    }

    public boolean ModificarArma(Arma arma) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        String[] ID = {String.valueOf(arma.getID())};

        values.put(Arma_Estructura.NOMBRE, arma.getNombre());
        values.put(Arma_Estructura.CARACTERISTICAS, serialize(IntToText(arma.getCaracteristicas())));

        db.update(Arma_Estructura.TABLE_NAME, values, Arma_Estructura.ID_ARMA + " = ?", ID);

        return true;
    }


    public boolean ModificarModif(Modificador modif) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        String[] ID = {String.valueOf(modif.getID())};

        values.put(Modificador_Estructura.NOMBRE, modif.getNombre());
        values.put(Modificador_Estructura.CARACTERISTICAS, serialize(IntToText(modif.getCaracteristicas())));

        db.update(Modificador_Estructura.TABLE_NAME, values, Modificador_Estructura.ID_MODIFICADOR + " = ?", ID);

        return true;
    }

    public boolean ModificarPersonaje(Personaje_old pj) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        String[] ID = {String.valueOf(pj.getID())};

        values.put(Personaje_Estructura.NOMBRE, pj.getNombre_jugador());
        values.put(Personaje_Estructura.DESCRIPCIÓN, pj.getDescripción());
        values.put(Personaje_Estructura.PERSONAJE, pj.getNombre_personaje());
        values.put(Personaje_Estructura.CARACTERISTICAS, serialize(IntToText(pj.getCaracteristicas())));
        values.put(Personaje_Estructura.CARACTERISTICAS_TEMPORAL, serialize(IntToText(pj.getCaracteristicas_temporales())));
        values.put(Personaje_Estructura.CONOCIM_ESPECIALES, serialize(BoolToText(pj.getConocimientos_especiales())));
        values.put(Personaje_Estructura.PENALIZADORES, serialize(IntToText(pj.getPenalizadores())));
        values.put(Personaje_Estructura.ESPECIALES, serialize(IntToText(pj.getEspeciales())));
        values.put(Personaje_Estructura.NIVEL_PIFIA, pj.getNivelPifia());
        values.put(Personaje_Estructura.TIPOENTIDAD, pj.getTipoDeEntidad());
        values.put(Personaje_Estructura.TIPO_DE_PERSONAJE, pj.getTipoDePersonaje());
        values.put(Personaje_Estructura.ARMADURAS, serialize(IntToText(pj.getArmaduras())));
        values.put(Personaje_Estructura.ARMADURAS_NOMBRES, serialize(pj.getArmaduras_nombres()));
        values.put(Personaje_Estructura.ARMAS, serialize(IntToText(pj.getArmas())));
        values.put(Personaje_Estructura.ARMAS_NOMBRES, serialize(pj.getArmas_nombres()));
        values.put(Personaje_Estructura.TOKEN, pj.getToken());

        db.update(Personaje_Estructura.TABLE_NAME, values, Personaje_Estructura.ID_PERSONAJE + " = ?", ID);

        return true;
    }

    public boolean ModificarPersonajes(ArrayList<Personaje_old> personajes) {
        SQLiteDatabase db = getWritableDatabase();

        for (Personaje_old pj : personajes) {
            ContentValues values = new ContentValues();
            String[] ID = {String.valueOf(pj.getID())};

            values.put(Personaje_Estructura.NOMBRE, pj.getNombre_jugador());
            values.put(Personaje_Estructura.PERSONAJE, pj.getNombre_personaje());
            values.put(Personaje_Estructura.DESCRIPCIÓN, pj.getDescripción());
            values.put(Personaje_Estructura.CARACTERISTICAS, serialize(IntToText(pj.getCaracteristicas())));
            values.put(Personaje_Estructura.CARACTERISTICAS_TEMPORAL, serialize(IntToText(pj.getCaracteristicas_temporales())));
            values.put(Personaje_Estructura.CONOCIM_ESPECIALES, serialize(BoolToText(pj.getConocimientos_especiales())));
            values.put(Personaje_Estructura.PENALIZADORES, serialize(IntToText(pj.getPenalizadores())));
            values.put(Personaje_Estructura.ESPECIALES, serialize(IntToText(pj.getEspeciales())));
            values.put(Personaje_Estructura.NIVEL_PIFIA, pj.getNivelPifia());
            values.put(Personaje_Estructura.TIPOENTIDAD, pj.getTipoDeEntidad());
            values.put(Personaje_Estructura.TIPO_DE_PERSONAJE, pj.getTipoDePersonaje());
            values.put(Personaje_Estructura.ARMADURAS, serialize(IntToText(pj.getArmaduras())));
            values.put(Personaje_Estructura.ARMADURAS_NOMBRES, serialize(pj.getArmaduras_nombres()));
            values.put(Personaje_Estructura.ARMAS, serialize(IntToText(pj.getArmas())));
            values.put(Personaje_Estructura.ARMAS_NOMBRES, serialize(pj.getArmas_nombres()));
            values.put(Personaje_Estructura.TOKEN, pj.getToken());


            db.update(Personaje_Estructura.TABLE_NAME, values, Personaje_Estructura.ID_PERSONAJE + " = ?", ID);
        }

        return true;
    }

    //Añadir


    public int AñadirArma(Arma arma) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Arma_Estructura.NOMBRE, arma.getNombre());
        values.put(Arma_Estructura.CARACTERISTICAS, serialize(IntToText(arma.getCaracteristicas())));

        try {
            db.insertOrThrow(Arma_Estructura.TABLE_NAME, null, values);
        } catch (android.database.sqlite.SQLiteConstraintException e) {
            return 0;
        }

        String[] columna = {Arma_Estructura.ID_ARMA};

        Cursor c = getReadableDatabase().query(Arma_Estructura.TABLE_NAME, columna, null, null, null, null, Arma_Estructura.ID_ARMA);
        c.moveToLast();
        int index = c.getInt(c.getColumnIndex(Arma_Estructura.ID_ARMA));

        c.close();

        return index;
    }

    public int AñadirLugar(Lugar lugar) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Lugar_Estructura.NOMBRE, lugar.getNombre());
        values.put(Lugar_Estructura.DESCRIPCION, lugar.getDescripción());
        values.put(Lugar_Estructura.LUGAR_PADRE, lugar.getPadre());

        try {
            db.insertOrThrow(Lugar_Estructura.TABLE_NAME, null, values);
        } catch (android.database.sqlite.SQLiteConstraintException e) {
            return 0;
        }

        String[] columna = {Lugar_Estructura.ID};

        Cursor c = getReadableDatabase().query(Lugar_Estructura.TABLE_NAME, columna, null, null, null, null, Lugar_Estructura.ID);
        c.moveToLast();
        int index = c.getInt(c.getColumnIndex(Lugar_Estructura.ID));

        c.close();

        return index;
    }

    public void AñadirLog(Historial_DB log) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Historial_Estructura.TITULO, log.getTítulo());
        values.put(Historial_Estructura.CUERPO, log.getCuerpo());
        values.put(Historial_Estructura.PERSONAJE, log.getLugar());
        values.put(Historial_Estructura.TIMESTAMP, log.getTimestamp());

        try {
            db.insertOrThrow(Historial_Estructura.TABLE_NAME, null, values);
        } catch (android.database.sqlite.SQLiteConstraintException e) {

        }
    }


    public int AñadirModificador(Modificador modif) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Modificador_Estructura.NOMBRE, modif.getNombre());
        values.put(Modificador_Estructura.CARACTERISTICAS, serialize(IntToText(modif.getCaracteristicas())));

        try {
            db.insertOrThrow(Modificador_Estructura.TABLE_NAME, null, values);
        } catch (android.database.sqlite.SQLiteConstraintException e) {
            return 0;
        }

        String[] columna = {Modificador_Estructura.ID_MODIFICADOR};

        Cursor c = getReadableDatabase().query(Modificador_Estructura.TABLE_NAME, columna, null, null, null, null, Modificador_Estructura.ID_MODIFICADOR);
        c.moveToLast();
        int index = c.getInt(c.getColumnIndex(Modificador_Estructura.ID_MODIFICADOR));

        c.close();

        return index;
    }

    public int AñadirArmadura(Armadura armadura) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Armadura_Estructura.NOMBRE, armadura.getNombre());
        values.put(Armadura_Estructura.CARACTERISTICAS, serialize(IntToText(armadura.getCaracteristicas())));
        try {
            db.insertOrThrow(Armadura_Estructura.TABLE_NAME, null, values);
        } catch (android.database.sqlite.SQLiteConstraintException e) {
            return 0;
        }

        String[] columna = {Armadura_Estructura.ID_ARMADURA};

        Cursor c = getReadableDatabase().query(Armadura_Estructura.TABLE_NAME, columna, null, null, null, null, Armadura_Estructura.ID_ARMADURA);
        c.moveToLast();
        int index = c.getInt(c.getColumnIndex(Armadura_Estructura.ID_ARMADURA));

        c.close();

        return index;
    }


    public int AñadirPersonaje(Personaje_old pj) {

        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(Personaje_Estructura.NOMBRE, pj.getNombre_jugador());
        values.put(Personaje_Estructura.DESCRIPCIÓN, pj.getDescripción());
        values.put(Personaje_Estructura.PERSONAJE, pj.getNombre_personaje());
        values.put(Personaje_Estructura.CARACTERISTICAS, serialize(IntToText(pj.getCaracteristicas())));
        values.put(Personaje_Estructura.CARACTERISTICAS_TEMPORAL, serialize(IntToText(pj.getCaracteristicas_temporales())));
        values.put(Personaje_Estructura.CONOCIM_ESPECIALES, serialize(BoolToText(pj.getConocimientos_especiales())));
        values.put(Personaje_Estructura.PENALIZADORES, serialize(IntToText(pj.getPenalizadores())));
        values.put(Personaje_Estructura.ESPECIALES, serialize(IntToText(pj.getEspeciales())));
        values.put(Personaje_Estructura.NIVEL_PIFIA, pj.getNivelPifia());
        values.put(Personaje_Estructura.TIPOENTIDAD, pj.getTipoDeEntidad());
        values.put(Personaje_Estructura.TIPO_DE_PERSONAJE, pj.getTipoDePersonaje());
        values.put(Personaje_Estructura.ARMADURAS, serialize(IntToText(pj.getArmaduras())));
        values.put(Personaje_Estructura.ARMADURAS_NOMBRES, serialize(pj.getArmaduras_nombres()));
        values.put(Personaje_Estructura.ARMAS, serialize(IntToText(pj.getArmas())));
        values.put(Personaje_Estructura.ARMAS_NOMBRES, serialize(pj.getArmas_nombres()));
        values.put(Personaje_Estructura.TOKEN, pj.getToken());


        try {
            db.insertOrThrow(Personaje_Estructura.TABLE_NAME, null, values);
        } catch (android.database.sqlite.SQLiteConstraintException e) {
            System.out.println(e.toString());
            return -1;
        }


        String[] columna = {Personaje_Estructura.ID_PERSONAJE};

        Cursor c = getReadableDatabase().query(Personaje_Estructura.TABLE_NAME, columna, null, null, null, null, Personaje_Estructura.ID_PERSONAJE);
        c.moveToLast();
        int index = c.getInt(c.getColumnIndex(Personaje_Estructura.ID_PERSONAJE));

        c.close();

        return index;
    }

    //Eliminar

    public boolean EliminarArmadura(int ID) {

        SQLiteDatabase db = getWritableDatabase();

        String[] valor = {String.valueOf(ID)};

        db.delete(Armadura_Estructura.TABLE_NAME, Armadura_Estructura.ID_ARMADURA + " =?", valor);

        return true;
    }

    public boolean EliminarLugar(int ID) {

        SQLiteDatabase db = getWritableDatabase();

        String[] valor = {String.valueOf(ID)};

        db.delete(Lugar_Estructura.TABLE_NAME, Lugar_Estructura.ID + " =?", valor);

        return true;
    }

    public boolean EliminarArma(int ID) {

        SQLiteDatabase db = getWritableDatabase();

        String[] valor = {String.valueOf(ID)};

        db.delete(Arma_Estructura.TABLE_NAME, Arma_Estructura.ID_ARMA + " =?", valor);

        return true;
    }

    public boolean EliminarModif(int ID) {

        SQLiteDatabase db = getWritableDatabase();

        String[] valor = {String.valueOf(ID)};

        db.delete(Modificador_Estructura.TABLE_NAME, Modificador_Estructura.ID_MODIFICADOR + " =?", valor);

        return true;
    }

    public boolean EliminarPersonaje(int ID) {

        SQLiteDatabase db = getWritableDatabase();

        String[] valor = {String.valueOf(ID)};

        db.delete(Personaje_Estructura.TABLE_NAME, Personaje_Estructura.ID_PERSONAJE + " =?", valor);

        return true;
    }

    //Obtener

    public ArrayList<Arma> GetArmas() {
        ArrayList<Arma> Armas = new ArrayList<>();
        Cursor c = getReadableDatabase().query(Arma_Estructura.TABLE_NAME, null, null, null, null, null, null);

        if (c.getCount() != 0) {
            c.moveToFirst();
            boolean flag = true;

            while (flag) {

                Armas.add(new Arma(
                        c.getString(c.getColumnIndex(Arma_Estructura.NOMBRE)),
                        (TextToInt(derialize(c.getString(c.getColumnIndex(Arma_Estructura.CARACTERISTICAS))))),
                        c.getInt(c.getColumnIndex(Arma_Estructura.ID_ARMA))
                ));
                if (c.isLast()) {
                    flag = false;
                }
                c.moveToNext();
            }
        }
        c.close();
        return Armas;
    }

    public ArrayList<Historial_DB> GetHistorial() {
        ArrayList<Historial_DB> historial = new ArrayList<>();

        Cursor c = getReadableDatabase().query(Historial_Estructura.TABLE_NAME, null, null, null, null, null, null);

        if (c.getCount() != 0) {
            c.moveToFirst();
            boolean flag = true;

            while (flag) {

                historial.add(new Historial_DB(
                        c.getString(c.getColumnIndex(Historial_Estructura.TITULO)),
                        c.getString(c.getColumnIndex(Historial_Estructura.CUERPO)),
                        c.getLong(c.getColumnIndex(Historial_Estructura.TIMESTAMP)),
                        c.getInt(c.getColumnIndex(Historial_Estructura.ID_LOG)),
                        c.getString(c.getColumnIndex(Historial_Estructura.PERSONAJE))
                ));
                if (c.isLast()) {
                    flag = false;
                }
                c.moveToNext();
            }
        }
        c.close();

        return historial;
    }

    public ArrayList<Modificador> GetModif() {
        ArrayList<Modificador> Modificadores = new ArrayList<>();
        Cursor c = getReadableDatabase().query(Modificador_Estructura.TABLE_NAME, null, null, null, null, null, null);

        if (c.getCount() != 0) {
            c.moveToFirst();
            boolean flag = true;

            while (flag) {

                Modificadores.add(new Modificador(
                        c.getInt(c.getColumnIndex(Modificador_Estructura.ID_MODIFICADOR)),
                        c.getString(c.getColumnIndex(Modificador_Estructura.NOMBRE)),
                        (TextToInt(derialize(c.getString(c.getColumnIndex(Modificador_Estructura.CARACTERISTICAS)))))
                ));
                if (c.isLast()) {
                    flag = false;
                }
                c.moveToNext();
            }
        }
        c.close();
        return Modificadores;
    }

    public ArrayList<Lugar> GetLugares() {
        ArrayList<Lugar> Lugares = new ArrayList<>();
        Cursor c = getReadableDatabase().query(Lugar_Estructura.TABLE_NAME, null, null, null, null, null, null);

        if (c.getCount() != 0) {
            c.moveToFirst();
            boolean flag = true;

            while (flag) {

                Lugares.add(new Lugar(
                        c.getString(c.getColumnIndex(Lugar_Estructura.NOMBRE)),
                        c.getString(c.getColumnIndex(Lugar_Estructura.DESCRIPCION)),
                        c.getString(c.getColumnIndex(Lugar_Estructura.LUGAR_PADRE)),
                        c.getInt(c.getColumnIndex(Lugar_Estructura.ID))
                ));
                if (c.isLast()) {
                    flag = false;
                }
                c.moveToNext();
            }
        }
        c.close();
        return Lugares;
    }


    public ArrayList<Armadura> GetArmaduras() {
        ArrayList<Armadura> Armaduras = new ArrayList<>();
        Cursor c = getReadableDatabase().query(Armadura_Estructura.TABLE_NAME, null, null, null, null, null, null);

        if (c.getCount() != 0) {
            c.moveToFirst();
            boolean flag = true;

            while (flag) {
                Armaduras.add(new Armadura(
                        c.getString(c.getColumnIndex(Armadura_Estructura.NOMBRE)),
                        TextToInt(derialize(c.getString(c.getColumnIndex(Armadura_Estructura.CARACTERISTICAS)))),
                        c.getInt(c.getColumnIndex(Armadura_Estructura.ID_ARMADURA))
                ));
                if (c.isLast()) {
                    flag = false;
                }
                c.moveToNext();
            }
        }
        c.close();
        return Armaduras;
    }


    public ArrayList<Personaje_old> GetPersonajes() {
        ArrayList<Personaje_old> personajes = new ArrayList<>();

        Cursor c = getReadableDatabase().query(Personaje_Estructura.TABLE_NAME, null, null, null, null, null, null);

        if (c.getCount() != 0) {
            c.moveToFirst();
            boolean flag = true;

            while (flag) {

                Personaje_old pj = new Personaje_old(
                        c.getInt(c.getColumnIndex(Personaje_Estructura.ID_PERSONAJE)),
                        c.getString(c.getColumnIndex(Personaje_Estructura.NOMBRE)),
                        c.getString(c.getColumnIndex(Personaje_Estructura.PERSONAJE)),
                        c.getString(c.getColumnIndex(Personaje_Estructura.DESCRIPCIÓN)),
                        c.getInt(c.getColumnIndex(Personaje_Estructura.NIVEL_PIFIA)),
                        c.getInt(c.getColumnIndex(Personaje_Estructura.TIPOENTIDAD)),
                        c.getInt(c.getColumnIndex(Personaje_Estructura.TIPO_DE_PERSONAJE)),
                        TextToInt(derialize(c.getString(c.getColumnIndex(Personaje_Estructura.CARACTERISTICAS)))),
                        TextToInt(derialize(c.getString(c.getColumnIndex(Personaje_Estructura.CARACTERISTICAS_TEMPORAL)))),
                        TextToBool(derialize(c.getString(c.getColumnIndex(Personaje_Estructura.CONOCIM_ESPECIALES)))),
                        TextToInt(derialize(c.getString(c.getColumnIndex(Personaje_Estructura.PENALIZADORES)))),
                        TextToInt(derialize(c.getString(c.getColumnIndex(Personaje_Estructura.ESPECIALES)))),
                        TextToInt(derialize(c.getString(c.getColumnIndex(Personaje_Estructura.ARMADURAS)))),
                        (derialize(c.getString(c.getColumnIndex(Personaje_Estructura.ARMADURAS_NOMBRES)))),
                        TextToInt(derialize(c.getString(c.getColumnIndex(Personaje_Estructura.ARMAS)))),
                        (derialize(c.getString(c.getColumnIndex(Personaje_Estructura.ARMAS_NOMBRES)))), null, null
                );

                try {
                    byte[] token = c.getBlob(c.getColumnIndex(Personaje_Estructura.TOKEN));

                    pj.setToken(token);

                    byte[] image = c.getBlob(c.getColumnIndex(Personaje_Estructura.IMAGEN));

                    pj.setImagen(image);

                } catch (java.lang.IllegalStateException e) {

                } catch (java.lang.NullPointerException e) {
                    System.out.println(pj.getNombre_personaje() + " no posee imagen. ");
                }

                personajes.add(pj);


                if (c.isLast()) {
                    flag = false;
                }
                c.moveToNext();
            }
        }
        c.close();
        return personajes;
    }


    //Utiles varios


    public static final String ARRAY_DIVIDER = "#";

    public static final String[] FloatToText(float[] valores) {
        String[] cadena = new String[valores.length];
        for (int i = 0; i < valores.length; i++) {
            cadena[i] = String.valueOf(valores[i]);
        }
        return cadena;
    }

    public static final float[] TextToFloat(String[] cadena) {
        float[] valores = new float[cadena.length];
        for (int i = 0; i < cadena.length; i++) {
            valores[i] = Integer.parseInt(cadena[i]);
        }
        return valores;
    }

    public static final String[] BoolToText(boolean[] valores) {
        String[] cadena = new String[valores.length];
        for (int i = 0; i < valores.length; i++) {
            if (valores[i]) {
                cadena[i] = "1";
            } else {
                cadena[i] = "0";
            }
        }
        return cadena;
    }

    public static final boolean[] TextToBool(String[] cadena) {
        boolean[] valores = new boolean[cadena.length];
        for (int i = 0; i < cadena.length; i++) {
            valores[i] = IntToBool(Integer.parseInt(cadena[i]));
        }
        return valores;
    }

    public static final String[] IntToText(int[] valores) {
        String[] cadena = new String[valores.length];
        for (int i = 0; i < valores.length; i++) {
            cadena[i] = String.valueOf(valores[i]);
        }
        return cadena;
    }

    public static final String[] LongToText(long[] valores) {
        String[] cadena = new String[valores.length];
        for (int i = 0; i < valores.length; i++) {
            cadena[i] = String.valueOf(valores[i]);
        }
        return cadena;
    }

    public static final int[] TextToInt(String[] cadena) {
        int[] valores = new int[cadena.length];
        for (int i = 0; i < cadena.length; i++) {
            valores[i] = Integer.parseInt(cadena[i]);
        }
        return valores;
    }

    public static final long[] TextToLong(String[] cadena) {
        long[] valores = new long[cadena.length];
        for (int i = 0; i < cadena.length; i++) {
            valores[i] = Long.parseLong(cadena[i]);
        }
        return valores;
    }

    public static final String serialize(String content[]) {
        return TextUtils.join(ARRAY_DIVIDER, content);
    }

    public static final String[] derialize(String content) {
        return content.split(ARRAY_DIVIDER);
    }

    public static final boolean IntToBool(int i) {
        if (i == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static final int transformar_Texto(String text) {

        int resultado = 0;

        try {
            resultado = Integer.valueOf(text);
            return resultado;
        } catch (NumberFormatException e) {
            System.out.println(e.toString());
            return 0;
        }

    }


}
