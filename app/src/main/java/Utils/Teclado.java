package Utils;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.ToggleButton;

import com.nugganet.arget.mastertoolkit.Principal;
import com.nugganet.arget.mastertoolkit.R;

/**
 * Created by Arget on 26/3/2018.
 */

public class Teclado {

    private boolean boton_hide = true;
    private Principal p;
    private boolean activo = true;
    private int lastTipoDeTeclado = 0;
    private boolean mayusq_hold = false;
    private boolean first_inic = true;

    public Teclado(Principal p, boolean activo) {
        this.p = p;
        this.activo = activo;

        ToggleButton toggle_mayusc = (ToggleButton) p.findViewById(R.id.teclado_alf_mayusc);

        toggle_mayusc.setTextOn("\uD83C\uDD30");
        toggle_mayusc.setTextOff("⇧");

        //((Button) p.findViewById(R.id.teclado_alf_borrar)).setWebContent("⟵");


    }

    public boolean isActivo() {
        return activo;
    }

    private void teclado(int accion) {
        if (activo) {
            try {
                if (p.getCurrentFocus() instanceof EditText) {
                    EditText editando = (EditText) p.getCurrentFocus();
                    String text = ((EditText) p.getCurrentFocus()).getText().toString();
                    int cursor = editando.getSelectionStart();

                    if (accion == -1) {//borrar
                        if (!text.isEmpty()) {

                            text = text.substring(0, cursor - 1) + text.substring(cursor);
                            cursor = cursor - 1;

                        }
                    } else if (accion == -2) {//negativo
                        if (!text.isEmpty()) {
                            if (text.charAt(0) == '-') {
                                text = String.valueOf(text.subSequence(1, text.length()));
                            } else {
                                text = "-" + text;
                            }
                        }

                    } else if (accion == -3) {//siguiente

                        if (p.getCurrentFocus().focusSearch(View.FOCUS_DOWN) instanceof EditText) {
                            p.getCurrentFocus().focusSearch(View.FOCUS_DOWN).requestFocus();
                        } else if (p.getCurrentFocus().focusSearch(View.FOCUS_RIGHT) instanceof EditText) {
                            p.getCurrentFocus().focusSearch(View.FOCUS_RIGHT).requestFocus();
                        } else if (p.getCurrentFocus().focusSearch(View.FOCUS_LEFT) instanceof EditText) {
                            p.getCurrentFocus().focusSearch(View.FOCUS_LEFT).requestFocus();
                        }

                        final EditText ed = (EditText) p.getCurrentFocus();
                        ed.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ((EditText) ed).setSelection(((EditText) ed).getText().toString().length());
                            }
                        },5);

                    } else if (accion == -5) {//anterior

                        if (p.getCurrentFocus().focusSearch(View.FOCUS_UP) instanceof EditText) {
                            p.getCurrentFocus().focusSearch(View.FOCUS_UP).requestFocus();
                        } else if (p.getCurrentFocus().focusSearch(View.FOCUS_LEFT) instanceof EditText) {
                            p.getCurrentFocus().focusSearch(View.FOCUS_LEFT).requestFocus();
                        } else if (p.getCurrentFocus().focusSearch(View.FOCUS_RIGHT) instanceof EditText) {
                            p.getCurrentFocus().focusSearch(View.FOCUS_RIGHT).requestFocus();
                        }

                        final EditText ed = (EditText) p.getCurrentFocus();
                        ed.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ((EditText) ed).setSelection(((EditText) ed).getText().toString().length());
                            }
                        },5);

                    } else if (accion == -4) {//borrar todo_
                        if (lastTipoDeTeclado == 1) {
                            text = "";

                        } else {
                            text = "";
                        }


                    } else {//añadir numero

                        if (accion < 10) {

                            if (text.isEmpty()) {
                                text = text.substring(0, cursor) + String.valueOf(accion) + text.substring(cursor);
                            } else {
                                if (cursor == 0 & text.substring(0, 1).contains("-")) {
                                    text = text.substring(0, 1) + String.valueOf(accion) + text.substring(1);
                                } else {
                                    text = text.substring(0, cursor) + String.valueOf(accion) + text.substring(cursor);
                                }
                            }

                            cursor = cursor + 1;

                            // text = text + String.valueOf(accion);
                        } else {
                            if (accion == 164) {
                                if (((ToggleButton) p.findViewById(R.id.teclado_alf_mayusc)).isChecked()) {

                                    text = text.substring(0, cursor) + String.valueOf("Ñ") + text.substring(cursor);
                                    cursor = cursor + 1;
                                } else {
                                    text = text.substring(0, cursor) + String.valueOf("ñ") + text.substring(cursor);
                                    cursor = cursor + 1;
                                }
                            } else if (accion == 200) {

                                if (lastTipoDeTeclado == 1) {

                                    text = text.substring(0, cursor) + " " + text.substring(cursor);
                                    cursor = cursor + 1;

                                } else {
                                    if (!text.isEmpty()) {

                                        text = text.substring(0, cursor - 1) + text.substring(cursor);
                                        cursor = cursor - 1;

                                    }
                                }

                            } else {
                                if (((ToggleButton) p.findViewById(R.id.teclado_alf_mayusc)).isChecked()) {
                                    text = text.substring(0, cursor) + String.valueOf(Character.toChars(accion)) + text.substring(cursor);
                                    cursor = cursor + 1;
                                } else {
                                    text = text.substring(0, cursor) + String.valueOf(Character.toChars(accion + 32)) + text.substring(cursor);
                                    cursor = cursor + 1;
                                }
                            }

                        }
                    }

                    if (((ToggleButton) p.findViewById(R.id.teclado_alf_mayusc)).isChecked() & !mayusq_hold) {
                        ((ToggleButton) p.findViewById(R.id.teclado_alf_mayusc)).setChecked(false);
                    }

                    if (accion != -3 & accion != -5) {
                        ((EditText) p.getCurrentFocus()).setText(text);
                        if (cursor > text.length()) {
                            ((EditText) p.getCurrentFocus()).setSelection(text.length());
                        } else {
                            ((EditText) p.getCurrentFocus()).setSelection(cursor);
                        }

                    }
                }

            } catch (java.lang.NullPointerException e) {
                System.out.println("Error en teclado!" + e);
            } catch (java.lang.StringIndexOutOfBoundsException e) {
                System.out.println("Error en teclado!" + e);
            }
        }
    }

    public void controladorTeclas_LClick(int id_tecla) {
        switch (id_tecla) {
            case R.id.button_teclado_Borrar:
            case R.id.teclado_alf_borrar:
                teclado(-4);
                break;
        }
    }

    public void controladorToggleLongPress(int id) {
        switch (id){
            case R.id.button_teclado_Borrar:
            case R.id.teclado_alf_borrar:
                teclado(-4);
                break;
            case R.id.teclado_alf_mayusc:
                ((ToggleButton) p.findViewById(R.id.teclado_alf_mayusc)).setChecked(true);
                mayusq_hold = true;
                ((ToggleButton) p.findViewById(R.id.teclado_alf_mayusc)).setText("\uD83C\uDD70");
                break;
        }
    }

    public void controladorToggleButton(int id_toggle, boolean isChecked) {
        if (id_toggle == R.id.teclado_alf_mayusc) {
            if (isChecked) {

                ((Button) p.findViewById(R.id.teclado_alf_a)).setText("A");
                ((Button) p.findViewById(R.id.teclado_alf_b)).setText("B");
                ((Button) p.findViewById(R.id.teclado_alf_c)).setText("C");
                ((Button) p.findViewById(R.id.teclado_alf_d)).setText("D");
                ((Button) p.findViewById(R.id.teclado_alf_e)).setText("E");
                ((Button) p.findViewById(R.id.teclado_alf_f)).setText("F");
                ((Button) p.findViewById(R.id.teclado_alf_g)).setText("G");
                ((Button) p.findViewById(R.id.teclado_alf_h)).setText("H");
                ((Button) p.findViewById(R.id.teclado_alf_i)).setText("I");
                ((Button) p.findViewById(R.id.teclado_alf_j)).setText("J");
                ((Button) p.findViewById(R.id.teclado_alf_k)).setText("K");
                ((Button) p.findViewById(R.id.teclado_alf_l)).setText("L");
                ((Button) p.findViewById(R.id.teclado_alf_m)).setText("M");
                ((Button) p.findViewById(R.id.teclado_alf_n)).setText("N");
                ((Button) p.findViewById(R.id.teclado_alf_o)).setText("O");
                ((Button) p.findViewById(R.id.teclado_alf_p)).setText("P");
                ((Button) p.findViewById(R.id.teclado_alf_q)).setText("Q");
                ((Button) p.findViewById(R.id.teclado_alf_r)).setText("R");
                ((Button) p.findViewById(R.id.teclado_alf_s)).setText("S");
                ((Button) p.findViewById(R.id.teclado_alf_t)).setText("T");
                ((Button) p.findViewById(R.id.teclado_alf_u)).setText("U");
                ((Button) p.findViewById(R.id.teclado_alf_v)).setText("V");
                ((Button) p.findViewById(R.id.teclado_alf_w)).setText("W");
                ((Button) p.findViewById(R.id.teclado_alf_x)).setText("X");
                ((Button) p.findViewById(R.id.teclado_alf_y)).setText("Y");
                ((Button) p.findViewById(R.id.teclado_alf_z)).setText("Z");
                ((Button) p.findViewById(R.id.teclado_alf_ñ)).setText("Ñ");

            } else {

                mayusq_hold = false;
                ((Button) p.findViewById(R.id.teclado_alf_a)).setText("a");
                ((Button) p.findViewById(R.id.teclado_alf_b)).setText("b");
                ((Button) p.findViewById(R.id.teclado_alf_c)).setText("c");
                ((Button) p.findViewById(R.id.teclado_alf_d)).setText("d");
                ((Button) p.findViewById(R.id.teclado_alf_e)).setText("e");
                ((Button) p.findViewById(R.id.teclado_alf_f)).setText("f");
                ((Button) p.findViewById(R.id.teclado_alf_g)).setText("g");
                ((Button) p.findViewById(R.id.teclado_alf_h)).setText("h");
                ((Button) p.findViewById(R.id.teclado_alf_i)).setText("i");
                ((Button) p.findViewById(R.id.teclado_alf_j)).setText("j");
                ((Button) p.findViewById(R.id.teclado_alf_k)).setText("k");
                ((Button) p.findViewById(R.id.teclado_alf_l)).setText("l");
                ((Button) p.findViewById(R.id.teclado_alf_m)).setText("m");
                ((Button) p.findViewById(R.id.teclado_alf_n)).setText("n");
                ((Button) p.findViewById(R.id.teclado_alf_o)).setText("o");
                ((Button) p.findViewById(R.id.teclado_alf_p)).setText("p");
                ((Button) p.findViewById(R.id.teclado_alf_q)).setText("q");
                ((Button) p.findViewById(R.id.teclado_alf_r)).setText("r");
                ((Button) p.findViewById(R.id.teclado_alf_s)).setText("s");
                ((Button) p.findViewById(R.id.teclado_alf_t)).setText("t");
                ((Button) p.findViewById(R.id.teclado_alf_u)).setText("u");
                ((Button) p.findViewById(R.id.teclado_alf_v)).setText("v");
                ((Button) p.findViewById(R.id.teclado_alf_w)).setText("w");
                ((Button) p.findViewById(R.id.teclado_alf_x)).setText("x");
                ((Button) p.findViewById(R.id.teclado_alf_y)).setText("y");
                ((Button) p.findViewById(R.id.teclado_alf_z)).setText("z");
                ((Button) p.findViewById(R.id.teclado_alf_ñ)).setText("ñ");

            }


        }


    }

    public void controladorSpinner(int id_spinner) {
        if (id_spinner == R.id.spinner_quick_num) {

            if(first_inic){
                first_inic = false;
            }else{
                if (p.getCurrentFocus() instanceof EditText) {
                    String valor = ((Spinner) p.findViewById(R.id.spinner_quick_num)).getSelectedItem().toString();
                    ((EditText) p.getCurrentFocus()).setText(valor);
                }
            }



        }
    }

    public void controladorTeclas_Click(int id_tecla) {
        switch (id_tecla) {
            case R.id.button_teclado_Anterior_cursor:
                try {
                    ((EditText) p.getCurrentFocus()).setSelection(((EditText) p.getCurrentFocus()).getSelectionStart() - 1);
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }

                break;

            case R.id.button_teclado_Siguiente_cursor:
                try {
                    ((EditText) p.getCurrentFocus()).setSelection(((EditText) p.getCurrentFocus()).getSelectionStart() + 1);
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }

                break;

            case R.id.teclado_alf_borrar:
                teclado(-1);
                break;
            case R.id.teclado_alf_a:
                teclado(65);
                break;
            case R.id.teclado_alf_b:
                teclado(66);
                break;
            case R.id.teclado_alf_c:
                teclado(67);
                break;
            case R.id.teclado_alf_d:
                teclado(68);
                break;
            case R.id.teclado_alf_e:
                teclado(69);
                break;
            case R.id.teclado_alf_f:
                teclado(70);
                break;
            case R.id.teclado_alf_g:
                teclado(71);
                break;
            case R.id.teclado_alf_h:
                teclado(72);
                break;
            case R.id.teclado_alf_i:
                teclado(73);
                break;
            case R.id.teclado_alf_j:
                teclado(74);
                break;
            case R.id.teclado_alf_k:
                teclado(75);
                break;
            case R.id.teclado_alf_l:
                teclado(76);
                break;
            case R.id.teclado_alf_m:
                teclado(77);
                break;
            case R.id.teclado_alf_n:
                teclado(78);
                break;
            case R.id.teclado_alf_o:
                teclado(79);
                break;
            case R.id.teclado_alf_p:
                teclado(80);
                break;
            case R.id.teclado_alf_q:
                teclado(81);
                break;
            case R.id.teclado_alf_r:
                teclado(82);
                break;
            case R.id.teclado_alf_s:
                teclado(83);
                break;
            case R.id.teclado_alf_t:
                teclado(84);
                break;
            case R.id.teclado_alf_u:
                teclado(85);
                break;
            case R.id.teclado_alf_v:
                teclado(86);
                break;
            case R.id.teclado_alf_w:
                teclado(87);
                break;
            case R.id.teclado_alf_x:
                teclado(88);
                break;
            case R.id.teclado_alf_y:
                teclado(89);
                break;
            case R.id.teclado_alf_z:
                teclado(90);
                break;
            case R.id.teclado_alf_ñ:
                teclado(164);
                break;


            case R.id.button_teclado_Anterior:
                teclado(-5);
                break;
            case R.id.button_teclado_Siguiente:
                teclado(-3);
                break;



            case R.id.button_teclado_Negativo:
                teclado(-2);
                break;

            case R.id.button_teclado_0:
                teclado(0);
                break;
            case R.id.button_teclado_1:
                teclado(1);
                break;
            case R.id.button_teclado_2:
                teclado(2);
                break;
            case R.id.button_teclado_3:
                teclado(3);
                break;
            case R.id.button_teclado_4:
                teclado(4);
                break;
            case R.id.button_teclado_5:
                teclado(5);
                break;
            case R.id.button_teclado_6:
                teclado(6);
                break;
            case R.id.button_teclado_7:
                teclado(7);
                break;
            case R.id.button_teclado_8:
                teclado(8);
                break;
            case R.id.button_teclado_9:
                teclado(9);
                break;

            case R.id.button_teclado_Borrar:
                teclado(200);
                break;
        }
    }

    public boolean controlTeclado(int tipo) {

        if (activo) {

            boolean flag = false;

            LinearLayout Teclado = (LinearLayout) p.findViewById(R.id.Teclado);
            LinearLayout Teclado_numerico = (LinearLayout) p.findViewById(R.id.Layout_teclas_numericas);
            LinearLayout Teclado_alfabetico = (LinearLayout) p.findViewById(R.id.Layout_teclas_alfabeto);


            if (Teclado_numerico.getVisibility() == View.VISIBLE | Teclado_alfabetico.getVisibility() == View.VISIBLE) {
                flag = true;
            }

            switch (tipo) {
                case -1:
                    Teclado_numerico.setVisibility(View.GONE);
                    Teclado_alfabetico.setVisibility(View.GONE);
                    Teclado.setVisibility(View.GONE);
                    break;
                case 0:
                    if(boton_hide=true){
                        if (lastTipoDeTeclado != 0) {
                            controlTeclado(lastTipoDeTeclado);
                        }
                    }else{
                        Teclado_numerico.setVisibility(View.GONE);
                        Teclado_alfabetico.setVisibility(View.GONE);
                        ((Button) p.findViewById(R.id.button_teclado_Borrar)).setVisibility(View.GONE);
                        ((Button) p.findViewById(R.id.button_teclado_Negativo)).setVisibility(View.GONE);
                        ((Button) p.findViewById(R.id.button_teclado_Siguiente)).setVisibility(View.GONE);
                        boton_hide = true;
                    }

                    break;
                case 1:
                    lastTipoDeTeclado = 1;
                    Teclado_numerico.setVisibility(View.GONE);
                    Teclado.setVisibility(View.VISIBLE);
                    ((Button) p.findViewById(R.id.button_teclado_Borrar)).setVisibility(View.VISIBLE);
                    ((Button) p.findViewById(R.id.button_teclado_Borrar)).setText("␣");
                    ((Button) p.findViewById(R.id.button_teclado_Siguiente)).setVisibility(View.VISIBLE);
                    ((Button) p.findViewById(R.id.button_teclado_Negativo)).setVisibility(View.GONE);
                    Teclado_alfabetico.setVisibility(View.VISIBLE);
                    boton_hide = false;
                    break;

                case 2:
                    lastTipoDeTeclado = 2;
                    Teclado.setVisibility(View.VISIBLE);
                    Teclado_alfabetico.setVisibility(View.GONE);
                    ((Button) p.findViewById(R.id.button_teclado_Borrar)).setText("⌫");
                    ((Button) p.findViewById(R.id.button_teclado_Borrar)).setVisibility(View.VISIBLE);
                    ((Button) p.findViewById(R.id.button_teclado_Negativo)).setVisibility(View.VISIBLE);
                    ((Button) p.findViewById(R.id.button_teclado_Siguiente)).setVisibility(View.VISIBLE);
                    Teclado_numerico.setVisibility(View.VISIBLE);
                    boton_hide = false;
                    break;
            }


            return flag;
        }
        return false;
    }


}
