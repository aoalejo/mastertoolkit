package Utils;

import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by Arget on 2/2/2018.
 */

public class MyAppWebViewClient extends WebViewClient {

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        return false;
        /*

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        view.getContext().startActivity(intent);
        return true;
        */
    }
}
